﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grasshopper.Kernel;

namespace Donkey
{
    public class Global_Comp : GH_Component
    {
        public Global_Comp()
            : base("Scale Forces", "Setup",
                "Global setup for preview.",// + Donkey.Info.Mark,
                "Donkey", "A.Utilities") {}

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddNumberParameter("Force scale", "F", "Scale (ratio) to visualize forces", GH_ParamAccess.item);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            //throw new NotImplementedException();
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            double scaleFactor = 1;
            if (DA.GetData(0, ref scaleFactor))
            {
                //GlobalValues gv = new GlobalValues();
                    GlobalValues.ForceScale = scaleFactor;
            }

        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{298CF491-B046-4A7A-9F3F-E3C62DB0213C}"); }
        }

        public override void MovedBetweenDocuments(GH_Document oldDocument, GH_Document newDocument)
        {
            this.ExpireSolution(true);
            base.MovedBetweenDocuments(oldDocument, newDocument);
        }

        public override void DocumentContextChanged(Grasshopper.Kernel.GH_Document document, Grasshopper.Kernel.GH_DocumentContext context)
        {
            this.ExpireSolution(true);
            base.DocumentContextChanged(document, context);
        }

        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.donkey; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }

        //public override void ExpirePreview(bool redraw)
        //{
        //    this.ExpireSolution(true);
        //    base.ExpirePreview(redraw);
        //}

        //public override void RemovedFromDocument(GH_Document document)
        //{
        //    this.ExpireSolution(true);
        //    base.RemovedFromDocument(document);
        //}
    }
}
