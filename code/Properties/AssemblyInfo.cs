﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;
using Grasshopper.Kernel;
using System;
using Donkey;
using System.Diagnostics;

#region BASE ASSEMBLY PROPERTY
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Structural Evaluation Assistant")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Donkey")]
[assembly: AssemblyCopyright("GNU GPLv3")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("adff69c2-b507-4acf-8fe2-d2f547c0415b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
[assembly: NeutralResourcesLanguage("")]
[assembly: AssemblyFileVersion("0.0.0")]
[assembly: AssemblyVersion("0.0.0.0")]
#endregion

//namespace Donkey
//{
//    //public static class Info
//    //{
//    //    private static Assembly m_assembly = Assembly.GetEntryAssembly();

//    //    public const string Description = "Structural analysis for designers";
//    //    public const string Name = "Donkey v0.82";
//    //    public const string Author_name = "Lukáš Kurilla";
//    //    public const string Author_contact = "mail@kruilluk.net";
//    //    public const string Licence = "GNU GPLv3";

//    //    public static string Version
//    //    {
//    //        get 
//    //        {
//    //            Assembly assembly = Assembly.GetExecutingAssembly();
//    //            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
//    //            string version = fvi.FileVersion;
//    //            return version; 
//    //        }
//    //    }

//    //    public static string Mark
//    //    {
//    //        get { return Environment.NewLine + "---" +Environment.NewLine + "Donkey: v" + Version; }
//    //    }

//        //public static string Version_3
//        //{
//        //    get { 
//        //        object[] version = m_assembly.GetCustomAttributes(typeof(AssemblyVersionAttribute), false);
//        //        return version[0].ToString();
//        //    }
//        //}

//        //public static string Version_2
//        //{
//        //    get { return AssemblyVersionAttribute.GetCustomAttribute(m_assembly, typeof(AssemblyVersionAttribute)).ToString(); }
//        //}

//        //static public string Units
//        //{
//        //    get 
//        //    {
//        //        Rhino.UnitSystem units = Rhino.RhinoDoc.ActiveDoc.ModelUnitSystem;
//        //        if ((int)units == 4)
//        //            return "[m]";
//        //        if ((int)units == 2)
//        //            return "[mm]";
//        //        return "[?]";

//        //    }
//        //}

//        //    UnsafeNativeMethods.ON_Intersect_MeshPolyline_Fill(pCMX, count, points, faceIds);
//        //    return points;
//        //}

//        //[DllImport("rhcommon_c", CallingConvention = CallingConvention.Cdecl)]
//        //internal static extern IntPtr ON_Intersect_MeshLine(IntPtr pConstMesh, Point3d from, Point3d to, ref int count);
//    }
//}
    namespace Donkey.Properties
{
   public class Donkey_AssemblyInfo : GH_AssemblyInfo
    {
        public override string Description
        {
            get { return "Structural Evaluation Assistant"; }
        }

        public override System.Drawing.Bitmap Icon
        {
            get { return Resources.analysis; }
        }

        public override string Name
        {
            get { return "Donkey v0.84"; }
        }

        public override string Version
        {
            get { return "0.84"; }
        }

        public override Guid Id
        {
            get { return new Guid("{EBCF12FF-B0EE-4B10-954D-D78B97877D31}"); }
        }

        public override string AuthorName
        {
            get { return "Lukas Kurilla"; }
        }

        public override string AuthorContact
        {
            get { return "mail@kurilluk.net"; }
        }
    }

    public class Donkey_AssemblyPriority : Grasshopper.Kernel.GH_AssemblyPriority
    {
        public override GH_LoadingInstruction PriorityLoad()
        {
            Grasshopper.Instances.ComponentServer.AddCategoryShortName("Donkey", "Dk");
            Grasshopper.Instances.ComponentServer.AddCategorySymbolName("Donkey", 'D');
            Grasshopper.Instances.ComponentServer.AddCategoryIcon("Donkey", Resources.head_3);
            return GH_LoadingInstruction.Proceed;
        }
    }
}