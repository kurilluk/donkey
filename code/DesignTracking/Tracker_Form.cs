﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Donkey.Properties;
using Donkey.Analysis;
using Donkey.Model.Resulting;

namespace Donkey.DesignTracking
{
    public class Tracker_Form : Form
    {
        internal CheckBox tracking;
        private Panel solutions_canvas;
        private Label label1;
        private IContainer components;

        private List<Solution_Button> solutions;
        private Label label2;
        private Label label3;

        private I_DS_Comp m_component;

        public Tracker_Form(I_DS_Comp component)
        {
            this.m_component = component;
            this.InitializeComponent();
            this.tracking.Checked = component.TrackDesing;
            //this.InitForm();
            this.InitContent();
        }

        public void DesingTrack_change()
        {
            this.tracking.Checked = m_component.TrackDesing;
        }

        public void AddSolution(int sol_index, ResultModel model, bool actual)
        {
            int height = this.solutions_canvas.Size.Height;
            int a = (height / 2) - 1;
            int width = this.solutions_canvas.Size.Width - (int)(a * 0.80); //22

            int x_pos = (int)(width * model.Efficiency);

            int y_pos;
            if (model.Overloading > 0)
                y_pos = 1;
            else
              y_pos = a + 4; //33

            Color color;
            if (model.Overloading == 0)
                color = ResultModel.Gradient.ColourAt(model.Efficiency);
            else
                color = ResultModel.Gradient.ColourAt(1 + model.Overloading);

            if (actual)
            {
                if (this.solutions != null && this.solutions.Count > 0)
                    this.solutions[solutions.Count - 1].IsActual = false;
            }

            Solution_Button solution = new Solution_Button(color, actual);
            //solution.Checked = true;
            //Solution_radioButton.Appearance = System.Windows.Forms.Appearance.Button;
            solution.BackColor = System.Drawing.Color.White;
            //Solution_radioButton.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            solution.Cursor = System.Windows.Forms.Cursors.Hand;
            solution.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            solution.Font = new System.Drawing.Font("Arial Narrow", 7.00F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            solution.Location = new System.Drawing.Point(x_pos, y_pos);
            solution.Size = new System.Drawing.Size(a, a); //30
            solution.TabIndex = 2;
            solution.TabStop = true;
            solution.Text = sol_index.ToString(); ;
            solution.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            solution.UseVisualStyleBackColor = false;
            solution.Name = sol_index.ToString();
            solution.Values = "Efficiency: " + model.Efficiency*100 + "%\r\nOverloading: " + model.Overloading*100+"%";

            if (actual)
                PrintSolution(solution);

            solution.CheckedChanged += new System.EventHandler(this.SelectedSolution);
            solution.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SolutionKey);
            this.solutions_canvas.Controls.Add(solution);

            for (int i = 0; i < solutions.Count; i++)
            {
                if (solutions[i].Checked)
                    solutions[i].Checked = false;
            }

            this.solutions.Add(solution); 
        }

        private void SolutionKey(object sender, KeyEventArgs e)
        {
            Solution_Button s = ((Solution_Button)sender);

            if (s.Checked) //TODO stange calling from changing checked...
            {
                if (e.KeyCode == Keys.Delete)
                {
                    this.m_component.FieldOfSolutions.RemoveAt((Int32.Parse(((RadioButton)sender).Name) - 1));
                    if (this.m_component.FieldOfSolutions.Count > 0)
                        this.m_component.m_model = this.m_component.FieldOfSolutions[this.m_component.FieldOfSolutions.Count - 1];
                    else
                        this.m_component.m_model = null;
                    this.m_component.no_analysis = true;
                    this.m_component.ExpireSolution(true);
                    this.solutions.Clear();
                    this.solutions_canvas.Controls.Clear();
                    this.InitContent();
                    //this.InitializeComponent();
                    this.Refresh();
                }
                //PrintSolution(s);
                //this.m_component.m_model = this.m_component.FieldOfSolutions[(Int32.Parse(((RadioButton)sender).Name) - 1)];
                //this.m_component.no_analysis = true;
                //this.m_component.ExpireSolution(true);
            }
        }

        public void ReloadPanel()
        {
            this.solutions.Clear();
            this.solutions_canvas.Controls.Clear();
            this.InitContent();
            //this.InitializeComponent();
            this.Refresh();
        }

        private void PrintSolution(Solution_Button solution)
        {
            this.label1.Text = "Solution ID:" + solution.Name + "\r\n" + solution.Values;
        }


        private void SelectedSolution(object sender, EventArgs e)
        {
            Solution_Button s = ((Solution_Button) sender);

            if (s.Checked) //TODO stange calling from changing checked...
            {
                PrintSolution(s);
                this.m_component.m_model = this.m_component.FieldOfSolutions[(Int32.Parse(((RadioButton)sender).Name) - 1)];
                this.m_component.no_analysis = true;
                this.m_component.ExpireSolution(true);
            }
        }


        protected override void Dispose(bool disposing)
        {
            //if (disposing && this.components != null)
                //this.components.Dispose();
            if(this.solutions != null)
                this.solutions.Clear();
            if (m_component != null && m_component.FieldOfSolutions != null && m_component.FieldOfSolutions.Count > 0)
            {
                this.m_component.m_model = this.m_component.FieldOfSolutions[this.m_component.FieldOfSolutions.Count - 1];
                this.m_component.no_analysis = true;
                this.m_component.ExpireSolution(true);
            }
            base.Dispose(disposing);
        }

        private void InitContent()
        {
            this.solutions = new List<Solution_Button>();
            int count = this.m_component.FieldOfSolutions.Count;
            if (m_component.FieldOfSolutions != null && count>0)
                for (int i = 0; i < count; i++ )
                {
                    ResultModel model = this.m_component.FieldOfSolutions[i];
                    if(i == count-1)
                        this.AddSolution(i + 1, model, true);
                    else
                        this.AddSolution(i + 1, model, false);
                }
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Tracker_Form));
            this.tracking = new System.Windows.Forms.CheckBox();
            this.solutions_canvas = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tracking
            // 
            this.tracking.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tracking.Location = new System.Drawing.Point(640, 56);
            this.tracking.Name = "tracking";
            this.tracking.Size = new System.Drawing.Size(98, 17);
            this.tracking.TabIndex = 1;
            this.tracking.Text = "tracking design";
            this.tracking.UseVisualStyleBackColor = true;
            this.tracking.CheckedChanged += new System.EventHandler(this.tracking_CheckedChanged);
            // 
            // solutions_canvas
            // 
            this.solutions_canvas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.solutions_canvas.BackColor = System.Drawing.Color.White;
            this.solutions_canvas.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("solutions_canvas.BackgroundImage")));
            this.solutions_canvas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.solutions_canvas.ForeColor = System.Drawing.Color.DimGray;
            this.solutions_canvas.Location = new System.Drawing.Point(63, 13);
            this.solutions_canvas.Name = "solutions_canvas";
            this.solutions_canvas.Size = new System.Drawing.Size(568, 60);
            this.solutions_canvas.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(637, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Information";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Problem";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(12, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Clear";
            // 
            // Tracker_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(750, 85);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.solutions_canvas);
            this.Controls.Add(this.tracking);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Tracker_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Design Tracker";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GH_ActiveSimulationForm_FormClosing);
            this.Load += new System.EventHandler(this.GH_ActiveSimulationForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void UpdateUI()
        {
        }

        private void GH_ActiveSimulationForm_Load(object sender, EventArgs e)
        {
            this.UpdateUI();
            //this.Owner.reset = true;
        }

        
        private void GH_ActiveSimulationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.m_component == null)
                return;
            this.m_component.m_form = null;
        }

        private void tracking_CheckedChanged(object sender, EventArgs e)
        {
            //CALL component status change
            this.m_component.TrackDesing = this.tracking.Checked;
        }

    }
}