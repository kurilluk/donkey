﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using Donkey.Model.Analytical;
using Donkey.Model.Resulting;
using System.Windows.Forms;
using System.Drawing;
using Grasshopper.GUI.Canvas;
using System.Windows.Forms.DataVisualization.Charting;
using Grasshopper.Kernel.Attributes;
using Grasshopper.GUI;
using System.Drawing.Drawing2D;
using Rhino;
using Rhino.DocObjects;
using System.Text;
using Donkey.DesignTracking;

namespace Donkey.Analysis
{
    public class SEA_Comp : GH_Component, I_DS_Comp
    {
        #region FIELD
        public ResultModel m_model { get; set; }
        private bool m_previewValue;
        private bool m_design_tracking;
        private double m_prev_Overloading = double.NaN;
        private double m_prev_Efficiency = double.NaN;
        public Tracker_Form m_form { get; set; }
        public List<ResultModel> FieldOfSolutions { get; set; }
        public bool no_analysis { get; set; }
        private double last_yieldStress;
        private ResultModel last_added_model;
        double yield_stress = Double.NaN;
        #endregion

        #region CONSTRUCTOR
        /// <summary>
        /// Initializes a new instance of the MyComponent1 class.
        /// </summary>
        public SEA_Comp()
            : base("SEA", "Structural Evaluation Assistant",
                "Evaulate and explore different design alternatives",// + Donkey.Info.Mark + " (beta)",
                "Donkey", "3.Analysis")
        {
            this.m_previewValue = false;
            this.m_design_tracking = false;
            this.FieldOfSolutions = new List<ResultModel>();
            this.no_analysis = false;
            this.m_model = (ResultModel)null;
            this.last_yieldStress = Double.NaN;
            this.last_added_model = (ResultModel)null;
        }
        #endregion

        #region PROPERTIES
        public bool TrackDesing
        {
            get { return m_design_tracking; }
            set
            {
                this.m_design_tracking = value;
                this.ExpirePreview(true);
            }
        }
        #endregion

        #region COMPONENT PARAM
        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new ResultModel_Param());
            pManager.AddNumberParameter("Yield Stress", "S", "Max allowed stress [MPa]", GH_ParamAccess.item);
            pManager[1].Optional = true;
            //TODO: Material - read E? , acess - list - for custom each elements?
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new ResultModel_Param());
        }
        #endregion

        #region SOLVE INSTANCE
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            this.ClearRuntimeMessages();

            if (no_analysis)
            {
                DA.SetData(0, m_model);
                if (m_model != null)
                    this.Message = m_model.MaxStress + " MPa";
                no_analysis = false;
                return;
            }

            //READ INPUT PARAMS
            ResultModel ref_model =  (ResultModel)null;
            if (!DA.GetData(0, ref ref_model))
                return;
            this.m_model = new ResultModel(ref_model);

            if(DA.GetData(1, ref yield_stress))
                m_model.ReCalculate_RelativeStress(yield_stress);

            if (yield_stress != last_yieldStress)
            {
                foreach (var model in FieldOfSolutions)
                    model.ReCalculate_RelativeStress(yield_stress);
                if (this.m_form != null)
                    this.m_form.ReloadPanel();
            }

            else if (m_design_tracking && m_model != null && !FieldOfSolutions.Contains(m_model))
            {
                this.FieldOfSolutions.Add(m_model);
                //ALSO ADD SOLUTION TO OPEN WINDOW
                if (this.m_form != null)
                    this.m_form.AddSolution(FieldOfSolutions.Count, m_model, true);
            }
            DA.SetData(0, m_model);
            //PRINT MAX STRESS
            if (m_model != null)
                this.Message = m_model.MaxStress + " MPa";

            this.last_yieldStress = yield_stress;
            this.last_added_model = m_model;
        }
        #endregion

        #region OVERRIDE READ/WRITE
        public override IEnumerable<string> Keywords
        {
            get
            {
                return new string[] { "FEM", "analysis" };
            }
        }
        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetBoolean("preview_data", this.m_previewValue);
            return base.Write(writer);
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            this.m_previewValue = reader.GetBoolean("preview_data");
            return base.Read(reader);
        }

        public override void ClearData()
        {
            if (!no_analysis)
            {
                if (this.m_model != null)
                {
                    this.m_model = null;
                }
                this.Message = " ";
            }

            base.ClearData();
        }
        #endregion

        #region PREVIEW
        public override void DrawViewportWires(IGH_PreviewArgs args)
        {
            if (m_model != null)
            {
                //ResultModel model = new ResultModel(m_model);
                m_model.DrawGeometryColour(args);
                if (this.m_previewValue)
                {
                    args.Display.DepthMode = Rhino.Display.DepthMode.AlwaysInFront;
                    m_model.DrawElementValue(args);
                    args.Display.DepthMode = Rhino.Display.DepthMode.Neutral;
                }
            }
        }
        #endregion

        #region BAKE

        public override void BakeGeometry(RhinoDoc doc, List<Guid> obj_ids)
        {
            ObjectAttributes att = Rhino.RhinoDoc.ActiveDoc.CreateDefaultAttributes();
            att.ColorSource = ObjectColorSource.ColorFromObject;
            this.BakeResultModel(doc, att, m_model);
        }

        public override void BakeGeometry(RhinoDoc doc, ObjectAttributes att, List<Guid> obj_ids)
        {
            this.BakeResultModel(doc, att, m_model);
        }

        private void BakeResultModel(RhinoDoc doc, ObjectAttributes att, ResultModel model)
        {
            att.ColorSource = ObjectColorSource.ColorFromObject;
            att.PlotColorSource = ObjectPlotColorSource.PlotColorFromObject;
            List<Guid> baked_objects = new List<Guid>();
            if (model != null && model.Geometries.Count > 0)
            {
                for (int e = 0; e < model.Elements.Count; e++)
                {
                    List<int> indexes = model.Elements[e].Geometries_ID;
                    for (int i = 0; i < indexes.Count; i++)
                    {
                        Color color = ResultModel.Gradient.ColourAt(model.Geometries_Stress_Relative[indexes[i]]);
                        att.PlotColor = color;
                        att.ObjectColor = color;
                        att.Name = "\u03B7: " + Math.Round(model.Geometries_Stress_Relative[indexes[i]], 3) * 100 + "%";
                        baked_objects.Add(model.Geometries[indexes[i]].Bake_Geometry(doc, att));
                    }
                }
                doc.Groups.Add("\u03B7: " + model.Efficiency + "% - " + model.Overloading + "%", baked_objects);
            }
        }
        #endregion

        #region MENU

        //REMOVE POSSIBILITY TO CHANGE NAME - NICKNAME OF THE COMPONENT
        public override bool AppendMenuItems(ToolStripDropDown menu)
        {
            this.Menu_AppendPreviewItem(menu);
            this.Menu_AppendEnableItem(menu);
            this.Menu_AppendBakeItem(menu);
            this.Menu_AppendRuntimeMessages(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            this.AppendAdditionalMenuItems(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            this.Menu_AppendObjectHelp(menu);
            return true;
        }

        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Preview value", this.PreviewValues, true, this.m_previewValue)
                .ToolTipText = "Preview evaluation valuses for each element.";
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Design tracking", this.TrackDesign, true, this.m_design_tracking)
                .ToolTipText = "Store and track designed solutions in order to compare them.";
            GH_DocumentObject.Menu_AppendSeparator(menu);
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Bake all", this.BakeAll)
                .ToolTipText = "Bake all stored design alternatives";
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Reset history", this.ResetHistory)
                .ToolTipText = "Reset previously stored design alternatives";
        }

        private void BakeAll(object sender, EventArgs e)
        {
            var doc = RhinoDoc.ActiveDoc;
            ObjectAttributes att = Rhino.RhinoDoc.ActiveDoc.CreateDefaultAttributes();
            att.ColorSource = ObjectColorSource.ColorFromObject;

            var undo = doc.BeginUndoRecord("BakeAll");
            foreach (var model in FieldOfSolutions)
            {
                this.BakeResultModel(doc, att, model);
            }
            doc.EndUndoRecord(undo);
            doc.Views.Redraw();
        }

        private void ResetHistory(object sender, EventArgs e)
        {
            this.FieldOfSolutions.Clear();
            this.last_added_model = null;
            // this.last_yieldStress = Double.NaN;
            this.m_form.ReloadPanel();
            //TODO: reset selection to null - info text
            this.ExpireSolution(true);
        }

        //OPEN DESIGN TRACKING WINDOW AND START TO TRACK DESING
        private void TrackDesign(object sender, EventArgs e)
        {
            //this.RecordUndoEvent("TrackingDesing");
            this.m_design_tracking = !m_design_tracking;
            if (m_design_tracking)
            {
                if (this.m_form == null)
                {
                    this.m_form = new Tracker_Form(this);
                    //this.m_form.Owner = owner;
                    GH_WindowsFormUtil.CenterFormOnCursor((Form)this.m_form, true);
                    ((Control)this.m_form).Show();
                    if (m_model != null && this.FieldOfSolutions != null && this.FieldOfSolutions.Count == 0)
                    {
                        this.m_form.AddSolution(1, m_model, true);
                        this.FieldOfSolutions.Add(m_model);
                    }
                }
                else
                    this.m_form.Select();
            }

            if (m_form != null)
            {
                m_form.tracking.Checked = this.m_design_tracking;
                m_form.Refresh();
            }
        }

        //PREVIEW VALUES -- TODO improve
        private void PreviewValues(object sender, EventArgs e)
        {
            this.RecordUndoEvent("ChangePreviewMode");
            this.m_previewValue = !m_previewValue;
            this.ExpirePreview(true);
        }
        #endregion

        #region COMPONENT SETUP
        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.evaluate; }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{7979B5DE-6CC4-4BCC-B625-68646E616D55}"); }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        public override bool IsPreviewCapable { get { return true; } }
        public override bool IsBakeCapable { get { return true; } }

        public override void CreateAttributes()
        {
            m_attributes = new Analysis_Att(this);
        }
        #endregion
    }
}