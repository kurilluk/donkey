﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ComponentModel;
using System.Threading;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel.Data;
using Grasshopper;
using System.IO;

namespace Donkey.Analysis
{
    /// <summary>
    /// Running MIDAS (external application) to analyse the structural model.
    /// </summary>
    public class FEAnalysis
    {
        #region FIELD

        //public delegate void RedrawComponent(int exitCode);
        //public RedrawComponent ExitProcess;

        public Process Analysis {get; private set;}
        private string m_fileName;
        private string m_workingPath;
        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        public FEAnalysis()
        {
            this.Analysis = Process.GetCurrentProcess();
        }

        public string Setup(string MidasPath, string workingDirectory, string fileName, bool popup, FEAnalysis_Comp comp, double elemSize, int elemCount, string arguments)
    {
            var m_arguments = "-OM_design2design -IO_ff vtu -IN_model " + fileName + ".model.vtu " + "-IN_meshGen_elemSize " + elemSize + " -IN_meshGen_elemCount " + elemCount + " " + arguments;
            //Setup process
            if (!popup)
            {
                //without popup window
                Analysis.StartInfo.WorkingDirectory = workingDirectory;
                Analysis.StartInfo.FileName = MidasPath; 
                Analysis.StartInfo.Arguments = m_arguments;
                Analysis.StartInfo.CreateNoWindow = true;
                Analysis.EnableRaisingEvents = true;
                Analysis.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
               // Analysis.Exited += new EventHandler(MIDAS_Exited);
            }
            else
            {
                //with popup window
                Analysis.StartInfo.WorkingDirectory = workingDirectory;
                Analysis.StartInfo.FileName = "cmd";
                Analysis.StartInfo.Arguments = " /K " + MidasPath + " " +  m_arguments;

                Analysis.StartInfo.CreateNoWindow = false;
                Analysis.StartInfo.UseShellExecute = true;
                Analysis.EnableRaisingEvents = true;
                Analysis.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                //Analysis.Exited += new EventHandler(MIDAS_Exited); 
            }

            return workingDirectory + "\\" + fileName + ".rslts.vtu";
        }
        #endregion

        #region METHODS
        //RUN COMPUTATION PROCESS
        public void Run()
        {
           //ADD if process is set?
            try
            {
                Analysis.Start();
            }
            catch (Exception ex)
            {
                throw new Exception("System can not find MIDAS application, please change the MIDAS path in component settings");
            }

            //CATCH SOLUTION EXEPTION - SYSTEM CAN NOT FIND THE FILE SPECIFIC..
        }
        ////EXITED EVENT METHOD
        //private void MIDAS_Exited(object sender, System.EventArgs e)
        //{
        //    //ExitProcess(this.Analysis.ExitCode);
        //}

        //PATH SPLITING METHOD
        //private void SplitFilePath(string filePath)
        //{
        //    //string res1 = filePath;
        //    //string res2 = filePath;
        //    string[] dumpArray = filePath.Split('\\', '/');
        //    int name_count = dumpArray[dumpArray.Length - 1].Length - 1;
        //    int path_count = filePath.Length - 1;
        //    //m_file = filePath.Remove(0, path_count - name_count);
        //    m_path = filePath.Remove(path_count - name_count, name_count + 1);

        //    m_file = dumpArray[dumpArray.Length - 1];
        //    dumpArray= m_file.Split('.');
        //    m_fileCut = dumpArray[0];
        //}
        #endregion
    }
}
