﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel.Data;
using Rhino.Geometry;
using Grasshopper;
using Grasshopper.GUI;
using Donkey.Model;
using Donkey.Extensions;
using Donkey.Model.Resulting;
using Donkey.Model.Analytical;
using System.Windows.Forms;
using Grasshopper.Kernel.Attributes;
using Grasshopper.GUI.Canvas;
using System.Xml;
using System.IO;

namespace Donkey.Analysis
{
    public class FEAnalysis_Comp : GH_Component
    {
        #region FIELD
        //public delegate void ExSolution();
        //private ExSolution solutionExpired;

        //private string resultFile;
        //private ResultModel oldBuffer;

        private Stopwatch timer;
        private String name;
        private bool m_popup;
        //private string m_midasPathDefault;
        public string GlobalSetupFile;

        private string m_arguments_local;
        public string Arguments_default { get; private set; }
        public bool isLocal_arguments;

        private string m_workingDirectory;
        public bool isLocal_workingDirectory;

        private string m_MIDASpath;
        public bool isLocal_MIDASpath;

        //public int EvalutateResult { get; private set; }        
        //public string EvaluateStatus {get; private set;}

        #endregion

        #region CONSTRUCTOR
        /// <summary>
        /// Initializes a new instance of the Analysis_Comp class.
        /// </summary>
        public FEAnalysis_Comp()
            : base("Analysis", "Analysis",
                "Structural analysis uses OOFEM", //+ Donkey.Info.Mark,
                "Donkey", "3.Analysis")
        {
            //ANALYSIS LOCAL/DEFAULT SETUP
            //this.solutionExpired += Recompute;
            this.m_popup = false;
            this.GlobalSetupFile = createGlobalSetupFile();
            //LOAD default arguments
            this.Arguments_default = "-P_RigidBodyToRigidArm\n" +
    "-P_solverOOFEM -\n-P_dw\n//-P_stability\n-IO_ff vtu\n-OUT_print_CSusage\n-OUT_moBFN -\n-OUT_print_model_parent\n" +
    "-OUT_print_property\n//-OUT_args"; //-P_stability , -OUT_args
            this.m_arguments_local = Arguments_default;
            this.isLocal_arguments = false;

            this.m_workingDirectory = "C:\\MIDAS\\TEMP";
            this.isLocal_workingDirectory = false;

            this.m_MIDASpath = "C:\\MIDAS\\midas.exe";
            this.isLocal_MIDASpath = false;

            //COMPONENT DEFAULT SATUS SETUP
            //EvaluateStatus = "ready";
            //EvalutateResult = -1;
            //oldBuffer = null;
            //resultFile = string.Empty;
        }

        private string createGlobalSetupFile()
        {
            string path =Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Donkey";
                if(!Directory.Exists(path))
                    Directory.CreateDirectory(path);

            return path + "\\setup.xml";
        }

        #endregion

        #region PROPERTY
        public string MIDASpath
        {
            get 
            {
                if(!this.isLocal_MIDASpath)
                {
                    if(File.Exists(this.GlobalSetupFile))
                        return this.ReadGlobalSetup("MIDASpath"); 

                }
                //if file don't exists return default value
                return this.m_MIDASpath;
            }
            set { this.m_MIDASpath = value;}

        }

        public string WorkingDirectory 
        {
            get 
            {
                if (!this.isLocal_workingDirectory)
                {
                    //read global value if global setup exists
                    if (File.Exists(this.GlobalSetupFile))
                        return this.ReadGlobalSetup("WorkingDirectory");
                }
                //if is local return local value
                return this.m_workingDirectory;
            }
            set { this.m_workingDirectory = value; }
        }

        public string Arguments
        {
            get
            {
                //if is not local
                if(!this.isLocal_arguments)
                {
                    //read global value if global setup exists
                    if (File.Exists(this.GlobalSetupFile))
                    {
                        return this.ReadGlobalSetup("Arguments");
                    }
                    // if global setup don't exists return default value
                    else { return this.Arguments_default; }
                }
                //if is local return local value
                return this.m_arguments_local;
            }
            set
            {
                this.m_arguments_local = value;
            }
        }
        #endregion

        #region COMPONENT PARAM
        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new AnalyticalModel_Param());
            //pManager.AddTextParameter("Name", "N", "Analysis file name", GH_ParamAccess.item, (FileName())); //command "C:\\Users\\kurilluk\\Dropbox\\Grant\\MPO\\example\\test"
            //pManager.AddTextParameter("arguments", "A", "Command arguments", GH_ParamAccess.item, "-IN_FE_OOFEM Beam3d -IN_elemsize 500.0 -IN_meshGenElemCount 10 -P_RigidBodyToRigidArm -P_solverOOFEM - -IO_ff vtu -OUT_print_CSusage -OUT_moBFN - -OUT_print_model_parent -OUT_print_property");
            //pManager.AddBooleanParameter("pop-up", "W", "Open analysis in new pop-up window", GH_ParamAccess.item, false);
            //pManager.AddTextParameter("MIDAS", "MI", "Path to the MIDAS", GH_ParamAccess.item, "C:\\MIDAS\\midas.exe");
            //pManager.AddParameter(new MidasPath_Param());
            //pManager[1].Optional = true;
            pManager.AddNumberParameter("Size", "S", "Subdivide element by size [mm]", GH_ParamAccess.item, 2500);
            pManager.AddIntegerParameter("Count", "C", "Subdivite element by count (beam only)", GH_ParamAccess.item, 5);
            pManager[1].Optional = true;
            pManager[2].Optional = true;
        }

        //private string FileName()
        //{
        //    string date = String.Empty;
        //    date += (DateTime.Today.Year - 2000).ToString();
        //    date += (DateTime.Today.Month < 10) ? "0" + DateTime.Today.Month.ToString() : DateTime.Today.Month.ToString();
        //    date += (DateTime.Today.Day < 10) ? "0" + DateTime.Today.Day.ToString() : DateTime.Today.Day.ToString();
        //    return date;
        //}

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            //pManager.Register_StringParam("status", "S", "Message about process");
            pManager.AddParameter(new ResultModel_Param());
        }
        #endregion

        #region SOLVE INSTANCE
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            VTU_Reader reader = new VTU_Reader();


            //READ INPUT PARAMS
            AnalyticalModel model = new AnalyticalModel();
            if (!DA.GetData(0, ref model))
                return;
            //name = "defaultName";
            //DA.GetData("Name", ref name);
            this.name = model.Name;

            //WRITE MODEL TO VTU FILE
            if (!Directory.Exists(this.WorkingDirectory))
            {
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Working directory is not correctly set, please change the path in component settings");
                return;
            }
            VTU_Writer file = new VTU_Writer();
            file.Write(model, this.WorkingDirectory + "\\" + name + ".model.vtu"); //TODO static method..?

            //WHY HERE? WHAT DOES IT MEAN
            Rhino.Runtime.HostUtils.DisplayOleAlerts(false);

            //CREATE ANALYSIS COMAND AND RUN ANALYSIS AS PROCESS
            FEAnalysis ap = new FEAnalysis();
            //SETUP ANALYSIS PROCESS AND READ RESULT FILE
            string[] arguments_list = Arguments.Split('\n');
            StringBuilder sb = new StringBuilder();
            foreach (string argument in arguments_list)
            {
                if (!argument.StartsWith("//"))
                    sb.Append(" " + argument);
            }

            double size = 2500;
            DA.GetData(1, ref size);
            if (size < 10)
            {
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Dividing size is too low. (minimum is 10 mm)");
                size = 10;
            }

            int count = 5;
            DA.GetData(2, ref count);

            string resultFile = ap.Setup(MIDASpath, this.WorkingDirectory, name, m_popup, this, size, count, sb.ToString()); //Arguments.Replace("\n", " ")
            //REGISTER REDRAW METHOD ON EXITED METHOD
            //ap.ExitProcess += Redraw;
            //RUN ANALYSIS PROCESS

            timer = Stopwatch.StartNew();
            ap.Run();
            //WAIT FOR ANALYSIS FIHISH
            //ap.Analysis.WaitForExit();
            bool ESC_down;
            int loop = 0;
            int wait = 50;
            do
            {
                ESC_down = GH_Document.IsEscapeKeyDown();
                //Debug.Print("ESC ="+ ESC_down);
                if (ESC_down)
                {
                    Print("Analysis was aborted!");
                    this.AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Calculation was aborted!");
                    this.timer.Stop();
                    ap.Analysis.Kill();
                    //DA.DisableGapLogic();          
                    return;
                }
                loop++;
                Print("Analysing, please wait... " + timer.ElapsedMilliseconds + " ms");

            }
            while (!ap.Analysis.WaitForExit(wait));
            Print("Command", false);
            timer.Stop();
            //Print("Analysis done in " + timer.ElapsedMilliseconds + " ms. [" + name +" "+ DateTime.Now.ToString("h:mm:ss")+"]", true);
            Print("Analysis of " + name + " done in " + timer.ElapsedMilliseconds + " ms.", true);
            //IF ANALYSIS WAS SUCCESSFUL
            //possible to use ap.Analysis.ExitCode with out delegate and else..
            if (ap.Analysis.ExitCode == -1073741510 || ap.Analysis.ExitCode == 0)
            {
                //RETURN RESULT MODEL
                ResultModel resultModel = reader.Read(resultFile);
                DA.SetData(0, resultModel);
                //READ BUFFER MODEL WHICH IS USING IN VP WHEN ANALYSIS IS RUNNING
                //this.oldBuffer = model;  //REDUCE BLINKING GEOMETRY IN VIEWPORT
                //this.oldBuffer = null;
            }
            else
            {
                //WHEN ANALYSIS WAS UNSUCCESSFUL
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Analysis calculation false!");
                DA.SetData(0, null);
            }
        }
            //THIS IS ALWAYS RETURNED 
            //DA.SetData(0, EvaluateStatus);
            //GH_Document d = this.OnPingDocument();
        #endregion

        #region OVERRIDE
        public override IEnumerable<string> Keywords
        {
            get
            {
                return new string[] { "FEM", "analysis" };
            }
        }
        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetString("analysis_arguments", this.m_arguments_local);
            writer.SetBoolean("analysis_IsLocal_arguments", this.isLocal_arguments);
            writer.SetBoolean("analysis_IsLocal_MIDASpath", this.isLocal_MIDASpath);
            writer.SetBoolean("analysis_IsLocal_WorkingDirectory", this.isLocal_workingDirectory);
            writer.SetString("analysis_workingDirectory", this.WorkingDirectory);
            writer.SetString("analysis_MIDASpath", this.m_MIDASpath);
            writer.SetBoolean("analysis_popup", this.m_popup);
            return base.Write(writer);
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            this.m_arguments_local = reader.GetString("analysis_arguments");
            this.isLocal_arguments = reader.GetBoolean("analysis_IsLocal_arguments");
            if (reader.ItemExists("analysis_IsLocal_MIDASpath"))
                this.isLocal_MIDASpath = reader.GetBoolean("analysis_IsLocal_MIDASpath");
            if (reader.ItemExists("analysis_IsLocal_WorkingDirectory"))
                this.isLocal_workingDirectory = reader.GetBoolean("analysis_IsLocal_WorkingDirectory");
            if (reader.ItemExists("analysis_MIDASpath"))
                this.m_MIDASpath = reader.GetString("analysis_MIDASpath");
            this.WorkingDirectory = reader.GetString("analysis_workingDirectory");
            this.m_popup = reader.GetBoolean("analysis_popup");
   
            //this.m_midasPath = ReadGlobalSetup(); //OR ONLY MEHTOD
            return base.Read(reader);
        }
        #endregion

        private void Print(string message)
        {
            Print(message, false);
        }

        private void Print(string message, bool write)
        {
            if (write)
                Rhino.RhinoApp.WriteLine(message);
            else
            {
                Rhino.RhinoApp.SetCommandPrompt(message);
                Rhino.RhinoApp.Wait();
            }

        }

        private string ReadGlobalSetup(string valueName)
        {
            string read = "";
            try
            {
                XmlReader doc = XmlReader.Create(this.GlobalSetupFile);
                while (doc.Read())
                     if (doc.Name == valueName)
                    {
                        read = doc.ReadElementString();
                    }
                doc.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return read;
        }


        #region MENU
        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Properties...", this.Menu_Settings, true, false).ToolTipText = "Define the solver and working directory paths and edit analysis arguments.";
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Show Console", this.Menu_PopupClicked, true, this.m_popup).ToolTipText = "Open console in pop-up window with running solver (developer settings).";
        }

        private void Menu_Settings(object sender, EventArgs e)
        {
            this.ShowCustomSetting();
        }

        private void Menu_PopupClicked(object sender, EventArgs e)
        {
            if(this.m_popup)
            {
                this.RecordUndoEvent("Nopop-up");
                this.m_popup = false;
                this.Message = (string)null;
                //this.Attributes.PerformLayout();
                //this.ExpirePreview(true);
                //this.Attributes.ExpireLayout();
            }else{
                this.RecordUndoEvent("Pop-up");
                this.m_popup = true;
                this.Message = "cmd";
                //this.Attributes.ExpireLayout();
                //this.Attributes.PerformLayout();
                //this.ExpirePreview(true);
                //this.ExpireSolution(true);
            }
            //this.ExpirePreview(true);
            this.ExpireSolution(true);
        }
        #endregion

        #region COMPONENT SETUP
        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.analysis; }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{23a8cde8-5f4b-49d6-8e7c-4100ceeffd36}"); }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }//.obscure; }
        }

        public override bool IsPreviewCapable { get { return false; } }
        public override bool IsBakeCapable { get { return false; } }

        #endregion

        //#region CUSTOM METHODS
        //public void Redraw(int exitCode)
        //{
        //   //this.EvaluateStatus = "stoped";
        //    this.EvalutateResult = exitCode;          
        //    //to make a thread-safe call to a Windows Forms control
        //    //calling ExpireSolution(true) and this.Locked = false;
        //    //Instances.DocumentEditor.Invoke(solutionExpired);
        //}

        //private void Recompute()
        //{
        //    this.Locked = false;
        //    this.ExpireSolution(true);
        //}
        //#endregion

        //private void Expired()
        //{
        //    //this.Locked = false;
        //    this.ExpireSolution(true);
        //}

        #region DOUBLE-CLICK
        public void ShowCustomSetting()
        {
            this.TriggerAutoSave();
            FEAnalysis_Form numberSliderPopup = new FEAnalysis_Form(this);
            //numberSliderPopup.Setup(this);
            GH_WindowsFormUtil.CenterFormOnCursor((Form)numberSliderPopup, true);
            if (numberSliderPopup.ShowDialog((IWin32Window)Instances.DocumentEditor) != DialogResult.OK)
                return;
            //Write to file..

            //this.Attributes.ExpireLayout();
            this.ExpireSolution(true);

            //OpenFileDialog midasDialog = new OpenFileDialog();
            ////midasDialog.Site.Name = "MIDAS global paht";
            //midasDialog.Title = "Set global MIDAS path";
            //midasDialog.Multiselect = false;
            //midasDialog.InitialDirectory = "C:\\MIDAS\\"; //Read from file if exist
            //midasDialog.Filter = "MIDAS|midas*.exe|All Files|*.*";
            //if (midasDialog.ShowDialog() == DialogResult.OK)
            //{
            //    string filePath = midasDialog.FileName; // in full path with midas.exe
            //}

            //m_settings = Path.GetDirectoryName(ofd.FileName);
        }
        #endregion

        public override void CreateAttributes()
        {
            m_attributes = new FEAnalysis_CompAttributes(this);
        }
    }

    public class FEAnalysis_CompAttributes : GH_ComponentAttributes
    {
        public FEAnalysis_CompAttributes(FEAnalysis_Comp owner)
            : base(owner)
        { }

        #region DOUBLE-CLICK
        public override GH_ObjectResponse RespondToMouseDoubleClick(GH_Canvas sender, GH_CanvasMouseEvent e)
        {
            ((FEAnalysis_Comp)this.Owner).ShowCustomSetting();
            return GH_ObjectResponse.Handled;
        }
        #endregion
 
    }
}