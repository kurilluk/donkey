﻿using Grasshopper.GUI.Canvas;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Attributes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Donkey.Analysis
{
    public class DesignTracker_ResizableComp_Attributes : GH_ResizableAttributes<DesignTracker_Comp>
    {
        private Rectangle m_graph;

        protected override Size MinimumSize
        {
            get
            {
                //float val1 = 0.0f;
                //List<IGH_Param>.Enumerator enumerator;
                //try
                //{
                //    enumerator = this.Owner.Params.Input.GetEnumerator();
                //    while (enumerator.MoveNext())
                //    {
                //        IGH_Param current = enumerator.Current;
                //        val1 = Math.Max(val1, current.Attributes.Bounds.Width);
                //    }
                //}
                //finally
                //{
                //    //enumerator.Dispose();
                //}
                //return new Size(Convert.ToInt32(val1 + 60f), 65);
                return new Size(250, 50);
            }
        }

        protected override Padding SizingBorders
        {
            get
            {
                return new Padding(6);
            }
        }

        public DesignTracker_ResizableComp_Attributes(DesignTracker_Comp component)
            : base(component)
        {
            this.Bounds = (RectangleF)new Rectangle(0, 0, 250, 150);
            this.m_graph = new Rectangle();
        }

        public override void AppendToAttributeTree(List<IGH_Attributes> atts)
        {
            base.AppendToAttributeTree(atts);
            IEnumerator<IGH_Param> enumerator = null;
            try
            {
                enumerator = this.Owner.Params.GetEnumerator();
                while (enumerator.MoveNext())
                    enumerator.Current.Attributes.AppendToAttributeTree(atts);
            }
            finally
            {
                if (enumerator != null)
                    enumerator.Dispose();
            }
        }

        public override void ExpireLayout()
        {
            base.ExpireLayout();
            this.Layout();
        }

        protected override void Layout()
        {
            base.Layout();
            RectangleF bounds = this.Bounds;
            bounds.Inflate(-(this.Owner.Params.InputWidth + 4f), -2f);
            //GH_ComponentAttributes.LayoutInputParams((IGH_Component) this.Owner, bounds);
            //bounds.X += (this.Owner.Params.InputWidth + 4f);

            GH_ComponentAttributes.LayoutInputParams((IGH_Component)this.Owner, bounds);
            bounds = this.Bounds;
            bounds.Inflate(-(this.Owner.Params.OutputWidth + 4f), -2f);
            GH_ComponentAttributes.LayoutOutputParams((IGH_Component)this.Owner, bounds);

            bounds = this.Bounds;
            float offset = this.Owner.Params.OutputWidth;
            if (this.Owner.Params.InputWidth > offset)
                offset = this.Owner.Params.InputWidth;
            bounds.Inflate(-(offset + 4f), -6f);
            m_graph = GH_Convert.ToRectangle(bounds);

        }

        protected override void Render(GH_Canvas canvas, Graphics graphics, GH_CanvasChannel channel)
        {
            switch (channel)
            {
                case GH_CanvasChannel.Wires:
                    foreach (IGH_Param input in Owner.Params.Input)
                    {
                        //List<IGH_Param>.Enumerator enumerator1;
                        ////enumerator1 = ;
                        //try
                        //{
                        //    enumerator1 = this.Owner.Params.Input.GetEnumerator();
                        //    while (enumerator1.MoveNext())
                        input.Attributes.RenderToCanvas(canvas, GH_CanvasChannel.Wires);
                        //    break;
                        //}
                        //finally
                        //{
                        //    //enumerator1.Dispose();
                        //}
                    }
                    break;

                case GH_CanvasChannel.Objects:
                    GH_Viewport viewport = canvas.Viewport;
                    // ISSUE: explicit reference operation
                    // ISSUE: variable of a reference type
                    RectangleF rec = this.Bounds;
                    double num1 = 10.0;
                    int num2 = viewport.IsVisible(ref rec, (float)num1) ? 1 : 0;
                    this.Bounds = rec;
                    if (num2 == 0)
                        break;

                    GH_Palette palette = GH_Palette.White;
                    switch (this.Owner.RuntimeMessageLevel)
                    {
                        case GH_RuntimeMessageLevel.Warning:
                            palette = GH_Palette.Warning;
                            break;
                        case GH_RuntimeMessageLevel.Error:
                            palette = GH_Palette.Error;
                            break;
                    }

                    GH_Capsule capsule = GH_Capsule.CreateCapsule(this.Bounds, palette);
                    capsule.SetJaggedEdges(false, false);
                    foreach (IGH_Param input in Owner.Params.Input)
                    {
                        capsule.AddInputGrip(input.Attributes.InputGrip.Y);
                    }
                    foreach (IGH_Param output in Owner.Params.Output)
                    {
                        capsule.AddOutputGrip(output.Attributes.OutputGrip.Y);
                    }

                    //graphics.SmoothingMode = SmoothingMode.HighQuality;
                    capsule.Render(graphics, this.Selected, this.Owner.Locked, true);
                    GH_PaletteStyle impliedStyle = GH_CapsuleRenderEngine.GetImpliedStyle(palette, this.Selected, this.Owner.Locked, this.Owner.Hidden);
                    GH_ComponentAttributes.RenderComponentParameters(canvas, graphics, (IGH_Component)this.Owner, impliedStyle);
                    Grasshopper.GUI.GH_GraphicsUtil.ShadowRectangle(graphics, m_graph, 6, 30);
                    graphics.DrawRectangle(Pens.Black, m_graph);

                    int x = m_graph.Width / 10;
                    int y = m_graph.Height / 10;
                    Grasshopper.GUI.GH_GraphicsUtil.Grid(graphics, (RectangleF)m_graph, x, y);

                    RectangleF rect = this.m_graph;
                    rect.Inflate(-10f, -10f);
      
                    //PointF[] solution_positions = new PointF[Owner.Solutions.Count];
                    //PointF prev_pt = PointF.Empty;
                    //for (int i = 0; i < Owner.Solutions.Count; i++)
                    //{
                    //    //PointF pt = new PointF(Owner.Solutions[i].pointerField.Location.X + (Owner.Solutions[i].pointerField.Width / 2), Owner.Solutions[i].pointerField.Location.Y + (Owner.Solutions[i].pointerField.Height / 2));
                    //    //if(i > 0)
                    //    //    graphics.DrawLine(Pens.Black,
                    //    PointF pt = new PointF(Owner.Solutions[i].pointerField.Location.X + (Owner.Solutions[i].pointerField.Width / 2), Owner.Solutions[i].pointerField.Location.Y + (Owner.Solutions[i].pointerField.Height / 2));
                        
                    //    if (!prev_pt.IsEmpty)
                    //        graphics.DrawLine(Pens.Black, prev_pt, pt);

                    //    //Owner.Solutions[i].MoveSolution(rect, rect.Location, graphics, (i + 1) == Owner.Solutions.Count);
                    //   // solution_positions[i] = new PointF(Owner.Solutions[i].pointerField.Location.X,Owner.Solutions[i].pointerField.Location.Y);
                        
                    //    prev_pt = new PointF(pt.X,pt.Y);
                    //}

                    for (int i = 0; i < Owner.Solutions.Count; i++)
                    {
                        //PointF pt = new PointF(Owner.Solutions[i].pointerField.Location.X + (Owner.Solutions[i].pointerField.Width / 2), Owner.Solutions[i].pointerField.Location.Y + (Owner.Solutions[i].pointerField.Height / 2));
                        //if(i > 0)
                        //    graphics.DrawLine(Pens.Black,
                        //PointF pt = new PointF(Owner.Solutions[i].pointerField.Location.X + (Owner.Solutions[i].pointerField.Width / 2), Owner.Solutions[i].pointerField.Location.Y + (Owner.Solutions[i].pointerField.Height / 2));

                        //if (!prev_pt.IsEmpty)
                           // graphics.DrawLine(Pens.Silver, prev_pt, pt);

                        Owner.Solutions[i].MoveSolution(rect, rect.Location, graphics, (i + 1) == Owner.Solutions.Count);
                        //solution_positions[i] = new PointF(Owner.Solutions[i].pointerField.Location.X, Owner.Solutions[i].pointerField.Location.Y);

                        //prev_pt = pt;
                    }
                    //if(Owner.Solutions.Count > 2)
                    //    graphics.DrawCurve(Pens.Silver, solution_positions);

                    break;
            }
        }

        public override GH_ObjectResponse RespondToMouseDown(GH_Canvas sender, Grasshopper.GUI.GH_CanvasMouseEvent e)
        {
            if (e.Button == MouseButtons.Left)
            {
                bool selected = false;
                int index = 0;
                for (int i = 0; i < Owner.Solutions.Count; i++)
                {
                    if (Owner.Solutions[i].pointerField.Contains(e.CanvasLocation))
                    {
                        if (!selected)
                        {
                            Owner.Solutions[i].selected = true;
                            selected = true;
                            index = i;
                        }else
                            Owner.Solutions[i].selected = false;
                    }
                    else { Owner.Solutions[i].selected = false; }
                }
                if (selected)
                {
                    this.ExpireLayout();
                    sender.Refresh();
                    Owner.Selected_solution = "selected: " + index;
                    Owner.solution_index = index;
                    Owner.ExpireSolution(true);
                    return GH_ObjectResponse.Handled;
                }
                else
                {
                    Owner.Selected_solution = "selected: NaN";
                    Owner.ExpireSolution(true);
                }
            }
            return base.RespondToMouseDown(sender, e);
        }

        public override GH_ObjectResponse RespondToMouseMove(GH_Canvas sender, Grasshopper.GUI.GH_CanvasMouseEvent e)
        {
            return base.RespondToMouseMove(sender, e);
            //return GH_ObjectResponse.Ignore;
        }
    }
}