﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using System.Drawing;
using System.Windows.Forms;
using Grasshopper.GUI.Canvas;

namespace Donkey.Analysis
{
    class DesignTacker_Param : GH_Param<DesignTracker_Goo>
    {

        #region CONSTRUCTOR
        public DesignTacker_Param()
            : base(new GH_InstanceDescription("E", "EV",
                "Des",//+ Donkey.Info.Mark,
                "Donkey", "3.Analysis"))
        {
        }

        #endregion
        public override Guid ComponentGuid
        {
            get { return new Guid("{43EDEE11-91BD-4BDC-88D5-07D9360EF173}"); }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.evaluate; }

        }

        public override GH_ParamKind Kind
        {
            get
            {
                return GH_ParamKind.floating;
            }
        }

        protected override void CollectVolatileData_FromSources()
        {
            base.CollectVolatileData_FromSources();
        }

        int i = 0;

        protected override void OnVolatileDataCollected()
        {
            if (!m_data.IsEmpty)
            {
                if(m_data.Count<DesignTracker_Goo>() > 1)
                    this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Many inputs, read only first one!");
                this.m_data.Clear();
                this.m_data.Append(new DesignTracker_Goo());
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "run " + i + " times");
                i++;
            }
        }

        public override void CreateAttributes()
        {
            //base.CreateAttributes();
            this.m_attributes = new Evaluation_Attributes(this);
        }

    }

    class Evaluation_Attributes : Grasshopper.Kernel.Attributes.GH_ResizableAttributes<DesignTacker_Param>
    {
        private GH_Capsule m_capsule;
        private List<Solution> solutions;
        private Random random = new Random();

        public Evaluation_Attributes(DesignTacker_Param owner)
            : base(owner)
        {
            this.Bounds = (RectangleF)new Rectangle(0, 0, 150, 150);
            this.solutions = new List<Solution>();
            solutions.Add(new Solution(0, 0.5, 0.5));
        }

        protected override Size MinimumSize
        {
            get
            {
                return new Size(50, 50);
            }
        }

        protected override Padding SizingBorders
        {
            get
            {
                return new Padding(5);
            }
        }

        //protected override void Render(GH_Canvas canvas, Graphics graphics, GH_CanvasChannel channel)
        //{
        //    if (channel == GH_CanvasChannel.Wires)
        //    {
        //        this.RenderIncomingWires(canvas.Painter, (IEnumerable<IGH_Param>)this.Owner.Sources, this.Owner.WireDisplay);
        //    }
        //    else
        //    {
        //        base.Render(canvas, graphics, channel);
        //    }
        //}

        public override void ExpireLayout()
        {
            base.ExpireLayout();
            //this.m_capsule.Dispose();
            //this.m_capsule = null;
            //this.solutions.Clear();
        }


        protected override void Layout()
        {
            //base.Layout();
            this.Bounds = new RectangleF(this.Pivot, this.Bounds.Size);
            //this.Bounds = (RectangleF)GH_Convert.ToRectangle(this.Bounds);
            //GH_Capsule myCapsule = GH_Capsule.CreateCapsule(this.Bounds, GH_Palette.Hidden, 5, 30);
            //GH_Palette palette = GH_Palette.White
            GH_Palette palette = GH_Palette.White;
            switch (this.Owner.RuntimeMessageLevel)
            {
                case GH_RuntimeMessageLevel.Warning:
                    palette = GH_Palette.Warning;
                    break;
                case GH_RuntimeMessageLevel.Error:
                    palette = GH_Palette.Error;
                    break;
            }
            GH_Capsule myCapsule = GH_Capsule.CreateCapsule(this.Bounds, palette, 15, 0);
            //myCapsule.SetJaggedEdges(false, true);
            myCapsule.AddInputGrip(this.InputGrip.Y);
            this.m_capsule = myCapsule;
            myCapsule.Dispose();

            //LayoutInputParams(this.Owner, this.Bounds);

            //this.solutions.Add(new Solution(solutions.Count, random.NextDouble(), random.NextDouble()));

            //float x = (float)(random.NextDouble());// * Bounds.Width);//+this.Pivot.X;
            //float y = (float)(random.NextDouble());// * Bounds.Height);//+this.Pivot.Y;
            //RectangleF rec = new RectangleF(x,y, 30f, 20f);
            //Rectangle rectangle = GH_Convert.ToRectangle(rec);
            //Chart chart1 = new Chart();
            //this.solutions.Add(rec);
            //Grasshopper.Kernel.Attributes.GH_ComponentAttributes.LayoutInputParams(base.Owner, this.Bounds);
            //this.Bounds = Grasshopper.Kernel.Attributes.GH_ComponentAttributes.LayoutBounds(base.Owner, this.Bounds);
            //Grasshopper.Kernel.Attributes.GH_ComponentAttributes.LayoutInputParams(base.Owner, this.Bounds);
        }

        protected override void PrepareForRender(GH_Canvas canvas)
        {
            base.PrepareForRender(canvas);
            //if (this.m_capsule == null)
            //{
            //    this.m_capsule = GH_Capsule.CreateCapsule(this.Bounds, GH_Palette.Hidden, 5, 30);
            //    this.m_capsule.AddInputGrip(this.InputGrip.Y);
            //    this.m_capsule.SetJaggedEdges(false, false);
            //}
        }

        protected override void Render(Grasshopper.GUI.Canvas.GH_Canvas canvas, Graphics graphics, Grasshopper.GUI.Canvas.GH_CanvasChannel channel)
        {
            switch (channel)
            {
                case GH_CanvasChannel.Wires:
                    //base.Render(canvas, graphics, channel);
                    //this.RenderIncomingWires(canvas.Painter, (IEnumerable<IGH_Param>)this.Owner.Params.Input,GH_ParamWireDisplay.@default);
                    this.RenderIncomingWires(canvas.Painter, (IEnumerable<IGH_Param>)this.Owner.Sources, this.Owner.WireDisplay);

                    break;

                case GH_CanvasChannel.Objects:
                    this.m_capsule.Render(graphics, this.Selected, this.Owner.Locked, true);
                    m_capsule.Dispose();
                    //base.Render(canvas, graphics, channel);
                    //m_capsule.Render(graphics, this.Selected, this.Owner.Locked, true);
                    //GH_Capsule myCapsule = GH_Capsule.CreateCapsule(this.Bounds, GH_Palette.Hidden, 5, 30);
                    //myCapsule.Render(graphics, this.Selected, this.Owner.Locked, true);
                    //myCapsule.SetJaggedEdges(true, true);
                    //myCapsule.AddInputGrip(this.InputGrip.X);
                    //myCapsule.Dispose();
                    //RectangleF rect = this.Bounds;
                    //rect.Inflate(-6f, -6f);
                    //Rectangle rectangle = GH_Convert.ToRectangle(rect);
                    //int x = rectangle.Width / 10;
                    //int y = rectangle.Height / 10;
                    //Grasshopper.GUI.GH_GraphicsUtil.Grid(graphics, (RectangleF) rectangle,x,y);
                    //////graphics.SetClip(rect);
                    //Grasshopper.GUI.GH_GraphicsUtil.ShadowRectangle(graphics, rectangle, 10, 50);
                    //graphics.DrawRectangle(Pens.Black, rectangle);

                    //this.AddSolution(graphics, 10, 10);
                    //int i = 0;
                    //foreach (RectangleF rec in solutions)
                    //{
                    //    RectangleF rect = rec;
                    //    rect.X *= this.Bounds.Width;
                    //    rect.Y *= this.Bounds.Height;
                    //    rect.X += this.Pivot.X;
                    //    rect.Y += this.Pivot.Y;
                    //    rect.X -= rect.Width/2;
                    //    rect.Y -= rect.Height/2;
                    //    //graphics.DrawEllipse(Pens.Black, rect);
                    //    //graphics.FillEllipse(Brushes.White, GH_Convert.ToRectangle(rect));
                    //    graphics.FillRectangle(Brushes.White, GH_Convert.ToRectangle(rect));
                    //    graphics.DrawString("" + i, GH_FontServer.Standard, Brushes.Black, rect, Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);
                    //        i++;
                    //}

                    RectangleF rect = this.Bounds;
                    rect.Inflate(-20f, -20f);

                    foreach (Solution solution in solutions)
                    {
                        solution.MoveSolution(rect, rect.Location, graphics, false);
                    }

                    break;

                default:
                    base.Render(canvas, graphics, channel);
                    break;

            }
        }

        public override GH_ObjectResponse RespondToMouseDown(GH_Canvas sender, Grasshopper.GUI.GH_CanvasMouseEvent e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (solutions[0].pointerField.Contains(e.CanvasLocation))
                {
                    solutions[0].selected = true;
                    this.ExpireLayout();
                    sender.Refresh();
                    return GH_ObjectResponse.Handled;
                }
                else { solutions[0].selected = false; }
            }
            return base.RespondToMouseDown(sender, e);
        }
    }
    public class Solution
    {
        #region FIELD
        public int index;
        RectangleF textField;
        public RectangleF pointerField;
        float size = 16f;
        double X_value;
        double Y_value;
        public bool selected = false;
        bool last = true;
        #endregion

        #region CONSTRUCTOR
        public Solution()
            : this(-1, 0, 0)
        { }

        public Solution(int index, double x_value, double y_value)
        {
            this.index = index;
            this.X_value = x_value;
            this.Y_value = y_value;
            textField = new RectangleF(0, 0, size, 8f);
            pointerField = new RectangleF(0, 0, size, size);
        }
        #endregion

        #region METHODS
        public void MoveSolution(RectangleF Bounds, PointF Pivot, Graphics graphics, bool last)
        {
            this.last = last;

            //MOVE
            //COPY RECTANGLE
            RectangleF text = textField;
            RectangleF pointer = pointerField;
            //RECALCULATE POSITION IN COMPONENT
            float X = (float)(Bounds.Width * (X_value));
            float Y = (float)(Bounds.Height * (Y_value));
            //MOVE SOLUTION WITH COMPONENT
            X += Pivot.X;
            Y += Pivot.Y;
            //Y += Bounds.Height;
            //MORE FRAMES
            text.X = X;
            text.Y = Y;
            pointer.X = X;
            pointer.Y = Y;

            //CENTER THE SOLUTION ON THE POSITION
            text.X -= text.Width / 2;
            text.Y -= text.Height / 2;
            pointer.X -= pointer.Width / 2;
            pointer.Y -= pointer.Height / 2;

            pointerField = pointer;

            //RENDER
            Font s = new Font(GH_FontServer.StandardBold.FontFamily, 6f, GH_FontServer.StandardBold.Style);
            RectangleF rect = GH_Convert.ToRectangle(pointer);
            if (selected)
            {
                rect.Inflate(+4f, +4f);
                graphics.FillEllipse(Brushes.Green, rect);
                graphics.DrawEllipse(Pens.Black, rect);
                graphics.DrawString("" + index, s, Brushes.White, text, Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);
                //Grasshopper.GUI.GH_GraphicsUtil.ShadowRectangle(graphics, GH_Convert.ToRectangle(pointer), 10, 50);
            }
            else if (last)
            {
                graphics.FillEllipse(Brushes.Black, rect);
                graphics.DrawEllipse(Pens.Black, rect);
                graphics.DrawString("" + index, s, Brushes.White, text, Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);

            }
            else
            {
                graphics.FillEllipse(Brushes.White, rect);
                graphics.DrawEllipse(Pens.Black, rect);
                graphics.DrawString("" + index, GH_FontServer.Small, Brushes.Black, text, Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);

            }
        }

        public void RenderSolution(Graphics graphics)
        {

        }
        #endregion


    }
}