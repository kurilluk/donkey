﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Drawing;

namespace Donkey.Analysis
{
    public class Analysis_Form : Form
    {
        #region FIELD
        private Analysis_Comp component;
        private CheckBox Pop_up;
        private ToolTip toolTip;
        private TextBox MIDASpath;
        private RichTextBox Arguments;
        private CheckBox MIDASpath_checkBox;
        private CheckBox Arguments_checkBox;
        private Button PathDialog;
        private Button OK;
        private TextBox WorkingDirectory;
        private CheckBox WorkingDirectory_checkBox;
        private Button DirectoryDialog;
        private Button Reset;
        #endregion

        #region CONSTRUCTOR
        public Analysis_Form(Analysis_Comp component)
        {
            this.component = component;

            // Set up the delays for the ToolTip.
            toolTip = new ToolTip();
            toolTip.InitialDelay = 100;
            toolTip.ReshowDelay = 50;
            // Force the ToolTip text to be displayed whether or not the form is active.
            toolTip.ShowAlways = true;

            InitializeComponent();
            PostInitialization();
        }

        private void PostInitialization()
        {
            this.MIDASpath.Text = component.MIDASpath;
            this.Arguments.Text = component.Arguments;
            this.MIDASpath_checkBox.Checked = component.isLocal_MIDASpath;
            this.Arguments_checkBox.Checked = component.isLocal_arguments;
            this.WorkingDirectory.Text = this.component.WorkingDirectory;
            this.WorkingDirectory_checkBox.Checked = component.isLocal_workingDirectory;
            toolTip.SetToolTip(this.Reset, "Restet global value" + Environment.NewLine + "to default analysis arguments");
            this.Pop_up.Checked = component.PopUp;
            this.PerformLayout();
        }
        #endregion

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MIDASpath = new System.Windows.Forms.TextBox();
            this.Arguments = new System.Windows.Forms.RichTextBox();
            this.MIDASpath_checkBox = new System.Windows.Forms.CheckBox();
            this.Arguments_checkBox = new System.Windows.Forms.CheckBox();
            this.PathDialog = new System.Windows.Forms.Button();
            this.OK = new System.Windows.Forms.Button();
            this.WorkingDirectory = new System.Windows.Forms.TextBox();
            this.WorkingDirectory_checkBox = new System.Windows.Forms.CheckBox();
            this.DirectoryDialog = new System.Windows.Forms.Button();
            this.Reset = new System.Windows.Forms.Button();
            this.Pop_up = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // MIDASpath
            // 
            this.MIDASpath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MIDASpath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MIDASpath.Location = new System.Drawing.Point(14, 289);
            this.MIDASpath.Margin = new System.Windows.Forms.Padding(2);
            this.MIDASpath.Name = "MIDASpath";
            this.MIDASpath.Size = new System.Drawing.Size(236, 23);
            this.MIDASpath.TabIndex = 1;
            this.MIDASpath.TextChanged += new System.EventHandler(this.Global_TextChanged);
            this.MIDASpath.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Global_Enter);
            // 
            // Arguments
            // 
            this.Arguments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Arguments.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Arguments.Location = new System.Drawing.Point(14, 29);
            this.Arguments.Margin = new System.Windows.Forms.Padding(2);
            this.Arguments.Name = "Arguments";
            this.Arguments.Size = new System.Drawing.Size(264, 193);
            this.Arguments.TabIndex = 0;
            this.Arguments.Text = "";
            // 
            // MIDASpath_checkBox
            // 
            this.MIDASpath_checkBox.AutoSize = true;
            this.MIDASpath_checkBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MIDASpath_checkBox.Location = new System.Drawing.Point(14, 272);
            this.MIDASpath_checkBox.Margin = new System.Windows.Forms.Padding(2);
            this.MIDASpath_checkBox.Name = "MIDASpath_checkBox";
            this.MIDASpath_checkBox.Size = new System.Drawing.Size(151, 17);
            this.MIDASpath_checkBox.TabIndex = 2;
            this.MIDASpath_checkBox.Text = "Path to MIDAS application:";
            this.MIDASpath_checkBox.UseVisualStyleBackColor = true;
            this.MIDASpath_checkBox.CheckedChanged += new System.EventHandler(this.IsChecked);
            // 
            // Arguments_checkBox
            // 
            this.Arguments_checkBox.AutoSize = true;
            this.Arguments_checkBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Arguments_checkBox.Location = new System.Drawing.Point(14, 10);
            this.Arguments_checkBox.Margin = new System.Windows.Forms.Padding(2);
            this.Arguments_checkBox.Name = "Arguments_checkBox";
            this.Arguments_checkBox.Size = new System.Drawing.Size(116, 17);
            this.Arguments_checkBox.TabIndex = 2;
            this.Arguments_checkBox.Text = "Analysis arguments:";
            this.Arguments_checkBox.UseVisualStyleBackColor = true;
            this.Arguments_checkBox.CheckedChanged += new System.EventHandler(this.IsChecked);
            // 
            // PathDialog
            // 
            this.PathDialog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PathDialog.Location = new System.Drawing.Point(255, 289);
            this.PathDialog.Margin = new System.Windows.Forms.Padding(2);
            this.PathDialog.Name = "PathDialog";
            this.PathDialog.Size = new System.Drawing.Size(22, 22);
            this.PathDialog.TabIndex = 3;
            this.PathDialog.Text = "...";
            this.PathDialog.UseVisualStyleBackColor = true;
            this.PathDialog.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OpenPathDialog);
            // 
            // OK
            // 
            this.OK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OK.Location = new System.Drawing.Point(14, 348);
            this.OK.Margin = new System.Windows.Forms.Padding(2);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(263, 26);
            this.OK.TabIndex = 4;
            this.OK.Text = "OK";
            this.OK.UseVisualStyleBackColor = true;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // WorkingDirectory
            // 
            this.WorkingDirectory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WorkingDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WorkingDirectory.Location = new System.Drawing.Point(14, 245);
            this.WorkingDirectory.Margin = new System.Windows.Forms.Padding(2);
            this.WorkingDirectory.Name = "WorkingDirectory";
            this.WorkingDirectory.Size = new System.Drawing.Size(236, 23);
            this.WorkingDirectory.TabIndex = 1;
            this.WorkingDirectory.TextChanged += new System.EventHandler(this.WD_TextChanged);
            this.WorkingDirectory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.WD_Enter);
            // 
            // WorkingDirectory_checkBox
            // 
            this.WorkingDirectory_checkBox.AutoSize = true;
            this.WorkingDirectory_checkBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WorkingDirectory_checkBox.Location = new System.Drawing.Point(14, 227);
            this.WorkingDirectory_checkBox.Margin = new System.Windows.Forms.Padding(2);
            this.WorkingDirectory_checkBox.Name = "WorkingDirectory_checkBox";
            this.WorkingDirectory_checkBox.Size = new System.Drawing.Size(143, 17);
            this.WorkingDirectory_checkBox.TabIndex = 2;
            this.WorkingDirectory_checkBox.Text = "Path to working directory:";
            this.WorkingDirectory_checkBox.UseVisualStyleBackColor = true;
            this.WorkingDirectory_checkBox.CheckedChanged += new System.EventHandler(this.IsChecked);
            // 
            // DirectoryDialog
            // 
            this.DirectoryDialog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DirectoryDialog.Location = new System.Drawing.Point(255, 245);
            this.DirectoryDialog.Margin = new System.Windows.Forms.Padding(2);
            this.DirectoryDialog.Name = "DirectoryDialog";
            this.DirectoryDialog.Size = new System.Drawing.Size(22, 22);
            this.DirectoryDialog.TabIndex = 3;
            this.DirectoryDialog.Text = "...";
            this.DirectoryDialog.UseVisualStyleBackColor = true;
            this.DirectoryDialog.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OpenDirectoryDialog);
            // 
            // Reset
            // 
            this.Reset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Reset.Location = new System.Drawing.Point(255, 10);
            this.Reset.Margin = new System.Windows.Forms.Padding(2);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(22, 15);
            this.Reset.TabIndex = 4;
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // Pop_up
            // 
            this.Pop_up.AutoSize = true;
            this.Pop_up.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Pop_up.Location = new System.Drawing.Point(14, 317);
            this.Pop_up.Name = "Pop_up";
            this.Pop_up.Size = new System.Drawing.Size(145, 17);
            this.Pop_up.TabIndex = 5;
            this.Pop_up.Text = "Pop-up mode (debugging)";
            this.Pop_up.UseVisualStyleBackColor = true;
            this.Pop_up.CheckedChanged += new System.EventHandler(this.Pop_up_CheckedChanged);
            // 
            // Analysis_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 387);
            this.Controls.Add(this.Pop_up);
            this.Controls.Add(this.OK);
            this.Controls.Add(this.Reset);
            this.Controls.Add(this.DirectoryDialog);
            this.Controls.Add(this.PathDialog);
            this.Controls.Add(this.WorkingDirectory_checkBox);
            this.Controls.Add(this.MIDASpath_checkBox);
            this.Controls.Add(this.Arguments_checkBox);
            this.Controls.Add(this.Arguments);
            this.Controls.Add(this.WorkingDirectory);
            this.Controls.Add(this.MIDASpath);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Analysis_Form";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Analysis settings";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region METHODS
        private void SetToolTip(Control control)
        {
            // Set up the ToolTip text for the Button and Checkbox.
            this.toolTip.SetToolTip(control, "Checked store data only as local value"+ Environment.NewLine+"unchecked store data as global value," +Environment.NewLine+"shared between components");
        }

        private void OpenPathDialog(object sender, MouseEventArgs e)
        {
            OpenFileDialog midasDialog = new OpenFileDialog();
            //midasDialog.Site.Name = "MIDAS global paht";
            midasDialog.Title = "Set global MIDAS path";
            midasDialog.Multiselect = false;
            midasDialog.InitialDirectory = "C:\\MIDAS\\"; //Read from file if exist
            midasDialog.Filter = "MIDAS|midas*.exe|All Files|*.*";
            if (midasDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = midasDialog.FileName; // in full path with midas.exe
                this.MIDASpath.Text = filePath;
            }
        }

        private void OpenDirectoryDialog(object sender, MouseEventArgs e)
        {
            FolderBrowserDialog dir = new FolderBrowserDialog();
            //if (Directory.Exists(WorkingDirectory.Text))
                dir.RootFolder = Environment.SpecialFolder.MyComputer;
            if (dir.ShowDialog() == DialogResult.OK)
            {
                this.WorkingDirectory.Text = dir.SelectedPath;
            }
        }

        private void IsChecked(object sender, EventArgs e)
        {
            this.StoreData();
            switch (((CheckBox)sender).Name)
            {
                case "Arguments_checkBox":
                    component.isLocal_arguments = ((CheckBox)sender).Checked;
                    this.Arguments.Text = component.Arguments;
                    break;
                case "WorkingDirectory_checkBox":
                    component.isLocal_workingDirectory = ((CheckBox)sender).Checked;
                    this.WorkingDirectory.Text = component.WorkingDirectory;
                    break;
                case "MIDASpath_checkBox":
                    component.isLocal_MIDASpath = ((CheckBox)sender).Checked;
                    this.MIDASpath.Text = component.MIDASpath;
                    break;
            }
        }

        private void OK_Click(object sender, EventArgs e)
        {
            if (DirectoryPathExists(WorkingDirectory))
            {
                this.StoreData();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        private void StoreData()
        {
            if (component.isLocal_arguments)
                ((Analysis_Comp)this.component).Arguments = this.Arguments.Text;
            if (component.isLocal_workingDirectory)
                ((Analysis_Comp)this.component).WorkingDirectory = this.WorkingDirectory.Text;
            if (component.isLocal_MIDASpath)
                ((Analysis_Comp)this.component).MIDASpath = this.MIDASpath.Text;
            //WRITTE GLOBAL SETTINGS
            if (File.Exists(component.GlobalSetupFile))
            {
                if (!component.isLocal_arguments)
                    this.EditGlobalSetup("Arguments", this.Arguments.Text);
                if (!component.isLocal_workingDirectory)
                    this.EditGlobalSetup("WorkingDirectory", this.WorkingDirectory.Text); //TODO as enum
                if (!component.isLocal_MIDASpath)
                    this.EditGlobalSetup("MIDASpath", this.MIDASpath.Text);
            }
            else
            {
                this.CreateGlobalSetup();
            }
        }

        private void Reset_Click(object sender, EventArgs e)
        {
            //File.Delete(component.GlobalSetupFile);
            this.Arguments_checkBox.Checked = false;                 
            this.Arguments.Text = component.Arguments_default;
            //this.GlobalPath.Text = component.MidasPath;
            //this.WorkingDirectory.Text = component.WorkingDirectory;
            //this.CreateGlobalSetup();
        }

        private bool CreateGlobalSetup()
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.NewLineChars = "\r\n";
            settings.NewLineHandling = NewLineHandling.Replace;
            settings.Encoding = new UTF8Encoding(false);

            XmlWriter doc = XmlWriter.Create(component.GlobalSetupFile, settings);
            doc.WriteStartDocument();
            //VTK FILE (Open)
            doc.WriteStartElement("DonkeyAnalysisSetup");
            //UNSTRUCTURED GRID (Open)
            doc.WriteStartElement("Arguments");
            doc.WriteString(this.Arguments.Text);
            doc.WriteEndElement();
            doc.WriteStartElement("MIDASpath");
            doc.WriteString(this.MIDASpath.Text);
            doc.WriteEndElement();
            doc.WriteStartElement("WorkingDirectory");
            doc.WriteString(this.WorkingDirectory.Text);
            doc.WriteEndElement();
            doc.WriteEndDocument();
            doc.Close();
            return true;
        }

        private bool EditGlobalSetup(string nodeName, string nodeValue)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(component.GlobalSetupFile);
            XmlNode node = doc.SelectSingleNode("DonkeyAnalysisSetup/"+nodeName);
            if(node!=null)
                node.FirstChild.Value = nodeValue;
            doc.Save(component.GlobalSetupFile);
            return true;
        }

        private void Global_TextChanged(object sender, EventArgs e)
        {
            FilePathExists(MIDASpath);
        }

        private void WD_TextChanged(object sender, EventArgs e)
        {
            DirectoryPathExists(WorkingDirectory);
        }

        private bool FilePathExists(TextBox textBox)
        {
            if (!File.Exists(textBox.Text))
            {
                textBox.ForeColor = Color.Red;
                return false;
            }
            else
            {
                textBox.ForeColor = Color.Black;
                return true;
            }
        }

        private bool DirectoryPathExists(TextBox textBox)
        {
            if (!Directory.Exists(textBox.Text))
            {
                textBox.ForeColor = Color.Red;
                return false;
            }
            else
            {
                textBox.ForeColor = Color.Black;
                return true;
            }
        }
        private void Global_Enter(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Return:
                    e.Handled = true;
                    OK_Click(sender, e);
                    break;
                case Keys.Escape:
                    this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                    this.Close();
                    break;
            }
        }

        private void WD_Enter(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Return:

                    if (DirectoryPathExists(WorkingDirectory))
                    {
                        e.Handled = true;
                        OK_Click(sender, e);
                    }
                    else
                    {
                        Directory.CreateDirectory(WorkingDirectory.Text);
                        WorkingDirectory.ForeColor = Color.Green;
                    }

                    break;
                case Keys.Escape:
                    this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                    this.Close();
                    break;
            }
        }

        private void Pop_up_CheckedChanged(object sender, EventArgs e)
        {
            this.component.PopUp = this.Pop_up.Checked;
        }
        #endregion
    }
}
