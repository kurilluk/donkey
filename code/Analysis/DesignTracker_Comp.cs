﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using Donkey.Model.Analytical;
using Donkey.Model.Resulting;
using System.Windows.Forms;
using System.Drawing;
using Grasshopper.GUI.Canvas;
using System.Windows.Forms.DataVisualization.Charting;
using Grasshopper.Kernel.Attributes;
using Grasshopper.GUI;
using System.Drawing.Drawing2D;

namespace Donkey.Analysis
{
    public class DesignTracker_Comp : GH_Component
    {

        private AnalyticalModel m_model = new AnalyticalModel();
        //private double m_averange = double.NaN;
        //private double m_max = double.NaN;
        //private double m_rate = double.NaN;
        //private string m_values = "NaN";
        //private double m_efficiency = double.NaN;
        public List<Solution> Solutions { get; private set; }
        private List<AnalyticalModel> m_models;
        private double last_penalty = double.NaN;
        private double last_efficiency = double.NaN;
        public string Selected_solution = string.Empty;
        public int solution_index = -1;

        /// <summary>
        /// Initializes a new instance of the MyComponent1 class.
        /// </summary>
        public DesignTracker_Comp()
            : base("DesignTracker", "DesignTracker",
                "Track and evaluate explored design alternatives (beta)",// + Donkey.Info.Mark + " (beta)",
                "Donkey", "3.Analysis")
        {
            this.Solutions = new List<Solution>();
            this.m_models = new List<AnalyticalModel>();
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new AnalyticalModel_Param());
            pManager.AddNumberParameter("efficiency", "\u03B7", "efficiency of profile/material" + Environment.NewLine + "based on von Mises yield criterion divided by equivalent stress", GH_ParamAccess.list);
            //pManager.AddParameter(new ResultModel_Param());
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new AnalyticalElement_Param());
            pManager.AddTextParameter("Evaluation", "E", "Best values", GH_ParamAccess.item);

        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            AnalyticalModel actual_model = new AnalyticalModel();
            if (!DA.GetData<AnalyticalModel>(0, ref actual_model))
                return;
            List<double> criteias = new List<double>();
            if (!DA.GetDataList(1, criteias))
                return;

            //if (criteias[1] != last_penalty && criteias[0] != last_efficiency)
            //{
                if (criteias[1] <= 1)
                {
                    float a = (float)Math.Round(criteias[0], 2);
                    float b = (float)Math.Round(criteias[1], 2);
       
                    Solutions.Add(new Solution(Solutions.Count, a, b));
                    m_models.Add(actual_model);
                }
                //last_penalty = criteias[1];
                //last_efficiency= criteias[0];
           // }
            //TODO if solution is closed to previous.. don't save it (remove duplicates..)

            //Solutions.Add(new Solution(Solutions.Count, 0, 0));
            //Solutions.Add(new Solution(Solutions.Count, 0.5, 0.5));
            //Solutions.Add(new Solution(Solutions.Count, 1, 1));
            //Solutions.Add(new Solution(Solutions.Count, 0.5, 0));
            //Solutions.Add(new Solution(Solutions.Count, 0, 0.5));
            //Solutions.Add(new Solution(Solutions.Count, 1, 0));
            //Solutions.Add(new Solution(Solutions.Count, 0, 1));
            //Solutions.Add(new Solution(Solutions.Count, 0.5, 1));
            //Solutions.Add(new Solution(Solutions.Count, 1, 0.5));
            //Solutions.Add(new Solution(Solutions.Count, 0.0001, 0.0001));

            //if (double.IsNaN(m_efficiency))
            //{
            //    //INIT VALUE
            //    this.m_efficiency = criteias[2];
            //    this.m_model = actual_model;
            //}
            //else
            //{
            //    if (m_efficiency > criteias[2])
            //    {
            //        this.m_model = actual_model;
            //        Message = "+" + Math.Round((m_efficiency - criteias[2]) * 100, 1) + "%";//Better, closer to 1
            //        this.m_efficiency = criteias[2];
            //        DA.SetData(0, null);
            //    }
            //    else
            //    {
            //        Message = "" + Math.Round((m_efficiency - criteias[2]) * 100, 1) + "%";//Inpire by previous model 
            //        DA.SetData(0, m_model);
            //    }

            //}

            //if (double.IsNaN(m_max))
            //{
            //    //CALCULATE INIT RATIO
            //    this.m_max = criteias[1]-1;
            //    if (criteias[0] <= 1)
            //        this.m_averange = 1 - criteias[0];
            //    else
            //        this.m_averange = criteias[0];
            //    if (m_max > 0)
            //        this.m_rate = m_averange + (m_max * 0.5); //ADD DRAWBACK IF MAX IS TO HIGHT
            //    else
            //        m_rate = m_averange;
            //    this.m_model = actual_model;

            //    this.m_values = criteias[0] + "/" + criteias[1];
            //}
            //else 
            //{
            //    //CALCULATE ACTUAL RATIO
            //    double max = criteias[1] - 1;
            //    double avarage;
            //    if (criteias[0] <= 1)
            //        avarage = 1 - criteias[0];
            //    else
            //        avarage = criteias[0];

            //    double rate;
            //    if(max > 0)
            //        rate = avarage + (max*0.5); //DRAWBACK
            //    else
            //        rate = avarage;

            //    double evaluate = Math.Round(m_rate - rate, 3);
            //    if (evaluate >= 0)
            //    {
            //        this.m_model = actual_model;
            //        this.m_rate = rate;
            //        if (evaluate != 0)
            //            Message = "+" + evaluate;//Better, closer to 1
            //        else
            //            Message = "+/- 0"; 
            //        DA.SetData(0, null);
            //        this.m_values = criteias[0] + "/" + criteias[1];
            //    }
            //    else
            //    {
            //        Message = "" + evaluate;//Inpire by previous model 
            //        DA.SetData(0, m_model);
            //    }
            //}

            if(solution_index >=0)
                DA.SetData(0, m_models[solution_index]);
            else
                DA.SetData(0, null);
            DA.SetData(1, this.Selected_solution);

        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Properties.Resources.donkey; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.hidden; } //.obscure; }
        }

        public override void ClearData()
        {
            base.ClearData();
            this.Message = " ";
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{4f44cd4d-044d-405e-a0a9-94eb02354022}"); }
        }

        #region MENU
        public override bool AppendMenuItems(ToolStripDropDown menu)
        {
            this.AppendAdditionalMenuItems(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            this.Menu_AppendPreviewItem(menu);
            this.Menu_AppendEnableItem(menu);
            this.Menu_AppendRuntimeMessages(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            this.Menu_AppendObjectHelp(menu);
            return true;
        }

        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            ToolStripMenuItem button = GH_DocumentObject.Menu_AppendItem(menu, "Reset value", new EventHandler(this.Reset));
        }

        private void Reset(object sender, EventArgs e)
        {
            this.RecordUndoEvent("ChangePreviewMode");
            this.Solutions.Clear();
            this.m_models.Clear();
            this.solution_index = -1;
            this.Selected_solution = "";
            //TODO other staff
            //this.m_efficiency = double.NaN;
            this.ExpireSolution(true);
            //this.ExpirePreview(true);
        }

        //private void ModeChange(object sender, EventArgs e)
        //{
        //    switch(sender.ToString())
        //    {
        //        case "Averange":
        //            this.m_AVERANGEmode = true;
        //            break;
        //        case "Max":
        //            this.m_AVERANGEmode = false;
        //            break;
        //    }
        //    this.RecordUndoEvent("ChangeMode");
        //    this.ExpireSolution(true);
        //}

        #endregion

        public override void CreateAttributes()
        {
            //base.CreateAttributes();
            m_attributes = new DesignTracker_ResizableComp_Attributes(this);
        }

    }

    public class Evaluation_ComponentAttributes : Grasshopper.Kernel.Attributes.GH_ComponentAttributes
    {
        private List<Solution> solutions;
        private Random random = new Random();

        public Evaluation_ComponentAttributes(DesignTracker_Comp own)
            : base(own)
        {
            //this.Bounds = (RectangleF)new Rectangle(0, 0, 150, 150);
            this.solutions = new List<Solution>();
        }

        protected override void Layout()
        {
            this.m_innerBounds = new RectangleF(Pivot.X, Pivot.Y, 300f, 200f);
            GH_ComponentAttributes.LayoutInputParams(base.Owner, this.m_innerBounds);
            GH_ComponentAttributes.LayoutOutputParams(base.Owner, this.m_innerBounds);
            this.Bounds = GH_ComponentAttributes.LayoutBounds(base.Owner, this.m_innerBounds);
            //base.Layout();
            this.solutions.Add(new Solution(solutions.Count, random.NextDouble(), random.NextDouble()));
        }

        protected override void Render(GH_Canvas canvas, Graphics graphics, GH_CanvasChannel channel)
        {
            switch (channel)
            {
                case GH_CanvasChannel.Wires:
                    base.Render(canvas, graphics, channel);
                    break;
                case GH_CanvasChannel.Objects:
                    base.RenderComponentCapsule(canvas, graphics, true, false, false, true, true, false);

                    RectangleF rect = this.Bounds;
                    rect.Inflate(-20f, -20f);

                    foreach (Solution solution in solutions)
                    {
                        solution.MoveSolution(rect, rect.Location, graphics,false);
                    }
                    break;
            }
        }
    }

    public class Evaluation_ResizableAttributes : GH_ResizableAttributes<DesignTracker_Comp>
    {
        private Rectangle m_graph;

        protected override Size MinimumSize
        {
            get
            {
                //float val1 = 0.0f;
                //List<IGH_Param>.Enumerator enumerator;
                //try
                //{
                //    enumerator = this.Owner.Params.Input.GetEnumerator();
                //    while (enumerator.MoveNext())
                //    {
                //        IGH_Param current = enumerator.Current;
                //        val1 = Math.Max(val1, current.Attributes.Bounds.Width);
                //    }
                //}
                //finally
                //{
                //    //enumerator.Dispose();
                //}
                //return new Size(Convert.ToInt32(val1 + 60f), 65);
                return new Size(50, 50);
            }
        }

        protected override Padding SizingBorders
        {
            get
            {
                return new Padding(6);
            }
        }

        public Evaluation_ResizableAttributes(DesignTracker_Comp component)
            : base(component)
        {
            this.Bounds = (RectangleF)new Rectangle(0, 0, 125, 100);
            this.m_graph = new Rectangle();
        }

        public override void AppendToAttributeTree(List<IGH_Attributes> atts)
        {
            base.AppendToAttributeTree(atts);
            IEnumerator<IGH_Param> enumerator = null;
            try
            {
                enumerator = this.Owner.Params.GetEnumerator();
                while (enumerator.MoveNext())
                    enumerator.Current.Attributes.AppendToAttributeTree(atts);
            }
            finally
            {
                if (enumerator != null)
                    enumerator.Dispose();
            }
        }

        public override void ExpireLayout()
        {
            base.ExpireLayout();
            this.Layout();
        }

        protected override void Layout()
        {
            base.Layout();
            RectangleF bounds = this.Bounds;
            bounds.Inflate(-(this.Owner.Params.InputWidth + 4f), -2f);
            //GH_ComponentAttributes.LayoutInputParams((IGH_Component) this.Owner, bounds);
            //bounds.X += (this.Owner.Params.InputWidth + 4f);
           
            GH_ComponentAttributes.LayoutInputParams((IGH_Component)this.Owner, bounds);
            bounds = this.Bounds;
            bounds.Inflate(-(this.Owner.Params.OutputWidth + 4f), -2f);
            GH_ComponentAttributes.LayoutOutputParams((IGH_Component)this.Owner, bounds);

            bounds = this.Bounds;
            float offset = this.Owner.Params.OutputWidth;
            if (this.Owner.Params.InputWidth > offset)
                offset = this.Owner.Params.InputWidth;
            bounds.Inflate(-( offset  + 4f), -6f);
            m_graph = GH_Convert.ToRectangle(bounds);

        }

        protected override void Render(GH_Canvas canvas, Graphics graphics, GH_CanvasChannel channel)
        {
            switch (channel)
            {
                case GH_CanvasChannel.Wires:
                    foreach (IGH_Param input in Owner.Params.Input)
                    {
                        //List<IGH_Param>.Enumerator enumerator1;
                        ////enumerator1 = ;
                        //try
                        //{
                        //    enumerator1 = this.Owner.Params.Input.GetEnumerator();
                        //    while (enumerator1.MoveNext())
                        input.Attributes.RenderToCanvas(canvas, GH_CanvasChannel.Wires);
                        //    break;
                        //}
                        //finally
                        //{
                        //    //enumerator1.Dispose();
                        //}
                    }
                    break;

                case GH_CanvasChannel.Objects:
                    GH_Viewport viewport = canvas.Viewport;
                    // ISSUE: explicit reference operation
                    // ISSUE: variable of a reference type
                    RectangleF rec = this.Bounds;
                    double num1 = 10.0;
                    int num2 = viewport.IsVisible(ref rec, (float)num1) ? 1 : 0;
                    this.Bounds = rec;
                    if (num2 == 0)
                        break;

                    GH_Palette palette = GH_Palette.White;
                    switch (this.Owner.RuntimeMessageLevel)
                    {
                        case GH_RuntimeMessageLevel.Warning:
                            palette = GH_Palette.Warning;
                            break;
                        case GH_RuntimeMessageLevel.Error:
                            palette = GH_Palette.Error;
                            break;
                    }

                    GH_Capsule capsule = GH_Capsule.CreateCapsule(this.Bounds, palette);
                    capsule.SetJaggedEdges(false, false);
                    foreach (IGH_Param input in Owner.Params.Input)
                    {
                        capsule.AddInputGrip(input.Attributes.InputGrip.Y);
                    }
                    foreach (IGH_Param output in Owner.Params.Output)
                    {
                        capsule.AddOutputGrip(output.Attributes.OutputGrip.Y);
                    }

                    //graphics.SmoothingMode = SmoothingMode.HighQuality;
                    capsule.Render(graphics, this.Selected, this.Owner.Locked, true);
                    GH_PaletteStyle impliedStyle = GH_CapsuleRenderEngine.GetImpliedStyle(palette, this.Selected, this.Owner.Locked, this.Owner.Hidden);
                    GH_ComponentAttributes.RenderComponentParameters(canvas, graphics, (IGH_Component)this.Owner, impliedStyle);
                    Grasshopper.GUI.GH_GraphicsUtil.ShadowRectangle(graphics, m_graph, 6, 30);
                    graphics.DrawRectangle(Pens.Black, m_graph);

                    int x = m_graph.Width / 10;
                    int y = m_graph.Height / 10;
                    Grasshopper.GUI.GH_GraphicsUtil.Grid(graphics, (RectangleF)m_graph, x, y);

                    RectangleF rect = this.m_graph;
                    rect.Inflate(-10f, -10f);
                    for (int i = 0; i < Owner.Solutions.Count; i++)
                    {
                        Owner.Solutions[i].MoveSolution(rect, rect.Location, graphics, (i + 1) == Owner.Solutions.Count);
                    }
                    break;
            }
        }

    }

    #region TRASH
    //public class Evaluation_Attributes : Grasshopper.Kernel.Attributes.GH_ResizableAttributes<Evaluation_Comp>
    //{
    //    private GH_Capsule m_capsule;
    //    private List<Solution> solutions;
    //    private Random random = new Random();

    //    public Evaluation_Attributes(Evaluation_Comp owner)
    //        : base(owner)
    //    {
    //        this.Bounds = (RectangleF)new Rectangle(0, 0, 150, 150);
    //        this.solutions = new List<Solution>();
    //        solutions.Add(new Solution(0, 0.5, 0.5));
    //    }

    //    protected override Size MinimumSize
    //    {
    //        get
    //        {
    //            return new Size(50, 50);
    //        }
    //    }

    //    protected override Padding SizingBorders
    //    {
    //        get
    //        {
    //            return new Padding(5);
    //        }
    //    }

    //    public override void ExpireLayout()
    //    {
    //        base.ExpireLayout();
    //        //this.m_capsule.Dispose();
    //        //this.m_capsule = null;
    //        //this.solutions.Clear();
    //    }


    //    protected override void Layout()
    //    {
    //        //base.Layout();
    //        this.Bounds = new RectangleF(this.Pivot, this.Bounds.Size);
    //        //this.Bounds = (RectangleF)GH_Convert.ToRectangle(this.Bounds);
    //        GH_Capsule myCapsule = GH_Capsule.CreateCapsule(this.Bounds, GH_Palette.Hidden, 5, 30);
    //        //myCapsule.SetJaggedEdges(false, true);
    //        myCapsule.AddInputGrip(this.InputGrip.Y);
    //        this.m_capsule = myCapsule;
    //        myCapsule.Dispose();

    //        //LayoutInputParams(this.Owner, this.Bounds);

    //        //this.solutions.Add(new Solution(solutions.Count, random.NextDouble(), random.NextDouble()));

    //        //float x = (float)(random.NextDouble());// * Bounds.Width);//+this.Pivot.X;
    //        //float y = (float)(random.NextDouble());// * Bounds.Height);//+this.Pivot.Y;
    //        //RectangleF rec = new RectangleF(x,y, 30f, 20f);
    //        //Rectangle rectangle = GH_Convert.ToRectangle(rec);
    //        //Chart chart1 = new Chart();
    //        //this.solutions.Add(rec);
    //        //Grasshopper.Kernel.Attributes.GH_ComponentAttributes.LayoutInputParams(base.Owner, this.Bounds);
    //        //this.Bounds = Grasshopper.Kernel.Attributes.GH_ComponentAttributes.LayoutBounds(base.Owner, this.Bounds);
    //        //Grasshopper.Kernel.Attributes.GH_ComponentAttributes.LayoutInputParams(base.Owner, this.Bounds);
    //    }

    //    //protected override void PrepareForRender(GH_Canvas canvas)
    //    //{
    //    //    base.PrepareForRender(canvas);
    //    //    //if (this.m_capsule == null)
    //    //    //{
    //    //    //    this.m_capsule = GH_Capsule.CreateCapsule(this.Bounds, GH_Palette.Hidden, 5, 30);
    //    //    //    this.m_capsule.AddInputGrip(this.InputGrip.Y);
    //    //    //    this.m_capsule.SetJaggedEdges(false, false);
    //    //    //}
    //    //}

    //    protected override void Render(Grasshopper.GUI.Canvas.GH_Canvas canvas, Graphics graphics, Grasshopper.GUI.Canvas.GH_CanvasChannel channel)
    //    {
    //        switch (channel)
    //        {
    //            case GH_CanvasChannel.Wires:
    //                base.Render(canvas, graphics, channel);
    //                //this.RenderIncomingWires(canvas.Painter, (IEnumerable<IGH_Param>)this.Owner.Params.Input,GH_ParamWireDisplay.@default);

    //                break;

    //            case GH_CanvasChannel.Objects:
    //                this.m_capsule.Render(graphics, this.Selected, this.Owner.Locked, true);
    //                m_capsule.Dispose();
    //                //base.Render(canvas, graphics, channel);
    //                //m_capsule.Render(graphics, this.Selected, this.Owner.Locked, true);
    //                //GH_Capsule myCapsule = GH_Capsule.CreateCapsule(this.Bounds, GH_Palette.Hidden, 5, 30);
    //                //myCapsule.Render(graphics, this.Selected, this.Owner.Locked, true);
    //                //myCapsule.SetJaggedEdges(true, true);
    //                //myCapsule.AddInputGrip(this.InputGrip.X);
    //                //myCapsule.Dispose();
    //                //RectangleF rect = this.Bounds;
    //                //rect.Inflate(-6f, -6f);
    //                //Rectangle rectangle = GH_Convert.ToRectangle(rect);
    //                //int x = rectangle.Width / 10;
    //                //int y = rectangle.Height / 10;
    //                //Grasshopper.GUI.GH_GraphicsUtil.Grid(graphics, (RectangleF) rectangle,x,y);
    //                //////graphics.SetClip(rect);
    //                //Grasshopper.GUI.GH_GraphicsUtil.ShadowRectangle(graphics, rectangle, 10, 50);
    //                //graphics.DrawRectangle(Pens.Black, rectangle);

    //            //this.AddSolution(graphics, 10, 10);
    //                //int i = 0;
    //                //foreach (RectangleF rec in solutions)
    //                //{
    //                //    RectangleF rect = rec;
    //                //    rect.X *= this.Bounds.Width;
    //                //    rect.Y *= this.Bounds.Height;
    //                //    rect.X += this.Pivot.X;
    //                //    rect.Y += this.Pivot.Y;
    //                //    rect.X -= rect.Width/2;
    //                //    rect.Y -= rect.Height/2;
    //                //    //graphics.DrawEllipse(Pens.Black, rect);
    //                //    //graphics.FillEllipse(Brushes.White, GH_Convert.ToRectangle(rect));
    //                //    graphics.FillRectangle(Brushes.White, GH_Convert.ToRectangle(rect));
    //                //    graphics.DrawString("" + i, GH_FontServer.Standard, Brushes.Black, rect, Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);
    //                //        i++;
    //                //}

    //                RectangleF rect = this.Bounds;
    //                rect.Inflate(-20f, -20f);

    //                foreach (Solution solution in solutions)
    //                {
    //                    solution.MoveSolution(rect, rect.Location, graphics);
    //                }

    //                break;

    //            default:
    //                base.Render(canvas, graphics, channel);
    //                break;

    //        }
    //    }

    //    public override GH_ObjectResponse RespondToMouseDown(GH_Canvas sender, Grasshopper.GUI.GH_CanvasMouseEvent e)
    //    {
    //        if (e.Button == MouseButtons.Left)
    //        {
    //            if (solutions[0].pointerField.Contains(e.CanvasLocation))
    //            {
    //                solutions[0].selected = true;
    //                this.ExpireLayout();
    //                sender.Refresh();
    //                return GH_ObjectResponse.Handled;
    //            }
    //            else { solutions[0].selected = false; }
    //        }
    //        return base.RespondToMouseDown(sender, e);
    //    }

    //    #region ADDITIONAL FUNKCTIONALITY

    //    //public static void LayoutInputParams(IGH_Component owner, RectangleF componentBox)
    //    //{
    //    //    int count = owner.Params.Input.Count;
    //    //    if (count == 0)
    //    //        return;
    //    //    int val1_1 = 0;
    //    //    List<IGH_Param>.Enumerator enumerator;
    //    //    try
    //    //    {
    //    //        enumerator = owner.Params.Input.GetEnumerator();
    //    //        while (enumerator.MoveNext())
    //    //        {
    //    //            IGH_Param current = enumerator.Current;
    //    //            val1_1 = Math.Max(val1_1, GH_FontServer.StringWidth(current.NickName, GH_FontServer.Standard));
    //    //        }
    //    //    }
    //    //    finally
    //    //    {
    //    //        //enumerator.Dispose();
    //    //    }
    //    //    int num1 = Math.Max(val1_1 + 6, 12);
    //    //    float num2 = componentBox.Height / (float)count;
    //    //    int num3 = 0;
    //    //    int num4 = count - 1;
    //    //    for (int index = num3; index <= num4; ++index)
    //    //    {
    //    //        IGH_Param ghParam = owner.Params.Input[index];
    //    //        if (ghParam.Attributes == null)
    //    //            ghParam.Attributes = (IGH_Attributes)new GH_LinkedParamAttributes(ghParam, owner.Attributes);
    //    //        float x = componentBox.X - (float)num1;
    //    //        float y = componentBox.Y + (float)index * num2;
    //    //        float width = (float)(num1 - 3);
    //    //        float height = num2;
    //    //        ghParam.Attributes.Pivot = new PointF(x + 0.5f * (float)num1, y + 0.5f * num2);
    //    //        ghParam.Attributes.Bounds = (RectangleF)GH_Convert.ToRectangle(new RectangleF(x, y, width, height));
    //    //    }
    //    //    bool flag = false;
    //    //    //int num5 = 0;
    //    //    //int num6 = count - 1;
    //    //    //for (int index = num5; index <= num6; ++index)
    //    //    //{
    //    //    //    IGH_Param ghParam = owner.Params.Input[index];
    //    //    //    GH_LinkedParamAttributes linkedParamAttributes = (GH_LinkedParamAttributes)ghParam.Attributes;
    //    //    //    linkedParamAttributes.m_renderTags = ghParam.StateTags;
    //    //    //    if (linkedParamAttributes.m_renderTags.Count == 0)
    //    //    //        linkedParamAttributes.m_renderTags = (GH_StateTagList)null;
    //    //    //    if (linkedParamAttributes.m_renderTags != null)
    //    //    //    {
    //    //    //        flag = true;
    //    //    //        Rectangle box = GH_Convert.ToRectangle(linkedParamAttributes.Bounds);
    //    //    //        linkedParamAttributes.m_renderTags.Layout(box, GH_StateTagLayoutDirection.Left);
    //    //    //        box = linkedParamAttributes.m_renderTags.BoundingBox;
    //    //    //        if (!box.IsEmpty)
    //    //    //            linkedParamAttributes.Bounds = RectangleF.Union(linkedParamAttributes.Bounds, (RectangleF)box);
    //    //    //    }
    //    //    //}
    //    //    if (!flag)
    //    //        return;
    //    //    float val1_2 = float.MaxValue;
    //    //    int num7 = 0;
    //    //    int num8 = count - 1;
    //    //    for (int index = num7; index <= num8; ++index)
    //    //    {
    //    //        IGH_Attributes attributes = owner.Params.Input[index].Attributes;
    //    //        val1_2 = Math.Min(val1_2, attributes.Bounds.X);
    //    //    }
    //    //    int num9 = 0;
    //    //    int num10 = count - 1;
    //    //    for (int index = num9; index <= num10; ++index)
    //    //    {
    //    //        IGH_Attributes attributes = owner.Params.Input[index].Attributes;
    //    //        RectangleF bounds = attributes.Bounds;
    //    //        bounds.Width = bounds.Right - val1_2;
    //    //        bounds.X = val1_2;
    //    //        attributes.Bounds = bounds;
    //    //    }
    //    //}

    //    //public override GH_ObjectResponse RespondToMouseDown(GH_Canvas sender, Grasshopper.GUI.GH_CanvasMouseEvent e)
    //    //{

    //    //                    if (solutions[0].Contains(e.CanvasLocation))
    //    //                    {
    //    //                        switch (GH_ParameterSide.Input)
    //    //                        {
    //    //                            case GH_ParameterSide.Input:
    //    //                                if (base.Owner.Params.Input[0] .CanRemoveParameter(GH_ParameterSide.Input, current.Index))
    //    //                                {
    //    //                                    Guid id = this.Owner.RecordUndoEvent("Remove Parameter");
    //    //                                    if (this.m_varParamCast.DestroyParameter(GH_ParameterSide.Input, current.Index))
    //    //                                    {
    //    //                                        if (current.Index == this.Owner.MasterParameterIndex)
    //    //                                            this.Owner.MasterParameterIndex = -1;
    //    //                                        else if (current.Index < this.Owner.MasterParameterIndex)
    //    //                                        {
    //    //                                            IGH_Component owner = this.Owner;
    //    //                                            owner.MasterParameterIndex = owner.MasterParameterIndex - 1;
    //    //                                        }
    //    //                                        this.Owner.Params.UnregisterInputParameter(this.Owner.Params.Input[current.Index]);
    //    //                                        this.m_varParamCast.VariableParameterMaintenance();
    //    //                                        this.Owner.Params.OnParametersChanged();
    //    //                                        this.Owner.ExpireSolution(true);
    //    //                                        break;
    //    //                                    }
    //    //                                    else
    //    //                                    {
    //    //                                        GH_Document ghDocument = this.Owner.OnPingDocument();
    //    //                                        if (ghDocument != null)
    //    //                                        {
    //    //                                            ghDocument.UndoServer.RemoveRecord(id);
    //    //                                            break;
    //    //                                        }
    //    //                                        else
    //    //                                            break;
    //    //                                    }
    //    //                                }
    //    //                                else
    //    //                                    break;

    //    //                        }
    //    //                        return GH_ObjectResponse.Handled;
    //    //                    }

    //    //        List<GH_ComponentAttributes.InsertParamRegion>.Enumerator enumerator2;
    //    //        if (this.m_insertRegions != null)
    //    //        {
    //    //            try
    //    //            {
    //    //                enumerator2 = this.m_insertRegions.GetEnumerator();
    //    //                while (enumerator2.MoveNext())
    //    //                {
    //    //                    GH_ComponentAttributes.InsertParamRegion current = enumerator2.Current;
    //    //                    if (current.Contains(e.CanvasLocation))
    //    //                    {
    //    //                        switch (current.Side)
    //    //                        {
    //    //                            case GH_ParameterSide.Input:
    //    //                                if (this.m_varParamCast.CanInsertParameter(GH_ParameterSide.Input, current.Index))
    //    //                                {
    //    //                                    Guid id = this.Owner.RecordUndoEvent("Insert Parameter");
    //    //                                    IGH_Param parameter = this.m_varParamCast.CreateParameter(GH_ParameterSide.Input, current.Index);
    //    //                                    if (parameter != null)
    //    //                                    {
    //    //                                        if (this.Owner.MasterParameterIndex >= current.Index)
    //    //                                        {
    //    //                                            IGH_Component owner = this.Owner;
    //    //                                            owner.MasterParameterIndex = owner.MasterParameterIndex + 1;
    //    //                                        }
    //    //                                        parameter.WireDisplay = this.ImpliedParameterWireStyle(current.Side, current.Index);
    //    //                                        this.Owner.Params.RegisterInputParam(parameter, current.Index);
    //    //                                        this.m_varParamCast.VariableParameterMaintenance();
    //    //                                        this.Owner.Params.OnParametersChanged();
    //    //                                        this.Owner.ExpireSolution(true);
    //    //                                        break;
    //    //                                    }
    //    //                                    else
    //    //                                    {
    //    //                                        GH_Document ghDocument = this.Owner.OnPingDocument();
    //    //                                        if (ghDocument != null)
    //    //                                        {
    //    //                                            ghDocument.UndoServer.RemoveRecord(id);
    //    //                                            break;
    //    //                                        }
    //    //                                        else
    //    //                                            break;
    //    //                                    }
    //    //                                }
    //    //                                else
    //    //                                    break;
    //    //                            case GH_ParameterSide.Output:
    //    //                                if (this.m_varParamCast.CanInsertParameter(GH_ParameterSide.Output, current.Index))
    //    //                                {
    //    //                                    Guid id = this.Owner.RecordUndoEvent("Insert Parameter");
    //    //                                    IGH_Param parameter = this.m_varParamCast.CreateParameter(GH_ParameterSide.Output, current.Index);
    //    //                                    if (parameter != null)
    //    //                                    {
    //    //                                        parameter.WireDisplay = this.ImpliedParameterWireStyle(current.Side, current.Index);
    //    //                                        this.Owner.RecordUndoEvent("Insert Parameter");
    //    //                                        this.Owner.Params.RegisterOutputParam(parameter, current.Index);
    //    //                                        this.m_varParamCast.VariableParameterMaintenance();
    //    //                                        this.Owner.Params.OnParametersChanged();
    //    //                                        this.Owner.ExpireSolution(true);
    //    //                                        break;
    //    //                                    }
    //    //                                    else
    //    //                                    {
    //    //                                        GH_Document ghDocument = this.Owner.OnPingDocument();
    //    //                                        if (ghDocument != null)
    //    //                                        {
    //    //                                            ghDocument.UndoServer.RemoveRecord(id);
    //    //                                            break;
    //    //                                        }
    //    //                                        else
    //    //                                            break;
    //    //                                    }
    //    //                                }
    //    //                                else
    //    //                                    break;
    //    //                        }
    //    //                        return GH_ObjectResponse.Handled;
    //    //                    }
    //    //                }
    //    //            }
    //    //            finally
    //    //            {
    //    //                enumerator2.Dispose();
    //    //            }
    //    //        }
    //    //    }
    //    //    return base.RespondToMouseDown(sender, e);
    //    //}

    //    //private GH_ParamWireDisplay ImpliedParameterWireStyle(GH_ParameterSide side, int index)
    //    //{
    //    //    int val2 = side != GH_ParameterSide.Input ? this.Owner.Params.Output.Count - 1 : this.Owner.Params.Input.Count - 1;
    //    //    if (val2 < 0)
    //    //        return GH_ParamWireDisplay.@default;
    //    //    int val1_1 = index;
    //    //    int val1_2 = index - 1;
    //    //    int val1_3 = Math.Max(val1_1, 0);
    //    //    int val1_4 = Math.Max(val1_2, 0);
    //    //    int index1 = Math.Min(val1_3, val2);
    //    //    int index2 = Math.Min(val1_4, val2);
    //    //    IGH_Param ghParam1;
    //    //    IGH_Param ghParam2;
    //    //    if (side == GH_ParameterSide.Input)
    //    //    {
    //    //        ghParam1 = this.Owner.Params.Input[index1];
    //    //        ghParam2 = this.Owner.Params.Input[index2];
    //    //    }
    //    //    else
    //    //    {
    //    //        ghParam1 = this.Owner.Params.Output[index1];
    //    //        ghParam2 = this.Owner.Params.Output[index2];
    //    //    }
    //    //    switch (ghParam1.WireDisplay)
    //    //    {
    //    //        case GH_ParamWireDisplay.faint:
    //    //            return GH_ParamWireDisplay.faint;
    //    //        case GH_ParamWireDisplay.hidden:
    //    //            return GH_ParamWireDisplay.hidden;
    //    //        default:
    //    //            switch (ghParam2.WireDisplay)
    //    //            {
    //    //                case GH_ParamWireDisplay.faint:
    //    //                    return GH_ParamWireDisplay.faint;
    //    //                case GH_ParamWireDisplay.hidden:
    //    //                    return GH_ParamWireDisplay.hidden;
    //    //                default:
    //    //                    return GH_ParamWireDisplay.@default;
    //    //            }
    //    //    }
    //    //}

    //    //private class InsertParamRegion
    //    //{
    //    //    public const float RadiusMin = 1.5f;
    //    //    public const float RadiusMax = 3f;
    //    //    private GH_ComponentAttributes m_attributes;
    //    //    private RectangleF m_bounds;
    //    //    private GH_ParameterSide m_side;
    //    //    private int m_index;

    //    //    public GH_ComponentAttributes Attributes
    //    //    {
    //    //        get
    //    //        {
    //    //            return this.m_attributes;
    //    //        }
    //    //    }

    //    //    public IGH_Component Component
    //    //    {
    //    //        get
    //    //        {
    //    //            return this.m_attributes.Owner;
    //    //        }
    //    //    }

    //    //    public RectangleF Bounds
    //    //    {
    //    //        get
    //    //        {
    //    //            return this.m_bounds;
    //    //        }
    //    //        set
    //    //        {
    //    //            this.m_bounds = value;
    //    //        }
    //    //    }

    //    //    public int Index
    //    //    {
    //    //        get
    //    //        {
    //    //            return this.m_index;
    //    //        }
    //    //        set
    //    //        {
    //    //            this.m_index = value;
    //    //        }
    //    //    }

    //    //    public GH_ParameterSide Side
    //    //    {
    //    //        get
    //    //        {
    //    //            return this.m_side;
    //    //        }
    //    //        set
    //    //        {
    //    //            this.m_side = value;
    //    //        }
    //    //    }

    //    //    public InsertParamRegion(GH_ComponentAttributes owner, GH_ParameterSide side, int index, PointF grip)
    //    //    {
    //    //        this.m_attributes = owner;
    //    //        this.m_index = index;
    //    //        this.m_side = side;
    //    //        this.m_bounds = new RectangleF(grip.X - 3f, grip.Y - 3f, 6f, 6f);
    //    //    }

    //    //    public virtual bool Contains(PointF pt)
    //    //    {
    //    //        if (!this.Bounds.Contains(pt))
    //    //            return false;
    //    //        double num1 = 0.5;
    //    //        double num2 = (double)this.Bounds.Left;
    //    //        RectangleF bounds = this.Bounds;
    //    //        double num3 = (double)bounds.Right;
    //    //        double num4 = num2 + num3;
    //    //        float x = (float)(num1 * num4);
    //    //        double num5 = 0.5;
    //    //        bounds = this.Bounds;
    //    //        double num6 = (double)bounds.Top + (double)this.Bounds.Bottom;
    //    //        float y = (float)(num5 * num6);
    //    //        return (double)GH_GraphicsUtil.Distance(pt, new PointF(x, y)) <= 3.0;
    //    //    }

    //    //    public virtual void Render(Graphics graphics, PointF cursor, int alpha)
    //    //    {
    //    //        if (this.Contains(cursor))
    //    //        {
    //    //            RectangleF bounds = this.Bounds;
    //    //            float top = bounds.Top + 0.5f;
    //    //            bounds = this.Bounds;
    //    //            float bottom = bounds.Bottom - 0.5f;
    //    //            float left;
    //    //            float right;
    //    //            switch (this.Side)
    //    //            {
    //    //                case GH_ParameterSide.Input:
    //    //                    bounds = this.Attributes.Bounds;
    //    //                    left = bounds.Left + 2f;
    //    //                    bounds = this.Bounds;
    //    //                    right = bounds.Left - 1f;
    //    //                    break;
    //    //                case GH_ParameterSide.Output:
    //    //                    bounds = this.Bounds;
    //    //                    left = bounds.Right + 1f;
    //    //                    bounds = this.Attributes.Bounds;
    //    //                    right = bounds.Right - 2f;
    //    //                    break;
    //    //            }
    //    //            RectangleF rec = RectangleF.FromLTRB(left, top, right, bottom);
    //    //            HatchBrush hatchBrush = new HatchBrush(HatchStyle.Percent50, Color.Black, Color.Transparent);
    //    //            //if (this.Index == 0)
    //    //            //    graphics.FillPolygon((Brush)hatchBrush, GH_ComponentAttributes.InsertBarTop(rec));
    //    //            //else if (this.Side == GH_ParameterSide.Input && this.Index == this.Attributes.Owner.Params.Input.Count)
    //    //            //    graphics.FillPolygon((Brush)hatchBrush, GH_ComponentAttributes.InsertBarBottom(rec));
    //    //            //else if (this.Side == GH_ParameterSide.Output && this.Index == this.Attributes.Owner.Params.Output.Count)
    //    //            //    graphics.FillPolygon((Brush)hatchBrush, GH_ComponentAttributes.InsertBarBottom(rec));
    //    //            //else
    //    //            //    graphics.FillPolygon((Brush)hatchBrush, GH_ComponentAttributes.InsertBarMiddle(rec));
    //    //            //hatchBrush.Dispose();
    //    //        }
    //    //        //GH_ComponentAttributes.RenderZUIIcon(graphics, this.Bounds, cursor, alpha, GH_ComponentAttributes.ZuiIcon.Insert);
    //    //    }
    //    //}

    //    //private class InsertParamRegion
    //    //{
    //    //    public const float RadiusMin = 1.5f;
    //    //    public const float RadiusMax = 3f;
    //    //    private GH_ComponentAttributes m_attributes;
    //    //    private RectangleF m_bounds;
    //    //    private GH_ParameterSide m_side;
    //    //    private int m_index;

    //    //    public GH_ComponentAttributes Attributes
    //    //    {
    //    //        get
    //    //        {
    //    //            return this.m_attributes;
    //    //        }
    //    //    }

    //    //    public IGH_Component Component
    //    //    {
    //    //        get
    //    //        {
    //    //            return this.m_attributes.Owner;
    //    //        }
    //    //    }

    //    //    public RectangleF Bounds
    //    //    {
    //    //        get
    //    //        {
    //    //            return this.m_bounds;
    //    //        }
    //    //        set
    //    //        {
    //    //            this.m_bounds = value;
    //    //        }
    //    //    }

    //    //    public int Index
    //    //    {
    //    //        get
    //    //        {
    //    //            return this.m_index;
    //    //        }
    //    //        set
    //    //        {
    //    //            this.m_index = value;
    //    //        }
    //    //    }

    //    //    public GH_ParameterSide Side
    //    //    {
    //    //        get
    //    //        {
    //    //            return this.m_side;
    //    //        }
    //    //        set
    //    //        {
    //    //            this.m_side = value;
    //    //        }
    //    //    }

    //    //    public InsertParamRegion(GH_ComponentAttributes owner, GH_ParameterSide side, int index, PointF grip)
    //    //    {
    //    //        this.m_attributes = owner;
    //    //        this.m_index = index;
    //    //        this.m_side = side;
    //    //        this.m_bounds = new RectangleF(grip.X - 3f, grip.Y - 3f, 6f, 6f);
    //    //    }

    //    //    public virtual bool Contains(PointF pt)
    //    //    {
    //    //        if (!this.Bounds.Contains(pt))
    //    //            return false;
    //    //        double num1 = 0.5;
    //    //        double num2 = (double)this.Bounds.Left;
    //    //        RectangleF bounds = this.Bounds;
    //    //        double num3 = (double)bounds.Right;
    //    //        double num4 = num2 + num3;
    //    //        float x = (float)(num1 * num4);
    //    //        double num5 = 0.5;
    //    //        bounds = this.Bounds;
    //    //        double num6 = (double)bounds.Top + (double)this.Bounds.Bottom;
    //    //        float y = (float)(num5 * num6);
    //    //        return (double)GH_GraphicsUtil.Distance(pt, new PointF(x, y)) <= 3.0;
    //    //    }

    //    //    public virtual void Render(Graphics graphics, PointF cursor, int alpha)
    //    //    {
    //    //        if (this.Contains(cursor))
    //    //        {
    //    //            RectangleF bounds = this.Bounds;
    //    //            float top = bounds.Top + 0.5f;
    //    //            bounds = this.Bounds;
    //    //            float bottom = bounds.Bottom - 0.5f;
    //    //            float left;
    //    //            float right;
    //    //            switch (this.Side)
    //    //            {
    //    //                case GH_ParameterSide.Input:
    //    //                    bounds = this.Attributes.Bounds;
    //    //                    left = bounds.Left + 2f;
    //    //                    bounds = this.Bounds;
    //    //                    right = bounds.Left - 1f;
    //    //                    break;
    //    //                case GH_ParameterSide.Output:
    //    //                    bounds = this.Bounds;
    //    //                    left = bounds.Right + 1f;
    //    //                    bounds = this.Attributes.Bounds;
    //    //                    right = bounds.Right - 2f;
    //    //                    break;
    //    //            }
    //    //            RectangleF rec = RectangleF.FromLTRB(left, top, right, bottom);
    //    //            HatchBrush hatchBrush = new HatchBrush(HatchStyle.Percent50, Color.Black, Color.Transparent);
    //    //            if (this.Index == 0)
    //    //                graphics.FillPolygon((Brush)hatchBrush, GH_ComponentAttributes.InsertBarTop(rec));
    //    //            else if (this.Side == GH_ParameterSide.Input && this.Index == this.Attributes.Owner.Params.Input.Count)
    //    //                graphics.FillPolygon((Brush)hatchBrush, GH_ComponentAttributes.InsertBarBottom(rec));
    //    //            else if (this.Side == GH_ParameterSide.Output && this.Index == this.Attributes.Owner.Params.Output.Count)
    //    //                graphics.FillPolygon((Brush)hatchBrush, GH_ComponentAttributes.InsertBarBottom(rec));
    //    //            else
    //    //                graphics.FillPolygon((Brush)hatchBrush, GH_ComponentAttributes.InsertBarMiddle(rec));
    //    //            hatchBrush.Dispose();
    //    //        }
    //    //        GH_ComponentAttributes.RenderZUIIcon(graphics, this.Bounds, cursor, alpha, GH_ComponentAttributes.ZuiIcon.Insert);
    //    //    }
    //    //}

    //    //private class RemoveParamRegion : GH_ComponentAttributes.InsertParamRegion
    //    //{
    //    //    private IGH_Param m_param;

    //    //    public IGH_Param Param
    //    //    {
    //    //        get
    //    //        {
    //    //            return this.m_param;
    //    //        }
    //    //    }

    //    //    public RemoveParamRegion(GH_ComponentAttributes owner, GH_ParameterSide side, int index, PointF grip, IGH_Param param)
    //    //        : base(owner, side, index, grip)
    //    //    {
    //    //        this.m_param = param;
    //    //    }

    //    //    public override void Render(Graphics graphics, PointF cursor, int alpha)
    //    //    {
    //    //        Color.FromArgb(100, 0, 0);
    //    //        Color.FromArgb(220, 20, 50);
    //    //        Color white = Color.White;
    //    //        if (this.Contains(cursor))
    //    //        {
    //    //            RectangleF bounds = this.Param.Attributes.Bounds;
    //    //            bounds.Inflate(-1f, -2f);
    //    //            RectangleF rect = bounds;
    //    //            rect.Inflate(-1f, -1f);
    //    //            HatchBrush hatchBrush = new HatchBrush(HatchStyle.Percent50, Color.Black, Color.Transparent);
    //    //            GraphicsPath path = new GraphicsPath();
    //    //            path.AddRectangle(bounds);
    //    //            path.AddRectangle(rect);
    //    //            graphics.FillPath((Brush)hatchBrush, path);
    //    //            path.Dispose();
    //    //            hatchBrush.Dispose();
    //    //        }
    //    //        GH_ComponentAttributes.RenderZUIIcon(graphics, this.Bounds, cursor, alpha, GH_ComponentAttributes.ZuiIcon.Remove);
    //    //    }
    //    //}

    //    #endregion
    //}

    //public class Solution
    //{
    //    #region FIELD
    //    public int index;
    //    RectangleF textField;
    //    public RectangleF pointerField;
    //    float size = 30f;
    //    double X_value;
    //    double Y_value;
    //    public bool selected = false;
    //    #endregion

    //    #region CONSTRUCTOR
    //    public Solution(int index, double x_value, double y_value)
    //    {
    //        this.index = index;
    //        this.X_value = x_value;
    //        this.Y_value = y_value;
    //        textField = new RectangleF(0, 0, size, 10f);
    //        pointerField = new RectangleF(0, 0, size, size);
    //    }
    //    #endregion

    //    #region METHODS
    //    public void MoveSolution(RectangleF Bounds, PointF Pivot, Graphics graphics)
    //    {
    //        //MOVE
    //        //COPY RECTANGLE
    //        RectangleF text = textField;
    //        RectangleF pointer = pointerField;
    //        //RECALCULATE POSITION IN COMPONENT
    //        float X = (float)(Bounds.Width * X_value);
    //        float Y = (float)(Bounds.Height * Y_value);
    //        //MOVE SOLUTION WITH COMPONENT
    //        X += Pivot.X;
    //        Y += Pivot.Y;
    //        //MORE FRAMES
    //        text.X = X;
    //        text.Y = Y;
    //        pointer.X = X;
    //        pointer.Y = Y;

    //        //CENTER THE SOLUTION ON THE POSITION
    //        text.X -= text.Width / 2;
    //        text.Y -= text.Height / 2;
    //        pointer.X -= pointer.Width / 2;
    //        pointer.Y -= pointer.Height / 2;

    //        pointerField = pointer;

    //        //RENDER
    //        if(selected)
    //            graphics.FillEllipse(Brushes.Black, GH_Convert.ToRectangle(pointer));
    //        else
    //            graphics.FillEllipse(Brushes.Silver, GH_Convert.ToRectangle(pointer));
    //        graphics.DrawString("" + index, GH_FontServer.Standard, Brushes.White, text, Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);
    //    }

    //    public void RenderSolution(Graphics graphics)
    //    {

    //    }
    //    #endregion

    //}
    #endregion
}