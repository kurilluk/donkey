﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Donkey.Model.Geometry;
using Rhino.Geometry;
using Donkey.Extensions;

namespace Donkey.Analysis
{
    public class ReaderBuffer
    {
        //POINTS
        public DataList<Node> Points { get; set; }
        //POINTS DATA
        public List<Vector3d> Displacement { get; set; }
        public List<Vector3d> ReactionForce { get; set; }
        //CELLS
        public List<int> Connectivity { get; set; }
        public List<int> Offsets { get; set; }
        public List<int> Types { get; set; }
        //CELL DATA
        public List<int> Element_ID { get; set; }
        public List<int> Geometry_ID { get; set; }
        public List<double> RelativeStress { get; set; }
        public List<double> RealStress { get; set; }
        //public List<double> BeamStress_N { get; set;} 
        public List<double> BeamStress_N_1 { get; set; }  // Start point
        public List<double> BeamStress_N_2{ get; set; }  // End point 
        //public List<double> BeamStress_V { get; set; }
        //public List<double> BeamStress_M { get; set; }
        public List<Vector3d> BeamStress_M1_xyz { get; set; }
        public List<Vector3d> BeamStress_M2_xyz { get; set; }
        //MODEL DATA
        public double Stability { get; set; }
        public double Volume { get; set; }

        //public ReaderBuffer()
        //{ this.Stability = 0; }

        public List<double> Calculate_N1_N2_Average()
        {
            List<double> Beam_AxialForce_N = new List<double>(BeamStress_N_1.Count);

            for (int i = 0; i < BeamStress_N_1.Count; i++)
            {
                Beam_AxialForce_N.Add((BeamStress_N_1[i] + BeamStress_N_2[i]) / 2);
            }

            return Beam_AxialForce_N;
        }
    }
}
