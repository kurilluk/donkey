﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using Grasshopper.GUI;
using Grasshopper.GUI.Canvas;
using Grasshopper.Kernel.Attributes;
using Donkey.DesignTracking;
using Donkey.Model.Resulting;
using Grasshopper.Kernel;

namespace Donkey.Analysis
{
    public class Analysis_Att : GH_ComponentAttributes
    {
        #region FIELD
        private RectangleF info;
        private RectangleF percente;
        private GH_Capsule m_capsule;
        #endregion
        //public Analysis_Att(Analysis_Comp owner)
        //    : base(owner)
        //{
        //    info = new RectangleF();
        //    percente = new RectangleF();
        //}
  
        public Analysis_Att(I_DS_Comp component) : base(component)
        {
            info = new RectangleF();
            percente = new RectangleF();
        }

        public override bool HasInputGrip { get { return true; } }
        public override bool HasOutputGrip { get { return true; } }

        #region LAYOUT
        protected override void Layout()
        {
            info = new RectangleF(Owner.Attributes.Pivot.X + 27F, Owner.Attributes.Pivot.Y + 20, 50F, 12F);
            percente = new RectangleF(Owner.Attributes.Pivot.X + 26F, Owner.Attributes.Pivot.Y + 5, 50F, 20F);
            m_innerBounds = new RectangleF(Owner.Attributes.Pivot.X - 0.35F, Owner.Attributes.Pivot.Y - 0.5f, 80F, 40F);

            m_capsule = GH_Capsule.CreateCapsule(this.Bounds, GH_Palette.Hidden, 5, 30);

            GH_ComponentAttributes.LayoutInputParams(base.Owner, this.m_innerBounds);
            GH_ComponentAttributes.LayoutOutputParams(base.Owner, this.m_innerBounds);
            this.Bounds = GH_ComponentAttributes.LayoutBounds(base.Owner, this.m_innerBounds);
        }
        #endregion

        #region RENDER
        protected override void Render(GH_Canvas canvas, Graphics graphics, GH_CanvasChannel channel)
        {
            switch (channel)
            {
                case GH_CanvasChannel.Wires:
                    base.Render(canvas, graphics, channel);
                    break;

                case GH_CanvasChannel.Objects:

                    //GH_Viewport viewport = canvas.Viewport;
                    //RectangleF bounds = this.Bounds;
                    //// ISSUE: explicit reference operation
                    //// ISSUE: variable of a reference type
                    //RectangleF rec = bounds;
                    //float num1 = 10.0F;
                    //Boolean visible = viewport.IsVisible(ref rec, num1);
                    //this.Bounds = bounds;
                    //if (!visible)
                    //    break;

                    base.RenderComponentCapsule(canvas, graphics, true, false, false, true, true, true);            

                    //GH_Palette palette = GH_Palette.Normal;
                    //// Adjust palette based on the Owner's worst case messaging level.
                    //switch (Owner.RuntimeMessageLevel)
                    //{
                    //    case GH_RuntimeMessageLevel.Warning:
                    //        palette = GH_Palette.Warning;
                    //        break;

                    //    case GH_RuntimeMessageLevel.Error:
                    //        palette = GH_Palette.Error;
                    //        break;
                    //}
                    //// Create a new Capsule without text or icon.
                    //GH_Capsule capsule = GH_Capsule.CreateCapsule(Bounds, palette);

                    //// Render the capsule using the current Selection, Locked and Hidden states.
                    //// Integer parameters are always hidden since they cannot be drawn in the viewport.
                    //capsule.Render(graphics, Selected, Owner.Locked, true);

                    //// Always dispose of a GH_Capsule when you're done with it.
                    //capsule.Dispose();
                    //capsule = null;

                    //bool hidden = true;
                    //if (this.Owner is IGH_PreviewObject)
                    //    hidden = ((IGH_PreviewObject)this.Owner).Hidden;

                    if (GH_Canvas.ZoomFadeLow == 0)
                        return;
                   // if(GH_Attributes<IGH_Param>.IsIconMode(this.Owner.IconDisplayMode))
                        graphics.DrawImage(Owner.Icon_24x24, Pivot.X, Pivot.Y + 5F, 24F, 24F);

                    StringFormat format = new StringFormat();
                    format.Alignment = StringAlignment.Near;
                    format.LineAlignment = StringAlignment.Center;
                    format.Trimming = StringTrimming.EllipsisCharacter;

                    //GH_Capsule ghCapsule = GH_Capsule.CreateCapsule(this.Bounds,
                    //    GH_CapsuleRenderEngine.GetImpliedPalette((IGH_ActiveObject)this.Owner));

                    //if (this.HasInputGrip)
                    //    ghCapsule.AddInputGrip(this.InputGrip.Y);
                    //if (this.HasOutputGrip)
                    //    ghCapsule.AddOutputGrip(this.OutputGrip.Y);

                    //ghCapsule.Render(graphics, this.Selected, false, hidden);
                    //ghCapsule.RenderEngine.RenderIcon(graphics, (Image)this.Owner.Icon_24x24, (RectangleF)Bounds, 0, 1);


                    //PointF pt1 = new PointF(Pivot.X + 26, Pivot.Y - 2.5F);
                    //PointF pt2 = new PointF(Pivot.X + 26, Pivot.Y + 32.5F);
                    //graphics.DrawLine(Pens.Black, pt1, pt2);

                    if (((I_DS_Comp)this.Owner).m_model == null)
                    {
                        graphics.DrawString("NaN%",
                                   Grasshopper.Kernel.GH_FontServer.Large,
                                   Brushes.Red,
                                   percente,
                                   format);
                        graphics.DrawString("NaN%",
                                    Grasshopper.Kernel.GH_FontServer.Small,
                                    Brushes.Red,
                                    info,
                                    format);
                        format.Dispose();
                        break;
                    }

                    //Color color;
                    //if (((Analysis_Comp)this.Owner).m_model.Overloading == 0)
                    //    color = ResultModel.Gradient.ColourAt(((Analysis_Comp)this.Owner).m_model.Efficiency);
                    //else
                    //    color = ResultModel.Gradient.ColourAt(1 + ((Analysis_Comp)this.Owner).m_model.Overloading);
                    //SolidBrush sb = new SolidBrush(color);

                    double modelOverloading = ((I_DS_Comp)this.Owner).m_model.Overloading;
                    if (modelOverloading == 0)
                    {
                        graphics.DrawString("" + ((I_DS_Comp)this.Owner).m_model.Efficiency * 100 + "%",
                                   Grasshopper.Kernel.GH_FontServer.Large,
                                   Brushes.Green,
                                   percente,
                                   format);
                        graphics.DrawString("- - -",
                                    Grasshopper.Kernel.GH_FontServer.Small,
                                    Brushes.Green,
                                    info,
                                    format);
                        //Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);
                    }
                    else
                    {
                        graphics.DrawString("" + ((I_DS_Comp)this.Owner).m_model.Efficiency * 100 + "%",
                                       Grasshopper.Kernel.GH_FontServer.Large,
                                       Brushes.Red,
                                       percente,
                                       format);
                        graphics.DrawString(modelOverloading * 100 + "%",
                                    Grasshopper.Kernel.GH_FontServer.Small,
                                    Brushes.Red,
                                    info,
                                    format);
                    }
                    format.Dispose();
                    break;
            }
        }
        #endregion

        #region DOUBLE-CLICK
        public override GH_ObjectResponse RespondToMouseDoubleClick(GH_Canvas sender, GH_CanvasMouseEvent e)
        {
            I_DS_Comp owner = this.Owner as I_DS_Comp;
            if (owner == null)
                return base.RespondToMouseDoubleClick(sender, e);
            if (owner.m_form == null)
            {
                owner.TrackDesing = true;
                owner.m_form = new Tracker_Form(owner);
                GH_WindowsFormUtil.CenterFormOnCursor((Form)owner.m_form, true);
                ((Control)owner.m_form).Show();
                if (owner.FieldOfSolutions != null && owner.FieldOfSolutions.Count == 0)
                {
                    owner.m_form.AddSolution(1, owner.m_model, true);
                    owner.FieldOfSolutions.Add(owner.m_model);
                }
            }
            else
            {
                owner.m_form.Show();
                owner.m_form.WindowState = FormWindowState.Normal;
                owner.m_form.BringToFront();
                owner.m_form.Select();
            }
            return GH_ObjectResponse.Handled;
        }

        //public override GH_ObjectResponse RespondToKeyDown(GH_Canvas sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.P)
        //    {
        //        ((Analysis_Comp)Owner).PopUp = !((Analysis_Comp)Owner).PopUp;
        //        Owner.ExpireSolution(true);
        //    }
        //    return GH_ObjectResponse.Handled;
        //}
        #endregion

    }
}
