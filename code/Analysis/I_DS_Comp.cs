﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Donkey.DesignTracking;
using Donkey.Model.Resulting;
using Grasshopper.Kernel;

namespace Donkey.Analysis
{
    public interface I_DS_Comp : IGH_Component
    {
        ResultModel m_model { get; set; }
        Tracker_Form m_form { get; set; }
        bool TrackDesing { get; set; }
        List<ResultModel> FieldOfSolutions { get; set; }
        //I_DS_Comp Owner { get; set; }
        bool no_analysis { get; set; } //EXPLORING PHASE - selecting different solutions (possibility to duplicate exploring window :) , visualize more models)
    }
}
