﻿using Grasshopper.Kernel;
using System;
using System.Runtime.InteropServices;     // DLL support
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Donkey.Analysis
{
    class test_MIDAS_dll : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the test_MIDAS_dll class.
        /// </summary>
        public test_MIDAS_dll()
            : base("test_MIDAS_dll", "Nickname",
                "Description",
                "Donkey", "3.Analysis")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddNumberParameter("N", "N", "N", GH_ParamAccess.item);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.Register_StringParam("S", "S", "S", GH_ParamAccess.item);
        }

        //[DllImport("TestLib.dll")]
        //public static extern void DisplayHelloFromDLL();

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            Console.WriteLine("This is C# program console message");
            Debug.WriteLine("This is Debug message and correction of it");
            //DisplayHelloFromDLL();
            string output = System.Console.ReadLine();

            DA.SetData(0, output);
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Donkey.Properties.Resources.donkey_5s;
            }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{3e942534-b72a-4413-9766-0c7139948846}"); }
        }
    }
}
