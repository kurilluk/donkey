﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel;
using System.Drawing;
using Grasshopper.Kernel.Data;
using Grasshopper.GUI;

namespace Donkey.Extensions
{
    class MidasPath_Param : GH_PersistentParam<GH_String>
    {
        #region CONSTRUCTOR

        public MidasPath_Param()
            : base(new Grasshopper.Kernel.GH_InstanceDescription("MIDAS", "P", "Optional local path to MIDAS application" + Environment.NewLine + "(have a more priority than global path)", "Donkey", "IO")) { }

        ////constructor 1 overload        
        //public GH_ParamFolder(GH_InstanceDescription nTag) : base(nTag){}

        ////constructor 2 overload      
        //public GH_ParamFolder(GH_InstanceDescription nTag, GH_ParamAccess access) : base(nTag, access) { }

        #endregion

        #region PARAM

        protected override GH_String InstantiateT()
        {
            return new GH_String();
        }

        protected override ToolStripMenuItem Menu_CustomMultiValueItem()
        {
            return new ToolStripMenuItem("MIDAS Path...", Donkey.Properties.Resources.donkey, new EventHandler(this.Menu_FolderBrowser));
        }

        //protected override ToolStripMenuItem Menu_CustomSingleValueItem()
        //{
        //    return base.Menu_CustomSingleValueItem();
        //}


        private void Menu_FolderBrowser(object sender, EventArgs e)
        {
            OpenFileDialog midasDialog = new OpenFileDialog();
            midasDialog.Multiselect = false;
            midasDialog.InitialDirectory = "C:\\MIDAS\\"; //Read from file if exist
            midasDialog.Filter = "MIDAS|midas*.exe|All Files|*.*";
            if (midasDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = midasDialog.FileName; // in full path with midas.exe

                this.RecordPersistentDataEvent("Set Folder Path");
                //base.m_data_persistent.Clear();
                //base.m_data_persistent.Add(new GH_String(dir.SelectedPath+ "\\"));
                //this.m_data.Clear();
                //this.m_data.Append(new GH_String(filePath));
                this.PersistentData.Clear();
                this.PersistentData.Insert(new GH_String(filePath), new GH_Path(0),0);
                //this.SetValue("MIDASpath", filePath);
                //this.ValuesChanged();
                //this.CollectData();
                this.ExpireSolution(true);
            }

        }
        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {
            base.AppendAdditionalMenuItems(menu);
            //this.Menu_AppendWireDisplay(menu);
            //this.Menu_AppendDisconnectWires(menu);
            //this.Menu_AppendReverseParameter(menu);
            //this.Menu_AppendFlattenParameter(menu);
            //this.Menu_AppendGraftParameter(menu);
            //this.Menu_AppendSimplifyParameter(menu);
            //GH_DocumentObject.Menu_AppendSeparator(menu);

            //if (this.Kind != GH_ParamKind.output)
            //{
            //    ToolStripMenuItem itemCustomSingle = this.Menu_CustomSingleValueItem();
            //    if (itemCustomSingle == null)
            //    {
            //        this.Menu_AppendPromptOne(menu);
            //    }
            //    else
            //    {
            //        itemCustomSingle.Enabled = this.SourceCount == 0;
            //        menu.Items.Add(itemCustomSingle);
            //    }
                //ToolStripMenuItem itemCustomPlural = this.Menu_CustomMultiValueItem();
                //if (itemCustomPlural == null)
                //{
                //    this.Menu_AppendPromptMore(menu);
                //}
                //else
                //{
                //    itemCustomPlural.Enabled = this.SourceCount == 0;
                //    menu.Items.Add(itemCustomPlural);
                //}
                //this.Menu_AppendManageCollection(menu);
                //GH_DocumentObject.Menu_AppendSeparator(menu);
                //this.Menu_AppendDestroyPersistent(menu);
                //this.Menu_AppendInternaliseData(menu);
                //this.Menu_AppendExtractParameter(menu);
            //}
        }




        //registrate own guid number
        public override Guid ComponentGuid
        {
            get { return new Guid("5116a4be-47c8-45a3-a5c0-c31551e73491"); }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.donkey; }
        }

        #endregion

        protected override GH_String PreferredCast(object data)
        {
            if (data is string)
            {
                return new GH_String((string)data);
            }
            return null;
        }

        protected override GH_GetterResult Prompt_Singular(ref GH_String value)
        {
            GH_GetterResult ghGetterResult = new GH_GetterResult();
            return ghGetterResult;
        }

        protected override GH_GetterResult Prompt_Plural(ref List<GH_String> value)
        {
            GH_GetterResult ghGetterResult = new GH_GetterResult();
            return ghGetterResult;
        }

        protected override ToolStripMenuItem Menu_CustomSingleValueItem()
        {
            string text = "";
            if (this.PersistentDataCount == 1)
            {
                GH_String ghString = this.PersistentData.get_FirstItem(false);
                if (ghString != null)
                    text = ghString.ToString();
            }
            if (text == null)
                text = "";
            ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem(string.Format("Set {0}", (object)this.TypeName));
            GH_DocumentObject.Menu_AppendTextItem(toolStripMenuItem.DropDown, text, new GH_MenuTextBox.KeyDownEventHandler(this.Menu_SingleStringValueKeyDown), (GH_MenuTextBox.TextChangedEventHandler)null, true, 200, true);
            return toolStripMenuItem;
        }

        protected void Menu_SingleStringValueKeyDown(GH_MenuTextBox sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Return:
                    e.Handled = true;
                    this.PersistentData.Clear();
                    string text = sender.Text;
                    if (text.Length > 0)
                        this.PersistentData.Append(new GH_String(text));
                    if (Control.ModifierKeys == Keys.Shift || Control.ModifierKeys == Keys.Control)
                        sender.CloseEntireMenuStructure();
                    this.ExpireSolution(true);
                    break;
                case Keys.Escape:
                    sender.CloseEntireMenuStructure();
                    break;
            }
        }
    }
}
