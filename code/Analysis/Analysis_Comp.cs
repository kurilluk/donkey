﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel.Data;
using Rhino.Geometry;
using Grasshopper;
using Grasshopper.GUI;
using Donkey.Model;
using Donkey.Extensions;
using Donkey.Model.Resulting;
using Donkey.Model.Analytical;
using System.Windows.Forms;
using Grasshopper.Kernel.Attributes;
using Grasshopper.GUI.Canvas;
using System.Xml;
using System.IO;
using Donkey.Model.Resulting.Interpretation;
using Rhino;
using Rhino.DocObjects;
using Donkey.DesignTracking;


namespace Donkey.Analysis
{
    public class Analysis_Comp : GH_Component, I_DS_Comp
    {
        #region FIELD
        //ANALYSIS PART
        private Stopwatch timer;
        private String name;
        private bool m_popup;
        public string GlobalSetupFile;
        private string m_arguments_local;
        public string Arguments_default { get; private set; }
        public bool isLocal_arguments;
        private string m_workingDirectory;
        public bool isLocal_workingDirectory;
        private string m_MIDASpath;
        public bool isLocal_MIDASpath;

        //EVALUATION PART
        public ResultModel m_model { get; set; }
        private bool m_previewValue;
        private bool m_design_tracking;
        private double m_prev_Overloading = double.NaN;
        private double m_prev_Efficiency = double.NaN;
        public Tracker_Form m_form { get; set; }
        public List<ResultModel> FieldOfSolutions { get; set; }
        public bool no_analysis { get; set; }
        #endregion

        #region CONSTRUCTOR
        /// <summary>
        /// Initializes a new instance of the Analysis_Comp class.
        /// </summary>
        public Analysis_Comp()
            : base("Analysis", "Analysis",
                "Structural analysis [v0.82]",
                "Donkey", "3.Analysis")
        {
            //ANALYSIS LOCAL/DEFAULT SETUP
            this.m_popup = false;
            this.GlobalSetupFile = createGlobalSetupFile();
            //LOAD default arguments
            this.Arguments_default = "-IN_meshGen_elemSize 500.0\n-IN_meshGen_elemCount 10\n-P_RigidBodyToRigidArm\n" +
    "-P_solverOOFEM -\n-P_dw\n//-P_stability\n-IO_ff vtu\n-OUT_print_CSusage\n-OUT_moBFN -\n-OUT_print_model_parent\n" +
    "-OUT_print_property\n//-OUT_args"; //-P_stability , -OUT_args
            this.m_arguments_local = Arguments_default;
            this.isLocal_arguments = false;

            this.m_workingDirectory = "C:\\MIDAS\\TEMP";
            this.isLocal_workingDirectory = false;

            this.m_MIDASpath = "C:\\MIDAS\\midas.exe";
            this.isLocal_MIDASpath = false;

            //EVAULATION PART
            this.m_previewValue = false;
            this.m_design_tracking = false;
            this.FieldOfSolutions = new List<ResultModel>();
            this.no_analysis = false;
            this.m_model = (ResultModel)null;
        }

        private string createGlobalSetupFile()
        {
            string path =Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Donkey";
                if(!Directory.Exists(path))
                    Directory.CreateDirectory(path);

            return path + "\\setup.xml";
        }

        #endregion

        #region PROPERTY
        public string MIDASpath
        {
            get 
            {
                if(!this.isLocal_MIDASpath)
                {
                    if(File.Exists(this.GlobalSetupFile))
                        return this.ReadGlobalSetup("MIDASpath"); 

                }
                //if file don't exists return default value
                return this.m_MIDASpath;
            }
            set { this.m_MIDASpath = value;}

        }

        public string WorkingDirectory 
        {
            get 
            {
                if (!this.isLocal_workingDirectory)
                {
                    //read global value if global setup exists
                    if (File.Exists(this.GlobalSetupFile))
                        return this.ReadGlobalSetup("WorkingDirectory");
                }
                //if is local return local value
                return this.m_workingDirectory;
            }
            set { this.m_workingDirectory = value; }
        }

        public string Arguments
        {
            get
            {
                //if is not local
                if(!this.isLocal_arguments)
                {
                    //read global value if global setup exists
                    if (File.Exists(this.GlobalSetupFile))
                    {
                        return this.ReadGlobalSetup("Arguments");
                    }
                    // if global setup don't exists return default value
                    else { return this.Arguments_default; }
                }
                //if is local return local value
                return this.m_arguments_local;
            }
            set
            {
                this.m_arguments_local = value;
            }
        }

        public bool PopUp
        {
            get { return m_popup; }
            set
            {
                    this.RecordUndoEvent("pop-up");
                    this.m_popup = value;
                    this.ExpirePreview(true);
            }
        }

        public bool TrackDesing
        {
            get { return m_design_tracking; }
            set 
            { 
                this.m_design_tracking = value;
                this.ExpirePreview(true);
            }
        }

        #endregion

        #region COMPONENT PARAM
        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new AnalyticalModel_Param());
        }

        private string FileName()
        {
            string date = String.Empty;
            date += (DateTime.Today.Year - 2000).ToString();
            date += (DateTime.Today.Month < 10) ? "0" + DateTime.Today.Month.ToString() : DateTime.Today.Month.ToString();
            date += (DateTime.Today.Day < 10) ? "0" + DateTime.Today.Day.ToString() : DateTime.Today.Day.ToString();
            return date;
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new ResultModel_Param());
        }
        #endregion

        #region SOLVE INSTANCE
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            this.ClearRuntimeMessages();

            if (PopUp)
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Remark, "Pop-up mode is running.");

            //READ INPUT PARAMS
            AnalyticalModel model = new AnalyticalModel();
            if (!DA.GetData(0, ref model))
                return;
            this.name = model.Name;

            //IF ONLY EXPLORE RESULTS - WITHOUT STRUCTURAL ANALYSIS RUN
            if (!no_analysis)
            {
                //RUN ANALYSIS
                m_model = RunAnalysis(model);
                //IF SUCESS AND DESIGN TRACKING IS ON - STORE SOLUTION
                if (m_model != null && m_design_tracking)
                {
                    this.FieldOfSolutions.Add(m_model);
                    //ALSO ADD SOLUTION TO OPEN WINDOW
                    if (this.m_form != null)
                        this.m_form.AddSolution(FieldOfSolutions.Count, m_model, true);
                }
                DA.SetData(0, m_model);
                //COMPARE PREVIOUS
                if (m_model != null)
                    ComparePrevious();
            }
            else
            {
                DA.SetData(0, m_model);
                no_analysis = false;
            }
        }
        #endregion

        #region OVERRIDE READ/WRITE
        public override IEnumerable<string> Keywords
        {
            get
            {
                return new string[] { "FEM", "analysis" };
            }
        }
        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetString("analysis_arguments", this.m_arguments_local);
            writer.SetBoolean("analysis_IsLocal_arguments", this.isLocal_arguments);
            writer.SetBoolean("analysis_IsLocal_MIDASpath", this.isLocal_MIDASpath);
            writer.SetBoolean("analysis_IsLocal_WorkingDirectory", this.isLocal_workingDirectory);
            writer.SetString("analysis_workingDirectory", this.WorkingDirectory);
            writer.SetString("analysis_MIDASpath", this.m_MIDASpath);
            writer.SetBoolean("analysis_popup", this.m_popup);
            writer.SetBoolean("preview_data", this.m_previewValue);
            return base.Write(writer);
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            this.m_arguments_local = reader.GetString("analysis_arguments");
            this.isLocal_arguments = reader.GetBoolean("analysis_IsLocal_arguments");
            if (reader.ItemExists("analysis_IsLocal_MIDASpath"))
                this.isLocal_MIDASpath = reader.GetBoolean("analysis_IsLocal_MIDASpath");
            if (reader.ItemExists("analysis_IsLocal_WorkingDirectory"))
                this.isLocal_workingDirectory = reader.GetBoolean("analysis_IsLocal_WorkingDirectory");
            if (reader.ItemExists("analysis_MIDASpath"))
                this.m_MIDASpath = reader.GetString("analysis_MIDASpath");
            this.WorkingDirectory = reader.GetString("analysis_workingDirectory");
            this.m_popup = reader.GetBoolean("analysis_popup");
            this.m_previewValue = reader.GetBoolean("preview_data");
            return base.Read(reader);
        }

        public override void ClearData()
        {
            if (!no_analysis)
            {
                if (this.m_model != null)
                {
                    this.m_model = null;
                }
                this.Message = " ";
            }

            base.ClearData();
        }
        #endregion

        #region PREVIEW
        public override void DrawViewportWires(IGH_PreviewArgs args)
        {
            if (m_model != null)
            {
                //ResultModel model = new ResultModel(m_model);
                m_model.DrawGeometryColour(args);
                if (this.m_previewValue)
                {
                    args.Display.DepthMode = Rhino.Display.DepthMode.AlwaysInFront;
                    m_model.DrawElementValue(args);
                    args.Display.DepthMode = Rhino.Display.DepthMode.Neutral;
                }
            }
        }
        #endregion

        #region BAKE

        public override void BakeGeometry(RhinoDoc doc, List<Guid> obj_ids)
        {
            ObjectAttributes att = Rhino.RhinoDoc.ActiveDoc.CreateDefaultAttributes();
            att.ColorSource = ObjectColorSource.ColorFromObject;
            this.BakeGeometry(doc, att, obj_ids);
        }

        public override void BakeGeometry(RhinoDoc doc, ObjectAttributes att, List<Guid> obj_ids)
        {
            att.ColorSource = ObjectColorSource.ColorFromObject;
            att.PlotColorSource = ObjectPlotColorSource.PlotColorFromObject;
            if (m_model != null && m_model.Geometries.Count > 0)
            {
                for (int e = 0; e < m_model.Elements.Count; e++)
                {
                    List<Guid> baked_objects = new List<Guid>();
                    List<int> indexes = m_model.Elements[e].Geometries_ID;
                    for (int i = 0; i < indexes.Count; i++)
                    {
                        Color color = ResultModel.Gradient.ColourAt(m_model.Geometries_Stress_Relative[indexes[i]]);
                        att.PlotColor = color;
                        att.ObjectColor = color;
                        att.Name = "\u03B7: " + Math.Round(m_model.Geometries_Stress_Relative[indexes[i]], 3) * 100 + "%";
                        baked_objects.Add(m_model.Geometries[indexes[i]].Bake_Geometry(doc, att));
                    }
                    doc.Groups.AddToGroup(e, baked_objects);
                }
            }
        }
        #endregion

        #region METHODS
        //COMPARE TO PREVIOUS ONE
        private void ComparePrevious()
        {
            double[] values = new double[2] { this.m_model.Efficiency, this.m_model.Overloading };
            if (!double.IsNaN(m_prev_Efficiency))
            {
                StringBuilder sb = new StringBuilder();
                double dif_eff = Math.Round((m_prev_Efficiency - values[0]) * 100, 1);
                if (dif_eff > 0)
                    sb.Append("" + -dif_eff + "%");
                else if (dif_eff == 0)
                    sb.Append("+/- 0%");
                else
                    sb.Append("+" + -dif_eff + "%");

                this.Message = sb.ToString();
            }
            this.m_prev_Overloading = values[1];
            this.m_prev_Efficiency = values[0];
        }

        //RUN ANALYSIS
        private ResultModel RunAnalysis(AnalyticalModel model)
        {
            VTU_Reader reader = new VTU_Reader();

            //CHECK WORKING DIRECTORY
            if (!Directory.Exists(this.WorkingDirectory))
            {
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Error,
                    "Working directory is not correctly set, please change the path in component settings");
                return null;
            }

            //WRITE MODEL TO VTU FILE
            VTU_Writer file = new VTU_Writer();
            file.Write(model, this.WorkingDirectory + "\\" + name + ".model.vtu"); //TODO static method..?

            //WHY HERE? WHAT DOES IT MEAN
            Rhino.Runtime.HostUtils.DisplayOleAlerts(false);

            //CREATE ANALYSIS COMAND AND RUN ANALYSIS AS PROCESS
            Analysis ap = new Analysis();
            //SETUP ANALYSIS PROCESS AND READ RESULT FILE
            string[] arguments_list = Arguments.Split('\n');
            StringBuilder sb = new StringBuilder();
            foreach (string argument in arguments_list)
            {
                if (!argument.StartsWith("//"))
                    sb.Append(" " + argument);
            }
            string resultFile = ap.Setup(MIDASpath, this.WorkingDirectory,
                name, m_popup, this, sb.ToString()); //Arguments.Replace("\n", " ")

            //RUN ANALYSIS PROCESS
            timer = Stopwatch.StartNew();
            ap.Run();
            //WAIT FOR ANALYSIS FIHISH
            //ap.Analysis.WaitForExit();
            bool ESC_down;
            int loop = 0;
            int wait = 50;
            do
            {
                ESC_down = GH_Document.IsEscapeKeyDown();
                //Debug.Print("ESC ="+ ESC_down);
                if (ESC_down)
                {
                    Print("Analysis was aborted!");
                    this.AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Calculation was aborted!");
                    this.timer.Stop();
                    ap.FEA.Kill();
                    //DA.DisableGapLogic();          
                    return null;
                }
                loop++;
                Print("Analysing, please wait... " + timer.ElapsedMilliseconds + " ms");

            }
            while (!ap.FEA.WaitForExit(wait));
            Print("Command", false);
            timer.Stop();
            //Print("Analysis done in " + timer.ElapsedMilliseconds + " ms. [" + name +" "+ DateTime.Now.ToString("h:mm:ss")+"]", true);
            Print("Analysis of " + name + " done in " + timer.ElapsedMilliseconds + " ms.", true);
            //IF ANALYSIS WAS SUCCESSFUL
            //possible to use ap.Analysis.ExitCode with out delegate and else..
            if (ap.FEA.ExitCode == -1073741510 || ap.FEA.ExitCode == 0)
            {
                //RETURN RESULT MODEL
                ResultModel resultModel = reader.Read(resultFile);
                return resultModel;

            }
            else
            {
                //WHEN ANALYSIS WAS UNSUCCESSFUL
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Analysis calculation false!");
                return null;
            }
 
        }

        //PRINT STATUS OF ANALYSIS
        private void Print(string message)
        {
            Print(message, false);
        }

        private void Print(string message, bool write)
        {
            if (write)
                Rhino.RhinoApp.WriteLine(message);
            else
            {
                Rhino.RhinoApp.SetCommandPrompt(message);
                Rhino.RhinoApp.Wait();
            }

        }

        //READ XML FILE FROM LOCAL DRIVE
        private string ReadGlobalSetup(string valueName)
        {
            string read = "";
            try
            {
                XmlReader doc = XmlReader.Create(this.GlobalSetupFile);
                while (doc.Read())
                    if (doc.Name == valueName)
                    {
                        read = doc.ReadElementString();
                    }
                doc.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return read;
        }
        
        #endregion
        
        #region MENU

        //REMOVE POSSIBILITY TO CHANGE NAME - NICKNAME OF THE COMPONENT
        public override bool AppendMenuItems(ToolStripDropDown menu)
        {
            this.Menu_AppendPreviewItem(menu);
            this.Menu_AppendEnableItem(menu);
            this.Menu_AppendBakeItem(menu);
            this.Menu_AppendRuntimeMessages(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            this.AppendAdditionalMenuItems(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            this.Menu_AppendObjectHelp(menu);
            return true;
        }

        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {          
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Preview value", this.PreviewValues, true, this.m_previewValue)
                .ToolTipText = "Preview evaluation valuses for each element.";
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Design tracking", this.TrackDesign, true, this.m_design_tracking)
                .ToolTipText = "Store and track designed solutions in order to compare them.";
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Settings...", this.Menu_Settings, true, false)
                .ToolTipText = "Show analysis settings.";  

        }

        //OPEN DESIGN TRACKING WINDOW AND START TO TRACK DESING
        private void TrackDesign(object sender, EventArgs e)
        {
            //this.RecordUndoEvent("TrackingDesing");
            this.m_design_tracking = !m_design_tracking;
            if (m_design_tracking)
            {
                if (this.m_form == null)
                {
                    this.m_form = new Tracker_Form(this);
                    //this.m_form.Owner = owner;
                    GH_WindowsFormUtil.CenterFormOnCursor((Form)this.m_form, true);
                    ((Control)this.m_form).Show();
                    if (this.FieldOfSolutions != null && this.FieldOfSolutions.Count == 0)
                    {
                        this.m_form.AddSolution(1, m_model, true);
                        this.FieldOfSolutions.Add(m_model);
                    }
                }
                else
                    this.m_form.Select();
            }

            if (m_form != null)
            {
                m_form.tracking.Checked = this.m_design_tracking;
                m_form.Refresh();
            }
        }

        //PREVIEW VALUES -- TODO improve
        private void PreviewValues(object sender, EventArgs e)
        {
            this.RecordUndoEvent("ChangePreviewMode");
            this.m_previewValue = !m_previewValue;
            this.ExpirePreview(true);
        }

        //OPEN SETTINGS WINDOW
        private void Menu_Settings(object sender, EventArgs e)
        {
            this.TriggerAutoSave();
            Analysis_Form numberSliderPopup = new Analysis_Form(this);
            GH_WindowsFormUtil.CenterFormOnCursor((Form)numberSliderPopup, true);
            if (numberSliderPopup.ShowDialog((IWin32Window)Instances.DocumentEditor) != DialogResult.OK)
                return;
            this.ExpireSolution(true);
        }

        #endregion

        #region COMPONENT SETUP
        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.evaluate; }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{6C265D1B-7D62-45AA-9548-92EC5641E1A6}"); }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.hidden; }
        }

        public override bool IsPreviewCapable { get { return true; } }
        public override bool IsBakeCapable { get { return true; } }

        public override void CreateAttributes()
        {
            m_attributes = new Analysis_Att(this);
        }

        #endregion
    }

}
