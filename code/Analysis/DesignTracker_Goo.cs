﻿using Donkey.Model.Resulting;
using Grasshopper.Kernel.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Donkey.Analysis
{
    public class DesignTracker_Goo : GH_Goo<GH_Integer>
    {
        #region FIELD
        #endregion

        #region CONSTRUCTOR
        #endregion

        #region OVERRIDE
        public override IGH_Goo Duplicate()
        {
            return new ResultModel(); ;
        }

        public override bool IsValid
        {
            get { return true; }
        }

        public override string ToString()
        {
            return "Results model print";
        }

        public override string TypeDescription
        {
            get { return "Results model print"; }
        }

        public override string TypeName
        {
            get { return "Evaluation model type"; }
        }
        #endregion

        //public override bool CastFrom(object source)
        //{
        //    ResultModel model = source as ResultModel;
        //    if (model == null)
        //        return false;
        //    else
        //        this.Value = model;
        //    return true;
        //}
    }
}
