﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Donkey.Extensions;
using Donkey.Model.Nodal;
using Donkey.Model;
using Rhino.Geometry;
using Donkey.Interface;
using Donkey.Model.Analytical;
using Donkey.Model.Geometry;

namespace Donkey.Analysis
{
    public class VTU_Writer
    {
        #region FIELD
        private XmlWriter m_doc;
        private StringBuilder m_connectivity, m_offsets, m_types;
        private StringBuilder m_crossSection_ID, m_material_ID, m_deadWeight_ID, m_areaLoad_ID, m_support_ID;
        private StringBuilder m_LCS_vector, m_ElementProperty;
        private StringBuilder m_hinge, m_nodalLoad, m_support;
        private StringBuilder m_dummy;
        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        public VTU_Writer() { }
        #endregion

        #region IFile_Writer Members
        //MAIN WRITE METHOD
        public void Write(AnalyticalModel model, string filePath)
        {
            m_doc = XmlWriter.Create(filePath, Settings());
            m_doc.WriteStartDocument();
            //VTK FILE (Open)
            m_doc.WriteStartElement("VTKFile");
            m_doc.WriteAttributeString("type", "UnstructuredGrid");
            m_doc.WriteAttributeString("version", "0.1");
            m_doc.WriteAttributeString("byte_order", "LittleEndian");
            //UNSTRUCTURED GRID (Open)
            m_doc.WriteStartElement("UnstructuredGrid");
            //PIECE (Open)
            m_doc.WriteStartElement("Piece");
            m_doc.WriteAttributeString("NumberOfPoints", model.Nodes.Count.ToString());
            m_doc.WriteAttributeString("NumberOfCells", model.Geometries.Count.ToString());
            //POINTS
            m_doc.WriteStartElement("Points");
            m_doc.WriteStartElement("DataArray");
            m_doc.WriteAttributeString("type", "Float32");
            m_doc.WriteAttributeString("NumberOfComponents", "3");
            m_doc.WriteAttributeString("format", "ascii");
            WritePoints(model.Nodes);
            m_doc.WriteEndElement();
            m_doc.WriteEndElement();
            //CELLS
            InitCells(model);
            m_doc.WriteStartElement("Cells");
            WriteDataArray("Int32", "connectivity", "ascii", m_connectivity);
            WriteDataArray("Int32", "offsets", "ascii", m_offsets);
            WriteDataArray("UInt8", "types", "ascii", m_types);
            m_doc.WriteEndElement();
            //POINT DATA
            InitPointData(model.Nodes);
            m_doc.WriteStartElement("PointData");
                //UNUSED m_doc.WriteAttributeString("Scalars", "Boundary_Conditions");
            WriteDataArray("ID_Boundary_Condition", m_nodalLoad);
            WriteDataArray("ID_SET_IDS_Prescribed_Values", m_support); //NODAL DOF
            WriteDataArray("Full_Hinge", m_hinge);
            m_doc.WriteEndElement();
            //CELL DATA
            m_doc.WriteStartElement("CellData");
                //UNUSED m_doc.WriteAttributeString("Tensors", "Rstress");
            WriteDataArray("Virtual", m_dummy);
            WriteDataArray("ID_Material", m_material_ID);
            WriteDataArray("ID_Cross-Section", m_crossSection_ID);
            WriteDataArray("ID_Boundary_Condition", m_deadWeight_ID);
            WriteDataArray("ID_Boundary_Condition", m_areaLoad_ID);
            WriteDataArray("ID_SET_IDS_Prescribed_Values", m_support_ID); //NODAL DOF
            WriteDataArray("Property", m_ElementProperty);
            WriteDataArray("LCS_xz_vector", m_LCS_vector, "Float32", 3);
            m_doc.WriteEndElement();
            //PIECE (Close)
            m_doc.WriteEndElement();
            //UNSTRUCTURED GRID (Close)
            m_doc.WriteEndElement();

            //APPENDED DATA (Open)
            m_doc.WriteStartElement("AppendedData");
            m_doc.WriteRaw("_");
            //CHARAKTERISTICS (Open)
            m_doc.WriteStartElement("Characteristics");
            //ADD COMMENT HERE(?)
            m_doc.WriteStartElement("PROBLEM_TYPE_DOF");
            m_doc.WriteAttributeString("Number", "1");
            m_doc.WriteStartElement("item");
            m_doc.WriteString("3dRot");
            m_doc.WriteEndElement();
            m_doc.WriteEndElement();
            //NODAL DOF - SUPPORT
            m_doc.WriteRaw(Environment.NewLine);
            m_doc.WriteStartElement("LIST_PRESCRIBED_VALUES");
            m_doc.WriteAttributeString("Number", model.Supports.Count.ToString());
            WriteAppendedDataItems(model.Supports);
            m_doc.WriteEndElement();
            //CROSS SECTION
            m_doc.WriteRaw(Environment.NewLine);
            m_doc.WriteStartElement("LIST_CROSS-SECTIONS");
            //model.Profiles.RemoveAt(0);
            m_doc.WriteAttributeString("Number", model.Profiles.Count.ToString());
            WriteAppendedDataItems(model.Profiles);
            m_doc.WriteEndElement();
            //MATERIALS
            m_doc.WriteRaw(Environment.NewLine);
            m_doc.WriteStartElement("LIST_MATERIALS");
            //model.Materials.RemoveAt(0);
            m_doc.WriteAttributeString("Number", model.Materials.Count.ToString());
            WriteAppendedDataItems(model.Materials);
            //m_doc.WriteRaw(Environment.NewLine);
            m_doc.WriteEndElement();
            //BOUNDARY CONDITIONS
            m_doc.WriteRaw(Environment.NewLine);
            m_doc.WriteStartElement("LIST_BOUNDARY_CONDITIONS");
            m_doc.WriteAttributeString("Number", model.Loads.Count.ToString());
            WriteAppendedDataItems(model.Loads);
            m_doc.WriteEndElement();
            //VTK FILE (Close)
            m_doc.WriteEndDocument();
            m_doc.Close();
        }
        #endregion

        #region INITIALIZE METHODS
        //INITIALIZE CELLS AND CELL DATA BUFFERS (ELEMENT/GEOMETRY)
        private void InitCells(AnalyticalModel model)
        {
            //FIX move to the constructor?
            m_connectivity = new StringBuilder();
            m_offsets = new StringBuilder();
            m_types = new StringBuilder();
            //================================
            m_material_ID = new StringBuilder();
            m_crossSection_ID = new StringBuilder();
            m_deadWeight_ID = new StringBuilder();
            m_areaLoad_ID = new StringBuilder();
            m_LCS_vector = new StringBuilder();
            m_support_ID = new StringBuilder();
            m_dummy = new StringBuilder();
            //=================================
            m_ElementProperty = new StringBuilder();

            int offsets_Count = 0;
            int element_Index = 0;
            foreach (AnalyticalElement element in model.Elements)
            {
                foreach (Geometry_Goo geometry in element.GetGeometries())
                {
                    foreach (int n_id in geometry.Nodes_ID)
                    {
                        m_connectivity.Append(n_id + " ");
                        offsets_Count++;
                    }

                    m_offsets.Append(offsets_Count + " ");
                    //HACK temporary solution - waiting for MIDAS implementation
                    if ((int)geometry.Type == 5 || (int)geometry.Type == 9)
                        m_types.Append(7 + " ");
                    else
                        m_types.Append((int)geometry.Type + " ");

                    //AppendedData
                    Vector3d v = geometry.Normal;
                    m_crossSection_ID.Append(((element.Profile_ID)+1) + " ");//+1
                    m_LCS_vector.Append(v.X + " " + v.Y + " " + v.Z + " ");
                    m_material_ID.Append(((element.Material_ID)+1) + " "); //+1
                    m_deadWeight_ID.Append(((element.DeadWeight) + 1) + " ");
                    m_areaLoad_ID.Append(((element.AreaLoad_ID) + 1) + " ");
                    m_support_ID.Append(((element.Support_ID) + 1) + " ");
                    m_dummy.Append(Convert.ToInt32(element.isDummy) + " ");
                    //TODO ================================================LATER BC-LOAD ELEMENT ID
                    m_ElementProperty.Append(element_Index + " ");
                }
                element_Index++;
            }
        }
        //INIT POINT DATA (NODAL LOAD, SUPPORT, HINGE)
        private void InitPointData(DataList<Node> nodes)
        {
            m_hinge = new StringBuilder();
            m_nodalLoad = new StringBuilder();
            m_support = new StringBuilder();
            //FOR EACH NODE IN MODEL READ DATA
            foreach (Node node in nodes)
            {
                m_hinge.Append(node.Hinge+ " "); //NO APPEND DATA (+0) DEFAULT 0 NO -1 IN NODE ID
                m_nodalLoad.Append((node.Load  +1)+ " "); //APPEND DATA (+1)
                m_support.Append((node.Support  +1)+ " "); //APPEND DATA (+1)
            }
        }

        //XML WRITER SETTINGS
        private XmlWriterSettings Settings()
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.NewLineChars = "\n"; //\r
            settings.NewLineHandling = NewLineHandling.Replace;
            settings.Encoding = new UTF8Encoding(false);
            return settings;
        }
        #endregion

        #region WRITE METHODS
        //WRITE POINTS
        private void WritePoints(DataList<Node> nodes)
        {
            foreach (Node node in nodes)
            {
                m_doc.WriteString(node.ToFile());
            }
        }    
        //WRITE APPENDED DATA ITEMS
        private void WriteAppendedDataItems<T>(List<T> list) where T: IAppendedData
        {
            for (int i = 0; i < list.Count; i++)
            {
                m_doc.WriteRaw(Environment.NewLine + m_doc.Settings.IndentChars.ToString());
                m_doc.WriteStartElement("item");
                m_doc.WriteString( (i+1) + " " + list[i].ToFile());
                m_doc.WriteEndElement();
            }
        }
        //WRITE DATA ARRAY(S)
        private void WriteDataArray(string type, string Name, string format, StringBuilder value)
        {
            m_doc.WriteStartElement("DataArray");
            m_doc.WriteAttributeString("type", type);
            m_doc.WriteAttributeString("Name", Name);
            m_doc.WriteAttributeString("format", format);
            m_doc.WriteString(value.ToString());
            m_doc.WriteEndElement();
        }
        private void WriteDataArray(string Name, StringBuilder value)
        {
            m_doc.WriteStartElement("DataArray");
            m_doc.WriteAttributeString("type", "Int32");
            m_doc.WriteAttributeString("Name", Name);
            m_doc.WriteAttributeString("format", "ascii");
            m_doc.WriteString(value.ToString());
            m_doc.WriteEndElement();
        }
        private void WriteDataArray(string Name, StringBuilder value, string type, int NumberOfComponents)
        {
            m_doc.WriteStartElement("DataArray");
            m_doc.WriteAttributeString("type", type);
            m_doc.WriteAttributeString("Name", Name);
            m_doc.WriteAttributeString("format", "ascii");
            m_doc.WriteAttributeString("NumberOfComponents", NumberOfComponents.ToString());
            m_doc.WriteString(value.ToString());
            m_doc.WriteEndElement();
        }
        #endregion
    }
}
