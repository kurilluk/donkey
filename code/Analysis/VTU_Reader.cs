﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Donkey.Extensions;
using Rhino.Geometry;
using Donkey.Model.Geometry;
using Donkey.Model.Nodal;
using Donkey.Model;
using Donkey.Model.Resulting;

namespace Donkey.Analysis
{
    public class VTU_Reader //StaticClass?
    {
        #region FIELDS
        private XmlReader doc;
        private ResultModel model;

        ////private List<IData> materials;
        ////private List<IData> crossSections;
        ////private List<IData> boundaryConditions;

        //private StringBuilder connectivity;
        //private StringBuilder offsets;
        //private StringBuilder types;

        //private StringBuilder CrossSection_ID;
        //private StringBuilder Material_ID;
        //private StringBuilder BoundaryCondition_ID;
        #endregion

        #region CONSTRUCTOR
        public VTU_Reader()
        {
            ////this.cls = cls;
            //pts = new List<GH_Node>();
            //cls = new List<GH_FrameElement>();
            //materials = new List<IData>();
            //crossSections = new List<IData>();
            //boundaryConditions = new List<IData>();

            //connectivity = new int[0];
            //offsets = new int[0];
            //types = new int[0];
            //CrossSection_ID = new StringBuilder();
            //Material_ID = new StringBuilder();
            //BoundaryCondition_ID = new StringBuilder();

            //clsCount = cls.LocalNodesCount;
            //offsetsCount = 0;

            //Inicialize(cls);

            //points = new StringBuilder();

            ////it works only for point buffer (for other it is unless)
            //ListToBuffer();  

            //ReadCellsElement();
        }
        #endregion

        #region MAIN METHODS
        public ResultModel Read(string FilePath)
        {
            //READ FILE TO BUFFER (Points,Cells, PointData, CellData)
            this.doc = XmlReader.Create(FilePath);
            ReaderBuffer rb = new ReaderBuffer();
            if (ReadPoints(ref rb) && ReadCells(ref rb) && ReadPointData(ref rb) && ReadCellData(ref rb) && ReadAppendedData(ref rb))
            {
                //WRITE BUFFER TO MODEL
                this.model = new ResultModel();
                //WriteNodes
                model.SetNodes(rb.Points);
                //WriteGeometries
                WriteGeometries(rb,ref model);
                //Write Elements
                WriteElements(rb, ref model);
                //WriteResultingData
                if (rb.Displacement != null)
                {
                    model.Displacement.SetVectors(rb.Displacement);
                    model.Reactions.SetReactions(rb.ReactionForce);
                    model.Forces.Beam_AxialForce_N = rb.Calculate_N1_N2_Average();  // calculate an average of end points forces
                    //model.Forces.BeamStress_V = rb.BeamStress_V;
                    //model.Forces.BeamStress_M = rb.BeamStress_M;
                    model.Forces.Beam_BendingMoment_M_pt1 = rb.BeamStress_M1_xyz;  // M1 - Start point
                    model.Forces.Beam_BendingMoment_M_pt2 = rb.BeamStress_M2_xyz; // M2 - End point
                    model.Geometries_Stress_Relative = rb.RelativeStress;
                    model.Geometries_Stress_Real = rb.RealStress;
                    //DATA INITIALIZATION
                    //model.Forces.CalculateAverageNValue(); //HACK TODO VALUE NO N-VALUE
                    //model appedned data
                    model.Stability = rb.Stability;
                    model.Volume = rb.Volume;
                    model.InitData();
                }
                //END
                this.doc.Close();
                return model;
            }
            return null;
            ////ReadFile
            //if (ReadNodes() && ReadGeometries())
            //{
            //   if (ReadPointData())
            //   {
            //        return model;
            //   }
            //}
        }
        #endregion

        #region READ FILE
        //READ POINTS
        private bool ReadPoints(ref ReaderBuffer buffer)
        {
            if (doc.ReadToFollowing("Points"))
            {
                if (doc.ReadToFollowing("DataArray"))
                {
                    List<double> pos = ReadArrDouble(doc.ReadString());
                    DataList<Node> points = new DataList<Node>();
                    for (int i = 0; i < pos.Count; i += 3)
                    {
                        //Append new nodes to the model
                        points.Add(new Node(pos[i], pos[i + 1], pos[i + 2]));
                    }
                    buffer.Points = points;
                    return true;
                }
            }
            return false;
        }
        //READ CELLS
        private bool ReadCells(ref ReaderBuffer cell)
        {
            if (doc.ReadToFollowing("Cells"))
            {
                XmlReader cells = doc.ReadSubtree();
                while (cells.Read())
                {
                    if (cells.HasAttributes && cells.Name == "DataArray")
                    {
                        switch (cells.GetAttribute("Name", cells.NamespaceURI))
                        {
                            case "connectivity":
                                cell.Connectivity = ReadArrInteger(doc.ReadString());
                                break;
                            case "offsets":
                                cell.Offsets = ReadArrInteger(doc.ReadString());
                                break;
                            case "types":
                                cell.Types = ReadArrInteger(doc.ReadString());
                                break;
                            default:
                                break;
                        }
                    }
                }
                return true;
            }
            return false;
        }
        //READ POINT DATA
        private bool ReadPointData(ref ReaderBuffer buffer)
        {
            if (doc.ReadToFollowing("PointData"))
            {
                XmlReader pointData = doc.ReadSubtree();
                while (pointData.Read())
                {
                    if (pointData.HasAttributes && pointData.Name == "DataArray")
                    {
                        switch (pointData.GetAttribute("Name", pointData.NamespaceURI))
                        {
                            case "uknw_displacement":
                                List<Vector3d> vectors = ReadArrVector(doc.ReadString());
                                buffer.Displacement = vectors;
                                break;
                            case "uknw_rotation":
                                //read rotation..                               
                                break;
                            case "reactions_forces":
                                List<Vector3d> reactions = ReadArrVector(doc.ReadString());
                                buffer.ReactionForce = reactions;
                                break;
                            default:
                                break;
                        }
                    }
                }
                return true;
            }
            return false;
        }
        //READ CELL DATA
        private bool ReadCellData(ref ReaderBuffer cell)
        {
            if (doc.ReadToFollowing("CellData"))
            {
                XmlReader pointData = doc.ReadSubtree();
                while (pointData.Read())
                {
                    if (pointData.HasAttributes && pointData.Name == "DataArray")
                    {
                        switch (pointData.GetAttribute("Name", pointData.NamespaceURI))
                        {
                            case "Property":
                                cell.Element_ID = ReadArrInteger(doc.ReadString());
                                break;
                            case "ID_model_parent":
                                cell.Geometry_ID = ReadArrInteger(doc.ReadString());
                                break;
                            case "stress_beam3D_1_NVyVz":
                                List<Vector3d> vectors_NVyVz_1 = ReadArrVector(doc.ReadString());
                                cell.BeamStress_N_1 = new List<double>(vectors_NVyVz_1.Count);
                                //cell.BeamStress_V = new List<double>(vectors_NVM.Count);
                                //cell.BeamStress_M = new List<double>(vectors_NVM.Count);
                                foreach (Vector3d vector in vectors_NVyVz_1)
                                {
                                    cell.BeamStress_N_1.Add(Math.Round(vector.X,3));
                                    //cell.BeamStress_V.Add(vector.Y);
                                    //cell.BeamStress_M.Add(vector.Z);
                                }
                                break;
                            case "stress_beam3D_2_NVyVz":
                                List<Vector3d> vectors_NVyVz_2 = ReadArrVector(doc.ReadString());
                                cell.BeamStress_N_2 = new List<double>(vectors_NVyVz_2.Count);
                                foreach (Vector3d vector in vectors_NVyVz_2)
                                {
                                    cell.BeamStress_N_2.Add(Math.Round(vector.X, 3));
                                }
                                break;
                            case "stress_beam3D_1_MxMyMz":
                                List<Vector3d> vectors_M1 = ReadArrVector(doc.ReadString());
                                cell.BeamStress_M1_xyz = vectors_M1;
                                break;
                            case "stress_beam3D_2_MxMyMz":
                                List<Vector3d> vectors_M2 = ReadArrVector(doc.ReadString());
                                cell.BeamStress_M2_xyz = vectors_M2;
                                break;
                            case "CSusage_elast":
                                cell.RealStress = ReadArrDouble(doc.ReadString());
                                break;
                            case "CSusage_elast_rel":
                                cell.RelativeStress = ReadArrDouble(doc.ReadString());
                                break;                            
                            default:
                                break;
                        }
                    }
                }
                return true;
            }
            return false;
        }
        //READ MODEL DATA
        private bool ReadAppendedData(ref ReaderBuffer buffer)
        {
            if (doc.ReadToFollowing("AppendedData"))
            {
                XmlReader cells = doc.ReadSubtree();
                while (cells.Read())
                {
                    switch (cells.Name)
                    {
                        case "Volume":
                            buffer.Volume = Double.Parse(doc.ReadString());
                            break;
                        case "Stability_critical_coefficient":
                            buffer.Stability = Double.Parse(doc.ReadString());
                            break;
                        default:
                            break;
                    }
                }
                return true;
            }
            return false;
        }

        #endregion

        #region WRITE TO MODEL
        //WRITE GEOMETRIES
        private bool WriteGeometries(ReaderBuffer cell,ref ResultModel model)
        {
            int a = 0;
            //FOR EACH GEOMETRY
            for (int o = 0; o < cell.Offsets.Count; o++)
            {
                //READ NODAL ID
                List<int> nodal_ID = new List<int>();
                for (int c = a; c < cell.Offsets[o]; c++)
                {
                    nodal_ID.Add(cell.Connectivity[c]);
                    a++;
                }
                //CREATE GEOMETRY BY TYPE
                switch (cell.Types[o])
                {
                    case (int)Model.Geometry.Types.VTK_LINE:
                        VTK_Line geo = new VTK_Line(nodal_ID, model); //ELEMENT ID = cell.Element_ID[o]
                        model.Geometries.AppendData(geo, false);
                        break;
                    case (int)Model.Geometry.Types.VTK_TRIANGLE:
                        VTK_Triangle geox = new VTK_Triangle(nodal_ID, model);
                        model.Geometries.AppendData(geox, false);
                        break;
                        //TODO NEXT DATA TYPE
                    default:
                        break;
                } 
            }
            return true;
        }
        //WRITE ELEMENTS
        private bool WriteElements(ReaderBuffer rb, ref ResultModel model)
        {
            int count = rb.Element_ID.Max();
            //CREATE ELEMENTS
            DataList<Element> elements = new DataList<Element>();
            for (int i = 0; i <= count; i++)
            {
                elements.Add(new Element(ref model));
            }
            //ADD GEOMETRY TO THE ELEMENTS
            for (int i = 0; i < rb.Element_ID.Count; i++)
            {
                elements[rb.Element_ID[i]].Geometries_ID.Add(i);
            }
            //INITIALIZE ELEMENT DATA (CENTER)
            for (int i = 0; i <= count; i++)
            {
                elements[i].InitCenter();
            }
            model.SetElements(elements);
            return true;
        }
        //WRITE RESULTING DATA

        //private bool ReadDisplacement(List<Vector3d> displacement)
        //{
        //    if (model.Nodes.Nodes == null || model.Nodes.Nodes.Count <= 1)
        //    {
        //        throw new ArgumentOutOfRangeException("Points read error");
        //    }
        //    if (displacement.Count == this.model.Nodes.Nodes.Count)
        //    {
        //        for (int i = 0; i < this.nl.Count; i++)
        //        {
        //            //this.pts[i].Value.Property.SetDisplacement(displacement[i]);
        //        }
        //        return true;
        //    }
        //    throw new ArgumentOutOfRangeException("Displacement is not same as points");
        //    //return false;
        //}
        #endregion

        #region UTILITY METHODS

        private string[] ReadArrString(string value)
        {
            char[] separators = new char[] { ' ', '\n' };
            return value.Trim().Split(separators, StringSplitOptions.RemoveEmptyEntries);
        }

        private List<int> ReadArrInteger(string value)
        {
            string[] arrString = ReadArrString(value);
            List<int> arrInteger = new List<int>(arrString.Length);
            for (int i = 0; i < arrString.Length; i++)
            {
                arrInteger.Add(int.Parse(arrString[i]));
            }
            return arrInteger;
        }

        private List<double> ReadArrDouble(string value)
        {
            string[] arrString = ReadArrString(value);
            List<double> arrDouble = new List<double>(arrString.Length);
            for (int i = 0; i < arrString.Length; i++)
            {
                //SOME TIME FALSE (string correct format) - MS BUG?
                arrDouble.Add(double.Parse(arrString[i]));
            }
            return arrDouble;
        }

        private List<Vector3d> ReadArrVector(string value)
        {
            List<double> arrDouble = ReadArrDouble(value);
            List<Vector3d> arrVector = new List<Vector3d>(arrDouble.Count / 3);
            for (int i = 0; i < arrDouble.Count; i += 3)
            {
                arrVector.Add(new Vector3d(arrDouble[i], arrDouble[i + 1], arrDouble[i + 2]));
            }
            return arrVector;
        }
        #endregion
    }
}
