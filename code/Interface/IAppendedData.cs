﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Donkey.Interface
{
   public interface IAppendedData
    {
        string ToFile();
    }
}
