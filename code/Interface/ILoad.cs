﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Donkey.Model.Analytical.Data;

namespace Donkey.Interface
{
    public interface ILoad
    {
        Load Type { get; }
    }
}
