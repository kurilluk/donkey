﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rhino.Geometry;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel;

namespace Donkey.Model.Geometry
{
    public class VTK_Quad : Geometry_Goo
    {
        #region FIELD
        public Mesh PreviewGeometry { get; private set; }
        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        protected VTK_Quad() : base() { }
        //DATA CONSTRUCTOR
        public VTK_Quad(List<int> nodes_ID, Model_Goo model)
            : this(nodes_ID, model, Vector3d.Unset) { }

        public VTK_Quad(List<int> nodes_ID, Model_Goo model, Vector3d normal)
            : base(Geometry.Types.VTK_QUAD, nodes_ID, model)
        {
            base.Normal = normal;
            GenerateMesh();
        }
        //COPY CONSTRUCTOR 
        public VTK_Quad(Geometry_Goo instance) : base(instance) { }
        //COPY CONSTRUCTOR REGISTER NEW MODEL
        public VTK_Quad(Geometry_Goo instance, Model_Goo model) : base(instance, model) { }
        #endregion

        #region METHODS
        public void GenerateMesh()
        {
            List<Point3d> pts = new List<Point3d>(this.Nodes_ID.Count);
            foreach (int id in this.Nodes_ID)
            {
                pts.Add(model.Nodes[id]);
            }
            //HACK FOR CLOSE POLYLINE
            pts.Add(model.Nodes[this.Nodes_ID[0]]);
            Polyline polyline = new Polyline(pts);
            //this.PreviewGeometry = Mesh.CreateFromPlanarBoundary(polyline.ToNurbsCurve(), new MeshingParameters());
            this.PreviewGeometry = Mesh.CreateFromClosedPolyline(polyline);
            //COULD BE IMPROVED OR CHANGE FOR CUSTOM MESHING PROPERTY

        }
        #endregion

        #region OVERRIDE

        public override Vector3d Normal
        {
            get
            {
                return base.m_normal;
            }
            protected set
            {
                base.m_normal = value;
            }
        }

        //DUPLICATE
        public override IGH_Goo Duplicate()
        {
            return new VTK_Quad(this);
        }
        //RESET AND CLONE GEOMETRY TO OTHER MODEL
        public override int CloneToModel(Model_Goo newModel, bool removeElementsDup, bool removeNodeDup)
        {
            List<int> newNodes_id = new List<int>(this.Nodes_ID.Count);
            foreach (int id in Nodes_ID)
            {
                newNodes_id.Add(newModel.Nodes.AppendData(new Node(model.Nodes[id]), removeNodeDup));
            }
            VTK_Quad newGeometry = new VTK_Quad(newNodes_id, newModel, this.Normal);
            return newModel.Geometries.AppendData(newGeometry, removeElementsDup);
        }
        //PREVIEW GEOMETRY
        public override void DrawViewportMeshes(GH_PreviewMeshArgs args)
        {
            args.Pipeline.DrawMeshShaded(this.PreviewGeometry, args.Material);
            ////List<Node> nodes = this.Value.Nodes;
            ////for (int i = 0; i < nodes.Count - 1; i++)
            ////{
            ////    Point3d pt1 = new Point3d(nodes[i].X, nodes[i].Y, nodes[i].Z);
            ////    Point3d pt2 = new Point3d(nodes[i + 1].X, nodes[i + 1].Y, nodes[i + 1].Z);
            ////    args.Pipeline.DrawLine(pt1, pt2, args.Color, args.Thickness);
            //List<Node> nodes = Value.GetNodes();
            //if (nodes.Count == 2)
            //{
            //args.Pipeline.DrawLine(this[0], this[1], args.Color, args.Thickness);
            //}
            //else
            //{
            //    throw new IndexOutOfRangeException("Wrong line geometry point count");
            //    //System.ArgumentException("Parameter cannot be null", "original");
            //}
        }

        public override void DrawVieportWires_Displacement(GH_PreviewWireArgs args)
        {
            //base.DrawViewportWires(args);
            args.Pipeline.DrawPolygon(this.GetDisplacementPoints(false), args.Color, true);
        }
        #endregion

    }
}
