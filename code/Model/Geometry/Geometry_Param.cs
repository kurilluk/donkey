﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Drawing;

namespace Donkey.Model.Geometry
{
    class Geometry_Param : GH_Param<Geometry_Goo>, IGH_PreviewObject  //HACK Geometry to ELEMENT param?
    {
        #region FIELDS
        private bool m_hidden;
        #endregion

        #region CONSTRUCTOR
        //Default constructor
        public Geometry_Param()
            : base(new Grasshopper.Kernel.GH_InstanceDescription("Geometry", "Geometry", "Donkey's model geometry", "Donkey", "Params")) { }
        //Overload constructor 1     
        public Geometry_Param(GH_InstanceDescription nTag) : base(nTag){}
        //Overload constructor 2     
        public Geometry_Param(GH_InstanceDescription nTag, GH_ParamAccess access) : base(nTag, access) { }
        //Component guid
        public override Guid ComponentGuid
        {
            get { return new Guid("{cd336603-1a82-454a-a393-0fb796e49f4e}"); }
        }
        #endregion

        #region IGH_PreviewObject Members
        Rhino.Geometry.BoundingBox IGH_PreviewObject.ClippingBox
        {
            get
            {
                BoundingBox bb = BoundingBox.Empty;
                if (!base.m_data.IsEmpty)
                {
                    foreach (Geometry_Goo geo in base.m_data.NonNulls)
                    {
                        if (geo.IsValid)
                        {
                            bb.Union(geo.ClippingBox);
                        }
                    }
                }
                return bb;
            }
        }

        void IGH_PreviewObject.DrawViewportMeshes(IGH_PreviewArgs args)
        {
            Rhino.Display.DisplayMaterial mat;
            //int thickness;
            if (this.Attributes.GetTopLevel.Selected)
            {
                mat = args.ShadeMaterial_Selected;
                //thickness = 2;
            }
            else
            {
                mat = args.ShadeMaterial;
                //thickness = 1;
            }

            if (!base.m_data.IsEmpty)
            {
                foreach (Geometry_Goo element in base.m_data.NonNulls)
                {
                    if (element.IsValid)
                    {
                        element.DrawViewportMeshes(new GH_PreviewMeshArgs(args.Viewport,args.Display,mat,args.MeshingParameters));
                    }
                }
            }
        }

        void IGH_PreviewObject.DrawViewportWires(IGH_PreviewArgs args)
        {
            Color colour;
            int thickness;
            if (this.Attributes.GetTopLevel.Selected)
            {
                colour = args.WireColour_Selected;
                thickness = 2;
            }
            else
            {
                colour = args.WireColour;
                thickness = 1;
            }

            if (!base.m_data.IsEmpty)
            {
                foreach (Geometry_Goo element in base.m_data.NonNulls)
                {
                    if (element.IsValid)
                    {
                        element.DrawViewportWires(new GH_PreviewWireArgs(args.Viewport, args.Display, colour, thickness));
                    }
                }
            }
        }

        bool IGH_PreviewObject.Hidden
        {
            get
            {
                return this.m_hidden;
            }
            set
            {
                this.m_hidden = value;
            }
        }

        bool IGH_PreviewObject.IsPreviewCapable
        {
            get { return true; }
        }

        #endregion
    }
}
