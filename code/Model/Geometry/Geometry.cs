﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rhino.Geometry;
using Donkey.Extensions;

namespace Donkey.Model.Geometry
{
    public class Geometry //TODO to AbstractClass IGeometry?
    {
        #region FIELD
        //public int Element_ID { get; private set; }     
        //public int Geometry_ID {get; private set;}
        public Types Type { get; private set; }
        public List<int> Nodes_ID { get; private set; }
        public Model_Goo model { get; set; }
        #endregion

        #region CONSTRUTOR
        //Blank constructor
        public Geometry() : this(Types.DUMMY, new List<int>(), new Resulting.ResultModel()) { }
        //Initial constructor
        public Geometry(Types type, List<int> nodes_ID, Model_Goo model) 
        {
            //this.Element_ID = element_ID;
            //this.Geometry_ID = geometry_ID;
            this.Type = type;
            this.Nodes_ID = nodes_ID;
            this.model = model;
        }
        //Copy contstuctor
        public Geometry(Geometry geo) : this(geo.Type, geo.Nodes_ID, new Resulting.ResultModel()) { }
        //Copy constructor with new nodalList reference
        public Geometry(Geometry geo, ref Model_Goo model) : this(geo.Type, geo.Nodes_ID, model) { }
        #endregion

        #region METHODS
        //GET NODE
        /// <summary>
        /// Get nodes from geometry
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Node this[int index]
        {
            get
            {
                if (index > this.Nodes_ID.Count-1)
                    throw new IndexOutOfRangeException();
                return model.Nodes[Nodes_ID[index]];
            }
        }
        //GET NODES
        public List<Node> GetNodes()
        {
                List<Node> newNodalList = new List<Node>(Nodes_ID.Count);
                foreach (int index in Nodes_ID)
                {
                    newNodalList.Add(model.Nodes[index]);
                }
                return newNodalList;
        }
        //GET POINTS
        public List<Point3d> GetPoints()
        {
            List<Point3d> newPointList = new List<Point3d>(Nodes_ID.Count);
            foreach (int index in Nodes_ID)
            {
                newPointList.Add(model.Nodes[index]);
            }
            return newPointList;
        }
        ////REGISTER new NodalList reference
        //public void RegisterNewNodalList(ref DataList<Node> newNodalList)
        //{
        //    this.model.Nodes = newNodalList;
        //}
        #endregion

        public enum Types
        {
            DUMMY = 0,
            VTK_LINE = 3,
            VTK_POLY_LINE = 4,
            VTK_TRIANGLE = 5,
            VTK_POLYGON = 7
        }

    }
}
