﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Rhino.Geometry;
using Grasshopper.Kernel.Types;
using Donkey.Extensions;
using Rhino;
using Rhino.DocObjects;

namespace Donkey.Model.Geometry
{
    public class VTK_Line : Geometry_Goo
    {
        #region FIELD
        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        protected VTK_Line() :base() { }
        //DATA CONSTRUCTOR
        public VTK_Line(List<int> nodes_ID, Model_Goo model)
            : base(Geometry.Types.VTK_LINE, nodes_ID, model) { }
        //COPY CONSTRUCTOR 
        public VTK_Line(Geometry_Goo instance) : base(instance){}
        //COPY CONSTRUCTOR REGISTER NEW MODEL
        public VTK_Line(Geometry_Goo instance, Model_Goo model) : base(instance, model) { }
        #endregion

        #region OVERRIDE
        //DUPLICATE
        public override IGH_Goo Duplicate()
        {
            return new VTK_Line(this);
        }
        //RESET AND CLONE GEOMETRY TO OTHER MODEL
        public override int CloneToModel(Model_Goo newModel, bool removeElementsDup, bool removeNodeDup)
        {
            List<int> newNodes_id = new List<int>(this.Nodes_ID.Count);
            foreach (int id in Nodes_ID)
            {
                newNodes_id.Add(newModel.Nodes.AppendData(new Node(model.Nodes[id]), removeNodeDup));
            }
            VTK_Line newGeometry = new VTK_Line(newNodes_id, newModel);
            return newModel.Geometries.AppendData(newGeometry, removeElementsDup);
        }
        //PREVIEW GEOMETRY
        public override void DrawViewportWires(GH_PreviewWireArgs args)
        {
            ////List<Node> nodes = this.Value.Nodes;
            ////for (int i = 0; i < nodes.Count - 1; i++)
            ////{
            ////    Point3d pt1 = new Point3d(nodes[i].X, nodes[i].Y, nodes[i].Z);
            ////    Point3d pt2 = new Point3d(nodes[i + 1].X, nodes[i + 1].Y, nodes[i + 1].Z);
            ////    args.Pipeline.DrawLine(pt1, pt2, args.Color, args.Thickness);
            //List<Node> nodes = Value.GetNodes();
            //if (nodes.Count == 2)
            //{
            args.Pipeline.DrawLine(this[0], this[1], args.Color, args.Thickness);
            //}
            //else
            //{
            //    throw new IndexOutOfRangeException("Wrong line geometry point count");
            //    //System.ArgumentException("Parameter cannot be null", "original");
            //}
        }

        public override void DrawVieportWires_Displacement(GH_PreviewWireArgs args)
        {
            if (model.DisplacementNodes != null && model.DisplacementNodes.Count > 0)
                args.Pipeline.DrawLine(GetDisplacementNode(0), GetDisplacementNode(1), args.Color, args.Thickness);
            else
                DrawViewportWires(args);
        }

        //BAKE GEOMETRY
        public override Guid Bake_Geometry(RhinoDoc doc, ObjectAttributes att)
        {
            //return doc.Objects.AddLine(GetDisplacementNode(0), GetDisplacementNode(1), att);
            return doc.Objects.AddLine(this[0], this[1], att);
        }
        #endregion
    }
}
