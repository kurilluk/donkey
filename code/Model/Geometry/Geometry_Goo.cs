﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel;
using Rhino.Geometry;
using Donkey.Interface;
using Donkey.Model.Analytical.Data;
using Rhino;
using Rhino.DocObjects;

namespace Donkey.Model.Geometry
{
    public class Geometry_Goo : GH_Goo<Object>, IGH_PreviewData, IModelParts
    {
        #region FIELD
        //BASE DATA
        public Types Type { get; private set; }
        public List<int> Nodes_ID { get; private set; }
        public Model_Goo model { get; set; }
        //CALCULATED DATA
        public Point3d Center { get; private set; }
        public Plane Local { get; protected set; }
        public NurbsCurve ProfileShape { get; private set; }
        public NurbsCurve TubeProfileShape { get; private set; }
        protected Vector3d m_normal;

        public virtual Vector3d Normal
        {
            get { return this.Local.Normal; }
            protected set { this.m_normal = value; }
        }
        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        protected Geometry_Goo() :this(Types.DUMMY, new List<int>(), new Resulting.ResultModel()) { }
        //DATA CONSTRUCTOR
        public Geometry_Goo(Types type, List<int> nodes_ID, Model_Goo model) 
        {
            this.Type = type;
            this.Nodes_ID = nodes_ID;
            this.model = model;
            this.m_normal = Vector3d.Unset;
            //CALCULATE DATA
            this.InitData();
        }
        //COPY CONSTRUCTOR BASIC
        public Geometry_Goo(Geometry_Goo instance)
            : this(instance, instance.model) { }
        //COPY CONSTRUCTOR REGISTER NEW MODEL
        public Geometry_Goo(Geometry_Goo instance, Model_Goo model) 
        {
            this.Type = instance.Type;
            this.Nodes_ID = new List<int>(instance.Nodes_ID);
            this.model = new Model_Goo(model);
            this.Center = new Point3d(instance.Center);
            this.Local = new Plane(instance.Local);
            this.ProfileShape = new NurbsCurve(instance.ProfileShape);
            this.TubeProfileShape = new NurbsCurve(instance.TubeProfileShape);
            this.Normal = new Vector3d(instance.Normal);
        }
        //INIT DATA METHOD
        private void InitData()
        {
            this.GetCenter();
            //this.GetLocalPlane();
        }
        #endregion

        #region METHODS
        public virtual int CloneToModel(Model_Goo newModel, bool removeElementsDup, bool removeNodeDup)
        {
            List<int> newNodes_id = new List<int>(this.Nodes_ID.Count);
            foreach (int id in Nodes_ID)
            {
                newNodes_id.Add(newModel.Nodes.AppendData(new Node(model.Nodes[id]), removeNodeDup));
            }
            Geometry_Goo newGeometry = new Geometry_Goo(this.Type, newNodes_id, newModel);
            return newModel.Geometries.AppendData(newGeometry, removeElementsDup);
        }

        //GET NODE
        /// <summary>
        /// Get nodes from geometry
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Node this[int index]
        {
            get
            {
                if (index > this.Nodes_ID.Count - 1)
                    throw new IndexOutOfRangeException("Geometry has less nodes as you expect.");
                return model.Nodes[Nodes_ID[index]];
            }
        }

        public Node GetDisplacementNode(int index)
        {
                if (index > this.Nodes_ID.Count - 1)
                    throw new IndexOutOfRangeException("Geometry has less nodes as you expect.");
                if (this.model.DisplacementNodes != null && model.DisplacementNodes.Count > 0)
                    return model.DisplacementNodes[Nodes_ID[index]];
                else 
                {
                    return model.Nodes[Nodes_ID[index]];
                    throw new NullReferenceException("No displacement geometry was set yet.");
                }
        }

        //GET NODES
        public List<Node> GetNodes()
        {
            List<Node> newNodalList = new List<Node>(Nodes_ID.Count);
            foreach (int index in Nodes_ID)
            {
                newNodalList.Add(model.Nodes[index]);
            }
            return newNodalList;
        }

        //GET POINTS
        public List<Point3d> GetPoints()
        {
            return this.GetPoints(false);
        }

        public List<Point3d> GetPoints(bool close)
        {
            List<Point3d> newPointList = new List<Point3d>(Nodes_ID.Count);
            foreach (int index in Nodes_ID)
            {
                newPointList.Add(model.Nodes[index]);
            }
            if(close)
                newPointList.Add(model.Nodes[Nodes_ID[0]]);
            return newPointList;
        }

        public List<Point3d> GetDisplacementPoints(bool close)
        {
            List<Point3d> newPointList = new List<Point3d>(Nodes_ID.Count);
            foreach (int index in Nodes_ID)
            {
                if(model.DisplacementNodes != null && model.DisplacementNodes.Count > 0)
                    newPointList.Add(model.DisplacementNodes[index]);
                else
                    newPointList.Add(model.Nodes[index]);
            }
            if (close)
            {
                if (model.DisplacementNodes != null && model.DisplacementNodes.Count > 0)
                    newPointList.Add(model.DisplacementNodes[Nodes_ID[0]]);
                else
                    newPointList.Add(model.Nodes[Nodes_ID[0]]);
            }
            return newPointList;
 
        }
        ////REGISTER new NodalList reference
        //public void RegisterNewNodalList(ref DataList<Node> newNodalList)
        //{
        //    this.model.Nodes = newNodalList;
        //}
        #endregion

        #region EQUALS
        //OVERRIDE EQUALS OBJECT
        public override bool Equals(System.Object obj)
        {
            if (obj == null)
                return false;
            if (obj is Geometry_Goo)
                return GeometryEquals((Geometry_Goo)obj);
            return false;
        }
        //EQUALS FOR GEOMETRY OBJECT
        public bool Equals(Geometry_Goo g)
        {
            if ((object)g == null)
                return false;
            // Return true if the fields match:
            return GeometryEquals(g);
        }

        //GEOMETRY EQUALS
        private bool GeometryEquals(Geometry_Goo g)
        {
            if (this.Type != g.Type)
                return false;
            if (this.Nodes_ID.Count != g.Nodes_ID.Count)
                return false;
            for (int i = 0; i < this.Nodes_ID.Count; i++)
            {
                //if (this.Nodes_ID[i] != g.Nodes_ID[i])
                //    return false;
                if (!g.Nodes_ID.Contains(this.Nodes_ID[i]))
                    return false;
            }
            return true;
        }
        //GENERATE HASHCODE
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion

        #region GH_Goo Member
        public override IGH_Goo Duplicate()
        {
            return new Geometry_Goo(this);
        }

        public override bool IsValid
        {
            get { return true; }
        }

        public override string ToString()  //HACK FOR EACH NODES IN GEOMETRY
        {
            StringBuilder sb = new StringBuilder();
            foreach(Point3d pt in this.GetPoints())
            {
                sb.Append(pt.ToString() + "\n");
            }
            return sb.ToString();
        }

        public override string TypeDescription
        {
            get { return "Geometry of Donkey's model"; }
        }

        public override string TypeName
        {
            get { return this.Type.ToString(); }
        }
        #endregion

        #region IGH_PreviewData
        public virtual Rhino.Geometry.BoundingBox ClippingBox
        {
            get 
            {
                BoundingBox bb = BoundingBox.Empty;
                if(this.ProfileShape != null)
                    bb.Union(this.ProfileShape.GetBoundingBox(false));
                bb.Union(new BoundingBox(this.GetPoints()));
                return bb; 
            }
        }

        public virtual void DrawViewportMeshes(GH_PreviewMeshArgs args)
        {
            //override by child if it needs
        }

        public virtual void DrawViewportWires(GH_PreviewWireArgs args) //normal preview
        {
            //override by child if it needs
        }

        public virtual void DrawVieportWires_Displacement(GH_PreviewWireArgs args)
        {
            //have to override
        }

        public void DrawProfile(GH_PreviewWireArgs args)
        {
            if(this.ProfileShape != null)
                args.Pipeline.DrawCurve(this.ProfileShape, args.Color, args.Thickness);

            if (this.TubeProfileShape != null)
                args.Pipeline.DrawCurve(this.TubeProfileShape, args.Color, args.Thickness);
        }
        #endregion

        #region IGH_BakeAwareData
        public virtual Guid Bake_Geometry(RhinoDoc doc, ObjectAttributes att)
        {
            //different for different geometry
            return Guid.Empty;
        }

        #endregion

        #region LOCAL GEOMETRY CALCULATION
        //Find center of the geometry
        protected virtual void GetCenter()
        {
            double x = 0;
            double y = 0;
            double z = 0;
            int count = this.Nodes_ID.Count;
            foreach (Node node in this.GetNodes())
            {
                x += node.X;
                y += node.Y;
                z += node.Z;
            }
            this.Center = new Point3d(x/count , y/count, z/count);
        }
        //COMPUTE LOCAL PLANE
        //protected virtual void GetLocalPlane()
        //{ 
        //    GetLocalPlane(Vector3d.ZAxis);
        //}
        public virtual void GetLocalPlane(Vector3d ZAxis)
        {
            //if (this.Center == null)
                //this.GetCenter();
            Point3d origin = this.Center;
            if (!this.Center.IsValid)
                throw new Exception("Center point was not calculated yet!");
            List<Point3d> pts = this.GetPoints();
            Vector3d Xlocal = new Vector3d(pts[1] - pts[0]);
            Vector3d Ylocal;

            Ylocal = Vector3d.CrossProduct(ZAxis, Xlocal);
            if (Ylocal.X == 0 && Ylocal.Y == 0 && Ylocal.Z == 0)
            {
                Ylocal = Vector3d.CrossProduct((-Vector3d.XAxis) * Xlocal.Z, Xlocal);
            }
            this.Local = new Plane(origin, Xlocal, Ylocal);
        }
        //SET PROFILE SHAPE
        public void SetProfileShape(Profile profile)
        {
            double width = profile.Width / 2;
            double height = profile.Height / 2;
            double thickness = profile.Thickness;
            //HACK - this recalculate local coordinates (maybe previous calculation in init methot is not needed)
            GetLocalPlane(profile.orientation.Zaxis);
            Plane YZplane = new Plane(Local.Origin,Local.YAxis,Local.ZAxis);
            switch (profile.Type)
            {
                case Profile.types.Circle:
                    this.ProfileShape = new Ellipse(YZplane, width, height).ToNurbsCurve();
                    this.TubeProfileShape = null;
                    break;
                case Profile.types.Rectangle:
                    this.ProfileShape = new Rectangle3d(YZplane, new Interval(-width, width), new Interval(-height, height)).ToNurbsCurve();
                    this.TubeProfileShape = null;
                    break;
                case Profile.types.Circle_tube:
                    this.ProfileShape = new Ellipse(YZplane, width, height).ToNurbsCurve();
                    this.TubeProfileShape = new Ellipse(YZplane, width - thickness, height - thickness).ToNurbsCurve();
                    break;
                case Profile.types.Rectangle_tube:
                    this.ProfileShape = new Rectangle3d(YZplane, new Interval(-width, width), new Interval(-height, height)).ToNurbsCurve();
                    this.TubeProfileShape = new Rectangle3d(YZplane, new Interval(-width + thickness, width - thickness), new Interval(-height + thickness, height - thickness)).ToNurbsCurve();
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region IModelParts 
        //REGISTER MODEL //OR USE SPECIFIC COPY CONSTRUCTOR FOR REGISTER NEW MODEL!!!
        public void RegisterModel(Model_Goo model)
        {
            this.model = model;
        }

        public IModelParts Clone
        {
            get { return new Geometry_Goo(this); }
        }
        #endregion



        //#region ENUM TYPES
        //public enum Types
        //{
        //    DUMMY = 0,
        //    VTK_LINE = 3,
        //    VTK_POLY_LINE = 4,
        //    VTK_TRIANGLE = 5,
        //    VTK_POLYGON = 7
        //}
        //#endregion

    }
}
