﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Donkey.Model.Geometry
{
    public enum Types
    {
        DUMMY = 0,
        VTK_LINE = 3,
        VTK_POLY_LINE = 4,
        VTK_TRIANGLE = 5,
        VTK_POLYGON = 7,
        VTK_QUAD = 9 
    }
}
