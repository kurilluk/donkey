﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rhino.Geometry;
using Donkey.Interface;

namespace Donkey.Model.Geometry
{
    public class Node : IModelParts
    {
        #region FIELD
        private double m_x, m_y, m_z;
        //ANALYTICAL DATA
        private int m_hinge, m_load, m_support;
        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        //public Node() :this(Point3d.Unset){ }
        ////GH_Point constructor
        //public Node(Point3d point) : this(point.X, point.Y, point.Z) { }
        //DATA CONSTRUCTOR
        public Node(double x, double y, double z)
            :this(x,y,z,0,-1,-1)
        {  }
        public Node(double x, double y, double z, int hinge, int load, int support)
        {
            this.m_x = x;
            this.m_y = y;
            this.m_z = z;
            //ANALYTICAL DATA
            this.m_hinge = hinge;
            this.m_load = load;
            this.m_support = support;
        }
        //COPY CONSTRUCTOR
        public Node(Node instance) : this(instance.X, instance.Y, instance.Z)
        {
            //ANALYTICAL DATA
            this.m_hinge = 0; // instance.Hinge;
            this.m_load = -1; // instance.Load;
            this.m_support = -1; // instance.Support;
            //not so nice, but it is needed for analytical model component
        }
        #endregion

        #region METHODS
        public override string ToString()
        {
            return "{" + Math.Round(X, 1) + " " + Math.Round(Y, 1) + " " + Math.Round(Z, 1) + "}";
        }
        //TODO add Interface?
        public string ToFile()
        {
            return X + " " + Y + " " + Z + " ";
            //return Math.Round(X, 10) + " " + Math.Round(Y, 10) + " " + Math.Round(Z, 10) + " ";
        }

        //public Node Move(Vector3d vector)
        //{
        //    return new Node(X + vector.X, Y + vector.Y, Z + vector.Z);
        //}
        #endregion

        #region EQUALS
        //OVERRIDE EQUALS OBJECT
        public override bool Equals(System.Object obj)
        {
            if (obj == null)
                return false;
            // If parameter cannot be cast to Point return false.
            // Return true if the fields match:
            if(obj is Node)
                return EqualsCoordinates((Node)obj, 1);
            return false;
        }

        //EQUALS FOR NODE OBJECT
        public bool Equals(Node n)
        {
            // If parameter is null return false:
            if ((object)n == null)
            {
                return false;
            }
            // Return true if the fields match:
            return EqualsCoordinates(n, 1);
        }

        //COORDINATES EQUALS
        private bool EqualsCoordinates(Node n, int round)
        {
            return (Math.Round(this.X, round) == Math.Round(n.X, round)) && (Math.Round(this.Y, round) == Math.Round(n.Y, round)) && (Math.Round(this.Z, round) == Math.Round(n.Z, round));
        }

        //GENERATE HASHCODE
        public override int GetHashCode()
        {
            return this.m_x.GetHashCode() ^ this.m_y.GetHashCode() ^ this.m_z.GetHashCode();
            //unchecked // Overflow is fine, just wrap
            //{
            //    //In colision change hash integer
            //    int hash = 17;
            //    // Suitable nullity checks etc, of course :)
            //    hash = hash * 23 + X.GetHashCode();
            //    hash = hash * 23 + Y.GetHashCode();
            //    hash = hash * 23 + Z.GetHashCode();
            //    return hash;
            //}
        }
        #endregion

        #region PROPERTY
        //POSITIONS
        public double X
        {
            get { return this.m_x; }
        }
        public double Y
        {
            get { return this.m_y; }
        }
        public double Z
        {
            get { return this.m_z; }
        }
        //ANALYTICAL DATA
        public int Hinge
        {
            get { return this.m_hinge; }
            set { this.m_hinge = value; }
        }
        public int Load
        {
            get { return this.m_load; }
            set { this.m_load = value; }
        }
        public int Support
        {
            get { return this.m_support; }
            set { this.m_support = value; }
        }
        #endregion

        #region IDonkey_ModelParts
        public void RegisterModel(Model_Goo model)
        {
        }
        #endregion

        #region STATIC
        public static implicit operator Point3d(Node node)
        {
            return new Point3d(node.m_x,node.m_y,node.m_z);
        }

        public static implicit operator Node(Point3d point)
        {
            return new Node(point.X, point.Y, point.Z);
        }

        public static Node operator +(Node node, Vector3d vector)
        {
            return new Node(node.m_x + vector.X, node.m_y + vector.Y, node.m_z + vector.Z);
        }

        public static Node operator /(Node node, double scale)
        {
            return new Node(node.m_x/scale, node.m_y/scale, node.m_z/scale);
        }

        public static bool operator ==(Node node1, Node node2)
        {
            return Equals(node1, node2);
        }

        public static bool operator !=(Node node1, Node node2)
        {
            return !Equals(node1, node2);
        }
        #endregion

        #region Draft
        //public double this[int index]
        //{
        //    get
        //    {
        //        if (index == 0)
        //            return this.m_x;
        //        if (1 == index)
        //            return this.m_y;
        //        if (2 == index)
        //            return this.m_z;
        //        else
        //            throw new IndexOutOfRangeException();
        //    }
        //    set
        //    {
        //        if (index == 0)
        //            this.m_x = value;
        //        else if (1 == index)
        //        {
        //            this.m_y = value;
        //        }
        //        else
        //        {
        //            if (2 != index)
        //                throw new IndexOutOfRangeException();
        //            this.m_z = value;
        //        }
        //    }
        //}
        #endregion
    }
}
