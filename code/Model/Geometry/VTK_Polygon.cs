﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel;
using Rhino.Geometry;

namespace Donkey.Model.Geometry
{
    public class VTK_Polygon : Geometry_Goo
    {
        #region FIELD
        public Mesh PreviewGeometry { get; private set; }
        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        protected VTK_Polygon() :base() { }
        //DATA CONSTRUCTOR
        public VTK_Polygon(List<int> nodes_ID, Model_Goo model, Plane local_plane)
            : base(Geometry.Types.VTK_POLYGON, nodes_ID, model) 
        {
            List<Point3d> pts = new List<Point3d>(nodes_ID.Count);
            foreach (int id in nodes_ID)
            {
                pts.Add(model.Nodes[id]);
            }
            //HACK FOR CLOSE POLYLINE
            pts.Add(model.Nodes[nodes_ID[0]]);
            Polyline polyline = new Polyline(pts);
            this.PreviewGeometry = Mesh.CreateFromPlanarBoundary(polyline.ToNurbsCurve(), new MeshingParameters());
            //COULD BE IMPROVED OR CHANGE FOR CUSTOM MESHING PROPERTY

            this.Local = local_plane;
 
        }
        //COPY CONSTRUCTOR 
        public VTK_Polygon(Geometry_Goo instance) : base(instance){}
        //COPY CONSTRUCTOR REGISTER NEW MODEL
        public VTK_Polygon(Geometry_Goo instance, Model_Goo model) : base(instance, model) { }
        #endregion

        #region OVERRIDE
        //DUPLICATE
        public override IGH_Goo Duplicate()
        {
            return new VTK_Polygon(this);
        }
        //RESET AND CLONE GEOMETRY TO OTHER MODEL
        public override int CloneToModel(Model_Goo newModel, bool removeElementsDup, bool removeNodeDup)
        {
            List<int> newNodes_id = new List<int>(this.Nodes_ID.Count);
            foreach (int id in Nodes_ID)
            {
                newNodes_id.Add(newModel.Nodes.AppendData(new Node(model.Nodes[id]), removeNodeDup));
            }
            VTK_Polygon newGeometry = new VTK_Polygon(newNodes_id, newModel,this.Local);
            return newModel.Geometries.AppendData(newGeometry, removeElementsDup);
        }
        //PREVIEW GEOMETRY
        public override void DrawViewportMeshes(GH_PreviewMeshArgs args)
        {
            args.Pipeline.DrawMeshShaded(this.PreviewGeometry, args.Material);
            //args.Pipeline.DrawMesh
            ////List<Node> nodes = this.Value.Nodes;
            ////for (int i = 0; i < nodes.Count - 1; i++)
            ////{
            ////    Point3d pt1 = new Point3d(nodes[i].X, nodes[i].Y, nodes[i].Z);
            ////    Point3d pt2 = new Point3d(nodes[i + 1].X, nodes[i + 1].Y, nodes[i + 1].Z);
            ////    args.Pipeline.DrawLine(pt1, pt2, args.Color, args.Thickness);
            //List<Node> nodes = Value.GetNodes();
            //if (nodes.Count == 2)
            //{
            //args.Pipeline.DrawLine(this[0], this[1], args.Color, args.Thickness);
            //}
            //else
            //{
            //    throw new IndexOutOfRangeException("Wrong line geometry point count");
            //    //System.ArgumentException("Parameter cannot be null", "original");
            //}
        }

        public override void DrawVieportWires_Displacement(GH_PreviewWireArgs args)
        {
            //base.DrawViewportWires(args);
            args.Pipeline.DrawPolygon(this.GetDisplacementPoints(false), args.Color, true);
        }
        #endregion

    }
}
