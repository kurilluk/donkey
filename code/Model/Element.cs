﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Donkey.Model.Resulting;
using Donkey.Interface;
using Donkey.Model.Geometry;
using Rhino.Geometry;
using Donkey.Model.Analytical;

namespace Donkey.Model
{
    public class Element : IModelParts
    {
        #region FIELD
        public string Name { get; set; }
        public Model_Goo model;
        public List<int> Geometries_ID { get; protected set; }
        public Point3d Center { get; private set; }
        public Element.Types GeometryType { get; private set; }
        #endregion

        #region CONSTRUCTOR
        //BLANK CONTRUCTOR
        protected Element() {}
        //DATA CONSTRUCTOR
        public Element(List<int> geometry_id, Model_Goo model)
        {
            this.model = model;
            this.Geometries_ID = geometry_id;
            this.InitData();
            Geometry.Types geo_type = model.Geometries[geometry_id[0]].Type;
            if (geo_type == Geometry.Types.VTK_TRIANGLE || geo_type == Geometry.Types.VTK_QUAD || geo_type == Geometry.Types.VTK_POLYGON)
                this.GeometryType = Element.Types.SHELL;
            else
                this.GeometryType = Element.Types.BEAM;
        }
        //SPECIFIC DATA CONSTRUCOR
        public Element(ref ResultModel model)
        {
            this.model = model;
            this.Geometries_ID = new List<int>();
            this.Center = Point3d.Unset;
            //this.GeometryType = 
        }
        //COPY CONSTRUCTOR
        public Element(Element instance) : this(instance, ref instance.model) { }
        public Element(Element instance, ref Model_Goo model)
        {
            this.model = model;
            this.Geometries_ID = new List<int>(instance.Geometries_ID);
            this.GeometryType = instance.GeometryType;
            this.InitCenter();
            this.Name = instance.Name;
        }
        //DATA INITIALIZATON
        private void InitData()
        {
            this.InitCenter();
        }
        #endregion

        #region METHODS
        //RESET AND CLONE GEOMETRY TO OTHER MODEL
        public virtual bool CloneToModel(AnalyticalModel newModel, bool removeElementsDup, bool removeNodeDup)
        { return false; }
        //IDonkey_ModelParts METHOD
        public void RegisterModel(Model_Goo model)
        {
            this.model = model;
        }

        public List<Geometry_Goo> GetGeometries()
        {
            List<Geometry_Goo> geometries = new List<Geometry_Goo>(Geometries_ID.Count);
            foreach (int ID in Geometries_ID)
            {
                geometries.Add(model.Geometries[ID]);
            }
            return geometries;
        }

        public void InitCenter()
        {
            Point3d pt = Point3d.Origin;
            int count = Geometries_ID.Count;
            foreach (Geometry_Goo geo in GetGeometries())
            {
                pt += geo.Center;
            }
            this.Center = pt / count;
        }
        #endregion

        public enum Types
        {
            BEAM = 0,
            SHELL = 1
        }
    }
}
