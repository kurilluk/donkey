﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Donkey.Model.Geometry;

namespace Donkey.Model
{
    public class GeometryList
    {
        #region FIELD
        public List<Geometry_Goo> Geometries { get; private set; }
        #endregion
        
        #region CONSTRUCTOR
        //Blank constructor
        public GeometryList() { this.Geometries = new List<Geometry_Goo>(); }
        //Initial constructor
        public GeometryList(List<Geometry_Goo> geometry)
        {
            this.Geometries = geometry;
        }
        //Copy constructor
        public GeometryList(GeometryList instance) 
        {
            this.Geometries = instance.Clone(); 
        }
        #endregion

        #region METHODS
        private List<Geometry_Goo> Clone()
        {
            List<Geometry_Goo> cloneList = new List<Geometry_Goo>(Geometries.Count);
            foreach (Geometry_Goo geometry in Geometries)
            {
                cloneList.Add(geometry);
            }
            return cloneList;
        }

        public int AppendGeometry(Geometry_Goo geometry, bool removeDuplicates)
        {
            int count = Geometries.Count;
            if (removeDuplicates)
            {
                for (int i = 0; i < count; i++)
                {
                    if (geometry.Equals(Geometries[i]))
                    {
                        geometry = Geometries[i];
                        //TODO removedDuplicate info
                        return i;
                    }
                }
            }
            Geometries.Add(geometry);
            return count;
        }
        #endregion
    }
}
