﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Donkey.Model.Geometry;

namespace Donkey.Model
{
    public class NodalList  //HACK later all List change to Gen.Collection extension (specific add and clone)
    {
        #region FIELD
        public virtual List<Node> Nodes { get; protected set; }
        #endregion
        
        #region CONSTRUCTOR
        //Blank constructor
        public NodalList() { this.Nodes = new List<Node>(); }
        //Initial constructor
        public NodalList(List<Node> nodes)
        {
            this.Nodes = nodes;
        }
        //Copy constructor
        public NodalList(NodalList instance) 
        {
            this.Nodes = instance.Clone(); 
        }
        #endregion

        #region METHODS
        public List<Node> Clone()
        {
            List<Node> cloneList  = new List<Node>(Nodes.Count);
            foreach(Node node in Nodes)
            {
                cloneList.Add(node);
            }
            return cloneList;
        }

        public int AppendNode( Node node, bool removeDuplicates )
        {
            int count = Nodes.Count;
            if (removeDuplicates)
            {
                for (int i = 0; i < count; i++)
                {
                    if (node.Equals(Nodes[i]))
                    {
                        node = Nodes[i];
                        //TODO removedDuplicate info
                        return i;
                    }
                }
            }
            Nodes.Add(node);
            return count;
        }
        #endregion

    }
}
