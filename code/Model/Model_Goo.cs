﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel;
using Rhino.Geometry;
using Donkey.Model.Geometry;
using Donkey.Extensions;
using System.Drawing;

namespace Donkey.Model
{
    public class Model_Goo : GH_Goo<Object>, IGH_PreviewData
    {
        #region FIELD
        public string Name { get; set; }
        public virtual DataList<Node> Nodes { get; set; }
        public DataList<Geometry_Goo> Geometries { get; protected set; }
        public virtual DataList<Element> Elements { get; protected set; }
        public DataList<Node> DisplacementNodes { get; set; }
        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        protected Model_Goo() : this(new DataList<Node>(), new DataList<Geometry_Goo>(),new DataList<Element>()) { }
        //DATA CONSTRUCTOR
        protected Model_Goo(DataList<Node> nodes, DataList<Geometry_Goo> geometry,DataList<Element> elements)
        {//HACK WHY NEW DATA LIST IS NEEDED?
            this.Nodes = nodes; //new DataList<Node>(nodes);
            this.Geometries = geometry; //new DataList<Geometry_Goo>(geometry);
            this.Elements = elements; //new DataList<Element>(elements);
            this.DisplacementNodes = new DataList<Node>();
        }
        //COPY CONSTRUCTOR
        public Model_Goo(Model_Goo instance)
        {
            if (instance != null)
            {
                this.Nodes = new DataList<Node>(instance.Nodes);
                this.Geometries = new DataList<Geometry_Goo>(instance.Geometries, this);
                this.Elements = new DataList<Element>(instance.Elements, this);
                this.DisplacementNodes = new DataList<Node>(instance.DisplacementNodes, this);
                this.Name = instance.Name;
            }
        }
        #endregion

        #region GH_Goo Member
        public override IGH_Goo Duplicate()
        {
            return new Model_Goo(this);
        }

        public override bool IsValid
        {
            get { return true; }
        }

        public override string ToString()
        {
            return "Model";
        }

        public override string TypeDescription
        {
            get { return "Donkey's model"; }
        }

        public override string TypeName
        {
            get { return "Model/Element"; }
        }
        #endregion

        #region IGH_PreviewData
        public virtual Rhino.Geometry.BoundingBox ClippingBox
        {
            get
            {
                BoundingBox bb = BoundingBox.Empty;
                foreach (Geometry_Goo geo in this.Geometries)
                {
                    if (geo.IsValid)
                    {
                        bb.Union(geo.ClippingBox);
                    }
                }
                return bb;
            }
        }

        public virtual void DrawViewportMeshes(GH_PreviewMeshArgs args)
        {
            //override by child if it needs
        }

        public virtual void DrawViewportWires(GH_PreviewWireArgs args)
        {
            //override by child if it needs
        }
        #endregion

        #region PREVIEW
        //GEOMETRY
        public void DrawWireGeometry(GH_PreviewWireArgs args)
        {
            for (int i = 0; i < this.Geometries.Count; i++)
            {
                this.Geometries[i].DrawViewportWires(args);
            }
        }

        public void DrawMeshGeometry(GH_PreviewMeshArgs args)
        {
            for (int i = 0; i < this.Geometries.Count; i++)
            {
                this.Geometries[i].DrawViewportMeshes(args);
            }
        }
        //INDEXES
        public void DrawNodeIndex(IGH_PreviewArgs args)
        {
            for (int i = 0; i < this.Nodes.Count; i++)
            {
                string value = i + "";
                Point3d pt = this.Nodes[i];
                //args.Display.Draw2dText(value, Color.Black, pt, true, 16);
                //args.Display.DrawDot(pt, value);
                DrawLabel(args, value, pt,Color.Black,150, 10, 70);
            }
        }
        public void DrawGeometryIndex(IGH_PreviewArgs args)
        {
            for (int i = 0; i < this.Geometries.Count; i++)
            {
                string value = i + "";
                Point3d pt = this.Geometries[i].Center;
                args.Display.Draw2dText(value, Color.White, pt, false, 10);
            }
        }

        public static void DrawLabel(IGH_PreviewArgs args, string value, Point3d origin, Color color, double distance, int dot_size, int font_size)
        {
            args.Display.DepthMode = Rhino.Display.DepthMode.AlwaysInFront;
            Plane plane;
            args.Viewport.GetCameraFrame(out plane);
            plane.Origin = origin;
            Point3d pt_direction = plane.PointAt(distance, distance);
            Vector3d direction = origin - pt_direction;
            //direction.Unitize();
            //direction *= 300;

            Circle dot = new Circle(plane, origin, dot_size);
            args.Display.DrawCircle(dot, color, 3);

            Line line = new Line(origin, origin + (direction * 0.75));
            args.Display.DrawLine(line, color, 1);

            ////DRAW CROSS
            //Point3d pt1 = plane.PointAt(0, distance);
            //Point3d pt2 = plane.PointAt(distance,0);
            //Vector3d vec1 = origin - pt1;
            //Vector3d vec2 = origin - pt2;
            
            //args.Display.DrawLine(new Line(origin,vec1,dot_size*2),color,2);
            //args.Display.DrawLine(new Line(origin, vec1, -dot_size * 2), color, 2);
            //args.Display.DrawLine(new Line(origin, vec2, dot_size * 2), color, 2);
            //args.Display.DrawLine(new Line(origin, vec2, -dot_size * 2), color, 2);

            Rhino.Display.Text3d text3d = new Rhino.Display.Text3d(value, plane, font_size);
            BoundingBox bb = text3d.BoundingBox;
            Vector3d aligment = origin - bb.Center;
            //aligment += direction;
            Point3d text_point = origin + (direction + aligment);
            args.Display.Draw3dText(text3d, color, text_point);
            //args.Display.DrawBoxCorners(bb, Color.Black, 100);

            args.Display.DepthMode = Rhino.Display.DepthMode.Neutral;

 
        }

        public void DrawElementIndex(IGH_PreviewArgs args)
        {
            for (int i = 0; i < this.Elements.Count; i++)
            {
                string value = i + "";
                Point3d pt = Elements[i].Center;
                DrawLabel(args, value, pt,Color.White,-200, 20,90);


                //Plane plane;
                //args.Viewport.GetCameraFrame(out plane);
                //plane.Origin = pt;
                ////args.Display.Draw2dText(value, Color.White, pt, true, 20);
                ////args.Display.Draw3dText(value, Color.White, plane, 80, "Arial");

                //Rhino.Display.Text3d text3d = new Rhino.Display.Text3d(value, plane, 80);
                //BoundingBox bb = text3d.BoundingBox;
                //Point3d pt2 = bb.Center;
                //Vector3d vector = pt - pt2;
                //Point3d pt3 = pt + vector;
                ////args.Display.Draw3dText(text3d, Color.White, pt3);
                ////Point3d[] pts = bb.GetCorners();
                ////Circle circle = new Circle(plane,pt,80);
                ////new Circle(pts[1]+vector, pts[2]+vector, pts[5]+vector);
                ////args.Display.DrawCircle(circle, Color.White, 2);

                ////args.Display.DrawMarker(pt, vector*3, Color.Black, 2);
                ////args.Display.Draw2dText(value, Color.Black, pt + (vector * 3), false);

                //Point3d pt4 = pt + (vector * 8);
                //plane.Origin = pt4;
                //Rhino.Display.Text3d text3d_A = new Rhino.Display.Text3d(value, plane, 80);
                //BoundingBox bb_A = text3d_A.BoundingBox;
                ////Point3d[] pts_A = bb_A.GetCorners();
                ////Line line = new Line(pt,pt+(vector*5));
                //Vector3d vec = bb_A.Center - pt;
                //Line line = new Line(pt, pt + (vec * 0.75));
                //args.Display.DrawLine(line, Color.Black, 2);
                //args.Display.Draw3dText(text3d_A, Color.Black, pt4);
                //Circle dot = new Circle(plane, pt, 10);
                //args.Display.DrawCircle(dot, Color.Black, 2);
                ////args.Display.DrawBoxCorners(bb_A, Color.Black,100);

            }
        }
        #endregion
    }
}
