﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel.Types;
using Donkey.Extensions;
using Donkey.Model.Geometry;
using Grasshopper.Kernel;
using Donkey.Model.Analytical.Data;
using Donkey.Interface;
using Donkey.Model.Nodal.AnalyticalData;

namespace Donkey.Model.Analytical
{
    public class AnalyticalModel : Model_Goo
    {
        #region FIELD
        //APPEND DATA IN VTU FILE
        public DataList<Load> Loads { get; set; }
        public DataList<NodalDOF> Supports { get; set; }
        public DataList<Material> Materials { get; set; }
        public DataList<Profile> Profiles { get; set; }
        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        public AnalyticalModel()
            : base()
        { InitData(); }
        //DATA CONSTRUCTOR
        public AnalyticalModel(DataList<Node> nodes, DataList<Geometry_Goo> geometries, DataList<Element> elements)
            : base(nodes, geometries, elements)
        { InitData(); }
        //COPY CONSTRUCTOR
        public AnalyticalModel(AnalyticalModel instance)
            : base(instance)
        {
            this.Loads = instance.Loads;
            this.Supports = instance.Supports;
            this.Materials = instance.Materials;
            this.Profiles = instance.Profiles;
        }
        //INIT DATA
        private void InitData()
        {
            this.Profiles = new DataList<Profile>();
            this.Materials = new DataList<Material>();
            this.Supports = new DataList<NodalDOF>();
            this.Loads = new DataList<Load>();
            this.Loads.Insert(0, new DeadWeight());
            //this.Profiles.Insert(0, Profile.None);
            //this.Materials.Insert(0, Material.None);
        }
        #endregion

        #region METHODS

        #endregion

        #region OVERRIDE
        public override IGH_Goo Duplicate()
        {
            return new AnalyticalModel(this);
        }

        public override string ToString()
        {
            //return "Scale: " + this.Displacement.scale + "\n First node:" + this.Nodes[0].ToString() +"\n Geometry center:" + this.Geometries[0].GetCenter().ToString() +"\n Model rating, min and max rate/value of other data";
            string str = "";
            if (this.Name != "")
                str = "Model: " + this.Name + Environment.NewLine +"------"+ Environment.NewLine;
            foreach (AnalyticalElement element in this.Elements)
            {
                str += element.ToString();
               //if (element.DeadWeight >= 0)
                    //str += "Load: Deadweight" + Environment.NewLine;
             foreach (int id in element.Geometries_ID)
            {
                List<int> nodes = base.Geometries[id].Nodes_ID;
                foreach (int n in nodes)
                {
                    if (base.Nodes[n].Support >= 0 && this.Supports.Count > base.Nodes[n].Support)
                        str += "Nodal_support: " + this.Supports[base.Nodes[n].Support].ToString() + " (" + n + ")" + Environment.NewLine; //+ this.Supports[base.Nodes[n].Support].Name +" "

                    if (base.Nodes[n].Load >= 0 && this.Loads.Count > base.Nodes[n].Load)
                        str += this.Loads[base.Nodes[n].Load].ToString() + " (" + n + ")" + Environment.NewLine;

                    if (base.Nodes[n].Hinge > 0)
                        str += "Full hinge: " + base.Nodes[n].ToString() + " (" + n + ")" + Environment.NewLine;
                }
                //node.
            }
                    str += Environment.NewLine;
            }
            return str;
            //return this.Value.ToString(); //"AnalyticalModel print";
        }

        //public override string TypeDescription
        //{
        //    get { return "DONKEY model of analysis result"; }
        //}

        //public override string TypeName
        //{
        //    get { return "Result"; }
        //}

        //public void SetNodes(DataList<Node> nodes)
        //{
        //    base.Nodes = nodes;
        //}

        //public void SetElements(DataList<Element> elements)
        //{
        //    base.Elements = elements;
        //}
        #endregion

        #region PREVIEW
        //GEOMETRY PROFILE
        public void DrawGeometryProfile(GH_PreviewWireArgs args)
        {
            for (int i = 0; i < this.Geometries.Count; i++)
            {
                this.Geometries[i].DrawProfile(args);
            }
        }
        #endregion
    }
}
