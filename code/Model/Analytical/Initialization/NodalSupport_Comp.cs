﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel.Data;
using Rhino.Geometry;
using System.Drawing;
using System.Windows.Forms;
using Grasshopper;
using Microsoft.VisualBasic.CompilerServices;
using System.Runtime.CompilerServices;
using Donkey.Model.Analytical.Data;


namespace Donkey.Model.Nodal.AnalyticalData
{
    public class NodalSupport_Comp : GH_Component
    {
        #region FIELD
        private NodalDOF m_dof;
        private bool m_custom;
        private Support m_support;
        #endregion

        #region CONSTRUCTOR
        public NodalSupport_Comp()
            : base("NodalSupport", "Support", "Nodal support defined by DOF" 
            //+ Donkey.Info.Mark
            , "Donkey", "2.Conditions")
        {
            this.m_dof = new NodalDOF();
            this.m_custom = false;
        }
        #endregion

        #region OVERRIDE
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddPointParameter("Node", "N", "Point as model node", GH_ParamAccess.item);
            pManager.HideParameter(0);
            pManager.AddParameter(new NodalDOF_Param());
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new AnalyticalModelData_Param());
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //READ INPUT PARAMS
            Point3d node = Point3d.Unset;
            bool m_node = DA.GetData("Node", ref node);

            DA.GetData<NodalDOF>("DOF", ref this.m_dof); 
            //this.NickName = m_dof.ToString();

            this.m_support = new Support(node,m_dof);
            DA.SetData(0, m_support);
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("E74D17F5-3624-4FF9-B8D1-E40F851511A1"); }
        }

        public override BoundingBox ClippingBox
        {
            get
            {
                base.ClippingBox.Union(this.m_support.BBox);
                return base.ClippingBox;
            }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.support_nodal; }

        }
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }
        #endregion

        #region METHODS MY
        //private void SetDOF(Object sender, EventArgs e)
        //{
        //    switch (sender.ToString())
        //    {
        //        case "Full":
        //            this.m_dof = new NodalDOF();
        //            this.m_custom = false;
        //            break;
        //        case "Y_Lock":
        //            this.m_dof = new NodalDOF(0,1,0,0,0,0);
        //            this.m_custom = false;
        //            break;
        //        case "Full_T":
        //            this.m_dof = new NodalDOF(1,1,1,0,0,0);
        //            this.m_custom = false;
        //            break;
        //        case "Full_R":
        //            this.m_dof = new NodalDOF(0,0,0,1,1,1);
        //            this.m_custom = false;
        //            break;
        //        case "None":
        //            this.m_dof = new NodalDOF(0, 0, 0, 0, 0, 0);
        //            this.m_custom = false;
        //            break;
        //        case "Custom":
        //            //TODO int from NickName
        //            Load_NickNameToDOF();
        //            //this.m_dof = new GH_DOF((int)s.Substring(0, 1), (int)s.Substring(1, 1), (int)s.Substring(2, 1), (int)s.Substring(4, 1), (int)s.Substring(5, 1), (int)s.Substring(6, 1));
        //            //TODO event reload
        //            //this.m_dof = new GH_DOF();
        //            this.m_custom = true;
        //            break;

        //        default:
        //            this.m_dof = new NodalDOF();
        //            break;
        //    }
        //    this.ExpireSolution(true);
        //}

        //private void Load_NickNameToDOF()
        //{
        //    if (m_custom)
        //    {
        //        string s = this.NickName;
        //        //CharEnumerator em = s.GetEnumerator();
        //        //Rhino.RhinoApp.WriteLine(em.Current.ToString());
        //    }
        //}

        //protected void Menu_AppendObjectNameMY(ToolStripDropDown menu)
        //{
        //    ToolStripTextBox item = new ToolStripTextBox(this.NickName)
        //    {
        //        Text = this.NickName,
        //        Tag = this.NickName
        //    };
        //    if (this is IGH_ActiveObject)
        //    {
        //        item.Enabled = ((IGH_ActiveObject)this).MutableNickName;
        //    }
        //    //item.TextChanged += new EventHandler(this.Menu_NameItemTextChanged);
        //    item.KeyDown += new KeyEventHandler(this.Menu_NameItemKeyDownMY);
        //    menu.Items.Add(item);
        //}

        //private void Menu_NameItemKeyDownMY(object sender, KeyEventArgs e)
        //{
        //    try
        //    {
        //        ToolStripTextBox item = (ToolStripTextBox)sender;
        //        switch (e.KeyCode)
        //        {
        //            case Keys.Escape:
        //                this.NickName = string.Format("{0}", RuntimeHelpers.GetObjectValue(item.Tag));
        //                break;

        //            case Keys.Return:
        //                this.OnObjectChanged(GH_ObjectEventType.NickNameAccepted);
        //                //TODO read NickName to load DOF
        //                Load_NickNameToDOF();
        //                break;

        //            default:
        //                return;
        //        }
        //        item.Owner.Hide();
        //        this.Attributes.GetTopLevel.ExpireLayout();
        //        //GH_InstanceServer.RedrawCanvas();
        //        Instances.RedrawCanvas();
        //    }
        //    catch (Exception exception1)
        //    {
        //        ProjectData.SetProjectError(exception1);
        //        Exception ex = exception1;
        //        ProjectData.ClearProjectError();
        //    }
        //}

        //public override bool AppendMenuItems(ToolStripDropDown menu)
        //{

        //    //return base.AppendMenuItems(menu);
        //    //Menu_AppendItem(menu, "jozko", hendlrer, true, false);
        //    //Menu_AppendItem(menu, "Full", SetDOF, null, true, m_dof.ToString() == "111/111",);
        //    Menu_AppendGenericMenuItem(menu, "Full", SetDOF, this.Icon, null, true, m_dof.ToString() == "111/111");
        //    Menu_AppendGenericMenuItem(menu, "Y_Lock", SetDOF, this.Icon, null, true, m_dof.ToString() == "010/000");
        //    Menu_AppendGenericMenuItem(menu, "Full_T", SetDOF, this.Icon, null, true, m_dof.ToString() == "111/000");
        //    Menu_AppendGenericMenuItem(menu, "Full_R", SetDOF, this.Icon, null, true, m_dof.ToString() == "000/111");
        //    Menu_AppendGenericMenuItem(menu, "None", SetDOF, this.Icon, null, true, m_dof.ToString() == "000/000");
        //    Menu_AppendGenericMenuItem(menu, "Custom", SetDOF, this.Icon, null, true, m_custom);
        //    Menu_AppendObjectName(menu);
        //    //Menu_AppendObjectMyName(menu);

        //    return true;
        //}
        #endregion

    }
}