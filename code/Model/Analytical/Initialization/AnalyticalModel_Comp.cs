﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using Donkey.Model.Analytical.Data;
using System.Windows.Forms;
using Grasshopper.Kernel.Data;
using Donkey.Model.Geometry;
using Donkey.Model.Nodal.AnalyticalData;

namespace Donkey.Model.Analytical.Initialization
{
    public class AnalyticalModel_Comp : GH_Component
    {
        private bool m_removeNodeDuplicates;
        private bool m_removeGeometryDuplicates;
        private bool m_Z_Support;
        private bool m_preview_nodes;
        private bool m_preview_elements;
        private bool m_global_dead_load;
        AnalyticalModel finalModel;

        /// <summary>
        /// Initializes a new instance of the AnalyticalModel_Comp class.
        /// </summary>
        public AnalyticalModel_Comp()
            : base("Model", "Model",
                "Analytical model",// + Donkey.Info.Mark,
                "Donkey", "3.Analysis")
        {
            finalModel = new AnalyticalModel();
            this.m_removeNodeDuplicates = true;
            this.m_removeGeometryDuplicates = true;
            this.m_Z_Support = false;
            this.m_preview_nodes = true;
            this.m_preview_elements = true;
            this.m_global_dead_load = true;
            this.ObjectChanged += AnalyticalModel_Comp_ObjectChanged;
            this.Message = this.NickName;
        }

        void AnalyticalModel_Comp_ObjectChanged(IGH_DocumentObject sender, GH_ObjectChangedEventArgs e)
        {
            //this.finalModel.Name = this.NickName;
            this.Message = this.NickName;
            //this.Params.Output[0].
            ////this.Params.Output[0].ExpireSolution(true);
            //this.Params.Output[0].ComputeData();
            //this.Params.Output[0].CollectData();
            ////this.Params.Output[0].CreateProxySources();
            //this.Params.Output[0].ExpirePreview(true);
            ////this.Params.Output[0].CreateAttributes();
            //this.ExpireSolution(true);
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new AnalyticalElement_Param(), "Element(s)", "E", "Model element(s)", GH_ParamAccess.list);
            pManager[0].DataMapping = GH_DataMapping.Flatten;
            pManager.AddParameter(new AnalyticalModelData_Param(), "Conditions", "C", "Analytical model conditions" + Environment.NewLine + "(supports, loads, hinges, ect.)", GH_ParamAccess.list);
            pManager[1].Optional = true;
            pManager[1].DataMapping = GH_DataMapping.Flatten;
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new AnalyticalModel_Param());
            //pManager.AddTextParameter("S", "s", "sss", GH_ParamAccess.item);
        }

        protected override void BeforeSolveInstance()
        {
            this.Message = NickName;
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //READ ELEMENTS
            //GH_Structure<AnalyticalModel> models_tree = new GH_Structure<AnalyticalModel>(); 
            List<AnalyticalModel> models = new List<AnalyticalModel>();
            if (!DA.GetDataList<AnalyticalModel>(0, models))
                return;
            //MERGE ELEMENTS' MODELS TO ONE MODEL

            AnalyticalModel curentModel;
            finalModel = new AnalyticalModel();

            //models_tree.Simplify(GH_SimplificationMode.CollapseAllOverlaps);
            //models_tree.Flatten(null);
            //models = models_tree.FlattenData();

            int duplicatedElements = 0;
            for (int m = 0; m < models.Count; m++)
            {
                curentModel = new AnalyticalModel(models[m]);

                for (int e = 0; e < curentModel.Elements.Count; e++)
                {
                    if (!curentModel.Elements[e].CloneToModel(finalModel, this.m_removeGeometryDuplicates, this.m_removeNodeDuplicates))
                        duplicatedElements++;
                }
            }
            if (duplicatedElements > 0)
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, duplicatedElements + " duplicated elements were removed!");

            //READ NODAL BOUNDARY CONDITION
            List<AnalyticalModelData> dataList = new List<AnalyticalModelData>();
            DA.GetDataList<AnalyticalModelData>(1, dataList);

            //ADD Z nodal supports tp dataList..
            if (m_Z_Support)
            {
                //for each nodes in Z <= 0 create support
                foreach (Node node in finalModel.Nodes)
                {
                    if (node.Z <= 0)
                        //add support to dataList
                        dataList.Add(new Support(node, new NodalDOF()));
                }
            }

            if(!m_global_dead_load)
                foreach (AnalyticalElement element in finalModel.Elements)
                {
                    element.DeadWeight = -1;
                }

            int appendFailed = 0;
            foreach (AnalyticalModelData data in dataList)
            {
                if (!data.AppendData(finalModel))
                    appendFailed++;
            }
            if (appendFailed > 0)
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, appendFailed + " append data cannot find refered geometry!");
            //}

            if (finalModel.Supports.Count <= 0)
            {
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "No support is defined in the model!");
            }

            finalModel.Name = this.NickName;
            DA.SetData(0, finalModel);
        }

        public override void ClearData()
        {
            this.finalModel = null;
            base.ClearData();
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.model; }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{69157943-f4ff-40da-859e-b650040df44e}"); }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }

        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetBoolean("remove_node_duplicates", this.m_removeNodeDuplicates);
            writer.SetBoolean("remove_geometry_duplicates", this.m_removeGeometryDuplicates);
            writer.SetBoolean("set_z_nodal_supports", this.m_Z_Support);
            writer.SetBoolean("preview_nodes", this.m_preview_nodes);
            writer.SetBoolean("preview_elements", this.m_preview_elements);
            writer.SetBoolean("global_dead_load", this.m_global_dead_load);
            return base.Write(writer);
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            if (reader.ItemExists("remove_node_duplicates"))
                this.m_removeNodeDuplicates = reader.GetBoolean("remove_node_duplicates");
            if (reader.ItemExists("remove_geometry_duplicates"))
                this.m_removeGeometryDuplicates = reader.GetBoolean("remove_geometry_duplicates");
            if (reader.ItemExists("set_z_nodal_supports"))
                this.m_Z_Support = reader.GetBoolean("set_z_nodal_supports");
            if (reader.ItemExists("preview_nodes"))
                this.m_preview_nodes = reader.GetBoolean("preview_nodes");
            if (reader.ItemExists("preview_elements"))
                this.m_preview_elements = reader.GetBoolean("preview_elements");
            if (reader.ItemExists("global_dead_load"))
                this.m_global_dead_load = reader.GetBoolean("global_dead_load");
            return base.Read(reader);
        }

        public override bool IsBakeCapable
        {
            get
            {
                return false;
            }
        }
        #region PREVIEW

        public override void DrawViewportWires(IGH_PreviewArgs args)
        {
            base.DrawViewportWires(args);

            if (finalModel != null)
            {
                args.Display.DepthMode = Rhino.Display.DepthMode.AlwaysInFront;
                if(m_preview_nodes)
                    finalModel.DrawNodeIndex(args);
                if(m_preview_elements)
                    finalModel.DrawElementIndex(args);
                args.Display.DepthMode = Rhino.Display.DepthMode.Neutral;
            }
        }
        #endregion

        #region MENU
        //public override bool AppendMenuItems(ToolStripDropDown menu)
        //{
        //    this.Menu_AppendPreviewItem(menu);
        //    this.Menu_AppendEnableItem(menu);
        //    this.Menu_AppendRuntimeMessages(menu);
        //    GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
        //    this.AppendAdditionalMenuItems(menu);
        //    GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
        //    this.Menu_AppendObjectHelp(menu);
        //    return true;
        //}

        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Show Node ID", new EventHandler(this.Set_previewNodes), true, this.m_preview_nodes).ToolTipText = "Show nodes and their IDs.";
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Show Element ID", new EventHandler(this.Set_previewElements), true, this.m_preview_elements).ToolTipText = "Show elements IDs.";
            GH_DocumentObject.Menu_AppendSeparator(menu);
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Remove Nodal duplicates", new EventHandler(this.RemoveNodeDuplicates), true, this.m_removeNodeDuplicates).ToolTipText = "Remove node's duplicates in the model";
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Remove Geometrical duplicates", new EventHandler(this.RemoveGeometryDuplicates), true, this.m_removeGeometryDuplicates).ToolTipText = "Remove geometry duplicates in the model";
            GH_DocumentObject.Menu_AppendSeparator(menu);
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Supports from ground nodes", new EventHandler(this.Set_Z_Support), true, this.m_Z_Support).ToolTipText = "Supports all nodes below or equal to 0 in Z coordinate";
            GH_DocumentObject.Menu_AppendSeparator(menu);
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Dead load (global)", new EventHandler(this.Set_Global_DeadLoad), true, this.m_global_dead_load).ToolTipText = "Override the dead load (self-weight) for each element.";
        
        }

        private void Set_Global_DeadLoad(object sender, EventArgs e)
        {
            this.RecordUndoEvent("GlobalDeadLoad");
            this.m_global_dead_load = !m_global_dead_load;
            this.ExpireSolution(true);
        }

        private void Set_previewNodes(object sender, EventArgs e)
        {
            this.RecordUndoEvent("PreviewNodesID");
            this.m_preview_nodes = !m_preview_nodes;
            this.ExpirePreview(true);
        }

        private void Set_previewElements(object sender, EventArgs e)
        {
            this.RecordUndoEvent("PreviewElementsID");
            this.m_preview_elements = !m_preview_elements;
            this.ExpirePreview(true);
        }

        private void RemoveNodeDuplicates(object sender, EventArgs e)
        {
            this.RecordUndoEvent("RemoveNodeDuplicatesInModel");
            this.m_removeNodeDuplicates = !m_removeNodeDuplicates;
            this.ExpireSolution(true);
            //this.ExpirePreview(true);
        }

        private void RemoveGeometryDuplicates(object sender, EventArgs e)
        {
            this.RecordUndoEvent("RemoveGeometryDuplicatesInModel");
            this.m_removeGeometryDuplicates = !m_removeGeometryDuplicates;
            this.ExpireSolution(true);
            //this.ExpirePreview(true);
        }

        private void Set_Z_Support(object sender, EventArgs e)
        {
            this.RecordUndoEvent("Set_Z_Support");
            this.m_Z_Support = !this.m_Z_Support;
            this.ExpireSolution(true);
        }

        #endregion

    }
}