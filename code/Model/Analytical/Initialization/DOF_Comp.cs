﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Donkey.Model.Nodal.AnalyticalData;
using Grasshopper.GUI.Canvas;
using System.Drawing;
using System.Windows.Forms;
using Grasshopper.Kernel.Attributes;

namespace Donkey.Model.Analytical.Initialization
{
    /// <summary>
    /// This is testing DOF componene (no param) with specific attributes
    /// </summary>
    public class DOF_Comp : GH_Component
    {
        public NodalDOF m_dof;

        public DOF_Comp()
            : base("DOF", "DOF", "Degree of freedom", "Donkey", "Testing")
            {
                this.m_dof = new NodalDOF();
            }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new NodalDOF_Param());
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            DA.SetData(0, this.m_dof);
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("9801C04E-3E69-454D-B9E9-C4FF97EE27F7"); }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.hidden; }
        }

        public override void CreateAttributes()
        {
            m_attributes = new DOF_Attributes(this);
        }

        //protected override void CollectVolatileData_Custom()
        //{
        //    VolatileData.Clear();
        //    m_data.Append(new NodalDOF(m_dof));
        //}

        //protected override void OnComponentLoaded()
        //{
        //    this.m_dof = new NodalDOF();
        //    this.ExpireSolution(true);
        //}

        public override void CollectData()
        {
            base.CollectData();
            this.ExpireSolution(true);
        }
    }

    public class DOF_Attributes : GH_Attributes<DOF_Comp>
    {
        public DOF_Attributes(DOF_Comp owner) : base(owner) { }

        public override bool HasInputGrip { get { return false; } }
        public override bool HasOutputGrip { get { return true; } }

        #region LAYOUT
        private RectangleF TextArea
        {
            get
            {
                RectangleF rec = Bounds;
                rec.Width -= 32;
                rec.X = rec.Left + rec.Height;
                return rec;
            }
        }
        private RectangleF ButtonArea
        {
            get
            {
                RectangleF rec = Bounds;
                rec.X = rec.Left; //rec.Right +rec.Height;
                rec.Width = rec.Height;
                rec.Inflate(-6, -6);
                return rec;
            }
        }

        protected override void Layout()
        {
            Bounds = new System.Drawing.RectangleF(Pivot, new SizeF(112, 24));
        }
        #endregion

        #region RENDER
        protected override void Render(GH_Canvas canvas, Graphics graphics, GH_CanvasChannel channel)
        {
            if (channel == GH_CanvasChannel.Objects)
            {
                GH_Capsule capsule = GH_Capsule.CreateCapsule(Bounds, GH_Palette.White);
                capsule.AddOutputGrip(OutputGrip);
                capsule.Render(graphics, Selected, Owner.Locked, true);
                capsule.Dispose();

                GH_Capsule button = GH_Capsule.CreateCapsule(ButtonArea, GH_Palette.Grey);
                button.Render(graphics, Selected, Owner.Locked, false);
                button.Dispose();

                graphics.DrawString(Owner.Name,
                                    Grasshopper.Kernel.GH_FontServer.Standard,
                                    Brushes.Black,
                                    TextArea,
                                    Grasshopper.GUI.GH_TextRenderingConstants.NearCenter);
            }
        }
        #endregion

        #region MENU
        //Sender - handler pre menu
        public override GH_ObjectResponse RespondToMouseDown(GH_Canvas sender, Grasshopper.GUI.GH_CanvasMouseEvent e)
        {
            if (ButtonArea.Contains(e.CanvasLocation))
            {
                ToolStripDropDown menu = new ToolStripDropDown();
            //base.AppendAdditionalMenuItems(menu);
                ToolStripMenuItem toolStripMenuItem1 = GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Full", SetDOF, true, this.Owner.m_dof.ToString() == "111/111");
            toolStripMenuItem1.ToolTipText = "Rectangle cross section profile for Donkey." + Environment.NewLine + "If thikness is more that 0, then create tube profile";

            ToolStripMenuItem toolStripMenuItem2 = GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Y_Lock", SetDOF, true, this.Owner.m_dof.ToString() == "010/000");
            toolStripMenuItem2.ToolTipText = "Circle cross section profile for Donkey." + Environment.NewLine + "If thikness is more that 0, then create tube profile";

            menu.Show(sender, e.ControlLocation);
                return GH_ObjectResponse.Handled;
            }

            return base.RespondToMouseDown(sender, e);
        }

        private void SetDOF(Object sender, EventArgs e)
        {
            switch (sender.ToString())
            {
                case "Full":
                    Owner.m_dof = new NodalDOF();
                    //this.m_custom = false;
                    break;
                case "Y_Lock":
                    Owner.m_dof = new NodalDOF(0, 1, 0, 0, 0, 0);
                    //this.m_custom = false;
                    break;
                case "Full_T":
                    Owner.m_dof = new NodalDOF(1, 1, 1, 0, 0, 0);
                    //this.m_custom = false;
                    break;
                case "Full_R":
                    Owner.m_dof = new NodalDOF(0, 0, 0, 1, 1, 1);
                    //this.m_custom = false;
                    break;
                case "None":
                    Owner.m_dof = new NodalDOF(0, 0, 0, 0, 0, 0);
                    //this.m_custom = false;
                    break;
                case "Custom":
                    //TODO int from NickName
                    //Load_NickNameToDOF();
                    //this.m_dof = new GH_DOF((int)s.Substring(0, 1), (int)s.Substring(1, 1), (int)s.Substring(2, 1), (int)s.Substring(4, 1), (int)s.Substring(5, 1), (int)s.Substring(6, 1));
                    //TODO event reload
                    //this.m_dof = new GH_DOF();
                    //this.m_custom = true;
                    break;

                default:
                    Owner.m_dof = new NodalDOF();
                    break;
            }
            Owner.ExpireSolution(true);
        }
        #endregion

    }
}
