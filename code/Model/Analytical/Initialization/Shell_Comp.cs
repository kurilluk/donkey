﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using Donkey.Model.Analytical.Data;
using Grasshopper.Kernel.Types;
using Rhino.Collections;
using Donkey.Model.Geometry;
using Rhino;
using Rhino.DocObjects;

namespace Donkey.Model.Analytical.Initialization
{
    public class Shell_Comp : GH_Component
    {
        #region FIELD
        Rhino.UnitSystem actual_units;
        #endregion

        /// <summary>
        /// Initializes a new instance of the Shell_Comp class.
        /// </summary>
        public Shell_Comp()
            : base("Shell", "Shell",
                "Shell element", // + Donkey.Info.Mark,
                "Donkey", "1.Elements")
        {
            Rhino.RhinoDoc.DocumentPropertiesChanged += new EventHandler<Rhino.DocumentEventArgs>(this.ReloadSetup); //new EventHandler<Rhino.DocumentEventArgs>(
            this.actual_units = Rhino.RhinoDoc.ActiveDoc.ModelUnitSystem;
            this.ObjectChanged += Shell_Comp_ObjectChanged;
        }

        void Shell_Comp_ObjectChanged(IGH_DocumentObject sender, GH_ObjectChangedEventArgs e)
        {
            this.Message = this.NickName;
        }

        private void ReloadSetup(object sender, DocumentEventArgs e)
        {
            if (e.Document.ModelUnitSystem != this.actual_units)
            {
                this.actual_units = e.Document.ModelUnitSystem;
                this.ExpireSolution(true);
            }
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            //pManager.AddBrepParameter("Geometry", "G", "Set shell shape", GH_ParamAccess.item);
            pManager.AddGeometryParameter("Geometry", "G", "Set planar shell shape", GH_ParamAccess.item);
            pManager.HideParameter(0);
            pManager.AddNumberParameter("Thickness", "T", "Shell thickness", GH_ParamAccess.item,10.0);
            pManager.AddParameter(new Material_Param(), "Material", "M", "Element material", GH_ParamAccess.item);
            pManager[2].Optional = true;
            pManager[1].Optional = true;
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new AnalyticalElement_Param());
            //pManager.AddMeshParameter("M", "M", "M", GH_ParamAccess.item);
            //pManager.AddPointParameter("P", "p", "p", GH_ParamAccess.list);
            //pManager.AddCurveParameter("c", "c", "c", GH_ParamAccess.list);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            if(this.actual_units != UnitSystem.Millimeters)
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Please change your model units to mm.");

            //READ INPUT PARAMS
            //GH_Brep brep = new GH_Brep();
            //if (!DA.GetData("Geometry", ref brep))
            //    return;
            GeometryBase geo = null;
            //IGH_GeometricGoo geo = null;
            GH_Brep brep = new GH_Brep();
            if (!DA.GetData("Geometry", ref geo))
                return;
            //read Profile input
            double profileThickness = 10.0;
            DA.GetData(1, ref profileThickness);
            //read Material input
            Material_Goo material = new Material_Goo();
            DA.GetData(2, ref material);

            //SET COMMON MODEL
            AnalyticalModel final_model = new AnalyticalModel();

            //ANALYSE GEO TYPE
            bool shell_added = false;
            switch (geo.ObjectType)
            {
                case(ObjectType.Curve):
                    shell_added = Shell_from_Curve((Curve)geo, ref final_model, material, profileThickness);
                    break;
                case(ObjectType.Surface):
                    shell_added = Shell_from_Brep(Brep.CreateFromSurface(((Surface)geo)), ref final_model, material, profileThickness);
                    break;
                case(ObjectType.Brep):
                    shell_added = Shell_from_Brep((Brep)geo, ref final_model, material, profileThickness);
                    break;
                case(ObjectType.Mesh):
                    //this.AddRuntimeMessage(GH_RuntimeMessageLevel.Remark, "Comming soon");
                    Mesh mesh = (Mesh)geo;
                    if(mesh == null)
                        break;
                    //Rhino.Geometry.Collections.MeshVertexList vertices = ((Mesh)geo).Vertices;
                    //Rhino.Geometry.Collections.MeshTopologyEdgeList edge_list = ((Mesh)geo).TopologyEdges;
                    Rhino.Geometry.Collections.MeshVertexList top_vertices = mesh.Vertices;
                    Rhino.Geometry.Collections.MeshFaceList faces = mesh.Faces;
                    mesh.FaceNormals.ComputeFaceNormals();
                    Rhino.Geometry.Collections.MeshFaceNormalList normals = mesh.FaceNormals;

                    //for (int f = 0; f < faces.Count; f++ )
                    //{
                    //    List<int> nodes_id = new List<int>();
                    //    nodes_id.Add(final_model.Nodes.AppendData((Point3d)top_vertices[faces[f].A], true)); //HACK don't control duplicates (use topology from mesh as nodes_id, verticles add as nodes to model)
                    //    nodes_id.Add(final_model.Nodes.AppendData((Point3d)top_vertices[faces[f].B], true));
                    //    nodes_id.Add(final_model.Nodes.AppendData((Point3d)top_vertices[faces[f].C], true));
                    //    if(faces[f].IsQuad)
                    //        nodes_id.Add(final_model.Nodes.AppendData((Point3d)top_vertices[faces[f].D], true));
                    //    //new vtk_pixel, else new vtk_triangle > add to VTK geometry for shell element
                    //}

                    //DIRECLY LOAD MESH TO MODEL (work only for one individal mesh in the model)
                    foreach (Point3d vertex in top_vertices)
                    {
                        final_model.Nodes.Add(vertex); //HACK add cast from Point3f (or list)
                    }

                    int i = 0;
                    List<int> VTK_geometry = new List<int>();
                    foreach (MeshFace face in faces)
                    {
                        List<int> nodes_id = new List<int>();
                        nodes_id.Add(face.A);
                        nodes_id.Add(face.B);
                        nodes_id.Add(face.C);
                        if (face.IsQuad)
                        {
                            nodes_id.Add(face.D);
                            //create vtk_quad
                            VTK_geometry.Add(final_model.Geometries.AppendData(new VTK_Quad(nodes_id, final_model, normals[i]), false));
                        }
                        else
                        {
                            //create vtk_triangle
                            VTK_geometry.Add(final_model.Geometries.AppendData(new VTK_Triangle(nodes_id, final_model, normals[i]), false));
                        }
                        i++;
                    }
          
                    Profile profile = new Profile(Profile.types.cs, 0, 0, profileThickness);
                    AnalyticalElement element = new AnalyticalElement(VTK_geometry, profile, material.Value, final_model);
                    element.Name = NickName;
                    final_model.Elements.AppendData(element, false);
                    shell_added = true;
                    break;
                default:
                    this.AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Unsupported geometry: "+geo.ObjectType +"!");
                    //this.Message = "";
                    return;
                    //break;
            }
            //this.Message = geo.ObjectType.ToString();

            //if (final_model.Elements.Count == 0)
            //{
            //    this.AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Shell element was not created!");
            //    return;
            //}
            if (!shell_added)
                DA.SetData(0, null);
                //this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Shell element was not created!");


            DA.SetData(0, final_model);
        }

        private bool Shell_from_Brep(Brep brep, ref AnalyticalModel model, Material_Goo material, double profileThickness)
        {
            bool created = true;
            foreach(Surface srf in brep.Surfaces)
            {
                if (!srf.IsPlanar())
                    this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Sufrace is not planar!");
            }
            Curve[] curves = brep.GetWireframe(-1);
            foreach (Curve curve in (Curve.JoinCurves(curves)))
            {
                if (!Shell_from_Curve(curve, ref model, material, profileThickness))
                    created = false;
            }
            return created;
        }

        private bool Shell_from_Curve(Curve crv, ref AnalyticalModel model, Material_Goo material, double profileThickness)
        {
            if (crv.IsClosed)
            {
                List<int> nodes_id = ConvertToVTK_Polygon(crv, model);

                Plane local_plane;
                //bool planar = crv.IsPlanar(Rhino.RhinoDoc.ActiveDoc.ModelAbsoluteTolerance);
                if (!crv.TryGetPlane(out local_plane, Rhino.RhinoDoc.ActiveDoc.ModelAbsoluteTolerance))
                {
                    this.AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Geometry is not planar!");
                    return false;
                }
                List<int> VTK_geometry = new List<int>();
                VTK_geometry.Add(model.Geometries.AppendData(new VTK_Polygon(nodes_id, model, local_plane), false));

                Profile profile = new Profile(Profile.types.cs, 0, 0, profileThickness);
                AnalyticalElement element = new AnalyticalElement(VTK_geometry, profile, material.Value, model);
                model.Elements.AppendData(element, false);
                return true;
            }
            else
            {
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Outline is not closed!");
                return false;
            }
 
        }

        private List<int> ConvertToVTK_Polygon(Curve crv, AnalyticalModel model)
        {
            //List<int> geo_id = new List<int>();
            List<int> nodes_id = new List<int>();
            List<Curve> segments = new List<Curve>();
            CurveSegments(segments, crv, true);
            foreach (Curve segment in segments)
            {
                //FOR EACH CURVES
                if (!segment.IsLinear())
                {
                    double[] parameters;
                    double length = segment.GetLength();
                    if (length < 1500)
                    {
                        parameters = segment.DivideByCount(3, true);
                    }
                    else
                    {
                        int count = (int)Math.Round(length / 500);
                        parameters = segment.DivideByCount(count, true);
                    }
                    for (int t = 0; t < parameters.Length; t++) // -1 removed, circle bug
                    {
                        nodes_id.Add(model.Nodes.AppendData(segment.PointAt(parameters[t]), true));
                        //nodes_id.Add(model.Nodes.AppendData(segment.PointAt(parameters[t + 1]), true));
                        //VTK_Line geo = new VTK_Line(nodes_id, model);
                        //geo_id.Add(model.Geometries.AppendData(geo, false));
                        //nodes_id = new List<int>();
                    }

                }
                //FOR EACH LINES
                else
                {
                    nodes_id.Add(model.Nodes.AppendData(segment.PointAtStart, true));
                    //nodes_id.Add(model.Nodes.AppendData(segment.PointAtEnd, true));
                    //VTK_Line geo = new VTK_Line(nodes_id, model);
                    //geo_id.Add(model.Geometries.AppendData(geo, false));
                }
            }

            return nodes_id;
        }
        protected bool CurveSegments(List<Curve> L, Curve crv, bool recursive)
        {
            if (crv == null)
                return false;
            PolyCurve polyCurve = crv as PolyCurve;
            if (polyCurve != null)
            {
                if (recursive)
                    polyCurve.RemoveNesting();
                Curve[] curveArray = polyCurve.Explode();
                if (curveArray == null || curveArray.Length == 0)
                    return false;
                if (recursive)
                {
                    foreach (Curve crv1 in curveArray)
                        this.CurveSegments(L, crv1, recursive);
                }
                else
                {
                    foreach (Curve curve in curveArray)
                        L.Add(((GeometryBase)curve).DuplicateShallow() as Curve);
                }
                return true;
            }
            else
            {
                PolylineCurve polylineCurve = crv as PolylineCurve;
                if (polylineCurve != null)
                {
                    if (recursive)
                    {
                        for (int index = 0; index < polylineCurve.PointCount - 1; ++index)
                            L.Add((Curve)new LineCurve(polylineCurve.Point(index), polylineCurve.Point(index + 1)));
                    }
                    else
                        L.Add(((Curve)polylineCurve).DuplicateCurve());
                    return true;
                }
                else
                {
                    Polyline polyline;
                    if (crv.TryGetPolyline(out polyline))
                    {
                        if (recursive)
                        {
                            for (int index = 0; index < ((RhinoList<Point3d>)polyline).Count - 1; ++index)
                                L.Add((Curve)new LineCurve(((RhinoList<Point3d>)polyline)[index], ((RhinoList<Point3d>)polyline)[index + 1]));
                        }
                        else
                            L.Add((Curve)new PolylineCurve((IEnumerable<Point3d>)polyline));
                        return true;
                    }
                    else
                    {
                        LineCurve lineCurve = crv as LineCurve;
                        if (lineCurve != null)
                        {
                            L.Add(((Curve)lineCurve).DuplicateCurve());
                            return true;
                        }
                        else
                        {
                            ArcCurve arcCurve = crv as ArcCurve;
                            if (arcCurve != null)
                            {
                                L.Add(((Curve)arcCurve).DuplicateCurve());
                                return true;
                            }
                            else
                            {
                                NurbsCurve nurbsCurve = crv.ToNurbsCurve();
                                if (nurbsCurve == null)
                                    return false;
                                //  Interval domain1 = ((Curve) nurbsCurve).get_Domain();
                                //  // ISSUE: explicit reference operation
                                //  double num1 = ((Interval) @domain1).get_Min();
                                //  Interval domain2 = ((Curve) nurbsCurve).get_Domain();
                                //  // ISSUE: explicit reference operation
                                //  double max = ((Interval) @domain2).get_Max();
                                //  int count = L.Count;
                                //  double num2;
                                //  while (((Curve) nurbsCurve).GetNextDiscontinuity((Continuity) 7, num1, max, out num2))
                                //  {
                                //    Interval interval;
                                //    // ISSUE: explicit reference operation
                                //    ((Interval)@interval)..ctor(num1, num2);
                                //    // ISSUE: explicit reference operation
                                //    if (((Interval) @interval).get_Length() < 0.0 / 1.0)
                                //    {
                                //      num1 = num2;
                                //    }
                                //    else
                                //    {
                                //      Curve curve = ((Curve) nurbsCurve).DuplicateCurve().Trim(interval);
                                //      if (((CommonObject) curve).get_IsValid())
                                //        L.Add(curve);
                                //      num1 = num2;
                                //    }
                                //  }
                                //  if (L.Count == count)
                                L.Add((Curve)nurbsCurve);
                                //  return true;
                            }
                            return true;
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.shell; }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{da178a2d-20f0-4e36-b724-dca082c6eef8}"); }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }
    }
}