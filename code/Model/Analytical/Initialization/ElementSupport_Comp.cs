﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel.Data;
using Rhino.Geometry;
using System.Drawing;
using System.Windows.Forms;
using Grasshopper;
using Microsoft.VisualBasic.CompilerServices;
using System.Runtime.CompilerServices;
using Donkey.Model.Analytical.Data;
using Donkey.Model.Analytical;


namespace Donkey.Model.Nodal.AnalyticalData
{
    public class ElementSupport_Comp : GH_Component
    {
        #region FIELD
        private NodalDOF m_dof;
        private List<Support> m_supports;
        #endregion

        #region CONSTRUCTOR
        public ElementSupport_Comp()
            : base("ElementSupport", "Support", "Element support defined by DOF"// + Donkey.Info.Mark
            , "Donkey", "2.Conditions")
        {
            this.m_dof = NodalDOF.None;
            this.m_supports = new List<Support>();
        }
        #endregion

        #region OVERRIDE
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new AnalyticalElement_Param(), "Element", "E", "Model element", GH_ParamAccess.item);
            pManager.HideParameter(0);
            pManager.AddParameter(new NodalDOF_Param());
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new AnalyticalElement_Param());
            pManager.HideParameter(0);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //READ INPUT PARAMS
            AnalyticalModel element = null;
            bool m_node = DA.GetData(0, ref element);

            DA.GetData<NodalDOF>("DOF", ref this.m_dof);
            //this.NickName = m_dof.ToString();

            AnalyticalModel model = new AnalyticalModel();

            foreach (AnalyticalElement e in element.Elements)
            {
                e.CloneToModel(model,false,false);
            }

            foreach (AnalyticalElement el in model.Elements)
            {
                el.Support = this.m_dof;
                this.m_supports.Add(new Support(el.Center, m_dof));
            }

            DA.SetData(0,model);
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{3B365A33-4E96-4A40-B874-D6E9C4316BE0}"); }
        }

        public override BoundingBox ClippingBox
        {
            get { return base.ClippingBox; }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.support_element; }

        }
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }
        #endregion


        public override void ClearData()
        {
            base.ClearData();
            this.m_supports.Clear();
        }

        public override void DrawViewportWires(IGH_PreviewArgs args)
        {
            GH_PreviewWireArgs wireArgs;
            if (this.Attributes.GetTopLevel.Selected)
            {
                wireArgs = new GH_PreviewWireArgs(args.Viewport, args.Display, args.WireColour_Selected, 2);
            }
            else
            {
                wireArgs = new GH_PreviewWireArgs(args.Viewport, args.Display, args.WireColour, 1);
            }
            if (m_supports != null)
                foreach(Support support in m_supports)
                    support.DrawViewportWires(wireArgs);
        }
        
    }
}