﻿using System;
using System.Collections.Generic;
using Grasshopper.Kernel;
using Rhino.Geometry;
using Rhino.Collections;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel.Attributes;
using Donkey.Model;
using Donkey.Model.Geometry;
using Donkey.Model.Analytical.Data;
using Rhino.Runtime;
using System.Windows.Forms;
using Rhino;

namespace Donkey.Model.Analytical.Initialization
{
    public class Beam_Comp : GH_Component
    {
        #region FIELD
        Rhino.UnitSystem actual_units;
        private bool m_dummy;
        #endregion

        /// <summary>
        /// Initializes a new instance of the ElementBeam_Comp class.
        /// </summary>
        public Beam_Comp()
            : base("Beam", "Beam",
                "Beam element",// + Donkey.Info.Mark,
                "Donkey", "1.Elements")
        {
            this.m_dummy = false;
            Rhino.RhinoDoc.DocumentPropertiesChanged += new EventHandler<Rhino.DocumentEventArgs>(this.ReloadSetup); //new EventHandler<Rhino.DocumentEventArgs>(
            this.actual_units = Rhino.RhinoDoc.ActiveDoc.ModelUnitSystem;
            this.ObjectChanged += Beam_Comp_ObjectChanged;
            //if (this.IconDisplayMode == GH_IconDisplayMode.icon)
            this.Message = this.NickName;
            //this.IconDisplayMode = GH_IconDisplayMode.icon;
        }

        void Beam_Comp_ObjectChanged(IGH_DocumentObject sender, GH_ObjectChangedEventArgs e)
        {
            //if(this.IconCapableUI)
            //if(GH_Attributes<IGH_Param>.IsIconMode(this.IconDisplayMode)
            ///if (this.IconDisplayMode == GH_IconDisplayMode.icon)
                this.Message = this.NickName;
            //ExpireSolution(true);
        }

        private void ReloadSetup(object sender, DocumentEventArgs e)
        {
            if (e.Document.ModelUnitSystem != this.actual_units)
            {
                this.actual_units = e.Document.ModelUnitSystem;
                this.ExpireSolution(true);
            }
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddCurveParameter("Geometry", "G", "Set beam shape", GH_ParamAccess.item);
            pManager.HideParameter(0);
            pManager.AddParameter(new Profile_Param());
            pManager.AddParameter(new Material_Param(),"Material","M","Element material",GH_ParamAccess.item);
            pManager[2].Optional = true;
            pManager[1].Optional = true;
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new AnalyticalElement_Param());
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            this.ClearRuntimeMessages();

            if (this.actual_units != UnitSystem.Millimeters)
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Please change your model units to mm.");

            //READ INPUT PARAMS
            //List<GH_Curve> crvs = new List<GH_Curve>();
            GH_Curve crv = new GH_Curve();
            //if (!DA.GetDataList("Geometry", crvs))
            if (!DA.GetData("Geometry", ref crv))
            {
                DA.AbortComponentSolution();
                return;
            }
            //read Profile input
            Profile_Goo profile = new Profile_Goo();
            bool pro_loaded = DA.GetData(1, ref profile);
            //read Material input
            Material_Goo material = new Material_Goo();
            bool mat_loaded = DA.GetData(2, ref material);


            //SET COMMON MODEL
            AnalyticalModel model = new AnalyticalModel();
            //for (int i =0; i < crvs.Count; i++)
            //{
                ////CAST RHINO GEOMETRY TO DONKEY GEOMETRY (TODO DYNAMIC)
                //List<int> nodes_id = new List<int>(2);
                //nodes_id.Add(model.Nodes.AppendData(crv.Value.PointAtStart,true));
                //nodes_id.Add(model.Nodes.AppendData(crv.Value.PointAtEnd, true));
                ////REGISTER GEOMETRY TO MODEL
                //VTK_Line geo = new VTK_Line(nodes_id, model);

                //REGISTER GEOMETRY IN THE MODEL
                List<int> geo_id = ConvertToVTK(crv.Value, model);
                //geo_id.Add(model.Geometries.AppendData(geo,false));

                //REGISTER ELEMENT IN THE MODEL
             AnalyticalElement element;
             if (m_dummy)
             {
                 element = new Dummy_Element(geo_id, model);
                 this.AddRuntimeMessage(GH_RuntimeMessageLevel.Remark, "Dummy object");
                 //this.Message = "DUMMY";
             }
             else
             {
                 element = new AnalyticalElement(geo_id, profile.Value, material.Value, model);
                 //this.Message = "";
             }

             element.Name = this.NickName; //TODO in constructor?
             model.Elements.AppendData(element,false);
            //}

            DA.SetData(0, model);
        }

        protected override void BeforeSolveInstance()
        {
            //if (this.IconDisplayMode == GH_IconDisplayMode.icon)
            this.Message = NickName;
        }


        #region MENU

        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            ToolStripMenuItem toolStripMenuItem1 = GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Dummy", new EventHandler(this.Dummy_Click), true, this.m_dummy);
            toolStripMenuItem1.ToolTipText = "Create dummy element" + Environment.NewLine + "with none material and profile.";
        }

        private void Dummy_Click(object sender, EventArgs e)
        {
            this.RecordUndoEvent("Dummy");
            this.m_dummy = !this.m_dummy;
            this.ExpireSolution(true);
        }


        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetBoolean("dummy", this.m_dummy);
            return base.Write(writer);
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            this.m_dummy = reader.GetBoolean("dummy");
            return base.Read(reader);
        }

        #endregion

        /// <summary>
        /// Convert and register Rhino input geometry in the model
        /// </summary>
        /// <param name="crv">Input Rhino curve</param>
        /// <returns>Indexes of geometry in the model</returns>

        private List<int> ConvertToVTK(Curve crv, AnalyticalModel model)
        {
            List<int>geo_id = new List<int>();
            List<Curve> segments = new List<Curve>();
            CurveSegments(segments, crv, true);
            foreach (Curve segment in segments)
            {
                List<int> nodes_id = new List<int>();
                //FOR EACH CURVES
                if (!segment.IsLinear())
                {
                    double[] parameters;
                    double length = segment.GetLength();
                    if ( length < 1500)
                    {
                        parameters = segment.DivideByCount(3, true);
                    }
                    else
                    {
                        int count = (int)Math.Round(length / 500);
                        parameters = segment.DivideByCount(count, true);
                    }
                    for (int t = 0; t < parameters.Length - 1; t++)
                    {
                        nodes_id.Add(model.Nodes.AppendData(segment.PointAt(parameters[t]), true));
                        nodes_id.Add(model.Nodes.AppendData(segment.PointAt(parameters[t + 1]), true));
                        VTK_Line geo = new VTK_Line(nodes_id, model);
                        geo_id.Add(model.Geometries.AppendData(geo, false));
                        nodes_id = new List<int>();
                    }

                }
                //FOR EACH LINES
                else
                {
                    nodes_id.Add(model.Nodes.AppendData(segment.PointAtStart, true));
                    nodes_id.Add(model.Nodes.AppendData(segment.PointAtEnd, true));
                    VTK_Line geo = new VTK_Line(nodes_id, model);
                    geo_id.Add(model.Geometries.AppendData(geo, false));
                }
            }

            return geo_id;
        }
        protected bool CurveSegments(List<Curve> L, Curve crv, bool recursive)
    {
      if (crv == null)
        return false;
      PolyCurve polyCurve = crv as PolyCurve;
      if (polyCurve != null)
      {
        if (recursive)
          polyCurve.RemoveNesting();
        Curve[] curveArray = polyCurve.Explode();
        if (curveArray == null || curveArray.Length == 0)
          return false;
        if (recursive)
        {
          foreach (Curve crv1 in curveArray)
            this.CurveSegments(L, crv1, recursive);
        }
        else
        {
          foreach (Curve curve in curveArray)
            L.Add(((GeometryBase) curve).DuplicateShallow() as Curve);
        }
        return true;
      }
      else
      {
        PolylineCurve polylineCurve = crv as PolylineCurve;
        if (polylineCurve != null)
        {
          if (recursive)
          {
            for (int index = 0; index < polylineCurve.PointCount - 1; ++index)
              L.Add((Curve) new LineCurve(polylineCurve.Point(index), polylineCurve.Point(index + 1)));
          }
          else
            L.Add(((Curve) polylineCurve).DuplicateCurve());
          return true;
        }
        else
        {
          Polyline polyline;
          if (crv.TryGetPolyline(out polyline))
          {
            if (recursive)
            {
              for (int index = 0; index < ((RhinoList<Point3d>) polyline).Count - 1; ++index)
                L.Add((Curve) new LineCurve(((RhinoList<Point3d>) polyline)[index], ((RhinoList<Point3d>) polyline)[index + 1]));
            }
            else
              L.Add((Curve) new PolylineCurve((IEnumerable<Point3d>) polyline));
            return true;
          }
          else
          {
            LineCurve lineCurve = crv as LineCurve;
            if (lineCurve != null)
            {
              L.Add(((Curve) lineCurve).DuplicateCurve());
              return true;
            }
            else
            {
              ArcCurve arcCurve = crv as ArcCurve;
              if (arcCurve != null)
              {
                L.Add(((Curve) arcCurve).DuplicateCurve());
                return true;
              }
              else
              {
                NurbsCurve nurbsCurve = crv.ToNurbsCurve();
                if (nurbsCurve == null)
                  return false;
              //  Interval domain1 = ((Curve) nurbsCurve).get_Domain();
              //  // ISSUE: explicit reference operation
              //  double num1 = ((Interval) @domain1).get_Min();
              //  Interval domain2 = ((Curve) nurbsCurve).get_Domain();
              //  // ISSUE: explicit reference operation
              //  double max = ((Interval) @domain2).get_Max();
              //  int count = L.Count;
              //  double num2;
              //  while (((Curve) nurbsCurve).GetNextDiscontinuity((Continuity) 7, num1, max, out num2))
              //  {
              //    Interval interval;
              //    // ISSUE: explicit reference operation
              //    ((Interval)@interval)..ctor(num1, num2);
              //    // ISSUE: explicit reference operation
              //    if (((Interval) @interval).get_Length() < 0.0 / 1.0)
              //    {
              //      num1 = num2;
              //    }
              //    else
              //    {
              //      Curve curve = ((Curve) nurbsCurve).DuplicateCurve().Trim(interval);
              //      if (((CommonObject) curve).get_IsValid())
              //        L.Add(curve);
              //      num1 = num2;
              //    }
              //  }
              //  if (L.Count == count)
                  L.Add((Curve) nurbsCurve);
              //  return true;
              }
              return true;
            }
          }
        }
      }
    }
        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.beam; }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{fb913f47-f2b4-41ce-92f3-cc3516c443a1}"); }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }
    }
}