﻿    using System;
using System.Collections.Generic;
using System.Text;
using Grasshopper.Kernel;
using System.Drawing;
using System.Windows.Forms;
using Grasshopper.Kernel.Attributes;
using Grasshopper.GUI.Canvas;
using Grasshopper.GUI;
using Grasshopper;
using Donkey.Model.Analytical.Initialization;

namespace Donkey.Model.Analytical.Data
{
    public class Material_Comp : GH_Component
    {
        #region FIELD
        private Material_Goo m_material;
        private bool m_custom;
        #endregion

        #region CONSTRUCTOR
        public Material_Comp()
            : base("Material", "Material", "A bunch of material presets" //+ Donkey.Info.Mark
            , "Donkey", "1.Elements")
        {
            this.m_material = new Material_Goo();
            this.m_custom = false;
        }
        #endregion

        #region OVERRIDE

        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            this.m_material.Write(writer);
            writer.SetBoolean("custom_material", this.m_custom);
            //this.Params.Write(writer);
            return base.Write(writer);
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            this.m_material.Read(reader);
            this.m_custom = reader.GetBoolean("custom_material");
            //this.Params.Read(reader);
            return base.Read(reader);
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new Material_Param());
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //this.Message = Donkey.Info.Version;
            DA.SetData(0, new Material_Goo(this.CurrentValue));
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("4b659137-3efc-4f23-a1d8-4487f94ae7f2"); }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.material; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        public override bool IsBakeCapable
        {
            get { return false; }
        }

        public override bool IsPreviewCapable { get { return true; } }

        public override void CreateAttributes()
        {
            m_attributes = new Material_CompAttributes_noJaggedEdges(this);
        }
        #endregion

        #region MENU
        public override bool AppendMenuItems(ToolStripDropDown menu)
        {
            this.AppendAdditionalMenuItems(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            //this.Menu_AppendPreviewItem(menu);
            this.Menu_AppendEnableItem(menu);
            //this.Menu_AppendWarningsAndErrors(menu);
            this.Menu_AppendRuntimeMessages(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            this.Menu_AppendObjectHelp(menu);
            return true;
        }

        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            //ToolStripMenuItem toolStripMenuItem5 = GH_DocumentObject.Menu_AppendItem(menu, "None", SetDOF, true, !this.m_custom && this.m_dof.ToString() == "000/000");
            //toolStripMenuItem5.ToolTipText = "000/000" + Environment.NewLine + "0 is freedom, 1 is lock";

            //return base.AppendMenuItems(menu);
            //ToolStripMenuItem toolStripMenuItem1 = GH_DocumentObject.Menu_AppendItem(menu, "Steel", SetMaterial, true, !this.m_custom && this.m_material.Value.GetName() == Material.Steel.GetName());
            //ToolStripMenuItem toolStripMenuItem2 = GH_DocumentObject.Menu_AppendItem(menu, "Timber", SetMaterial, true, !this.m_custom && this.m_material.Value.GetName() == Material.ConiferousTimber.GetName());
            //ToolStripMenuItem toolStripMenuItem3 = GH_DocumentObject.Menu_AppendItem(menu, "Concrete", SetMaterial, true, !this.m_custom && this.m_material.Value.GetName() == Material.Concrete.GetName());
            //ToolStripMenuItem toolStripMenuItem4 = GH_DocumentObject.Menu_AppendItem(menu, "Rigid", SetMaterial, true, !this.m_custom && this.m_material.Value.Type == Material.types.RigidBody);
            
            //Menu_AppendObjectName(menu);
            //Menu_AppendObjectMyName(menu);

            foreach (Material mat in Material.Database.Values)
            {
                GH_DocumentObject.Menu_AppendItem(menu, mat.Name, SetMaterial_value, true, !this.m_custom && this.m_material.Value.Equals(mat)); //this.m_material.Value.Name == mat.Name || this.m_material.Value.Name == mat.Name + " (C)");
            }

            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);

            ToolStripMenuItem toolStripMenuItem7 = GH_DocumentObject.Menu_AppendItem(menu, "Custom", SetMaterial_value, true, this.m_custom);
            toolStripMenuItem7.ToolTipText = "Custom value, double click to component";

        }

        private void SetMaterial_value(Object sender, EventArgs e)
        {
            if (sender.ToString() != Material.names.Custom.ToString())
            {
                this.m_material = new Material_Goo(Material.Database[sender.ToString()]);
                this.m_custom = false;
            }
            else
            {
                this.ShowCustomSetting();
            }
            this.ExpireSolution(true);
        }

        //private void SetMaterial(Object sender, EventArgs e)
        //{
        //    switch (sender.ToString())
        //    {
        //        case "Steel":
        //            this.m_material = new Material_Goo(Material.Steel);
        //            this.m_custom = false;
        //            break;

        //        case "Timber":
        //            this.m_material = new Material_Goo(Material.ConiferousTimber);
        //            this.m_custom = false;
        //            break;

        //        case "Rigid":
        //            this.m_material = new Material_Goo(Material.types.RigidBody);
        //            this.m_custom = false;
        //            break;

        //        case "Custom":
        //            //this.m_custom = true;
        //            ShowCustomSetting();
        //            break;

        //        default:
        //            this.m_material = new Material_Goo(Material.Concrete);
        //            this.m_custom = false;
        //            break;
        //    }
        //    //this.m_mat = new Material(this.m_selected);
        //    this.ExpireSolution(true);
        //}
        #endregion

        #region DOUBLE-CLICK
        public void ShowCustomSetting()
        {
            //System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            //ofd.Multiselect = true;
            //ofd.Filter = "Data Sources (*.ini)|*.ini*|All Files|*.*";
            //if (ofd.ShowDialog() == DialogResult.OK)
            //{
            //    string[] filePath = ofd.FileNames;
            //    string[] safeFilePath = ofd.SafeFileNames;
            //}
            //this.m_custom = true;
            //this.m_dof = new NodalDOF(2, 2, 2, 2, 2, 2);
            ////m_settings = Path.GetDirectoryName(ofd.FileName);
            //this.ExpireSolution(true);

            this.TriggerAutoSave();
            Material_Form numberSliderPopup = new Material_Form(this);
            //numberSliderPopup.Setup(this);
            GH_WindowsFormUtil.CenterFormOnCursor((Form)numberSliderPopup, true);
            if (numberSliderPopup.ShowDialog((IWin32Window)Instances.DocumentEditor) != DialogResult.OK)
                return;
            if (this.m_material.Value.Name == Material.names.Custom.ToString())
            { this.m_custom = true; }
            else { this.m_custom = false; }
            this.Attributes.ExpireLayout();
            this.ExpireSolution(true);
        }
        #endregion

        public Material CurrentValue
        {
            get
            {
                if (this.m_material != null)
                    return this.m_material.Value;
                return new Material();
            }

            set { this.m_material = new Material_Goo(value); }

        }
    }

    public class Material_CompAttributes : GH_ComponentAttributes //GH_Attributes<FEM_Comp>
    {
        //FEM_Comp Owner;

        public Material_CompAttributes(Material_Comp owner)
            : base(owner)
        {
            //Owner = owner;
        }

        public override bool HasInputGrip { get { return false; } }
        public override bool HasOutputGrip { get { return true; } }

        #region LAYOUT
        protected override void Layout()
        {
            base.Layout();
            this.Pivot = (PointF)GH_Convert.ToPoint(base.Pivot);
            //this.m_innerBounds = GH_ComponentAttributes.LayoutComponentBox(base.Owner);
            this.m_innerBounds = new System.Drawing.RectangleF(Pivot, new SizeF(80, 24));
            //this.m_innerBounds = new RectangleF(Owner.Attributes.Pivot.X - 0.5f * (float)1, Owner.Attributes.Pivot.Y - 0.5f * (float)1, (float)112, (float)24);
            //this.Bounds = new System.Drawing.RectangleF(Pivot, new SizeF(112, 24));
            GH_ComponentAttributes.LayoutInputParams(base.Owner, this.m_innerBounds);
            GH_ComponentAttributes.LayoutOutputParams(base.Owner, this.m_innerBounds);
            this.Bounds = GH_ComponentAttributes.LayoutBounds(base.Owner, this.m_innerBounds);
            //this.LayoutJaggedEdges();
            //this.m_capsule.SetJaggedEdges(false, false);
        }
        #endregion

        #region RENDER

        protected override void PrepareForRender(GH_Canvas canvas)
        {
            base.PrepareForRender(canvas);
        }

        protected override void Render(GH_Canvas canvas, Graphics graphics, GH_CanvasChannel channel)
        {
            switch (channel)
            {
                case GH_CanvasChannel.Wires:
                    base.Render(canvas, graphics, channel);
                    break;

                case GH_CanvasChannel.Objects:
                    base.RenderComponentCapsule(canvas, graphics, true, false, true, true, true, false);

                    graphics.DrawString(((Material_Comp)Owner).CurrentValue.GetName(),
                                    Grasshopper.Kernel.GH_FontServer.Standard,
                                    Brushes.Black,
                                    this.m_innerBounds,
                                    Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);
                    break;
            }
        }
        #endregion

        #region DOUBLE-CLICK
        public override GH_ObjectResponse RespondToMouseDoubleClick(GH_Canvas sender, GH_CanvasMouseEvent e)
        {
            ((Material_Comp)this.Owner).ShowCustomSetting();
            return GH_ObjectResponse.Handled;
        }
        #endregion
    }

    public class Material_CompAttributes_noJaggedEdges : GH_ComponentAttributes //GH_Attributes<FEM_Comp>
    {
        //FEM_Comp Owner;

        public Material_CompAttributes_noJaggedEdges(Material_Comp component)
            : base(component)
        {
            //this.m_innerBounds = new System.Drawing.RectangleF(Pivot, new SizeF(80, 24));
            ////this.Bounds = (RectangleF)new Rectangle(0, 0, 80, 24);
            //this.Bounds = GH_ComponentAttributes.LayoutBounds(base.Owner, this.m_innerBounds);
        }

        #region LAYOUT
        protected override void Layout()
        {
            //base.Layout();
            //RectangleF bounds = this.Bounds;
            //bounds.Inflate(-(this.Owner.Params.InputWidth + 4f), -2f);
            //GH_ComponentAttributes.LayoutInputParams((IGH_Component)this.Owner, bounds);
            //bounds = this.Bounds;
            //bounds.Inflate(-(this.Owner.Params.OutputWidth + 4f), -2f);
            //GH_ComponentAttributes.LayoutOutputParams((IGH_Component)this.Owner, bounds);
            //bounds = this.Bounds;

            this.Pivot = (PointF)GH_Convert.ToPoint(base.Pivot);
            //this.m_innerBounds = GH_ComponentAttributes.LayoutComponentBox(base.Owner);
            this.m_innerBounds = new System.Drawing.RectangleF(Pivot, new SizeF(80, 24));
            //this.m_innerBounds = new RectangleF(Owner.Attributes.Pivot.X - 0.5f * (float)1, Owner.Attributes.Pivot.Y - 0.5f * (float)1, (float)112, (float)24);
            //this.Bounds = new System.Drawing.RectangleF(Pivot, new SizeF(112, 24));
            GH_ComponentAttributes.LayoutInputParams(base.Owner, this.m_innerBounds);
            GH_ComponentAttributes.LayoutOutputParams(base.Owner, this.m_innerBounds);
            this.Bounds = GH_ComponentAttributes.LayoutBounds(base.Owner, this.m_innerBounds);
            //this.LayoutJaggedEdges();
            //this.m_capsule.SetJaggedEdges(false, false);
        }
        #endregion

        #region RENDER

        //protected override void PrepareForRender(GH_Canvas canvas)
        //{
        //    base.PrepareForRender(canvas);
        //}

        protected override void Render(GH_Canvas canvas, Graphics graphics, GH_CanvasChannel channel)
        {
            switch (channel)
            {
                case GH_CanvasChannel.Wires:
                    base.Render(canvas, graphics, channel);
                    break;

                case GH_CanvasChannel.Objects:

                    GH_Viewport viewport = canvas.Viewport;
                    RectangleF rec = this.Bounds;
                    double num1 = 10.0;
                    int num2 = viewport.IsVisible(ref rec, (float)num1) ? 1 : 0;
                    this.Bounds = rec;
                    if (num2 == 0)
                        break;

                    GH_Palette palette = GH_Palette.White;
                    switch (this.Owner.RuntimeMessageLevel)
                    {
                        case GH_RuntimeMessageLevel.Warning:
                            palette = GH_Palette.Warning;
                            break;
                        case GH_RuntimeMessageLevel.Error:
                            palette = GH_Palette.Error;
                            break;
                    }

                    GH_Capsule capsule = GH_Capsule.CreateCapsule(this.Bounds, palette);
                    capsule.SetJaggedEdges(false, false);
                    foreach (IGH_Param input in Owner.Params.Input)
                    {
                        capsule.AddInputGrip(input.Attributes.InputGrip.Y);
                    }
                    foreach (IGH_Param output in Owner.Params.Output)
                    {
                        capsule.AddOutputGrip(output.Attributes.OutputGrip.Y);
                    }

                    //graphics.SmoothingMode = SmoothingMode.HighQuality;
                    capsule.Render(graphics, this.Selected, this.Owner.Locked, true);
                    GH_PaletteStyle impliedStyle = GH_CapsuleRenderEngine.GetImpliedStyle(palette, this.Selected, this.Owner.Locked, this.Owner.Hidden);
                    GH_ComponentAttributes.RenderComponentParameters(canvas, graphics, (IGH_Component)this.Owner, impliedStyle);

                    graphics.DrawString(((Material_Comp)Owner).CurrentValue.GetName(),
                                    Grasshopper.Kernel.GH_FontServer.Standard,
                                    Brushes.Black,
                                    this.m_innerBounds,
                                    Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);
                    break;
                    
                    //base.RenderComponentCapsule(canvas, graphics, true, false, true, true, true, false);

                    //graphics.DrawString(((Material_Comp)Owner).CurrentValue.GetName(),
                    //                Grasshopper.Kernel.GH_FontServer.Standard,
                    //                Brushes.Black,
                    //                this.m_innerBounds,
                    //                Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);
                    //break;
            }
        }
        #endregion

        #region DOUBLE-CLICK
        public override GH_ObjectResponse RespondToMouseDoubleClick(GH_Canvas sender, GH_CanvasMouseEvent e)
        {
            Material_Comp component = this.Owner as Material_Comp;
            if (component == null)
                return base.RespondToMouseDoubleClick(sender, e);
            component.ShowCustomSetting();
            return GH_ObjectResponse.Handled;
        }
        #endregion
    }
}
