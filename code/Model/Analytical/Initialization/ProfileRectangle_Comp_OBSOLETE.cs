﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Drawing;

namespace Donkey.Model.Analytical.Data
{
    public class ProfileRectangle_Comp : GH_Component
    {
        public ProfileRectangle_Comp() : base("ProfileRectangle", "Rectangle", "Rectangular cross section for Donkey", "Donkey", "Elements") { }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            //pManager.AddParameter(new GH_ParamCells(), "Cells", "Cells", "A collection of Donkey's PointData values", GH_ParamAccess.item);
            pManager.AddNumberParameter("width", "W", "width", GH_ParamAccess.item, 10);
            pManager.AddNumberParameter("height", "H", "height", GH_ParamAccess.item, 10);
            pManager.AddNumberParameter("thickness", "T", "thickness", GH_ParamAccess.item, 0.0);
            //pManager.Register_VectorParam("orientation", "orientation", "height orientation");
            pManager[2].Optional = true;
            //pManager[3].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new Profile_Param());
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //read values from input
            double width = 0;
            DA.GetData<double>("width", ref width);

            double height = 0;
            DA.GetData<double>("height", ref height);

            double thickness = 0;
            DA.GetData<double>("thickness", ref thickness);

            //Vector3d vector = Vector3d.Unset;
            //bool orientation_loaded = DA.Property<Vector3d>("orientation", ref vector);

            Profile_Goo profile;

            //if (orientation_loaded)
            //{
            //    CrossSections.refNode orientation = new CrossSections.refNode(vector.X, vector.Y, vector.Z);
            //    cs = new GH_CrossSections(CrossSections.types.Rectangle, width, height, thickness, orientation);
            //}
            //else
            //{
                profile = new Profile_Goo(Profile.types.Rectangle, width, height, thickness, new Profile.LocalCoordinateSystem());
            //}
            
            DA.SetData(0, profile);
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("359a0bed-0cc0-4ed5-9628-a64b45a13041"); }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.profile; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.hidden; }
        }

        public override bool IsBakeCapable
        {
            get
            {
                return false;
            }
        }
    }
}
