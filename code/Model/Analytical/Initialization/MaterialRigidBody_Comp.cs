﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Drawing;

namespace Donkey.Model.Analytical.Data
{
    public class MaterialRigidBody_Comp : GH_Component
    {
        public MaterialRigidBody_Comp() : base("RigidBody", "Material", "Rigid body material for Donkey", "Donkey", "Obsolete") { }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            //pManager.AddParameter(new GH_ParamCells(), "Cells", "Cells", "A collection of Donkey's PointData values", GH_ParamAccess.item);
            //pManager.Register_DoubleParam("width","width","width",10);
            //pManager.Register_DoubleParam("height", "height", "height", 10);
            //pManager.Register_DoubleParam("thickness", "thickness", "thickness",0.0);
            //pManager.Register_VectorParam("orientation", "orientation", "orientation");
            //pManager[2].Optional = true;
            //pManager[3].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new Material_Param(), "Material", "Material", "DonkeyMaterial", GH_ParamAccess.item);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {

            Material_Goo mat = new Material_Goo(Material.types.RigidBody);

            DA.SetData(0, mat);
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("eb93efea-ba75-4c71-b3c5-3ab099f7ebe9"); }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.material; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.hidden; }
        }

        public override bool IsBakeCapable
        {
            get{return false;}
        }
    }
}
