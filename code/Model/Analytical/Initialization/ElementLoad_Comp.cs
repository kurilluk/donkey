﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel.Data;
using Rhino.Geometry;
using Donkey.Model.Analytical.Data;


namespace Donkey.Model.Analytical.Initialization
{
    public class ElementLoad_Comp : GH_Component
    {
        // Fields
        private List<AreaLoad> elements_load; 

        //constructor
        public ElementLoad_Comp()
            : base("ElementLoad", "Load", "Area load [N/mm - N/mm\u00B2]" //+ Donkey.Info.Mark
            , "Donkey", "2.Conditions")
        {
            this.elements_load = new List<AreaLoad>();
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new AnalyticalElement_Param(), "Element", "E", "Model element", GH_ParamAccess.item);
            pManager.HideParameter(0);
            pManager.AddVectorParameter("Direction", "V", "Nodal load direction vector", GH_ParamAccess.item, new Vector3d(0, 0, -1));
            pManager[1].Optional = true;
            pManager.HideParameter(1);
            pManager.AddNumberParameter("Weight", "W", "Nodal weight value [kg]", GH_ParamAccess.item, 100);
            pManager[2].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new AnalyticalElement_Param());
            pManager.HideParameter(0);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //READ INPUT PARAMS
            AnalyticalModel element = null;
            bool m_node = DA.GetData(0, ref element);

            Vector3d vector = Vector3d.Unset;
            bool m_vector = DA.GetData("Direction", ref vector);

            double weight = 100;
            bool m_force = DA.GetData("Weight", ref weight);

            AnalyticalModel model = new AnalyticalModel();

            foreach (AnalyticalElement e in element.Elements)
            {
                //AnalyticalElement ae = new AnalyticalElement((AnalyticalElement)e);
                e.CloneToModel(model,false,false);
            }


            double g = 9.80655;

            foreach (AnalyticalElement el in model.Elements)
            {
                Geometry.Types type = el.model.Geometries[el.Geometries_ID[0]].Type;
                if (type == Geometry.Types.VTK_LINE || type == Geometry.Types.VTK_POLY_LINE)
                {
                    //element = "Element: Beam Element";
                    el.AddAreaLoad(vector, (weight * g * 0.001));
                }
                else
                {
                    //element = "Element: Shell Element";
                    el.AddAreaLoad(vector, (weight * g * 0.000001));
                }

                this.elements_load.Add(el.Load);
            }

            DA.SetData(0, model);
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{9700C85D-A6B8-440E-8733-CCD1515D5AC7}"); }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.load_elment; }

        }
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.quinary; }
        }

        public override void ClearData()
        {
            base.ClearData();
            this.elements_load.Clear();
        }

        public override void DrawViewportWires(IGH_PreviewArgs args)
        {
            GH_PreviewWireArgs wireArgs;
            if (this.Attributes.GetTopLevel.Selected)
            {
                wireArgs = new GH_PreviewWireArgs(args.Viewport, args.Display, args.WireColour_Selected, 2);
            }
            else
            {
                wireArgs = new GH_PreviewWireArgs(args.Viewport, args.Display, args.WireColour, 1);
            }
            if(elements_load != null)
                foreach(AreaLoad element_load in elements_load)
                    element_load.DrawViewportWires(wireArgs);
        }
    }
}
