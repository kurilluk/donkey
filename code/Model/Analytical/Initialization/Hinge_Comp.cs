﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using Donkey.Model.Analytical.Data;

namespace Donkey.Model.Analytical.Initialization
{
    public class Hinge_Comp : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the Hinge_Comp class.
        /// </summary>
        public Hinge_Comp()
            : base("Hinge", "Hinge",
                "Nodal full hinge",// + Donkey.Info.Mark,
                "Donkey", "2.Conditions")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddPointParameter("Node", "N", "Point as model node", GH_ParamAccess.item);
            pManager.HideParameter(0);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new AnalyticalModelData_Param());
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //READ INPUT PARAMS
            Point3d node = Point3d.Unset;
            bool m_node = DA.GetData("Node", ref node);

            DA.SetData(0, new Hinge(node));
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.hinge; }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{8a8aaa65-9f99-4d47-b7d9-5676a89b4bde}"); }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.quarternary; }
        }
    }
}