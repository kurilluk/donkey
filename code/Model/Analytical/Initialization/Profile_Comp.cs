﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Drawing;
using System.Windows.Forms;

namespace Donkey.Model.Analytical.Data
{
    public class Profile_Comp : GH_Component
    {
        private Profile.types m_mode;

        public Profile_Comp()
            : base("Profile", "Profile", "Element cross-section", // + Donkey.Info.Mark,
            "Donkey", "1.Elements") 
        {
            this.m_mode = Profile.types.Circle;
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            //pManager.AddParameter(new GH_ParamCells(), "Cells", "Cells", "A collection of Donkey's PointData values", GH_ParamAccess.item);
            pManager.AddNumberParameter("Width", "W", "Profile width [mm]", GH_ParamAccess.item, 10);
            pManager.AddNumberParameter("Height", "H", "Profile height [mm]", GH_ParamAccess.item, 10);
            pManager.AddNumberParameter("Thickness", "T", "Tube profile thickness [mm] (optional)", GH_ParamAccess.item);
            pManager.AddVectorParameter("Orientation", "O", "Height orientation - Local Z axis (optional)",GH_ParamAccess.item);
            pManager[2].Optional = true;
            pManager[3].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new Profile_Param());
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            this.ClearRuntimeMessages();
            //read values from input
            double width = 0;
            DA.GetData<double>("Width", ref width);

            double height = 0;
            DA.GetData<double>("Height", ref height);

            double thickness = 0;
            if(DA.GetData<double>("Thickness", ref thickness))
                if ((2 * thickness) >= width || (2 * thickness) >= height)
                {
                    thickness = 0;
                    this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Thikness of profile colliding with profile size");
                }
            //if (!isTube)
            //    thickness = 0;

            Vector3d vector = Vector3d.Unset;
            bool orientation_defined = DA.GetData<Vector3d>("Orientation", ref vector);

            Profile_Goo profile;

            switch (this.m_mode)
                {
                    case Profile.types.Rectangle:
                        this.Message = "Rectangle";//"\u25A0";
                        if(orientation_defined)
                            profile = new Profile_Goo(Profile.types.Rectangle, width, height, thickness, new Profile.LocalCoordinateSystem(vector));                      
                        else
                            profile = new Profile_Goo(Profile.types.Rectangle, width, height, thickness, new Profile.LocalCoordinateSystem());
                        break;

                    case Profile.types.Circle:
                        this.Message = "Circle";//"\u2B24;
                        if(orientation_defined)
                            profile = new Profile_Goo(Profile.types.Circle, width, height, thickness, new Profile.LocalCoordinateSystem(vector));
                        else
                        profile = new Profile_Goo(Profile.types.Circle, width, height, thickness, new Profile.LocalCoordinateSystem());
                        break;

                    default:
                        this.Message = (string)null;
                        profile = new Profile_Goo();
                        break;
                }
           
            DA.SetData(0, profile);
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("01A6CCC3-3CDA-488A-B3CF-D4A6021154EE"); }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.profile; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        public override bool IsBakeCapable
        {
            get
            {
                return false;
            }
        }

        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetInt32("profile_id", (int)this.m_mode);
            return base.Write(writer);
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            this.m_mode = (Profile.types)reader.GetInt32("profile_id");
            return base.Read(reader);
        }
        

        #region MENU

        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            ToolStripMenuItem toolStripMenuItem1 = GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Rectangle", new EventHandler(this.Menu_RectangleClicked), true, this.m_mode == Profile.types.Rectangle);
            toolStripMenuItem1.ToolTipText = "Rectangle cross section profile for Donkey." + Environment.NewLine + "If thikness is more that 0, then create tube profile";

            ToolStripMenuItem toolStripMenuItem2 = GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Circle", new EventHandler(this.Menu_CircleClicked), true, this.m_mode == Profile.types.Circle);
            toolStripMenuItem2.ToolTipText = "Circle cross section profile for Donkey." + Environment.NewLine + "If thikness is more that 0, then create tube profile";
        }

        #endregion

        private void  Menu_RectangleClicked(object sender, EventArgs e)
        {
            if (this.m_mode == Profile.types.Rectangle)
                return;
            this.RecordUndoEvent("Rectangle");
            this.m_mode = Profile.types.Rectangle;
            this.ExpireSolution(true);
        }

        private void Menu_CircleClicked(object sender, EventArgs e)
        {
            if (this.m_mode == Profile.types.Circle)
                return;
            this.RecordUndoEvent("Circle");
            this.m_mode = Profile.types.Circle;
            this.ExpireSolution(true);
        }
    }
}
