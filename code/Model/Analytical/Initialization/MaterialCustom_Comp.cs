﻿using System;
using System.Collections.Generic;
using System.Text;
using Grasshopper.Kernel;
using System.Drawing;
using Donkey.Model.Analytical.Initialization;

namespace Donkey.Model.Analytical.Data
{
    public class MaterialCustom_Comp : GH_Component
    {
        #region FIELD
        #endregion

        #region CONSTRUCTOR
        public MaterialCustom_Comp()
            : base("Material (C)", "Material", "Custom material", "Donkey", "A.Utilities")
        { }
        #endregion

        #region OVERRIDE
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            //pManager.AddParameter(new GH_ParamCells(), "Cells", "Cells", "A collection of Donkey's PointData values", GH_ParamAccess.item);
            pManager.AddNumberParameter("density", "\x03C1", "density [kg/mm\u00B3]", GH_ParamAccess.item, 7850.0e-09);
            pManager.AddNumberParameter("modulus", "E", "Young's modulus [MPa]", GH_ParamAccess.item, 210.0e+03);
            pManager.AddNumberParameter("nu", "\x03BF", "Poisson's ratio", GH_ParamAccess.item, 0.21);
            pManager.AddNumberParameter("tAlpha", "\x03B1", "Coefficient of thermal expansion", GH_ParamAccess.item, 0.000012); //\u0251
            pManager.AddNumberParameter("Ry", "Ry", "Yield stress [MPa]", GH_ParamAccess.item, 15);
        }


        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new Material_Param());
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //read values from input
            double density = 0;
            DA.GetData<double>("density", ref density);

            double E = 0;
            DA.GetData<double>("modulus", ref E);

            double nu = 0;
            DA.GetData<double>("nu", ref nu);

            double tAlpha = 0;
            DA.GetData<double>("tAlpha", ref tAlpha);

            double Ry = 0;
            DA.GetData<double>("Ry", ref Ry);

            //create new material
            Material_Goo mat = new Material_Goo(density, E, nu, tAlpha, Ry);

            DA.SetData(0, mat);
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("fe687adf-8298-4d52-bbbf-6879855f5fcd"); }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.material; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        public override bool IsBakeCapable
        {
            get{return false;}
        }
        #endregion
    }
}
