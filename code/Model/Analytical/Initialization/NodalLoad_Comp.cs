﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel.Data;
using Rhino.Geometry;
using Donkey.Model.Analytical.Data;


namespace Donkey.Model.Analytical.Initialization
{
    public class NodalLoad_Comp : GH_Component
     {
        // Fields
        //none

        //constructor
        public NodalLoad_Comp()
            : base("NodalLoad", "Load", "Nodal load [N]" //+ Donkey.Info.Mark
            , "Donkey", "2.Conditions")
        {  }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddPointParameter("Node", "N", "Point as model node", GH_ParamAccess.item);
            pManager.HideParameter(0);
            pManager.AddVectorParameter("Direction", "V", "Nodal load direction vector", GH_ParamAccess.item,new Vector3d(0,0,-1));
            pManager[1].Optional = true;
            pManager.HideParameter(1);
            pManager.AddNumberParameter("Weight", "W", "Nodal weight value [kg]", GH_ParamAccess.item, 100);
            pManager[2].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new AnalyticalModelData_Param());
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //READ INPUT PARAMS
            Point3d node = Point3d.Unset;
            bool m_node = DA.GetData("Node",ref node);

            Vector3d vector = Vector3d.Unset;
            bool m_vector = DA.GetData("Direction",ref vector);

            double weight = 100;
            bool m_force = DA.GetData("Weight", ref weight);

            DA.SetData(0, new NodalLoad(node, vector, Math.Round(weight * 9.80655,3)));
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{be9541bc-8ac3-440d-b9d8-25f0a86c01eb}"); }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.load_nodal; }

        }
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.quinary; }
        }
    }
}
