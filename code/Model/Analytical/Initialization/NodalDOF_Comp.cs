﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel.Data;
using Rhino.Geometry;
using System.Drawing;
using System.Windows.Forms;
using Grasshopper;
using Microsoft.VisualBasic.CompilerServices;
using System.Runtime.CompilerServices;
using Donkey.Model.Analytical.Data;
using Grasshopper.Kernel.Attributes;
using Grasshopper.GUI.Canvas;
using Grasshopper.GUI;


namespace Donkey.Model.Nodal.AnalyticalData
{
    public class NodalDOF_Comp : GH_Component
    {
        #region FIELD
        private NodalDOF m_dof;
        private bool m_custom;
        #endregion

        #region CONSTRUCTOR
        public NodalDOF_Comp()
            : base("DOF", "DOF", "Nodal degree of freedom" + Environment.NewLine + "(Movement/Rotation)" 
            //+ Donkey.Info.Mark
            , "Donkey", "2.Conditions") //"[Tx Ty Tz / Rx Ry Rz]"
        {
            this.m_dof = new NodalDOF();
            this.m_custom = false;
        }
        #endregion

        #region OVERRIDE

        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            this.m_dof.Write(writer);
            writer.SetBoolean("custom_dof",this.m_custom);
            return base.Write(writer);
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            this.m_dof.Read(reader);
            this.m_custom = reader.GetBoolean("custom_dof");
            return base.Read(reader);
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {

        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new NodalDOF_Param());
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //if (m_custom)
            //{
            //    this.Message = "Custom";
            //}
            //else
            //{
            //    this.Message = (string)null;
            //}

            this.Message = m_dof.Name;

            DA.SetData(0, m_dof);
        }
        #endregion

        #region MENU
        public override bool AppendMenuItems(ToolStripDropDown menu)
        {
            this.AppendAdditionalMenuItems(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            //this.Menu_AppendPreviewItem(menu);
            this.Menu_AppendEnableItem(menu);
            this.Menu_AppendRuntimeMessages(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            this.Menu_AppendObjectHelp(menu);
            return true;
        }

        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            //ToolStripMenuItem toolStripMenuItem5 = GH_DocumentObject.Menu_AppendItem(menu, "None", SetDOF, true, !this.m_custom && this.m_dof.ToString() == "000/000");
            //toolStripMenuItem5.ToolTipText = "000/000" + Environment.NewLine + "0 is freedom, 1 is lock";

            //ToolStripMenuItem toolStripMenuItem1 = GH_DocumentObject.Menu_AppendItem(menu, "Full", SetDOF, true, !this.m_custom && this.m_dof.ToString() == "111/111");
            //toolStripMenuItem1.ToolTipText = "111/111" + Environment.NewLine + "0 is freedom, 1 is lock";

            //ToolStripMenuItem toolStripMenuItem3 = GH_DocumentObject.Menu_AppendItem(menu, "Full_T", SetDOF, true, !this.m_custom && this.m_dof.ToString() == "111/000");
            //toolStripMenuItem3.ToolTipText = "111/000" + Environment.NewLine + "0 is freedom, 1 is lock";

            //ToolStripMenuItem toolStripMenuItem4 = GH_DocumentObject.Menu_AppendItem(menu, "Full_R", SetDOF, true, !this.m_custom && this.m_dof.ToString() == "000/111");
            //toolStripMenuItem4.ToolTipText = "000/111" + Environment.NewLine + "0 is freedom, 1 is lock";

            //ToolStripMenuItem toolStripMenuItem2 = GH_DocumentObject.Menu_AppendItem(menu, "Only_T/y", SetDOF, true, !this.m_custom && this.m_dof.ToString() == "010/000");
            //toolStripMenuItem2.ToolTipText = "010/000" + Environment.NewLine + "0 is freedom, 1 is lock";

            CreateMenu(menu);

            //ToolStripMenuItem toolStripMenuItem6 = GH_DocumentObject.Menu_AppendItem(menu, "Only_TR/y", SetDOF, true, !this.m_custom && this.m_dof.ToString() == "010/010");
            //toolStripMenuItem6.ToolTipText = "010/010" + Environment.NewLine + "0 is freedom, 1 is lock";

            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);

            ToolStripMenuItem toolStripMenuItem7 = GH_DocumentObject.Menu_AppendItem(menu, "Custom", SetDOF_value, true, this.m_custom);
            toolStripMenuItem7.ToolTipText = "Custom value, double click to component";

        }

        private void CreateMenu(ToolStripDropDown menu)
        {
            foreach (NodalDOF dof in NodalDOF.Database.Values)
            {
                GH_DocumentObject.Menu_AppendItem(menu, dof.Name, SetDOF_value, true, !this.m_custom && this.m_dof.Name == dof.Name);
                //toolStripMenuItem6.ToolTipText = "010/010" + Environment.NewLine + "0 is freedom, 1 is lock";
            }
 
        }

        private void SetDOF_value(Object sender, EventArgs e)
        {
            if (sender.ToString() != NodalDOF.names.Custom.ToString())
            {
                this.m_dof = NodalDOF.Database[sender.ToString()];
                this.m_custom = false;
            }
            else
            {
                this.ShowCustomSetting();
            }
            this.ExpireSolution(true);
        }

        //private void SetDOF(Object sender, EventArgs e)
        //{
        //    switch (sender.ToString())
        //    {
        //        case "Full":
        //            this.m_dof = new NodalDOF();
        //            this.m_custom = false;
        //            break;
        //        case "Only_T/y":
        //            this.m_dof = new NodalDOF(0, 1, 0, 0, 0, 0);
        //            this.m_custom = false;
        //            break;
        //        case "Only_TR/y":
        //            this.m_dof = new NodalDOF(0, 1, 0, 0, 1, 0);
        //            this.m_custom = false;
        //            break;
        //        case "Full_T":
        //            this.m_dof = new NodalDOF(1, 1, 1, 0, 0, 0, NodalDOF.names.Full_T);
        //            this.m_custom = false;
        //            break;
        //        case "Full_R":
        //            this.m_dof = new NodalDOF(0, 0, 0, 1, 1, 1, NodalDOF.names.Full_R);
        //            this.m_custom = false;
        //            break;
        //        case "None":
        //            this.m_dof = new NodalDOF(0, 0, 0, 0, 0, 0, NodalDOF.names.None);
        //            this.m_custom = false;
        //            break;
        //        case "Custom":
        //            this.ShowCustomSetting();
        //            break;

        //        default:
        //            this.m_dof = new NodalDOF();
        //            break;
        //    }
        //    this.ExpireSolution(true);
        //}

        public override Guid ComponentGuid
        {
            get { return new Guid("{87d1d55d-6afe-40cb-8227-4ff281dd05a9}"); }
        }

        public override BoundingBox ClippingBox
        {
            get
            {
                return base.ClippingBox;
            }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.dof; }

        }
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.quarternary; }
        }

        #endregion

        #region DOUBLE-CLICK
        public void ShowCustomSetting()
        {
            //System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            //ofd.Multiselect = true;
            //ofd.Filter = "Data Sources (*.ini)|*.ini*|All Files|*.*";
            //if (ofd.ShowDialog() == DialogResult.OK)
            //{
            //    string[] filePath = ofd.FileNames;
            //    string[] safeFilePath = ofd.SafeFileNames;
            //}
            //this.m_custom = true;
            //this.m_dof = new NodalDOF(2, 2, 2, 2, 2, 2);
            ////m_settings = Path.GetDirectoryName(ofd.FileName);
            //this.ExpireSolution(true);

            this.TriggerAutoSave();
            NodalDOF_Form numberSliderPopup = new NodalDOF_Form(this);
            //numberSliderPopup.Setup(this);
            GH_WindowsFormUtil.CenterFormOnCursor((Form)numberSliderPopup, true);
            if (numberSliderPopup.ShowDialog((IWin32Window)Instances.DocumentEditor) != DialogResult.OK)
                return;
            if (!NodalDOF.Database.ContainsKey(this.m_dof.Name))
            {
                this.m_custom = true;
            }
            else { this.m_custom = false; }
            this.Attributes.ExpireLayout();
            this.ExpireSolution(true);
        }
        #endregion

        public NodalDOF CurrentValue
        {
            get 
            {
                if (this.m_dof != null)
                    return this.m_dof;
                return new NodalDOF();
            }

            set { this.m_dof = value; }

        }

        public override void CreateAttributes()
        {
            m_attributes = new DOF_Attributes_noJaggedEdges(this);
        }

        public override bool IsPreviewCapable { get { return true; } }
    }

    public class DOF_Attributes : GH_ComponentAttributes //GH_Attributes<FEM_Comp>
    {
        //FEM_Comp Owner;

        public DOF_Attributes(NodalDOF_Comp owner)
            : base(owner)
        {
            //Owner = owner;
        }

        public override bool HasInputGrip { get { return false; } }
        public override bool HasOutputGrip { get { return true; } }

        #region LAYOUT
        protected override void Layout()
        {
            this.Pivot = (PointF)GH_Convert.ToPoint(base.Pivot);
            //this.m_innerBounds = GH_ComponentAttributes.LayoutComponentBox(base.Owner);
            this.m_innerBounds = new System.Drawing.RectangleF(Pivot, new SizeF(80, 24));
            //this.m_innerBounds = new RectangleF(Owner.Attributes.Pivot.X - 0.5f * (float)1, Owner.Attributes.Pivot.Y - 0.5f * (float)1, (float)112, (float)24);
            //this.Bounds = new System.Drawing.RectangleF(Pivot, new SizeF(112, 24));
            GH_ComponentAttributes.LayoutInputParams(base.Owner, this.m_innerBounds);
            GH_ComponentAttributes.LayoutOutputParams(base.Owner, this.m_innerBounds);
            this.Bounds = GH_ComponentAttributes.LayoutBounds(base.Owner, this.m_innerBounds);
            //this.LayoutJaggedEdges();
        }
        #endregion

        #region RENDER        
        protected override void Render(GH_Canvas canvas, Graphics graphics, GH_CanvasChannel channel)
        {
            switch (channel)
            {
                case GH_CanvasChannel.Wires:
                    base.Render(canvas, graphics, channel);
                    break;

                case GH_CanvasChannel.Objects:
                    base.RenderComponentCapsule(canvas, graphics, true, false, false, true, true, true);

                    graphics.DrawString(((NodalDOF_Comp)Owner).CurrentValue.ToString(),
                                    Grasshopper.Kernel.GH_FontServer.Standard,
                                    Brushes.Black,
                                    this.m_innerBounds,
                                    Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);
                    break;
            }
        }
        #endregion

        #region DOUBLE-CLICK
        public override GH_ObjectResponse RespondToMouseDoubleClick(GH_Canvas sender, GH_CanvasMouseEvent e)
        {
            ((NodalDOF_Comp)this.Owner).ShowCustomSetting();
            return GH_ObjectResponse.Handled;
        }
        #endregion
    }

    public class DOF_Attributes_noJaggedEdges : GH_ComponentAttributes //with no message box
    {
        public DOF_Attributes_noJaggedEdges(NodalDOF_Comp component) : base(component) { }

        #region LAYOUT

        protected override void Layout()
        {
            this.Pivot = (PointF)GH_Convert.ToPoint(base.Pivot);
            this.m_innerBounds = new System.Drawing.RectangleF(Pivot, new SizeF(80, 24));
            GH_ComponentAttributes.LayoutInputParams(base.Owner, this.m_innerBounds);
            GH_ComponentAttributes.LayoutOutputParams(base.Owner, this.m_innerBounds);
            this.Bounds = GH_ComponentAttributes.LayoutBounds(base.Owner, this.m_innerBounds);
        }
        #endregion

        #region RENDER
        protected override void Render(GH_Canvas canvas, Graphics graphics, GH_CanvasChannel channel)
        {
            if (channel == GH_CanvasChannel.Objects)
            {
                //GH_Viewport viewport = canvas.Viewport;
                //RectangleF rec = this.Bounds;
                //double num1 = 10.0;
                //int num2 = viewport.IsVisible(ref rec, (float)num1) ? 1 : 0;
                //this.Bounds = rec;
                //if (num2 == 0)
                //    break;

                GH_Palette palette = GH_Palette.White;
                switch (this.Owner.RuntimeMessageLevel)
                {
                    case GH_RuntimeMessageLevel.Warning:
                        palette = GH_Palette.Warning;
                        break;
                    case GH_RuntimeMessageLevel.Error:
                        palette = GH_Palette.Error;
                        break;
                }

                GH_Capsule capsule = GH_Capsule.CreateCapsule(this.Bounds, palette);
                capsule.SetJaggedEdges(false, false);
                foreach (IGH_Param input in Owner.Params.Input)
                {
                    capsule.AddInputGrip(input.Attributes.InputGrip.Y);
                }
                foreach (IGH_Param output in Owner.Params.Output)
                {
                    capsule.AddOutputGrip(output.Attributes.OutputGrip.Y);
                }

                //graphics.SmoothingMode = SmoothingMode.HighQuality;
                capsule.Render(graphics, this.Selected, this.Owner.Locked, true);
                GH_PaletteStyle impliedStyle = GH_CapsuleRenderEngine.GetImpliedStyle(palette, this.Selected, this.Owner.Locked, this.Owner.Hidden);
                GH_ComponentAttributes.RenderComponentParameters(canvas, graphics, (IGH_Component)this.Owner, impliedStyle);

                //base.RenderComponentCapsule(canvas, graphics, true, false, false, true, true, true);

                graphics.DrawString(((NodalDOF_Comp)Owner).CurrentValue.ToString(),
                                Grasshopper.Kernel.GH_FontServer.Standard,
                                Brushes.Black,
                                this.m_innerBounds,
                                Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);
            }
        }
        #endregion

        #region DOUBLE-CLICK
        public override GH_ObjectResponse RespondToMouseDoubleClick(GH_Canvas sender, GH_CanvasMouseEvent e)
        {
            NodalDOF_Comp component = this.Owner as NodalDOF_Comp;
            if (component == null)
                return base.RespondToMouseDoubleClick(sender, e);
            component.ShowCustomSetting();
            return GH_ObjectResponse.Handled;
        }
        #endregion
    }
}
