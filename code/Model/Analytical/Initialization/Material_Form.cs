﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Donkey.Model.Analytical.Data;

namespace Donkey.Model.Analytical.Initialization
{
    public class Material_Form : Form
    {
        #region FIELD
        private Donkey.Model.Analytical.Data.Material_Comp component;
        private Donkey.Model.Analytical.Data.Material material;
        #endregion

        #region CONSTRUCTOR
        public Material_Form(Material_Comp component)
        {
            this.component = component;
            this.material = component.CurrentValue;
            InitializeComponent();

            this.Density.Increment = 0.00000001m;
            this.Density.Value = Convert.ToDecimal(this.material.Density);
            this.Modulus.Value = Convert.ToDecimal(material.E);
            this.Nu.Increment = 0.01m;
            this.Nu.Value = Convert.ToDecimal(material.nu);
            this.tAlpha.Increment = 0.000001m;
            this.tAlpha.Value = Convert.ToDecimal(material.tAlpha);
            this.Ry.Value = Convert.ToDecimal(material.Ry);
            this.CustomName.Text = this.material.Name;
        }
        #endregion

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.Density = new System.Windows.Forms.NumericUpDown();
            this.Density_label = new System.Windows.Forms.Label();
            this.Modulus = new System.Windows.Forms.NumericUpDown();
            this.Modulus_label = new System.Windows.Forms.Label();
            this.Nu = new System.Windows.Forms.NumericUpDown();
            this.Ratio_label = new System.Windows.Forms.Label();
            this.tAlpha = new System.Windows.Forms.NumericUpDown();
            this.tAlpha_label = new System.Windows.Forms.Label();
            this.Ry = new System.Windows.Forms.NumericUpDown();
            this.Ry_label = new System.Windows.Forms.Label();
            this.CustomName_label = new System.Windows.Forms.Label();
            this.CustomName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Density)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Modulus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tAlpha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ry)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOK.Location = new System.Drawing.Point(18, 209);
            this.btnOK.Margin = new System.Windows.Forms.Padding(2);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(275, 24);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.OK_Click);
            // 
            // Density
            // 
            this.Density.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Density.DecimalPlaces = 8;
            this.Density.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Density.Location = new System.Drawing.Point(18, 47);
            this.Density.Margin = new System.Windows.Forms.Padding(2);
            this.Density.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.Density.Name = "Density";
            this.Density.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Density.Size = new System.Drawing.Size(152, 26);
            this.Density.TabIndex = 1;
            this.Density.ValueChanged += new System.EventHandler(this.GenerateName);
            // 
            // Density_label
            // 
            this.Density_label.AutoSize = true;
            this.Density_label.Location = new System.Drawing.Point(188, 54);
            this.Density_label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Density_label.Name = "Density_label";
            this.Density_label.Size = new System.Drawing.Size(105, 13);
            this.Density_label.TabIndex = 2;
            this.Density_label.Text = "Density (ρ) [kg/mm3]";
            this.Density_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Modulus
            // 
            this.Modulus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Modulus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modulus.Increment = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.Modulus.Location = new System.Drawing.Point(18, 76);
            this.Modulus.Margin = new System.Windows.Forms.Padding(2);
            this.Modulus.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.Modulus.Name = "Modulus";
            this.Modulus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Modulus.Size = new System.Drawing.Size(152, 26);
            this.Modulus.TabIndex = 1;
            this.Modulus.ValueChanged += new System.EventHandler(this.GenerateName);
            // 
            // Modulus_label
            // 
            this.Modulus_label.AutoSize = true;
            this.Modulus_label.Location = new System.Drawing.Point(188, 83);
            this.Modulus_label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Modulus_label.Name = "Modulus_label";
            this.Modulus_label.Size = new System.Drawing.Size(94, 13);
            this.Modulus_label.TabIndex = 2;
            this.Modulus_label.Text = "Modulus (E) [MPa]";
            this.Modulus_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Nu
            // 
            this.Nu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Nu.DecimalPlaces = 3;
            this.Nu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nu.Location = new System.Drawing.Point(18, 107);
            this.Nu.Margin = new System.Windows.Forms.Padding(2);
            this.Nu.Name = "Nu";
            this.Nu.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Nu.Size = new System.Drawing.Size(152, 26);
            this.Nu.TabIndex = 1;
            this.Nu.ValueChanged += new System.EventHandler(this.GenerateName);
            // 
            // Ratio_label
            // 
            this.Ratio_label.AutoSize = true;
            this.Ratio_label.Location = new System.Drawing.Point(188, 114);
            this.Ratio_label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Ratio_label.Name = "Ratio_label";
            this.Ratio_label.Size = new System.Drawing.Size(89, 13);
            this.Ratio_label.TabIndex = 2;
            this.Ratio_label.Text = "Poisson\'s ratio (ο)";
            this.Ratio_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tAlpha
            // 
            this.tAlpha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tAlpha.DecimalPlaces = 6;
            this.tAlpha.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tAlpha.Location = new System.Drawing.Point(18, 136);
            this.tAlpha.Margin = new System.Windows.Forms.Padding(2);
            this.tAlpha.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.tAlpha.Name = "tAlpha";
            this.tAlpha.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tAlpha.Size = new System.Drawing.Size(152, 26);
            this.tAlpha.TabIndex = 1;
            this.tAlpha.ValueChanged += new System.EventHandler(this.GenerateName);
            // 
            // tAlpha_label
            // 
            this.tAlpha_label.AutoSize = true;
            this.tAlpha_label.Location = new System.Drawing.Point(188, 143);
            this.tAlpha_label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.tAlpha_label.Name = "tAlpha_label";
            this.tAlpha_label.Size = new System.Drawing.Size(90, 13);
            this.tAlpha_label.TabIndex = 2;
            this.tAlpha_label.Text = "Thermal alpha (α)";
            this.tAlpha_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Ry
            // 
            this.Ry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Ry.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ry.Location = new System.Drawing.Point(18, 165);
            this.Ry.Margin = new System.Windows.Forms.Padding(2);
            this.Ry.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.Ry.Name = "Ry";
            this.Ry.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Ry.Size = new System.Drawing.Size(152, 26);
            this.Ry.TabIndex = 1;
            this.Ry.ValueChanged += new System.EventHandler(this.GenerateName);
            // 
            // Ry_label
            // 
            this.Ry_label.AutoSize = true;
            this.Ry_label.Location = new System.Drawing.Point(188, 173);
            this.Ry_label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Ry_label.Name = "Ry_label";
            this.Ry_label.Size = new System.Drawing.Size(91, 13);
            this.Ry_label.TabIndex = 2;
            this.Ry_label.Text = "Yield stress [MPa]";
            this.Ry_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CustomName_label
            // 
            this.CustomName_label.AutoSize = true;
            this.CustomName_label.Location = new System.Drawing.Point(188, 25);
            this.CustomName_label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.CustomName_label.Name = "CustomName_label";
            this.CustomName_label.Size = new System.Drawing.Size(73, 13);
            this.CustomName_label.TabIndex = 2;
            this.CustomName_label.Text = "Material name";
            this.CustomName_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CustomName
            // 
            this.CustomName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CustomName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CustomName.Location = new System.Drawing.Point(18, 18);
            this.CustomName.Name = "CustomName";
            this.CustomName.Size = new System.Drawing.Size(152, 22);
            this.CustomName.TabIndex = 3;
            // 
            // Material_Form
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(314, 244);
            this.Controls.Add(this.CustomName);
            this.Controls.Add(this.Ry_label);
            this.Controls.Add(this.Modulus_label);
            this.Controls.Add(this.Ry);
            this.Controls.Add(this.Modulus);
            this.Controls.Add(this.Ratio_label);
            this.Controls.Add(this.Nu);
            this.Controls.Add(this.tAlpha_label);
            this.Controls.Add(this.CustomName_label);
            this.Controls.Add(this.Density_label);
            this.Controls.Add(this.tAlpha);
            this.Controls.Add(this.Density);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Material_Form";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Custom Material";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.Density)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Modulus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tAlpha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ry)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.NumericUpDown Density;
        private System.Windows.Forms.Label Density_label;
        private System.Windows.Forms.NumericUpDown Modulus;
        private System.Windows.Forms.Label Modulus_label;
        private System.Windows.Forms.NumericUpDown Nu;
        private System.Windows.Forms.Label Ratio_label;
        private System.Windows.Forms.NumericUpDown tAlpha;
        private System.Windows.Forms.Label tAlpha_label;
        private System.Windows.Forms.NumericUpDown Ry;
        private System.Windows.Forms.Label Ry_label;
        private System.Windows.Forms.Label CustomName_label;
        private System.Windows.Forms.TextBox CustomName;

        private void OK_Click(object sender, EventArgs e)
        {
            ((Material_Comp)this.component).CurrentValue = CreateMaterial(this.CustomName.Text);
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();

        }

        private void GenerateName(object sender, EventArgs e)
        {
            Material mat = CreateMaterial("default");
            this.CustomName.Text = mat.GetName();
        }

        private Material CreateMaterial(string name)
        {
            double density = (double)this.Density.Value;
            double modulus = (double)this.Modulus.Value;
            double nu = (double)this.Nu.Value;
            double alpha = (double)this.tAlpha.Value;
            double ry = (double)this.Ry.Value;
            return new Material(Material.types.IsoLinEl, density, modulus, nu, alpha, ry, name);
        }
    }
}
