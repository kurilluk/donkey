﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace Donkey.Model.Nodal.AnalyticalData
{
    public class NodalDOF_Form : Form
    {
        NodalDOF_Comp component;
        private ToolTip toolTip1;
        private System.ComponentModel.IContainer components;
        private Label label1;
        private Label label2;
        NodalDOF dof;

        public NodalDOF_Form(NodalDOF_Comp component)
        {
           this.component = component;
           this.dof = component.CurrentValue;
           InitializeComponent();

           this.Tx.Checked = Convert.ToBoolean(dof.Tx);
           this.Ty.Checked = Convert.ToBoolean(dof.Ty);
           this.Tz.Checked = Convert.ToBoolean(dof.Tz);
           this.Rx.Checked = Convert.ToBoolean(dof.Rx);
           this.Ry.Checked = Convert.ToBoolean(dof.Ry);
           this.Rz.Checked = Convert.ToBoolean(dof.Rz);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnOK = new System.Windows.Forms.Button();
            this.Tx = new System.Windows.Forms.CheckBox();
            this.Ty = new System.Windows.Forms.CheckBox();
            this.Tz = new System.Windows.Forms.CheckBox();
            this.Rx = new System.Windows.Forms.CheckBox();
            this.Ry = new System.Windows.Forms.CheckBox();
            this.Rz = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOK.Location = new System.Drawing.Point(43, 115);
            this.btnOK.Margin = new System.Windows.Forms.Padding(2);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(112, 24);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.OK_Click);
            // 
            // Tx
            // 
            this.Tx.AutoSize = true;
            this.Tx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Tx.Location = new System.Drawing.Point(15, 31);
            this.Tx.Margin = new System.Windows.Forms.Padding(2);
            this.Tx.Name = "Tx";
            this.Tx.Size = new System.Drawing.Size(57, 17);
            this.Tx.TabIndex = 1;
            this.Tx.Text = "Lock X";
            this.toolTip1.SetToolTip(this.Tx, "Checked lock movement on X axis");
            this.Tx.UseVisualStyleBackColor = true;
            // 
            // Ty
            // 
            this.Ty.AutoSize = true;
            this.Ty.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ty.Location = new System.Drawing.Point(15, 53);
            this.Ty.Margin = new System.Windows.Forms.Padding(2);
            this.Ty.Name = "Ty";
            this.Ty.Size = new System.Drawing.Size(57, 17);
            this.Ty.TabIndex = 2;
            this.Ty.Text = "Lock Y";
            this.Ty.UseVisualStyleBackColor = true;
            // 
            // Tz
            // 
            this.Tz.AutoSize = true;
            this.Tz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Tz.Location = new System.Drawing.Point(15, 75);
            this.Tz.Margin = new System.Windows.Forms.Padding(2);
            this.Tz.Name = "Tz";
            this.Tz.Size = new System.Drawing.Size(57, 17);
            this.Tz.TabIndex = 2;
            this.Tz.Text = "Lock Z";
            this.Tz.UseVisualStyleBackColor = true;
            // 
            // Rx
            // 
            this.Rx.AutoSize = true;
            this.Rx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Rx.Location = new System.Drawing.Point(122, 31);
            this.Rx.Margin = new System.Windows.Forms.Padding(2);
            this.Rx.Name = "Rx";
            this.Rx.Size = new System.Drawing.Size(57, 17);
            this.Rx.TabIndex = 1;
            this.Rx.Text = "Lock X";
            this.toolTip1.SetToolTip(this.Rx, "Checked lock rotation on X axis");
            this.Rx.UseVisualStyleBackColor = true;
            // 
            // Ry
            // 
            this.Ry.AutoSize = true;
            this.Ry.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ry.Location = new System.Drawing.Point(122, 53);
            this.Ry.Margin = new System.Windows.Forms.Padding(2);
            this.Ry.Name = "Ry";
            this.Ry.Size = new System.Drawing.Size(57, 17);
            this.Ry.TabIndex = 2;
            this.Ry.Text = "Lock Y";
            this.Ry.UseVisualStyleBackColor = true;
            // 
            // Rz
            // 
            this.Rz.AutoSize = true;
            this.Rz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Rz.Location = new System.Drawing.Point(122, 75);
            this.Rz.Margin = new System.Windows.Forms.Padding(2);
            this.Rz.Name = "Rz";
            this.Rz.Size = new System.Drawing.Size(57, 17);
            this.Rz.TabIndex = 2;
            this.Rz.Text = "Lock Z";
            this.Rz.UseVisualStyleBackColor = true;
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 100;
            this.toolTip1.ReshowDelay = 50;
            this.toolTip1.ShowAlways = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Movement";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(119, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Rotation";
            // 
            // NodalDOF_Form
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(198, 156);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Rz);
            this.Controls.Add(this.Tz);
            this.Controls.Add(this.Ry);
            this.Controls.Add(this.Ty);
            this.Controls.Add(this.Rx);
            this.Controls.Add(this.Tx);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NodalDOF_Form";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Custom Degree of Freedom";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.CheckBox Tx;
        private System.Windows.Forms.CheckBox Ty;
        private System.Windows.Forms.CheckBox Tz;
        private System.Windows.Forms.CheckBox Rx;
        private System.Windows.Forms.CheckBox Ry;
        private System.Windows.Forms.CheckBox Rz;

        private void OK_Click(object sender, EventArgs e)
        {
            int x,y,z;
            x = Convert.ToInt32(Tx.Checked);
            y = Convert.ToInt32(Ty.Checked);
            z = Convert.ToInt32(Tz.Checked);

            int Mx, My, Mz;
            Mx = Convert.ToInt32(Rx.Checked);
            My = Convert.ToInt32(Ry.Checked);
            Mz = Convert.ToInt32(Rz.Checked);

            //This will compare new DOF with database, if match, return database value with correct name, else name custom
            ((NodalDOF_Comp)this.component).CurrentValue = NodalDOF.IsOnDatabase(new NodalDOF(x, y, z, Mx, My, Mz));
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();

            //if (this.m_owner == null)
            //    return;
            //if (!this.m_changed)
            //    return;
            //try
            //{
            //    this.m_owner.RecordUndoEvent("Slider settings");
            //}
            //catch (Exception ex)
            //{
            //    ProjectData.SetProjectError(ex);
            //    ProjectData.ClearProjectError();
            //}
            //this.m_owner.NickName = this.txtName.Text;
            //this.m_owner.Expression = this.txtExpression.Text;
            //switch (this.m_rounding)
            //{
            //    case 1:
            //        this.m_owner.Slider.Type = GH_SliderAccuracy.Integer;
            //        break;
            //    case 2:
            //        this.m_owner.Slider.Type = GH_SliderAccuracy.Even;
            //        break;
            //    case 3:
            //        this.m_owner.Slider.Type = GH_SliderAccuracy.Odd;
            //        break;
            //    default:
            //        this.m_owner.Slider.Type = GH_SliderAccuracy.Float;
            //        break;
            //}
            //switch (this.cmbGripStyle.SelectedIndex)
            //{
            //    case 0:
            //        this.m_owner.Slider.GripDisplay = GH_SliderGripDisplay.Shape;
            //        break;
            //    case 1:
            //        this.m_owner.Slider.GripDisplay = GH_SliderGripDisplay.ShapeAndText;
            //        break;
            //    case 2:
            //        this.m_owner.Slider.GripDisplay = GH_SliderGripDisplay.Numeric;
            //        break;
            //    default:
            //        this.m_owner.Slider.GripDisplay = GH_SliderGripDisplay.ShapeAndText;
            //        break;
            //}
            //this.m_owner.Slider.Minimum = this.numLower.Value;
            //this.m_owner.Slider.Maximum = this.numUpper.Value;
            //this.m_owner.Slider.Value = this.sldValue.Value;
            //this.m_owner.Slider.DecimalPlaces = Convert.ToInt32(this.sldDigits.Value);
            //this.m_owner.Slider.FixDomain();
            //this.m_owner.Slider.FixValue();
        }

        //private void Check_Click(object sender, MouseEventArgs e)
        //{
        //    if (e.Button == MouseButtons.Right)
        //    {
        //        AllUncheck();
        //        ((CheckBox)sender).Checked = true;
        //    }
        //}

        //private void AllUncheck()
        //{
        //    Tx.Checked = false;
        //    Ty.Checked = false;
        //    Tz.Checked = false;
        //    Rx.Checked = false;
        //    Ry.Checked = false;
        //    Rz.Checked = false;
        //}
    }
}
