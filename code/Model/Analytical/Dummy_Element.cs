﻿using Donkey.Model.Analytical.Data;
using Donkey.Model.Nodal.AnalyticalData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Donkey.Model.Analytical
{
    public class Dummy_Element : AnalyticalElement
    {
        public Dummy_Element(List<int> geometry_id, Model_Goo model)
            : this(geometry_id, model, new AreaLoad(), NodalDOF.None) { }
        public Dummy_Element(List<int> geometry_id, Model_Goo model, AreaLoad load, NodalDOF dof)
            : base(geometry_id, Profile.None, Material.None, model, load, dof)
        {
            base.DeadWeight = -1;
            base.isDummy = true;
        }

        public override string ToString()
        {
            Geometry.Types type = this.model.Geometries[this.Geometries_ID[0]].Type;
            string element;
            if (type == Geometry.Types.VTK_LINE || type == Geometry.Types.VTK_POLY_LINE)
            {
                if (this.Name == "Beam")
                    element = "Element: Beam(Dummy)";
                else
                    element = "Element: " + this.Name + "(Dummy Beam)";
            }
            else
            {
                if (this.Name == "Shell")
                    element = "Element: Shell)";
                else
                    element = "Element:" + this.Name + "(Shell)";
            }
            element += Environment.NewLine //TODO read element type
                //+ "Geometry: " + this.model.Geometries[this.Geometries_ID[0]].TypeName + Environment.NewLine
                //+ "Profile: " + this.Profile.ToString() + Environment.NewLine
                //+ "Material: " + this.Material.GetName() + Environment.NewLine
                + this.Load.ToString();
                //TODO if load != null - print load
            if (this.Support != NodalDOF.None)
                element += "Element_support: " + this.Support.ToString() + Environment.NewLine;
            return element;
        }

        public override string Print()
        {
            return "Dummy element";
        }

        public override bool CloneToModel(AnalyticalModel newModel, bool removeElementsDup, bool removeNodeDup)
        {
            int geom_count, geom_id;
            List<int> newGeometries_id = new List<int>(this.Geometries_ID.Count);
            //FOR EACH GEOMETRY IN ELEMENT
            foreach (int id in Geometries_ID)
            {
                geom_count = newModel.Geometries.Count;
                geom_id = model.Geometries[id].CloneToModel(newModel,removeElementsDup,removeNodeDup);
                if (geom_count != geom_id)
                    return false; //ELEMENT GEOMETRY DUPLICATION
                newGeometries_id.Add(geom_id);
            }
            //CLONE THIS ELEMENT TO NEW MODEL
            Dummy_Element newElement = new Dummy_Element(newGeometries_id, newModel, this.Load, this.Support);
            newElement.Name = this.Name;
            newModel.Elements.AppendData(newElement, true);
            //REGISTER PROPERTIES ID AND CREATE APPENDED DATA FOR VTU FILE
            newElement.Profile_ID = -1;  //newModel.Profiles.AppendData(this.Profile, true);
            newElement.Material_ID = -1; //newModel.Materials.AppendData(this.Material, true);
            if (Load.isValid)
                newElement.AreaLoad_ID = newModel.Loads.AppendData(this.Load, true);
            if (Support != NodalDOF.None)
                newElement.Support_ID = newModel.Supports.AppendData(this.Support, true);
            return true;
        }


    }
}
