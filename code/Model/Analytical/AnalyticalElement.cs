﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Donkey.Model.Analytical.Data;
using Donkey.Model.Geometry;
using Rhino.Geometry;
using Donkey.Model.Nodal.AnalyticalData;

namespace Donkey.Model.Analytical
{
    public class AnalyticalElement : Element
    {
        #region FIELD
        public Material Material { get; protected set; }
        public Profile Profile { get; protected set; }
        public AreaLoad Load { get; protected set; }
        public int DeadWeight { get; set; }
        public int Material_ID { get; protected set; }
        public int Profile_ID { get; protected set; }
        public int AreaLoad_ID { get; set; }
        public NodalDOF Support { get; set;}
        public int Support_ID { get; set; }
        public bool isDummy { get; protected set;}
        #endregion

        #region CONSTRUCTOR
        //BLANK CONTRUCTOR
        private AnalyticalElement() {}
        //DATA CONSTRUCTOR
        public AnalyticalElement(List<int> geometry_id, Profile profile, Material material, Model_Goo model)
            : this(geometry_id, profile, material, model, new AreaLoad(),NodalDOF.None)
        { }
        public AnalyticalElement(List<int> geometry_id, Profile profile, Material material, Model_Goo model, AreaLoad load, NodalDOF dof)
            :base(geometry_id, model)
        {
            this.Profile = profile;
            this.Material = material;
            this.InitData();
            this.DeadWeight = 0; //DEFAULT DEADWEIGHT ON (-1 OFF)
            this.Profile_ID = -1;
            this.Material_ID = -1;
            this.AreaLoad_ID = -1;
            this.Load = load;
            this.Support_ID = -1;
            this.Support = dof;
            this.isDummy = false;
        }
        //COPY CONSTRUCTOR
        public AnalyticalElement(AnalyticalElement instance) : base(instance) { }
        public AnalyticalElement(AnalyticalElement instance, ref Model_Goo model)
            :base(instance,ref model)
        {
            this.Profile = instance.Profile;
            this.Profile_ID = instance.Profile_ID;
            this.Material = instance.Material;
            this.Material_ID = instance.Material_ID;
            this.DeadWeight = instance.DeadWeight;
            this.AreaLoad_ID = instance.AreaLoad_ID;
            this.Load = new AreaLoad(instance.Load); //HACK!!!
            this.Support = new NodalDOF(instance.Support);
            this.Support_ID = instance.Support_ID;
            this.isDummy = instance.isDummy;
        }

        private void InitData()
        {
            foreach (int id in Geometries_ID)
            {
                if(model.Geometries[id].Type == Geometry.Types.VTK_LINE || model.Geometries[id].Type == Geometry.Types.VTK_POLY_LINE)
                model.Geometries[id].SetProfileShape(Profile);
            }
        }
        #endregion

        #region METHODS
        //RESET AND CLONE GEOMETRY TO OTHER MODEL
        public override bool CloneToModel(AnalyticalModel newModel, bool removeElementsDup, bool removeNodeDup)
        {
            int geom_count,geom_id;
            List<int> newGeometries_id = new List<int>(this.Geometries_ID.Count);
            //FOR EACH GEOMETRY IN ELEMENT
            foreach (int id in Geometries_ID)
            {
                geom_count = newModel.Geometries.Count;
                geom_id = model.Geometries[id].CloneToModel(newModel,removeElementsDup, removeNodeDup);
                if(geom_count != geom_id)
                    return false; //ELEMENT GEOMETRY DUPLICATION
                newGeometries_id.Add(geom_id);
            }
            //CLONE THIS ELEMENT TO NEW MODEL
            AnalyticalElement newElement = new AnalyticalElement(newGeometries_id, this.Profile, this.Material, newModel, this.Load, this.Support);
            newElement.Name = this.Name;
            newModel.Elements.AppendData(newElement, true);
            //REGISTER PROPERTIES ID AND CREATE APPENDED DATA FOR VTU FILE
            newElement.Profile_ID = newModel.Profiles.AppendData(this.Profile, true);
            newElement.Material_ID = newModel.Materials.AppendData(this.Material, true);
            if(Load.isValid)
                newElement.AreaLoad_ID = newModel.Loads.AppendData(this.Load, true);
            if (Support != NodalDOF.None)
                newElement.Support_ID = newModel.Supports.AppendData(this.Support, true);
            return true; 
        }
        private void RegisterElementsProperties(AnalyticalModel model)
        {
            this.Profile_ID = model.Profiles.AppendData(this.Profile, true);
            this.Material_ID = model.Materials.AppendData(this.Material, true);
        }

        public override string ToString()
        {
            Geometry.Types type = this.model.Geometries[this.Geometries_ID[0]].Type;
            string element;
            if (type == Geometry.Types.VTK_LINE || type == Geometry.Types.VTK_POLY_LINE)
            {
                if (this.Name == "Beam" || this.Name == "")
                    element = "Element: Beam";
                else
                    element = "Element: " + this.Name + " (B)";
            }
            else
            {
                if(this.Name == "Shell" || this.Name == "")
                    element = "Element: Shell)";
                else
                    element = "Element:" + this.Name + " (S)";
            }
            string toString = element + Environment.NewLine //TODO read element type,  "Normal: "+ this.model.Geometries[this.Geometries_ID[0]].Normal
                //+ "Geometry: " + this.model.Geometries[this.Geometries_ID[0]].TypeName + Environment.NewLine
                + "Profile: " + this.Profile.ToString() + Environment.NewLine
                + "Material: " + this.Material.GetName() + Environment.NewLine
                + this.Load.ToString(); //posibility to return none load..
            if (this.Support != NodalDOF.None)
                toString += "Element_support: " + this.Support.ToString() + Environment.NewLine;
            return toString;
        }

        public virtual string Print()
        {
            Geometry.Types type = this.model.Geometries[this.Geometries_ID[0]].Type;
            string element;
            if (type == Geometry.Types.VTK_LINE || type == Geometry.Types.VTK_POLY_LINE)
            {
                element = "Beam, ";
            }
            else
            {
                element = "Shell, ";
            }
            return element + this.Profile.ToString() +", " + this.Material.GetName();
        }
        #endregion

        public void AddAreaLoad(Vector3d vector, double p)
        {
            this.Load = new AreaLoad(this, vector, p);
        }
    }
}
