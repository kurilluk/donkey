﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Drawing;

namespace Donkey.Model.Analytical
{
    class AnalyticalModel_Param : GH_Param<AnalyticalModel>, IGH_PreviewObject
    {
        #region FIELD
        public bool Hidden { get; set; }
        #endregion

        #region CONSTRUCTOR
        public AnalyticalModel_Param()
            : base(new Grasshopper.Kernel.GH_InstanceDescription("Model", "M", "Donkey analytical model", "Donkey", "Model")) { }

        //constructor 1 overload        
        public AnalyticalModel_Param(GH_InstanceDescription nTag) : base(nTag){}
  

        //constructor 2 overload      
        public AnalyticalModel_Param(GH_InstanceDescription nTag, GH_ParamAccess access) : base(nTag, access) { }

        //registrate own guid number
        public override Guid ComponentGuid
        {
            get { return new Guid("958b5d53-adb3-4094-981e-e82aa48ec284"); }
        }
       #endregion

        #region IGH_PreviewObject
        public Rhino.Geometry.BoundingBox ClippingBox
        {
            get 
            {
                BoundingBox bb = BoundingBox.Empty;
                foreach (AnalyticalModel model in m_data.NonNulls)
                {
                    bb.Union(model.ClippingBox);
                }
                return bb;
            }
        }

        public void DrawViewportMeshes(IGH_PreviewArgs args)
        {
        }

        public void DrawViewportWires(IGH_PreviewArgs args)
        {
            //GH_PreviewWireArgs wireArgs;
            //if (this.Attributes.GetTopLevel.Selected)
            //{
            //     wireArgs = new GH_PreviewWireArgs(args.Viewport, args.Display, args.WireColour_Selected, 2);
            //}
            //else
            //{
            //     wireArgs = new GH_PreviewWireArgs(args.Viewport, args.Display, args.WireColour, 1);
            //}
            //args.Display.DepthMode = Rhino.Display.DepthMode.AlwaysInFront;
            //foreach (AnalyticalModel model in m_data.NonNulls)
            //{
            //    model.DrawElementIndex(args);
            //    model.DrawNodeIndex(args);
            //    //model.DrawGeometryIndex(args);
            //}
            //args.Display.DepthMode = Rhino.Display.DepthMode.Neutral;
        }

        public bool IsPreviewCapable
        {
            get { return true; }
        }
        #endregion

        #region OVERRIDE
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.model; }
        }

        protected override string Format(AnalyticalModel Data)
        {
            if ((object)Data == null)
                return "Null";
            string str;

            str = Data.Elements.Count + " elements" + Environment.NewLine;
                str += Data.Supports.Count+ " support types" + Environment.NewLine;
                str +=  Data.Loads.Count+" load types";
            return str;
        }
        #endregion
    }
}
