﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using System.Windows.Forms;

namespace Donkey.Model.Nodal.AnalyticalData
{
    public class NodalDOF_Param : GH_Param<NodalDOF>
    {
        #region FIELD
        public NodalDOF m_dof;
        #endregion

        #region CONSTRUCTOR
        public NodalDOF_Param()
            : base(new GH_InstanceDescription("DOF", "\x03BD", "Nodal degree of freedom", "Donkey", "Initialization"))
            {
                this.m_dof = new NodalDOF();
            }
        #endregion

        #region METHODS OVERRIDE
        public override Guid ComponentGuid
        {
            get { return new Guid("186F4C0F-D0DC-401E-94E0-5787EE563B7B"); }
        }

        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.dof; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.hidden; }
        }
        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            GH_IO.Serialization.GH_IWriter chunk = writer.CreateChunk("DOF");
            m_dof.Write(chunk);
            return base.Write(writer);
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            GH_IO.Serialization.GH_IReader chunk = reader.FindChunk("DOF");
            m_dof.Read(chunk);
            return base.Read(reader);
        }
        #endregion

        protected override void CollectVolatileData_Custom()
        {
            VolatileData.Clear();
            m_data.Append(new NodalDOF(m_dof));
        }

        public override void CreateAttributes()
        {
            m_attributes = new NodalDOF_Attributes(this);
        }

        #region MENU
        //public override void AppendAdditionalMenuItems(System.Windows.Forms.ToolStripDropDown menu)
        //{
        //    base.AppendAdditionalMenuItems(menu);

        //    ToolStripMenuItem toolStripMenuItem1 = GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Full", SetDOF, true, this.m_dof.ToString() == "111/111");
        //    toolStripMenuItem1.ToolTipText = "Rectangle cross section profile for Donkey." + Environment.NewLine + "If thikness is more that 0, then create tube profile";

        //    ToolStripMenuItem toolStripMenuItem2 = GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Y_Lock", SetDOF, true, this.m_dof.ToString() == "010/000");
        //    toolStripMenuItem2.ToolTipText = "Circle cross section profile for Donkey." + Environment.NewLine + "If thikness is more that 0, then create tube profile";        
        //}

        //private void SetDOF(Object sender, EventArgs e)
        //{
        //    switch (sender.ToString())
        //    {
        //        case "Full":
        //            this.m_dof = new NodalDOF();
        //            //this.m_custom = false;
        //            break;
        //        case "Y_Lock":
        //            this.m_dof = new NodalDOF(0, 1, 0, 0, 0, 0);
        //            //this.m_custom = false;
        //            break;
        //        case "Full_T":
        //            this.m_dof = new NodalDOF(1, 1, 1, 0, 0, 0);
        //            //this.m_custom = false;
        //            break;
        //        case "Full_R":
        //            this.m_dof = new NodalDOF(0, 0, 0, 1, 1, 1);
        //            //this.m_custom = false;
        //            break;
        //        case "None":
        //            this.m_dof = new NodalDOF(0, 0, 0, 0, 0, 0);
        //            //this.m_custom = false;
        //            break;
        //        case "Custom":
        //            //TODO int from NickName
        //            //Load_NickNameToDOF();
        //            //this.m_dof = new GH_DOF((int)s.Substring(0, 1), (int)s.Substring(1, 1), (int)s.Substring(2, 1), (int)s.Substring(4, 1), (int)s.Substring(5, 1), (int)s.Substring(6, 1));
        //            //TODO event reload
        //            //this.m_dof = new GH_DOF();
        //            //this.m_custom = true;
        //            break;

        //        default:
        //            this.m_dof = new NodalDOF();
        //            break;
        //    }
        //    this.ExpireSolution(true);
        //}
        #endregion

        //protected override GH_GetterResult Prompt_Plural(ref List<NodalDOF> values)
        //{
        //    return GH_GetterResult.success;
        //}

        //protected override GH_GetterResult Prompt_Singular(ref NodalDOF value)
        //{
        //    return GH_GetterResult.cancel;
        //}
    }
}
