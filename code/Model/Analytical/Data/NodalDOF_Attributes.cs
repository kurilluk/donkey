﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Grasshopper.GUI.Canvas;
using System.Drawing;
using System.Windows.Forms;

namespace Donkey.Model.Nodal.AnalyticalData
{
    public class NodalDOF_Attributes : GH_Attributes<NodalDOF_Param>
    {
        public NodalDOF_Attributes(NodalDOF_Param owner) : base(owner) { }

        public override bool HasInputGrip { get { return false; } }
        public override bool HasOutputGrip { get { return true; } }

        #region LAYOUT
        private RectangleF TextArea
        {
            get
            {
                RectangleF rec = Bounds;
                rec.Width -= 32;
                rec.X = rec.Left + rec.Height;
                return rec;
            }
        }
        private RectangleF ButtonArea
        {
            get
            {
                RectangleF rec = Bounds;
                rec.X = rec.Left; //rec.Right +rec.Height;
                rec.Width = rec.Height;
                rec.Inflate(-6, -6);
                return rec;
            }
        }

        protected override void Layout()
        {
            Bounds = new System.Drawing.RectangleF(Pivot, new SizeF(112, 24));
        }
        #endregion

        #region RENDER
        protected override void Render(GH_Canvas canvas, Graphics graphics, GH_CanvasChannel channel)
        {
            if (channel == GH_CanvasChannel.Objects)
            {
                GH_Capsule capsule = GH_Capsule.CreateCapsule(Bounds, GH_Palette.White);
                capsule.AddOutputGrip(OutputGrip);
                capsule.Render(graphics, Selected, Owner.Locked, true);
                capsule.Dispose();

                GH_Capsule button = GH_Capsule.CreateCapsule(ButtonArea, GH_Palette.Grey);
                button.Render(graphics, Selected, Owner.Locked, false);
                button.Dispose();

                graphics.DrawString(Owner.m_dof.ToString(),
                                    Grasshopper.Kernel.GH_FontServer.Standard,
                                    Brushes.Black,
                                    TextArea,
                                    Grasshopper.GUI.GH_TextRenderingConstants.NearCenter);
            }
        }
        #endregion

        #region MENU
        //Sender - handler pre menu
        public override GH_ObjectResponse RespondToMouseDown(GH_Canvas sender, Grasshopper.GUI.GH_CanvasMouseEvent e)
        {
            if (ButtonArea.Contains(e.CanvasLocation))
            {
                ContextMenuStrip menu = new ContextMenuStrip();
                menu.ShowCheckMargin = true;
                menu.ShowImageMargin = false;

                ToolStripMenuItem toolStripMenuItem5 = GH_DocumentObject.Menu_AppendItem(menu, "None", SetDOF, true, this.Owner.m_dof.ToString() == "000/000");
                toolStripMenuItem5.ToolTipText = "000/000" + Environment.NewLine + "0 is freedom, 1 is lock";      

                ToolStripMenuItem toolStripMenuItem1 = GH_DocumentObject.Menu_AppendItem(menu, "Full", SetDOF, true, this.Owner.m_dof.ToString() == "111/111");
                toolStripMenuItem1.ToolTipText = "111/111" + Environment.NewLine + "0 is freedom, 1 is lock";

                ToolStripMenuItem toolStripMenuItem3 = GH_DocumentObject.Menu_AppendItem(menu, "Full_T", SetDOF, true, this.Owner.m_dof.ToString() == "111/000");
                toolStripMenuItem3.ToolTipText = "111/000" + Environment.NewLine + "0 is freedom, 1 is lock";

                ToolStripMenuItem toolStripMenuItem4 = GH_DocumentObject.Menu_AppendItem(menu, "Full_R", SetDOF, true, this.Owner.m_dof.ToString() == "000/111");
                toolStripMenuItem4.ToolTipText = "000/111" + Environment.NewLine + "0 is freedom, 1 is lock";

                ToolStripMenuItem toolStripMenuItem2 = GH_DocumentObject.Menu_AppendItem(menu, "Only_T/y", SetDOF, true, this.Owner.m_dof.ToString() == "010/000");
                toolStripMenuItem2.ToolTipText = "010/000" + Environment.NewLine + "0 is freedom, 1 is lock";

                ToolStripMenuItem toolStripMenuItem6 = GH_DocumentObject.Menu_AppendItem(menu, "Only_TR/y", SetDOF, true, this.Owner.m_dof.ToString() == "010/010");
                toolStripMenuItem6.ToolTipText = "010/010" + Environment.NewLine + "0 is freedom, 1 is lock";

                menu.Show(sender, e.ControlLocation);
                return GH_ObjectResponse.Handled;
            }

            return base.RespondToMouseDown(sender, e);
        }

        private void SetDOF(Object sender, EventArgs e)
        {
            switch (sender.ToString())
            {
                case "Full":
                    Owner.m_dof = new NodalDOF();
                    //this.m_custom = false;
                    break;
                case "Only_T/y":
                    Owner.m_dof = new NodalDOF(0, 1, 0, 0, 0, 0);
                    //this.m_custom = false;
                    break;
                case "Only_TR/y":
                    Owner.m_dof = new NodalDOF(0, 1, 0, 0, 1, 0);
                    //this.m_custom = false;
                    break;
                case "Full_T":
                    Owner.m_dof = new NodalDOF(1, 1, 1, 0, 0, 0);
                    //this.m_custom = false;
                    break;
                case "Full_R":
                    Owner.m_dof = new NodalDOF(0, 0, 0, 1, 1, 1);
                    //this.m_custom = false;
                    break;
                case "None":
                    Owner.m_dof = new NodalDOF(0, 0, 0, 0, 0, 0);
                    //this.m_custom = false;
                    break;
                case "Custom":
                    //TODO int from NickName
                    //Load_NickNameToDOF();
                    //this.m_dof = new GH_DOF((int)s.Substring(0, 1), (int)s.Substring(1, 1), (int)s.Substring(2, 1), (int)s.Substring(4, 1), (int)s.Substring(5, 1), (int)s.Substring(6, 1));
                    //TODO event reload
                    //this.m_dof = new GH_DOF();
                    //this.m_custom = true;
                    break;

                default:
                    Owner.m_dof = new NodalDOF();
                    break;
            }
            Owner.ExpireSolution(true);
        }
        #endregion
    }
}
