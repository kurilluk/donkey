﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Donkey.Interface;
using Grasshopper.Kernel.Types;

namespace Donkey.Model.Nodal.AnalyticalData
{
    [Serializable()]
    public class NodalDOF : GH_Goo<Object>, IModelParts, IAppendedData
    {
        #region FIELD
        private int[] m_data;
        private names m_name;
        
       public static Dictionary<string, NodalDOF> Database;

       static NodalDOF()
       {
           Database = new Dictionary<string, NodalDOF>()
           {
               {names.None.ToString(), new NodalDOF(0,0,0,0,0,0,names.None)},
               {names.Full.ToString(), new NodalDOF(1,1,1,1,1,1,names.Full)},
               {names.Full_R.ToString(), new NodalDOF(0,0,0,1,1,1,names.Full_R)},
               {names.Full_T.ToString(), new NodalDOF(1,1,1,0,0,0,names.Full_T)},
               {names.Only_Y.ToString(), new NodalDOF(0,1,0,0,1,0,names.Only_Y)}
           };
       }

       public static NodalDOF Get(names name)
       {
           return Database[name.ToString()];
       }

       public static NodalDOF None
       {
           get { return NodalDOF.Get(names.None); }
       }

       public static NodalDOF IsOnDatabase(NodalDOF dof)
       {
           foreach (NodalDOF d_dof in Database.Values)
           {
               if (d_dof.EqualsDOF(dof))
                   return d_dof;
           }
           return dof;
       }

        #endregion

        #region Constructor

        public NodalDOF() :this(1,1,1,1,1,1,names.Full){ }

        public NodalDOF(int tx, int ty, int tz, int rx, int ry, int rz) : this(tx, ty,tz, rx, ry, rz, names.Custom){}

        public NodalDOF(int tx, int ty, int tz, int rx, int ry, int rz, names name)
        {
            m_data = new int[6]{tx,ty,tz,rx,ry,rz};
            m_name = name;
        }

        public NodalDOF(NodalDOF dof)
        {
            this.m_data = dof.m_data;
            this.m_name = dof.m_name;
        }

        #endregion

        #region Parameters

        public string Name
        {
            get { return this.m_name.ToString(); }
        }

        public int Tx
        {
            get { return this.m_data[0]; }
            set { this.m_data[0] = value; }
        }

        public int Ty
        {
            get { return this.m_data[1]; }
            set { this.m_data[1] = value; }
        }

        public int Tz
        {
            get { return this.m_data[2]; }
            set { this.m_data[2] = value; }
        }

        public int Rx
        {
            get { return this.m_data[3]; }
            set { this.m_data[3] = value; }
        }

        public int Ry
        {
            get { return this.m_data[4]; }
            set { this.m_data[4] = value; }
        }

        public int Rz
        {
            get { return this.m_data[5]; }
            set { this.m_data[5] = value; }
        }

        #endregion

        #region METHODS

        public override string ToString()
        {
            return "" + Tx + "" + Ty + "" + Tz + "/" + Rx + "" + Ry + "" + Rz + "";
        }

        public string ToFile()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < m_data.Length; i++)
            {
                sb.Append(m_data[i] + " ");
            }
            return "NodePV 6 " + sb.ToString();
        }

        #endregion

        #region EQUALS

        //OVERRIDE EQUALS OBJECT
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Support return false.
            NodalDOF s = obj as NodalDOF;
            if ((System.Object)s == null)
            {
                return false;
            }

            // Return true if the fields match:
            return EqualsDOF(s);
        }

        public bool Equals(NodalDOF dof)
        {
            // If parameter is null return false:
            if ((object)dof == null)
            {
                return false;
            }

            // Return true if the fields match:
            return EqualsDOF(dof);
        }
        private bool EqualsDOF(NodalDOF dof)
        {
            for (int i = 0; i < this.m_data.Length; i++)
            {
                if (this.m_data[i] != dof.m_data[i])
                {
                    return false;
                }
            }
            return true;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 13;
                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + Rx.GetHashCode();
                hash = hash * 23 + Ry.GetHashCode();
                hash = hash * 23 + Rz.GetHashCode();
                hash = hash * 23 + Tx.GetHashCode();
                hash = hash * 23 + Ty.GetHashCode();
                hash = hash * 23 + Tz.GetHashCode();
                return hash;
            }

        }
        #endregion

        public void RegisterModel(Model_Goo model)
        {  }

        #region GH_Goo
        public override IGH_Goo Duplicate()
        {
            return new NodalDOF(this);
        }

        public override bool IsValid
        {
            get { return true; }
        }

        public override string TypeDescription
        {
            get { return this.ToString(); }
        }

        public override string TypeName
        {
            get { return "DOF"; }
        }
        #endregion

        #region OVERRIDE
        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetInt32("Tx", (int)this.Tx);
            writer.SetInt32("Ty", (int)this.Ty);
            writer.SetInt32("Tz", (int)this.Tz);
            writer.SetInt32("Rx", (int)this.Rx);
            writer.SetInt32("Ry", (int)this.Ry);
            writer.SetInt32("Rz", (int)this.Rz);
            writer.SetInt32("dofType", (int)this.m_name);
            return true;
        }
        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            this.Tx = reader.GetInt32("Tx");
            this.Ty = reader.GetInt32("Ty");
            this.Tz = reader.GetInt32("Tz");
            this.Rx = reader.GetInt32("Rx");
            this.Ry = reader.GetInt32("Ry");
            this.Rz = reader.GetInt32("Rz");
            this.m_name = (NodalDOF.names)reader.GetInt32("dofType");
            return true;
        }
        #endregion

        #region NAMES
        public enum names : int
        {
            Custom = -1,
            None = 0,
            Full = 1,
            Full_R = 2,
            Full_T = 3,
            Only_Y = 4,
            Only_TY = 5    
        }
        #endregion
    }
}
