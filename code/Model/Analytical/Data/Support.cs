﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rhino.Geometry;
using Grasshopper.Kernel.Types;
using Donkey.Model.Geometry;
using Grasshopper.Kernel;
using Donkey.Model.Nodal.AnalyticalData;
using System.Drawing;

namespace Donkey.Model.Analytical.Data
{
    public class Support : AnalyticalModelData
    {
        #region FIELD
        private NodalDOF m_dof;
        private Node m_pt;
        public double Dot_size { get; private set; }
        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        public Support() : this(Point3d.Unset, new NodalDOF()) { }
        //DATA CONSTRUCTOR
        public Support(Node node, NodalDOF dof)
            :base(-1, node)
        {
            this.m_pt = node;
            this.m_dof = dof;
            this.Dot_size = 100;

        }
        //COPY CONSTRUCTOR
        public Support(Support instance)
            :base (instance)
        {
            this.m_dof = instance.m_dof;
        }
        #endregion

        #region OVERRIDE TO FILE AND HOW TO APPEND DATA
        public override bool AppendData(AnalyticalModel model)
        {
            //FIND INDEX OF MODEL NODE
            Node node = model.Nodes.Find
                (delegate( Node n)
                {
                    return n.Equals(this.ModelPart);
                });
            if (node == null)
                return false; //OR model.Nodes.AppendData(this.ModelPart, false);
            //APPEND MODEL DATA
            int d_id = model.Supports.AppendData(this.m_dof, true);
            //SET DOF TO MODEL NODE
            node.Support = d_id;
            return true;
        }
        public override IGH_Goo Duplicate()
        {
            return new Support(this);
        }

        public override string ToString()
        {
            return "Nodal_support: "+ m_dof.ToString();
        }

        public override string TypeDescription
        {
            get { return "Support"; }
        }

        public override string TypeName
        {
            get { return "Support"; }
        }

        public override BoundingBox ClippingBox
        {
            get { return BoundingBox.Empty; }
        }
        #endregion

        #region PREVIEW
        public BoundingBox BBox
        {
            get { return new BoundingBox(m_pt.X - Dot_size*3,
                m_pt.Y - Dot_size * 3, m_pt.Z - Dot_size * 3,
                m_pt.X + Dot_size * 3, m_pt.Y + Dot_size * 3, m_pt.Z + Dot_size * 3);
            }
        }
        public override void DrawViewportWires(GH_PreviewWireArgs args)
        {
            //args.Pipeline.DrawDot((Node)this.ModelPart, m_dof.ToString()); //(this.ModelPart,new Vector3d(0,0,-1),args.Color);
            //args.Pipeline.Draw2dText(m_dof.ToString(), args.Color, (Node)this.ModelPart, false, 24);
            Point3d origin = (Node)this.ModelPart;
            double offset = 2.5;
            double dot_size = this.Dot_size;
            Color color = args.Color;
            //DRAW AXIES
            args.Pipeline.DrawLine(new Line(origin + Vector3d.XAxis * (dot_size * 0.75), Vector3d.XAxis, dot_size), color, 5);
            args.Pipeline.DrawLine(new Line(origin - Vector3d.XAxis * (dot_size * 0.75), -Vector3d.XAxis, dot_size), color, 5);

            args.Pipeline.DrawLine(new Line(origin + Vector3d.YAxis * (dot_size * 0.75), Vector3d.YAxis, dot_size), color, 5);
            args.Pipeline.DrawLine(new Line(origin - Vector3d.YAxis * (dot_size * 0.75), -Vector3d.YAxis, dot_size), color, 5);

            args.Pipeline.DrawLine(new Line(origin + Vector3d.ZAxis * (dot_size * 0.75), Vector3d.ZAxis, dot_size), color, 5);
            args.Pipeline.DrawLine(new Line(origin - Vector3d.ZAxis * (dot_size * 0.75), -Vector3d.ZAxis, dot_size), color, 5);

            if (this.m_dof.Tx == 0)
            {
                args.Pipeline.DrawArrowHead(origin + Vector3d.XAxis * dot_size * offset, Vector3d.XAxis, color, 0.0, dot_size * offset * 0.5);
                args.Pipeline.DrawArrowHead(origin - Vector3d.XAxis * dot_size * offset, -Vector3d.XAxis, color, 0.0, dot_size * offset * 0.5);
            }

            if (this.m_dof.Ty == 0)
            {
                args.Pipeline.DrawArrowHead(origin + Vector3d.YAxis * dot_size * offset, Vector3d.YAxis, color, 0.0, dot_size * offset * 0.5);
                args.Pipeline.DrawArrowHead(origin - Vector3d.YAxis * dot_size * offset, -Vector3d.YAxis, color, 0.0, dot_size * offset * 0.5);
            }

            if (this.m_dof.Tz == 0)
            {
                args.Pipeline.DrawArrowHead(origin + Vector3d.ZAxis * dot_size * offset, Vector3d.ZAxis, color, 0.0, dot_size * offset * 0.5);
                args.Pipeline.DrawArrowHead(origin - Vector3d.ZAxis * dot_size * offset, -Vector3d.ZAxis, color, 0.0, dot_size * offset * 0.5);
            }

            if (this.m_dof.Rx == 0)
            {
                Circle rot = new Circle(Plane.WorldYZ, origin + Vector3d.ZAxis * dot_size * offset * 0, dot_size * 0.75);
                args.Pipeline.DrawCircle(rot, color, 5);
            }

            if (this.m_dof.Ry == 0)
            {
                Circle rot = new Circle(Plane.WorldZX, origin + Vector3d.ZAxis * dot_size * offset * 0, dot_size * 0.75);
                args.Pipeline.DrawCircle(rot, color, 5);
            }

            if (this.m_dof.Rz == 0)
            {
                Circle rot = new Circle(Plane.WorldXY, origin + Vector3d.ZAxis * dot_size * offset * 0, dot_size * 0.75);
                args.Pipeline.DrawCircle(rot, color, 5);
            }
        }
        #endregion
    }
}
