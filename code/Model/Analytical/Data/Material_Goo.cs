﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel.Types;

namespace Donkey.Model.Analytical.Data
{
    public class Material_Goo : GH_Goo<Material>
    {
        #region PROPERTY

        #endregion

        #region CONSTRUCTOR

        //constructor default
        public Material_Goo() { this.Value = new Material(); }

        public Material_Goo(Material.types type) { this.Value = new Material(type); }

        public Material_Goo(double density, double E, double nu, double tAlpha, double Ry)
        {
            this.Value = new Material(Material.types.IsoLinEl, density, E, nu, tAlpha, Ry);
        }

        //constructor same type as base class
        public Material_Goo(Material mat)
        {
            this.Value = new Material(mat);
        }

        //copy constructor, same type
        public Material_Goo(Material_Goo instance)
        {
            this.Value = new Material(instance.Value);
        }
        #endregion

        #region GH_Goo Members
        public override IGH_Goo Duplicate()
        {
            return new Material_Goo(this);
        }

        public override bool IsValid
        {
            get { return true; }
        }

        public override string ToString()
        {
            return this.Value.ToString();
        }

        public override string TypeDescription
        {
            get { return "Donkey's materials"; }
        }

        public override string TypeName
        {
            get { return "D_Material"; }
        }

        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetDouble("Density", Value.Density);
            writer.SetDouble("E", Value.E);
            writer.SetDouble("nu", Value.nu);
            writer.SetDouble("Ry", Value.Ry);
            writer.SetDouble("tAlpha", Value.tAlpha);
            writer.SetInt32("Type", (int)Value.Type);

            return true;
        }
        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            Value.Density = reader.GetDouble("Density");
            Value.E = reader.GetDouble("E");
            Value.nu = reader.GetDouble("nu");
            Value.Ry = reader.GetDouble("Ry");
            Value.tAlpha  = reader.GetDouble("tAlpha");
            Value.Type = (Material.types)reader.GetInt32("Type");

            return true;
        }

        public override bool CastTo<Q>(ref Q target)
        {
            if (typeof(Q).IsAssignableFrom(typeof(double)))
            {
                object ptr = this.Value.Ry;
                target = (Q)ptr;
                return true;
            }

            if (typeof(Q).IsAssignableFrom(typeof(GH_Number)))
            {
                object ptr = new GH_Number(this.Value.Ry);
                target = (Q)ptr;
                return true;
            }

            return false;
        }
        #endregion
    }
}
