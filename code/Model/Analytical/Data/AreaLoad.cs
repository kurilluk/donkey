﻿using Grasshopper.Kernel.Types;
using Rhino.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Donkey.Model.Analytical.Data
{
    public class AreaLoad : NodalLoad
    {
        private AnalyticalElement m_element;
        private int dimension;
        public bool isValid {get; private set;}

        public AreaLoad()
        {
            this.isValid = false;
        }


        public AreaLoad(AnalyticalElement element, Vector3d forceDirection, double forceValue)
        :base(element.Center,forceDirection,forceValue)
        {
            this.isValid = true;
            this.m_element = element;
            Geometry.Types type = m_element.model.Geometries[m_element.Geometries_ID[0]].Type;
            if (type == Geometry.Types.VTK_LINE || type == Geometry.Types.VTK_POLY_LINE)
                this.dimension = 1;
            else
                this.dimension = 2;
        }

        public AreaLoad(AreaLoad instance)
            : base((NodalLoad)instance)
        {
            this.m_element = instance.m_element;
            this.dimension = instance.dimension;
            this.isValid = instance.isValid;
        }

        public override bool AppendData(AnalyticalModel model)
        {
            if (isValid)
            {
                //FIND INDEX OF MODEL NODE
                Element element = model.Elements.Find
                    (delegate(Element n)
                    {
                        return n.Equals(this.ModelPart);
                    });
                if (element == null)
                    return false; //OR model.Nodes.AppendData(this.ModelPart, false);
                //APPEND MODEL DATA
                int d_id = model.Loads.AppendData(this, true);
                //SET NODAL LOAD TO MODEL NODE
                if (element is AnalyticalElement)
                {
                    AnalyticalElement analytal_element = (AnalyticalElement)element;
                    analytal_element.AreaLoad_ID = d_id;
                    return true;
                }
            }
            return false;
        }

        public override string ToFile()
        {
            if (this.dimension == 1)
                return "ConstantEdgeLoad components 6 " + m_vector.X + " " + m_vector.Y + " " + m_vector.Z + " 0 0 0";
            else
                return "ConstantSurfaceLoad components 6 " + m_vector.X + " " + m_vector.Y + " " + m_vector.Z + " 0 0 0";          
        }
        public override IGH_Goo Duplicate()
        {
            return new AreaLoad(this);
        }

        public override string ToString()
        {
            if (isValid)
            {
                string type;
                string units;
                if (this.dimension == 1)
                {
                    type = "Edge";
                    units = "N/mm";
                }
                else
                {
                    type = "Surface";
                    units = "N/mm\u00B2";
                }

                return type+ "_load: " + this.m_force + " " +units+ ", {" + this.m_vector.ToString() + "}" + Environment.NewLine;
            }
            return "";
        }

        public override string TypeDescription
        {
            get { return "Area Load"; }
        }

        public override string TypeName
        {
            get { return "Area Load"; }
        }

        public override bool Equals(object obj)
        {
            if (obj is AreaLoad)
            {
                AreaLoad al_obj = (AreaLoad)obj;
                if (al_obj.isValid && this.dimension == al_obj.dimension)
                    return Equals(al_obj);
                else 
                    return false;
            }else
            return false;
        }
        //EQUALS FOR NODAL LOAD OBJECT
        public bool Equals(AreaLoad n)
        {
            // If parameter is null return false:
            if ((object)n == null)
                return false;
            if (n.Type != this.Type)
                return false;
            // Return true if the fields match:
            return EqualsCoordinates((AreaLoad)n);
        }
        //COORDINATES EQUALS
        private bool EqualsCoordinates(AreaLoad n)
        {
            return this.m_vector.Equals(n.m_vector);
        }
    }
}
