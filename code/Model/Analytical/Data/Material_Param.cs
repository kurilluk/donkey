﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
//using Donkey.Model.Analytical.Initialization;
using System.Drawing;
using System.Windows.Forms;
//using System.Windows.Forms;

namespace Donkey.Model.Analytical.Data
{
    public class Material_Param : GH_Param<Material_Goo>
    {
        #region FIELD
        private Material_Goo m_material; //TODO material 
        #endregion

        #region CONSTRUCTOR
        public Material_Param()
            : base(new GH_InstanceDescription("Material", "M", "A bunch of material presets", "Donkey", "Obsolete"))
            {
                m_material = new Material_Goo(new Material());
            }
        #endregion

        #region OVERRIDE
        protected override void CollectVolatileData_Custom()
        {
            VolatileData.Clear();
            m_data.Append(new Material_Goo(m_material));
        }
        
        public override Guid ComponentGuid
        {
            get { return new Guid("{30FE5ECD-96DF-44C1-A575-86C5728D6638}"); }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.material; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.hidden; }
        }
        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            GH_IO.Serialization.GH_IWriter chunk = writer.CreateChunk("Material");
            m_material.Write(chunk);

            return base.Write(writer);
        }
        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            GH_IO.Serialization.GH_IReader chunk = reader.FindChunk("Material");
            m_material.Read(chunk);

            return base.Read(reader);
        }

        public override void CreateAttributes()
        {
            m_attributes = new Material_Attributes(this);
        }
        #endregion

        #region MENU PARAM
        public override void AppendAdditionalMenuItems(System.Windows.Forms.ToolStripDropDown menu)
        {
            base.AppendAdditionalMenuItems(menu);
            //this.Menu_AppendWireDisplay(menu);
            //this.Menu_AppendDisconnectWires(menu);
            ////this.Menu_AppendReverseParameter(menu);
            ////this.Menu_AppendFlattenParameter(menu);
            ////this.Menu_AppendGraftParameter(menu);
            ////this.Menu_AppendSimplifyParameter(menu);
            //GH_DocumentObject.Menu_AppendSeparator((ToolStrip) menu);

        //    ToolStripMenuItem toolStripMenuItem1 = GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Steel", SetMaterial, true, this.m_material.Value.Name() == "Steel");
        //    toolStripMenuItem1.ToolTipText = "Rectangle cross section profile for Donkey." + Environment.NewLine + "If thikness is more that 0, then create tube profile";
        //    ToolStripMenuItem toolStripMenuItem2 = GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Timber", SetMaterial, true, this.m_material.Value.Name() == "Timber");
        //    toolStripMenuItem2.ToolTipText = "Circle cross section profile for Donkey." + Environment.NewLine + "If thikness is more that 0, then create tube profile";
        }

        //private void SetMaterial(Object sender, EventArgs e)
        //{
        //    switch (sender.ToString())
        //    {
        //        case "Steel":
        //            this.RecordUndoEvent("Steel");
        //            this.m_material = new Material_Goo(Material.Steel);
        //            this.ExpireSolution(true);
        //            break;
        //        case "Timber":
        //            this.RecordUndoEvent("Timber");
        //            this.m_material = new Material_Goo(Material.ConiferousTimber);
        //            this.ExpireSolution(true);
        //            break;
        //        default:
        //            break;
        //    }
        //}
        #endregion

        public Material CurrentPreset
        {
            get { return new Material(m_material.Value); }
            set
            {
                if (value == null)
                    value = new Material();
                m_material = new Material_Goo(new Material(value));
            }
        }
    }
}
