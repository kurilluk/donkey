﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Drawing;
namespace Donkey.Model.Analytical.Data
{
    class AnalyticalModelData_Param : GH_Param<AnalyticalModelData>, IGH_PreviewObject
    {
        #region FIELD
        public bool Hidden { get; set; }
        #endregion

         #region CONSTRUCTOR
        public AnalyticalModelData_Param()
            : base(new Grasshopper.Kernel.GH_InstanceDescription("Conditions", "C", "Analytical model conditions", "Donkey", "Model")) { }

        //constructor 1 overload        
        public AnalyticalModelData_Param(GH_InstanceDescription nTag) : base(nTag){}
  

        //constructor 2 overload      
        public AnalyticalModelData_Param(GH_InstanceDescription nTag, GH_ParamAccess access) : base(nTag, access) { }

        //registrate own guid number
        public override Guid ComponentGuid
        {
            get { return new Guid("68319f7d-ff7f-4a14-95d4-0b6378fb4ab9"); }
        }
       #endregion

        #region IGH_PreviewObject
        public Rhino.Geometry.BoundingBox ClippingBox
        {
            get
            {
                BoundingBox bb = BoundingBox.Empty;
                foreach (AnalyticalModelData data in m_data.NonNulls)
                {
                    bb.Union(data.ClippingBox);
                }
                return bb;
            }
        }

        public void DrawViewportMeshes(IGH_PreviewArgs args)
        {
        }

        public void DrawViewportWires(IGH_PreviewArgs args)
        {
            GH_PreviewWireArgs wireArgs;
            if (this.Attributes.GetTopLevel.Selected)
            {
                wireArgs = new GH_PreviewWireArgs(args.Viewport, args.Display, args.WireColour_Selected, 2);
            }
            else
            {
                wireArgs = new GH_PreviewWireArgs(args.Viewport, args.Display, args.WireColour, 1);
            }
            foreach (AnalyticalModelData data in m_data.NonNulls)
            {
                data.DrawViewportWires(wireArgs);
            }
        }

        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.model; }
        }

        public bool IsPreviewCapable
        {
            get { return true; }
        }
        #endregion
    }
}
