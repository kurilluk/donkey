﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Donkey.Interface;

namespace Donkey.Model.Analytical.Data
{
    public class Load : AnalyticalModelData
    {
        #region FIELD
        public LoadType Type { get; protected set; }
        #endregion

        #region CONSTRUCTOR
        protected Load() { }
        protected Load(int index, IModelParts modelPart)
            : base(index, modelPart) { }
        protected Load(Load instance)
            : base(instance) { }
        #endregion
    }

    public enum LoadType
    {
        None = 0,
        DeadWeight = 1,  //dimensionless
        NodalLoad = 2,   //N
        ConstantEdgeLoad = 3, //N/mm
        ConstantSurfaceLoad = 4,
        LinearEdgeLoad = 5,    //N/mm (two forces)
        LinearSurfaceLoad = 6, //N/mm2 or Face - OOFEM not support
        BodyLoad = 7     //N/mm3
    }

}
