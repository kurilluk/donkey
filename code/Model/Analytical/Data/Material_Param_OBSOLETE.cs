﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Grasshopper.GUI;
using System.Drawing;
using System.Windows.Forms;
using Donkey.Model.Nodal.AnalyticalData;

namespace Donkey.Model.Analytical.Data
{
    class Material_Param_OBSOLETE : GH_PersistentParam<Material_Goo>
    {
       #region CONSTRUCTOR

        public Material_Param_OBSOLETE()
            : base(new Grasshopper.Kernel.GH_InstanceDescription("Material", "M", "Material of element of Donkey model", "Donkey", "AnalyticalData")) { }

        //constructor 1 overload        
        public Material_Param_OBSOLETE(GH_InstanceDescription nTag) : base(nTag){}

        //constructor 2 overload      
        public Material_Param_OBSOLETE(GH_InstanceDescription nTag, GH_ParamAccess access) : base(nTag) 
        {
            this.Access = access;
        }

        #endregion

        #region PARAM

        //registrate own guid number
        public override Guid ComponentGuid
        {
            get { return new Guid("d5a87b7b-5c6c-477e-8169-cd3225c5b3d4"); }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.material; }
        }

        public override bool AppendMenuItems(ToolStripDropDown menu)
        {
            //return true;
            return base.AppendMenuItems(menu);
        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {
            // Place a call to the base class to ensure the default parameter menu
            // is still there and operational.
            //base.AppendAdditionalMenuItems(menu);

            // Now insert your own custom menu items.
            //GH_ActiveObject.Menu_AppendGenericMenuItem
            //Menu_AppendGenericMenuItem(menu, "Steel", Menu_MyCustomItemClicked, this.Icon, this, true, false);
        }

        protected override Material_Goo InstantiateT()
        {
            return new Material_Goo();
        }

        //private void Menu_MyCustomItemClicked(Object sender, EventArgs e)
        //{
        //    Rhino.RhinoApp.WriteLine("Alcohol doesn't affect me...");
        //}

        #endregion


        //why?? only remove from menu?
        protected override GH_GetterResult Prompt_Plural(ref List<Material_Goo> values)
        {
            return GH_GetterResult.cancel;
        }
        protected override GH_GetterResult Prompt_Singular(ref Material_Goo value)
        {
            return GH_GetterResult.cancel;
        }
    }
}
