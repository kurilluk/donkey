﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Donkey.Extensions;
using Donkey.Interface;

namespace Donkey.Model.Analytical.Data
{
    [Serializable()]
    public class Material : IAppendedData, IModelParts
    {
        #region STATIC

        //private static Material m_steel = new Material(types.IsoLinEl, 7850.0e-09, 210.0e+03, 0.20, 0.000012, 300); //FROM 230-1500 Ry
        //private static Material m_concrete = new Material(types.IsoLinEl, 2500.0e-09, 40.0e+03, 0.18, 0.000012, 15); //CANELED FOR NOW
        //private static Material m_coniferousTimber = new Material(types.IsoLinEl, 360.0e-09, 9.0e+03, 0.30, 0.000012, 15); //FROM 10-40 Ry
        //private static Material m_aluminium = new Material(types.IsoLinEl, 2150.0e-09, 70.0e+03, 0.33, 0.000012, 20); //Ry range??, tAlpha ??


        //public static Material Steel { get { return m_steel; } }
        //public static Material Concrete { get { return m_concrete; } }
        //public static Material ConiferousTimber { get { return m_coniferousTimber; } }

        public static Dictionary<string, Material> Database = new Dictionary<string, Material>()
            {
                {names.Steel.ToString(),new Material(types.IsoLinEl,7850.0e-09, 210.0e+03, 0.20, 0.000012, 300,names.Steel.ToString())},               //FROM 230-1500 Ry
                {names.Aluminium.ToString(),new Material(types.IsoLinEl,2150.0e-09, 70.0e+03, 0.33, 0.000012, 20, names.Aluminium.ToString())},   //Ry range??, tAlpha ??
                {names.Timber.ToString(),new Material(types.IsoLinEl,360.0e-09, 9.0e+03, 0.30, 0.000012, 15,names.Timber.ToString())}                    //FROM 10-40 Ry
            };

        //static Material()
        //{
        //    Database = new Dictionary<string, Material>()
        //    {
        //        {names.Steel.ToString(),new Material(types.IsoLinEl,7850.0e-09, 210.0e+03, 0.20, 0.000012, 300,names.Steel.ToString())},               //FROM 230-1500 Ry
        //        {names.Aluminium.ToString(),new Material(types.IsoLinEl,2150.0e-09, 70.0e+03, 0.33, 0.000012, 20, names.Aluminium.ToString())},   //Ry range??, tAlpha ??
        //        {names.Timber.ToString(),new Material(types.IsoLinEl,360.0e-09, 9.0e+03, 0.30, 0.000012, 15,names.Timber.ToString())}                    //FROM 10-40 Ry
        //    };
        //}

        public static Material Get(names name)
        {
            if (Database == null)
                return null;
            return Database[name.ToString()];
        }

        public static Material None
        {
            get { return new Material(types.None); }
        }


        #endregion

        #region FIELD
        //private types type;
        //private double density;
        //private double E;
        //private double nu;
        //private double tAlpha;
        //private double Ry;

        public types Type { get; set; }
        public double Density { get; set; }
        public double E { get; set; }
        public double nu { get; set; }
        public double tAlpha { get; set; }
        public double Ry { get; set; }
        public string Name { get; private set; }

        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        public Material() : this(types.IsoLinEl,7850.0e-09, 210.0e+03, 0.20, 0.000012, 300,names.Steel.ToString()) { }
        //Rigid Body CONSTRUCTOR (?)
        public Material(types type)
            : this(type,0.0, 0.0, 0.0, 0.0, 0.0)
        { }
        //DATA CONSTRUCTOR
        public Material(types type, double density, double E, double nu, double tAlpha, double Ry)
            : this(type, density, E, nu, tAlpha, Ry, names.Custom.ToString())
        { }

        public Material(types type, double density, double E, double nu, double tAlpha, double Ry, string name)
        {
            this.Type = type;
            this.Density = density;
            this.E = E;
            this.nu = nu;
            this.tAlpha = tAlpha;
            this.Ry = Ry;
            string newName = GetName();
            if (newName == names.Custom.ToString())
            {
                this.Name = name;
            }
            else { this.Name = newName; }
        }
        //COPY CONSTRUCTOR
        public Material(Material mat)
            : this(mat.Type, mat.Density, mat.E, mat.nu, mat.tAlpha, mat.Ry, mat.Name)
        { }

        private Material CorrectName(Material mat)
        {
            string name = mat.GetName();
            return new Material(mat.Type, mat.Density, mat.E, mat.nu, mat.tAlpha, mat.Ry, name);
        }
        #endregion

        #region METHODS

        public override string ToString()
        {
            if (this.Type == types.None)
                return "None";

            string name = Name;

            if (this.Type == types.RigidBody)
                return this.Type.ToString();

            return name +" material:"+ Environment.NewLine + Detail();
        }

        public string GetName()
        {
            if (this.Type == types.None)
                return "None";
            if (this.Type == Material.types.IsoLinEl)
            {
                string name = names.Custom.ToString();

                if (this.Equals(Material.Get(names.Steel)))
                {
                    if(this.Ry == 300)
                        return name = names.Steel.ToString();
                    if (this.Ry >= 230 && this.Ry <= 1500)
                        return name = names.Steel.ToString() + " (C)";
                }
                if (this.Equals(Material.Get(names.Aluminium)))
                {
                    return name = names.Aluminium.ToString();
                }
                if (this.Equals(Material.Get(names.Timber)))
                {
                    if (this.Ry == 15)
                        return name = names.Timber.ToString();
                    if (this.Ry >= 10 && this.Ry <= 40)
                        return name = names.Timber.ToString() + " (C)";
                }

                return name;
            }
            return "Rigid";
        }

        public string Detail()
        {
            return "[" + Density + "kg/mm\u00B3 " + E + "MPa " + nu + " " + tAlpha + " " + Ry + "MPa]";
        }

        public bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetDouble("Density", this.Density);
            writer.SetDouble("E", this.E);
            writer.SetDouble("nu", this.nu);
            writer.SetDouble("Ry", this.Ry);
            writer.SetDouble("tAlpha", this.tAlpha);
            writer.SetInt32("Type", (int)this.Type);
            writer.SetString("MaterialName", this.Name);

            return true;
        }
        public bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            this.Density = reader.GetDouble("Density");
            this.E = reader.GetDouble("E");
            this.nu = reader.GetDouble("nu");
            this.Ry = reader.GetDouble("Ry");
            this.tAlpha = reader.GetDouble("tAlpha");
            this.Type = (Material.types)reader.GetInt32("Type");
            this.Name = reader.GetString("MaterialName");

            return true;
        }

        #endregion

        #region EQUALS

        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if (obj == null || GetType() != obj.GetType()) return false;
            Material m = (Material)obj;
            return (Type == m.Type) && (Density == m.Density) && (E == m.E) && (nu == m.nu) && (tAlpha == m.tAlpha); // && (Ry == m.Ry);
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 15;
                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + Density.GetHashCode();
                hash = hash * 23 + E.GetHashCode();
                hash = hash * 23 + nu.GetHashCode();
                hash = hash * 23 + tAlpha.GetHashCode();
                return hash;
            }
        }
        #endregion

        #region TYPES
        public enum types : int
        {
            None = 0,
            IsoLinEl = 1,
            RigidBody = 2
        }
        #endregion

        #region NAMES
        public enum names : int
        {
            Custom = -1,
            Steel = 0,
            Aluminium = 1,
            Timber = 2,
            Concrete = 3
        }

        #endregion

        #region IAppendedData Members

        string IAppendedData.ToFile()
        {
            if (this.Type == types.IsoLinEl)
                return this.Type.ToString() + " E " + this.E + " nu " + this.nu + " tAlpha " + this.tAlpha + " density " + this.Density + " Ry " + this.Ry;

            if (this.Type == types.RigidBody)
                return this.Type.ToString();

            return "None";

        }

        #endregion

        public void RegisterModel(Model_Goo model)
        {
        }
    }
}
