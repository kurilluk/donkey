﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using System.Drawing;
using Grasshopper.GUI.Canvas;
using System.Windows.Forms;

namespace Donkey.Model.Analytical.Data
{
    public class Material_Attributes : GH_Attributes<Material_Param>
    {
        public Material_Attributes(Material_Param owner) : base(owner) { }

        public override bool HasInputGrip { get { return false; } }
        public override bool HasOutputGrip { get { return true; } }

        #region LAYOUT
        private RectangleF TextArea
        {
            get
            {
                RectangleF rec = Bounds;
                rec.Width -= 32;
                rec.X = rec.Left + rec.Height;
                return rec;
            }
        }
        private RectangleF ButtonArea
        {
            get
            {
                RectangleF rec = Bounds;
                rec.X = rec.Left; //rec.Right +rec.Height;
                rec.Width = rec.Height;
                rec.Inflate(-6, -6);
                return rec;
            }
        }

        protected override void Layout()
        {
            Bounds = new System.Drawing.RectangleF(Pivot, new SizeF(112, 24));
        }
        #endregion

        #region RENDER
        protected override void Render(GH_Canvas canvas, Graphics graphics, GH_CanvasChannel channel)
        {
            if (channel == GH_CanvasChannel.Objects)
            {
                GH_Capsule capsule = GH_Capsule.CreateCapsule(Bounds, GH_Palette.White);
                capsule.AddOutputGrip(OutputGrip);
                capsule.Render(graphics, Selected, Owner.Locked, true);
                capsule.Dispose();

                GH_Capsule button = GH_Capsule.CreateCapsule(ButtonArea, GH_Palette.Grey);
                button.Render(graphics, Selected, Owner.Locked, false);
                button.Dispose();

                graphics.DrawString(Owner.CurrentPreset.GetName(),
                                    Grasshopper.Kernel.GH_FontServer.Standard,
                                    Brushes.Black,
                                    TextArea,
                                    Grasshopper.GUI.GH_TextRenderingConstants.NearCenter);
            }
        }
        #endregion

        #region MENU
        //Sender - handler pre menu
        public override GH_ObjectResponse RespondToMouseDown(GH_Canvas sender, Grasshopper.GUI.GH_CanvasMouseEvent e)
        {
            if (ButtonArea.Contains(e.CanvasLocation))
            {
                ContextMenuStrip menu = new ContextMenuStrip();
                menu.ShowCheckMargin = true;
                menu.ShowImageMargin = false;
                ToolStripMenuItem toolStripMenuItem1 = GH_DocumentObject.Menu_AppendItem(menu, "Steel", new EventHandler(MenuSteelClicked), true, Owner.CurrentPreset.GetName() == "Steel");
                ToolStripMenuItem toolStripMenuItem2 = GH_DocumentObject.Menu_AppendItem(menu, "Concrete", new EventHandler(MenuConcreteClicked), true, Owner.CurrentPreset.GetName() == "Concrete");
                ToolStripMenuItem toolStripMenuItem3 = GH_DocumentObject.Menu_AppendItem(menu, "Timber", new EventHandler(MenuTimberClicked), true, Owner.CurrentPreset.GetName() == "Timber");

                toolStripMenuItem1.ToolTipText = "Steel material:" + Environment.NewLine + Owner.CurrentPreset.Detail();
                toolStripMenuItem2.ToolTipText = "Concrete material:" + Environment.NewLine + Owner.CurrentPreset.Detail();
                toolStripMenuItem3.ToolTipText = "ConiferousTimber material:" + Environment.NewLine + Owner.CurrentPreset.Detail();

                //GH_DocumentObject.Menu_AppendItem(menu, "Else", new EventHandler(MenuTimberClicked), true, false);
                //ToolStripMenuItem toolStripMenuItem1 = GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "NewMaterial", new EventHandler(this.MenuTimberClicked), true, Owner.CurrentPreset.Type == Material.types.IsoLinEl);
                menu.Show(sender, e.ControlLocation);
                //menu.Show(sender, (int)Bounds.Location.X + 24, (int)Bounds.Location.Y + 24);
                //menu.Show(sender,e.ControlX - 24, e.ControlY - 24);
                //menu.Show(sender, (int)Pivot.X + 24, (int)Pivot.Y + 24);
                //menu.Show(sender, (int)ButtonArea.X, (int)ButtonArea.Y);
                //menu.Show(sender, (Point)ButtonArea.Location);
                
                return GH_ObjectResponse.Handled;
            }

            return base.RespondToMouseDown(sender, e);
        }
        private void MenuSteelClicked(object sender, EventArgs e)
        {
            Owner.RecordUndoEvent("Steel Preset");
            Owner.CurrentPreset = new Material();
            Owner.ExpireSolution(true);
        }
        private void MenuConcreteClicked(object sender, EventArgs e)
        {
            Owner.RecordUndoEvent("Concrete Preset");
            Owner.CurrentPreset = Material.Get(Material.names.Concrete);
            Owner.ExpireSolution(true);
        }
        private void MenuTimberClicked(object sender, EventArgs e)
        {
            Owner.RecordUndoEvent("Timber Preset");
            Owner.CurrentPreset = Material.Get(Material.names.Timber);
            Owner.ExpireSolution(true);
        }
        #endregion
    }
}
