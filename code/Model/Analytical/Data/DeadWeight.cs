﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rhino.Geometry;

namespace Donkey.Model.Analytical.Data
{
    public class DeadWeight : Load
    {
        //private Vector3d gravitation = new Vector3d(0,0,-1);
        public DeadWeight()
        { this.Type = LoadType.DeadWeight; }
        public override string ToFile()
        {
            return "DeadWeight components 3 0 0 -1";
        }
    }
}
