﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel.Types;
using Donkey.Interface;
using Grasshopper.Kernel;
using Rhino.Geometry;

namespace Donkey.Model.Analytical.Data
{
    /// <summary>
    /// Base class for params of nodal data
    /// </summary>
    public class AnalyticalModelData : GH_Goo<Object>, IGH_PreviewData, IModelParts, IAppendedData
    {
        #region FIELD
        public int Index { get; set; }
        public IModelParts ModelPart { get; set; }
        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        public AnalyticalModelData() { }
        //DATA CONSTRUCTOR
        protected AnalyticalModelData(int index,IModelParts modelPart)
        {
            this.Index = index;
            this.ModelPart = modelPart;
        }
        //COPY CONSTRUCTOR
        protected AnalyticalModelData(AnalyticalModelData instance)
        {
            this.Index = instance.Index;
            this.ModelPart = instance.ModelPart;
        }
        #endregion
        //UNSET PROPERTY
        #region STATIC PROPERTY
        public static AnalyticalModelData Unset
        {
            get
            {
                return new NodalLoad();
            }
        }
        #endregion

        #region METHODS
        public virtual bool AppendData( AnalyticalModel model)
        { return true; }
        public virtual string ToFile()
        {
            return this.ToString();
        }
        #endregion
    
        #region GH_Goo
        public override IGH_Goo Duplicate()
        {
            return new AnalyticalModelData(this);
        }

        public override bool IsValid
        {
            get { return true; }
        }

        public override string ToString()
        {
            return "Null conditions";
        }

        public override string TypeDescription
        {
            get { return "Model conditions"; }
        }

        public override string TypeName
        {
            get { return "Model conditions"; }
        }
        #endregion

        #region IGH_PREVIEW DATA
        public virtual BoundingBox ClippingBox
        {
            get { return BoundingBox.Empty; }
        }

        public virtual void DrawViewportMeshes(GH_PreviewMeshArgs args)
        {  }

        public virtual void DrawViewportWires(GH_PreviewWireArgs args)
        {  }
        #endregion

        #region MODEL PARTS
        public void RegisterModel(Model_Goo model)
        { }
        #endregion
    }
}
