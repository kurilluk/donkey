﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rhino.Geometry;
using Grasshopper.Kernel.Types;
using Donkey.Model.Geometry;
using Grasshopper.Kernel;

namespace Donkey.Model.Analytical.Data
{
    public class NodalLoad : Load
    {
        #region FIELD
        protected Vector3d m_vector;
        protected Line m_line;
        protected double m_force;
        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        public NodalLoad() : this(Point3d.Unset,Vector3d.Unset, 0) { }
        //DATA CONSTRUCTOR
        public NodalLoad(Node node, Vector3d forceDirection, double forceValue)
            :base(-1, node)
        {
            this.Type = LoadType.NodalLoad;
            this.m_force = forceValue;
            this.m_vector = ScaleVector(forceDirection, forceValue);
            this.m_line = SetLine(node, this.m_vector);
        }
        //COPY CONSTRUCTOR
        public NodalLoad(NodalLoad instance)
            :base (instance)
        {
            this.Type = instance.Type;
            this.m_line = instance.m_line;
            this.m_vector = instance.m_vector;
            this.m_force = instance.m_force;
        }
        #endregion

        #region PRIVATE METHODS
        //SCALE VECTOR TO FORCE VALUE
        private Vector3d ScaleVector(Vector3d vector, double lenght)
        {
            Vector3d vec = new Vector3d(vector);
            vec.Unitize();
            return vec* lenght;
        }
        //CALCULATE PREVIEW LINE
        private Line SetLine(Point3d pt, Vector3d vector)
        {
            Line line = new Line(pt - vector, pt);
            return line;
        }
        #endregion

        #region OVERRIDE TO FILE AND HOW TO APPEND DATA
        public override bool AppendData(AnalyticalModel model)
        {
            //FIND INDEX OF MODEL NODE
            Node node = model.Nodes.Find
                (delegate( Node n)
                {
                    return n.Equals(this.ModelPart);
                });
            if (node == null)
                return false; //OR model.Nodes.AppendData(this.ModelPart, false);
            //APPEND MODEL DATA
            int d_id = model.Loads.AppendData(this, true);
            //SET NODAL LOAD TO MODEL NODE
            node.Load = d_id;
            return true;
        }
        public override string ToFile()
        {
            //TODO LOAD TYPE(ENUM)-this.Type.ToString() AND MOMENT BY Z AXIS
            return "NodalLoad components 6 " + m_vector.X + " " + m_vector.Y + " " + m_vector.Z + " 0 0 0";
        }
        public override IGH_Goo Duplicate()
        {
            return new NodalLoad(this);
        }

        public override string ToString()
        {
            return "Nodal_load: "+this.m_force + " N, {" + this.m_vector.ToString()+"}";
        }

        public override string TypeDescription
        {
            get { return "Nodal Load"; }
        }

        public override string TypeName
        {
            get { return "Nodal Load"; }
        }

        public override BoundingBox ClippingBox
        {
            get { return this.m_line.BoundingBox; }
        }
        #endregion

        #region EQUALS FOR APPEND DATA LIST
        //OVERRIDE EQUALS OBJECT
        public override bool Equals(System.Object obj)
        {
            if (obj == null)
                return false;
            // If parameter cannot be cast to Point return false.
            // Return true if the fields match:
            if (obj is NodalLoad)
                return Equals((NodalLoad)obj);
            return false;
        }
        //EQUALS FOR NODAL LOAD OBJECT
        public bool Equals(Load n)
        {
            // If parameter is null return false:
            if ((object)n == null)
                return false;
            if (n.Type != this.Type)
                return false;
            if(n is AreaLoad)
                return false;
            // Return true if the fields match:
            return EqualsCoordinates((NodalLoad)n);
        }
        //COORDINATES EQUALS
        private bool EqualsCoordinates(NodalLoad n)
        {
            return this.m_vector.Equals(n.m_vector);
        }
        //GENERATE HASHCODE
        public override int GetHashCode()
        {
            return this.m_vector.GetHashCode();
        }
        #endregion

        #region PREVIEW
        public override void DrawViewportWires(GH_PreviewWireArgs args)
        {
            args.Pipeline.DrawArrow(this.m_line, args.Color);
        }
        #endregion
    }
}
