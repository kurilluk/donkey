﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using System.Drawing;

namespace Donkey.Model.Analytical.Data
{
    class Profile_Param : GH_Param<Profile_Goo>
    {
        #region FIELD
        private Profile_Goo m_profile;
        #endregion

        #region CONSTRUCTOR
        public Profile_Param()
            : base(new Grasshopper.Kernel.GH_InstanceDescription("Profile", "P", "Element cross-section [mm]", "Donkey", "AnalyticalData")) 
            {
                m_profile = new Profile_Goo();
            }

        //constructor 1 overload        
        public Profile_Param(GH_InstanceDescription nTag) : base(nTag){}
  

        //constructor 2 overload      
        public Profile_Param(GH_InstanceDescription nTag, GH_ParamAccess access) : base(nTag, access) { }
        #endregion

        #region OVERRIDE
        //registrate own guid number
        public override Guid ComponentGuid
        {
            get { return new Guid("b927a6e4-5a7c-4558-a366-86d718a01719"); }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.profile; }
        }
        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            GH_IO.Serialization.GH_IWriter chunk = writer.CreateChunk("Profile");
            m_profile.Write(chunk);
            return base.Write(writer);
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            GH_IO.Serialization.GH_IReader chunk = reader.FindChunk("Profile");
            m_profile.Read(chunk);
            return base.Read(reader);
        }

        protected override void CollectVolatileData_Custom()
        {
            VolatileData.Clear();
            m_data.Append(new Profile_Goo(m_profile));
        }
       #endregion
    }
}
