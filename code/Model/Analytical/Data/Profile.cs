﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rhino.Geometry;
using Donkey.Interface;

namespace Donkey.Model.Analytical.Data
{
    public class Profile : IAppendedData, IModelParts
    {

        public static Profile None
        {
            get { return new Profile(types.None,0,0); }
        }

        #region FIELDS

        public types Type { get; private set;}
        public double Width { get; private set; }
        public double Height { get; private set; }
        public double Thickness { get; private set; }
        public LocalCoordinateSystem orientation; //USED BY PROFILE ORIENTATION DEFINITION from Profile component to GeometryGoo - Local
        private Plane local; //HACK REMOVE OBSOLETE
        //private double area;

        #endregion

        #region CONSTRUCTOR

        public Profile() : this(types.Circle, 100, 100, 0, new LocalCoordinateSystem()) { }

        public Profile(types type, double width, double height) : this(type, width, height, 0, new LocalCoordinateSystem()) { }

        public Profile(types type, double width, double height, double thickness) : this(type, width, height, thickness, new LocalCoordinateSystem()) { }

        public Profile(types type, double width, double height, double thickness, LocalCoordinateSystem orientation)
        {
            this.Type = type;
            if (width < 0)
                width = Math.Abs(width);
            this.Width = width;
            if (height < 0)
                height = Math.Abs(height);
            this.Height = height;
            if (thickness < 0)
                thickness = 0;
            this.Thickness = thickness;
            this.orientation = orientation;
            //this.area = area;
            CorrectType();
        }

        public Profile(Profile cs) : this(cs.Type, cs.Width, cs.Height, cs.Thickness, cs.orientation) { }

        #endregion

        #region METHODS
        //obsolete
        public void SetOrientation(Plane local)
        {
            this.local = new Plane(local);
            this.orientation.SetZaxis(this.local.ZAxis);
        }
        //obsolete?
        private void CorrectType()
        {
            if (this.Thickness > 0)
            {
                if (this.Type == types.Circle)
                {
                    this.Type = types.Circle_tube;
                }
                else if (this.Type == types.Rectangle)
                {
                    this.Type = types.Rectangle_tube;
                }
            }
            else
            {
                if (this.Type == types.Circle_tube)
                {
                    this.Type = types.Circle;
                }
                else if (this.Type == types.Rectangle_tube)
                {
                    this.Type = types.Rectangle;
                }
            }
        }

        public override string ToString()
        {
                if ((this.Type == types.Circle_tube) || (this.Type == types.Rectangle_tube))
                {
                    return Type.ToString() + "(" + Width + " x " + Height + " x " + Thickness + ")";// + orientation.ToString();
                }
                else if (this.Type == types.cs)
                {
                    return "Thickness " + Thickness;
                }
                else if (this.Type == types.None)
                {
                    return "None";
                }
                return Type.ToString() + "(" + Width + " x " + Height + ")";// +orientation.ToString();
        }
        #endregion

        #region PROPERTY
        //obsolete
        public Curve Shape
        {
            get
            {
                return
                    new Rectangle3d(local, new Interval(-Width / 2, Width / 2), new Interval(-Height / 2, Height / 2)).ToNurbsCurve();

                //new Ellipse(local, width, height).ToNurbsCurve().Rebuild(4,1,true); 
                }
        }
        #endregion

        #region EQUALS

        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if (obj == null || GetType() != obj.GetType()) return false;
            Profile cs = (Profile)obj;
            return (Type == cs.Type) && (Height == cs.Height) && (Width == cs.Width) && (Thickness == cs.Thickness)&& (orientation.Equals(cs.orientation)); //use equals or hash to compare refNode
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 15;
                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + Height.GetHashCode();
                hash = hash * 23 + Width.GetHashCode();
                hash = hash * 23 + Thickness.GetHashCode();               
                hash = hash * 23 + orientation.GetHashCode();
                return hash;
            }

        }

        #endregion

        #region TYPES
        public enum types
        {
            None = 0,
            Rectangle = 1,
            Circle = 2,
            Rectangle_tube = 3,
            Circle_tube = 4,
            TRUSScs = 5,
            cs = 6
        }
        #endregion

        #region IData Members
       string IAppendedData.ToFile()
        {
            if ((this.Type == types.Circle_tube) || (this.Type == types.Rectangle_tube))
            {
                return Type.ToString() + " width " + Width + " height " + Height + " thickness " + Thickness + " ";// +orientation.ToFile();
            }
            else if (this.Type == types.cs)
            { 
                return "2Dcs"+" thickness " + Thickness + " ";
            }
            return Type.ToString() + " width " + Width + " height " + Height + " "; //+ orientation.ToFile();
        }
        #endregion

        #region refNode - add to geometry?
        //class to be struc
        /// <summary>
        /// Orientation - definition from MIDAS
        /// </summary>
        public class LocalCoordinateSystem
        {
            //FIELD
            private double x, y, z;

            //CONSTRUCTOR
            public LocalCoordinateSystem() : this(0,0,1) { }

            public LocalCoordinateSystem(Vector3d vector) : this(vector.X, vector.Y, vector.Z) { }

            public LocalCoordinateSystem(double x, double y, double z)
            {
                this.x = x;
                this.y = y;
                this.z = z;
            }

            //METHODS
            public override string ToString()
            {
                return "{" + x + "; " + y + "; " + z+"}";  
            }

            public string ToFile()
            {
                return "LCS xz vector 3 " + x + " " + y + " " + z;
            }

            public void SetZaxis(Vector3d vector)
            {
                this.x = vector.X;
                this.y = vector.Y;
                this.z = vector.Z;
            }

            public Vector3d Zaxis
            {
                get { return new Vector3d(x, y, z); }
            }

            //EQUALS
            public override bool Equals(Object obj)
            {
                //Check for null and compare run-time types.
                if (obj == null || GetType() != obj.GetType()) return false;
                LocalCoordinateSystem rn = (LocalCoordinateSystem)obj;
                return (x == rn.x) && (y == rn.y) && (z == rn.z);
            }

            public override int GetHashCode()
            {
                unchecked // Overflow is fine, just wrap
                {
                    int hash = 13;
                    // Suitable nullity checks etc, of course :)
                    hash = hash * 23 + x.GetHashCode();
                    hash = hash * 23 + y.GetHashCode();
                    hash = hash * 23 + z.GetHashCode();
                    return hash;
                }

            }
        }

        #endregion

        public void RegisterModel(Model_Goo model)
        {
        }
    }
}
