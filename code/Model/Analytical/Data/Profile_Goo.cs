﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel.Types;

namespace Donkey.Model.Analytical.Data
{
    public class Profile_Goo : GH_Goo<Profile>
    {
        #region PROPERTY

        #endregion

        #region CONSTRUCTOR

        //constructor default
        public Profile_Goo() { this.Value = new Profile(); }

        public Profile_Goo(Profile.types type, double width, double height, double thickness, Profile.LocalCoordinateSystem orientation)
        {
            this.Value = new Profile(type,width, height,thickness, orientation);
        }

        //constructor same type as base class
        public Profile_Goo(Profile cs)
        {
            this.Value = new Profile(cs);
        }

        //copy constructor, same type
        public Profile_Goo(Profile_Goo instance)
        {
            this.Value = new Profile(instance.Value);
        }

        #endregion

        #region GH_Goo Members
        public override IGH_Goo Duplicate()
        {
            return new Profile_Goo(this);
        }

        public override bool IsValid
        {
            get { return true; }
        }

        public override string ToString()
        {
            return this.Value.ToString();
        }

        public override string TypeDescription
        {
            get { return "Donkey's cross section"; }
        }

        public override string TypeName
        {
            get { return "D_CrossSection"; }
        }

        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetDouble("Width", Value.Width);
            writer.SetDouble("Height", Value.Height);
            writer.SetDouble("Thickness", Value.Thickness);
            writer.SetInt32("Type", (int)Value.Type);
            return true;
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            double width = reader.GetDouble("Width");
            double height = reader.GetDouble("Height");
            double thickness = reader.GetDouble("Thickness");
            int type = reader.GetInt32("Type");
            this.Value = new Profile((Profile.types)type, width, height, thickness);
            return true;
        }
        #endregion
    }
}
