﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rhino.Geometry;
using Grasshopper.Kernel.Types;
using Donkey.Model.Geometry;
using Grasshopper.Kernel;

namespace Donkey.Model.Analytical.Data
{
    public class Hinge : AnalyticalModelData
    {
         #region FIELD
        private Circle m_circle;
        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        public Hinge() : this(Point3d.Unset) { }
        //DATA CONSTRUCTOR
        public Hinge(Node node)
            :base(-1, node)
        { this.m_circle = new Circle(Plane.WorldXY, node, 150); }
        //COPY CONSTRUCTOR
        public Hinge(Hinge instance)
            :base (instance)
        {  }
        #endregion

        #region OVERRIDE
        public override bool AppendData(AnalyticalModel model)
        {
            //FIND INDEX OF MODEL NODE
            Node node = model.Nodes.Find
                (delegate( Node n)
                {
                    return n.Equals(this.ModelPart);
                });
            if (node == null)
                return false;
            //SET FULL HINGE TO MODEL NODE
            node.Hinge = 1;
            return true;
        }

        public override IGH_Goo Duplicate()
        {
            return new Hinge(this);
        }

        public override string ToString()
        {
            return "Full hinge: " + this.ModelPart.ToString();
        }

        public override string TypeDescription
        {
            get { return "Full hinge"; }
        }

        public override string TypeName
        {
            get { return "Full hinge"; }
        }

        public override BoundingBox ClippingBox
        {
            get { return this.m_circle.BoundingBox; }
        }
        #endregion

        #region PREVIEW
        public override void DrawViewportWires(GH_PreviewWireArgs args)
        {
            //args.Pipeline.DrawCircle(this.m_circle, args.Color);
            args.Pipeline.DepthMode = Rhino.Display.DepthMode.AlwaysInFront;
            Plane plane;
            args.Viewport.GetCameraFrame(out plane);
            Circle dot = new Circle(plane, m_circle.Center, 75);
            args.Pipeline.DrawCircle(dot, args.Color, 5);
        }
        #endregion
    }
}
