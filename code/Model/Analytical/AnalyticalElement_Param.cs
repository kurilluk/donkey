﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Drawing;

namespace Donkey.Model.Analytical
{
    /// <summary>
    /// Analytical element parameter class
    /// </summary>
    public class AnalyticalElement_Param : GH_Param<AnalyticalModel>, IGH_PreviewObject
    {
        #region FIELD
        public bool Hidden { get; set; }
        #endregion

        #region CONSTRUCTOR
        /// <summary>
        /// Parameter of model analytical element
        /// </summary>
        public AnalyticalElement_Param()
            : base(new Grasshopper.Kernel.GH_InstanceDescription("Element", "E", "Model element",//+ Donkey.Info.Mark,
                "Donkey", "A.Utilities")) { }

        //constructor 1 overload        
        public AnalyticalElement_Param(GH_InstanceDescription nTag) : base(nTag){}
  

        //constructor 2 overload      
        public AnalyticalElement_Param(GH_InstanceDescription nTag, GH_ParamAccess access) : base(nTag, access) { }

        //registrate own guid number
        public override Guid ComponentGuid
        {
            get { return new Guid("3431e9b9-a064-45b2-981e-28614e8f5218"); }
        }
       #endregion

        #region IGH_PreviewObject
        public Rhino.Geometry.BoundingBox ClippingBox
        {
            get 
            {
                BoundingBox bb = BoundingBox.Empty;
                foreach (AnalyticalModel model in m_data.NonNulls)
                {
                    bb.Union(model.ClippingBox);
                }
                return bb;
            }
        }

        public void DrawViewportMeshes(IGH_PreviewArgs args)
        {
            Rhino.Display.DisplayMaterial mat;
            //int thickness;
            if (this.Attributes.GetTopLevel.Selected)
            {
                mat = args.ShadeMaterial_Selected;
                //thickness = 2;
            }
            else
            {
                mat = args.ShadeMaterial;
                //thickness = 1;
            }

            GH_PreviewMeshArgs meshArguments = new GH_PreviewMeshArgs(args.Viewport,args.Display,mat, args.MeshingParameters);

            foreach (AnalyticalModel model in m_data.NonNulls)
            {
                model.DrawMeshGeometry(meshArguments);
            }
        }

        public void DrawViewportWires(IGH_PreviewArgs args)
        {
            GH_PreviewWireArgs wireArgs;
            if (this.Attributes.GetTopLevel.Selected)
            {
                 wireArgs = new GH_PreviewWireArgs(args.Viewport, args.Display, args.WireColour_Selected, 2);
            }
            else
            {
                 wireArgs = new GH_PreviewWireArgs(args.Viewport, args.Display, args.WireColour, 1);
            }
            foreach (AnalyticalModel model in m_data.NonNulls)
            {
                model.DrawWireGeometry(wireArgs);
                model.DrawGeometryProfile(wireArgs);
            }
        }

        #endregion

        #region OVERRIDE
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.shell; }
        }

        //public override string ToString()
        //{
        //    return this.Format(this.m_data.
        //}

        protected override string Format(AnalyticalModel Data)
        {
            if ((object)Data == null)
                return "Null";
           string str = "";
           if (Data.Elements.Count == 0)
               return "Null";
           foreach (AnalyticalElement element in Data.Elements)
           {
               str += element.Print();// +Environment.NewLine;
           }
           return str;
            //return "Beam"; //TODO read element name
        }
        #endregion


        public bool IsPreviewCapable
        {
            get { return true; }
        }
    }
}
