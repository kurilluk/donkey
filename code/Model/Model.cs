﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Donkey.Model
{
    public class Model
    {
        #region FIELD
        public NodalList Nodes { get; protected set; }
        public GeometryList Geometry { get; protected set; }
        #endregion

        #region CONSTRUCTOR
        //Blank constructor
        public Model() :this(new NodalList(),new GeometryList()){}
        //Data constructor
        public Model(NodalList nodes, GeometryList geometry)
        {
            this.Nodes = new NodalList(nodes);
            this.Geometry = new GeometryList(geometry);
        }
        //Copy constructor
        public Model(Model instance)
        {
            this.Nodes = new NodalList(instance.Nodes);
            this.Geometry = new GeometryList(instance.Geometry);
        }
        #endregion
    }
}
