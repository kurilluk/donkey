﻿using System;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Collections.Generic;
using Grasshopper.Kernel;
using Rhino.Geometry;
using Donkey.Model.Resulting.Data;
using Grasshopper.Kernel.Attributes;
using Grasshopper.GUI.Canvas;
using Grasshopper.GUI;
using Rhino;
using Rhino.DocObjects;

namespace Donkey.Model.Resulting.Interpretation
{
    public class Evaluation_Comp : GH_Component, IGH_BakeAwareObject
    {
        #region FIELD
        internal ResultModel m_model = new ResultModel();
        //private bool m_AVERANGEmode = false;
        private bool m_previewValue;
        internal bool m_design_tracking;
        //private double m_prev_Averange = double.NaN;
        private double m_prev_Overloading = double.NaN;
        private double m_prev_Efficiency = double.NaN;
        internal DesignTracker_Form m_form;
        internal List<ResultModel> FieldOfSolutions;
        #endregion

        #region CONSTRUCTOR
        /// <summary>
        /// Initializes a new instance of the TestingResult class.
        /// </summary>
        public Evaluation_Comp()
            : base("Evaluation", "Evaluation",
                "Evaluate actual design solution. Evaluation based on efficiency of material use, \u03B7 [%]" + Environment.NewLine + "+ penalizaton/plastification value: shows structural overload",// + Donkey.Info.Mark,
                "Donkey", "4.Results") //"based on von Mises yield criterion J\u2082 criterion divided by equivalent stress" + Environment.NewLine + "If value is up to 100%, plastification, penalty value" + Donkey.Info.Mark,
        {
            //this.m_AVERANGEmode = false;
            this.m_previewValue = false;
            this.m_design_tracking = false;
            this.FieldOfSolutions = new List<ResultModel>();
        }
        #endregion

        #region OVERRIDE

        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetBoolean("preview_data", this.m_previewValue);
            //writer.SetBoolean("output_data", this.m_AVERANGEmode);
            return base.Write(writer);
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            this.m_previewValue = reader.GetBoolean("preview_data");
            //this.m_AVERANGEmode = reader.GetBoolean("output_data");
            return base.Read(reader);
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            //pManager.AddParameter(new Resulting.Result_Param());
            pManager.AddParameter(new Resulting.ResultModel_Param());
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            //pManager.AddParameter(new Resulting.Result_Param());
            //pManager.AddParameter(new Geometry.Geometry_Param());
            //pManager.AddNumberParameter("GeometryValue", "GV", "Value of model geometry", GH_ParamAccess.list);
            pManager.AddNumberParameter("efficiency", "\u03B7", "efficiency of profile/material" + Environment.NewLine + "based on equivalent stress (von Mises yield criterion) divided by the yield stress of a defined material" + Environment.NewLine + "(0) Averange value" + Environment.NewLine + "(1) Max value" + Environment.NewLine + "(2) Inefficiency [%]", GH_ParamAccess.list);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            if (!DA.GetData<ResultModel>(0, ref m_model))
                return;
            
            this.m_model = new ResultModel(m_model);

            //DA.SetData(0, m_model);
            //DA.SetDataList(1,m_model.Geometries);
            //DA.SetDataList(0, m_model.Relative_Stress);
            
            //IF SETUP IS AVERANGE or MAX
            //this.ModelAverageValue + "/" + this.modelMaxValue.ToString();
            //if (m_AVERANGEmode)
            //{
            //    this.Message = "Averange";
            //    DA.SetData(0, this.ModelAverageValue);
            //}
            //else
            //{
            //    this.Message = "Max";
            //    DA.SetData(0, this.ModelMaxValue);
            //}

            double[] values = new double[2] {this.m_model.Efficiency,this.m_model.Overloading};
            if(!double.IsNaN(m_prev_Efficiency))
            {
                StringBuilder sb = new StringBuilder();
                //double dif_average = Math.Round(m_prev_Averange - values[0],3);
                //if(dif_average > 0)
                //    sb.Append("+"+dif_average+"/");
                //else
                //    sb.Append(dif_average+"/"); 
                
                //double dif_max = Math.Round(m_prev_Max - values[1],3);
                //if(dif_max > 0)
                //    sb.Append("+"+dif_max);
                //else
                //    sb.Append(dif_max);

                //sb.Append("/");

                double dif_eff = Math.Round((m_prev_Efficiency - values[0])*100, 1);
                if (dif_eff > 0)
                    sb.Append("" + -dif_eff + "%");
                else if (dif_eff == 0)
                    sb.Append("+/- 0%");
                else
                    sb.Append("+" + -dif_eff + "%");

                this.Message = sb.ToString();
            }
            //this.m_prev_Averange = values[0];
            this.m_prev_Overloading = values[1];
            this.m_prev_Efficiency = values[0];

            DA.SetDataList(0,values);

            if (m_design_tracking)
            {
                if (this.FieldOfSolutions == null)
                    this.FieldOfSolutions = new List<ResultModel>();
                FieldOfSolutions.Add(this.m_model);
                this.m_form.AddSolution(FieldOfSolutions.Count, m_model, true);
            }
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.evaluate; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.hidden; }//.obscure; }
        }

        public override void ClearData()
        {
            if (this.m_model != null)
            {
                this.m_model = null;
            }
            this.Message = " ";
            base.ClearData();
        }


        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{a4098d16-05b0-4567-b330-c6d6db265b90}"); }
        }

        public override void DrawViewportWires(IGH_PreviewArgs args)
        {
            if (m_model != null)
            {
                //HACK duplicates of the model instance
                //base.DrawViewportWires(args); //Preview all param 
                ResultModel model = new ResultModel(m_model);
                m_model.DrawGeometryColour(args); //HACK DRAW MESH TOO?
                if(this.m_previewValue)
                    m_model.DrawElementValue(args);
                //model.DrawGeometryValue(args);
            }
        }

        //public override void DrawViewportMeshes(IGH_PreviewArgs args)
        //{ 
        //    //base.DrawViewportMeshes(args);
        //    if (m_model != null)
        //    {
        //        ResultModel model = new ResultModel(m_model);
        //    }
        //}

        public override bool IsPreviewCapable { get { return true; } }

        public override void CreateAttributes()
        {
            m_attributes = new Evaluation_CompAttributes(this);
        }
        #endregion

        #region BAKE
        public override bool IsBakeCapable
        {
            get
            {
                return true;
            }
        }

        public override void BakeGeometry(RhinoDoc doc, List<Guid> obj_ids)
        {
            ObjectAttributes att = Rhino.RhinoDoc.ActiveDoc.CreateDefaultAttributes();
            att.ColorSource = ObjectColorSource.ColorFromObject;
            this.BakeGeometry(doc, att, obj_ids);
        }

        public override void BakeGeometry(RhinoDoc doc, ObjectAttributes att, List<Guid> obj_ids)
        {
            att.ColorSource = ObjectColorSource.ColorFromObject;
            att.PlotColorSource = ObjectPlotColorSource.PlotColorFromObject;
            if (m_model != null && m_model.Geometries.Count > 0)
            {
                for (int e = 0; e < m_model.Elements.Count; e++)
                {
                    List<Guid> baked_objects = new List<Guid>();
                    List<int> indexes = m_model.Elements[e].Geometries_ID;
                    for (int i = 0; i < indexes.Count; i++)
                    {
                        Color color = ResultModel.Gradient.ColourAt(m_model.Geometries_Stress_Relative[indexes[i]]);
                        att.PlotColor = color;
                        att.ObjectColor = color;
                        att.Name = "\u03B7: " + Math.Round(m_model.Geometries_Stress_Relative[indexes[i]], 3)*100 + "%";
                        baked_objects.Add(m_model.Geometries[indexes[i]].Bake_Geometry(doc, att));
                    }
                    doc.Groups.AddToGroup(e, baked_objects);
                }
            }
        }
        #endregion

        #region MENU
        public override bool AppendMenuItems(ToolStripDropDown menu)
        {
            this.Menu_AppendPreviewItem(menu);
            this.Menu_AppendEnableItem(menu);
            this.Menu_AppendBakeItem(menu);
            this.Menu_AppendRuntimeMessages(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            this.AppendAdditionalMenuItems(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            this.Menu_AppendObjectHelp(menu);
            return true;
        }

        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {          
            //ToolStripMenuItem toolStripMenuItem1 = GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Averange", new EventHandler(this.ModeChange), true, this.m_AVERANGEmode);
            ////toolStripMenuItem1.ToolTipText = "Rectangle cross section profile for Donkey." + Environment.NewLine + "If thikness is more that 0, then create tube profile";

            //ToolStripMenuItem toolStripMenuItem2 = GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Max", new EventHandler(this.ModeChange), true, !this.m_AVERANGEmode);
            ////toolStripMenuItem2.ToolTipText = "Circle cross section profile for Donkey." + Environment.NewLine + "If thikness is more that 0, then create tube profile";

            //GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);

            ToolStripMenuItem toolStripMenuItem0 = GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Preview value", new EventHandler(this.PreviewChange), true, this.m_previewValue);
            //toolStripMenuItem1.ToolTipText = "Rectangle cross section profile for Donkey." + Environment.NewLine + "If thikness is more that 0, then create tube profile";

            ToolStripMenuItem toolStripMenuItem1 = GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Design tracking", new EventHandler(this.TrackDesign), true, this.m_design_tracking);
        }

        private void TrackDesign(object sender, EventArgs e)
        {
            this.RecordUndoEvent("TrackingDesing");
            this.m_design_tracking = !m_design_tracking;
            //this.ExpireSolution(true);
            //this.ExpirePreview(true);
            if (m_design_tracking)
            {
                if (this.m_form == null)
                {
                    this.m_form = new DesignTracker_Form(this);
                    //this.m_form.Owner = owner;
                    GH_WindowsFormUtil.CenterFormOnCursor((Form)this.m_form, true);
                    ((Control)this.m_form).Show();
                    //this.m_form.TopMost = true;
                    if (this.FieldOfSolutions != null && this.FieldOfSolutions.Count == 0)
                    {
                        this.m_form.AddSolution(1, m_model, true);
                        this.FieldOfSolutions.Add(m_model);
                    }
                }
                else
                    this.m_form.Select();
                //this.m_form.InitButtons();
            }

            if (m_form != null)
            {
                m_form.tracking.Checked = this.m_design_tracking;
                m_form.Refresh();
            }
                //else
            //{
            //    if (m_form != null)
            //    {
            //        //this.m_form.Close();
            //        this.m_form.Dispose();
            //        this.m_form = null;
            //    }
            //}
        }

        private void PreviewChange(object sender, EventArgs e)
        {
            this.RecordUndoEvent("ChangePreviewMode");
            this.m_previewValue = !m_previewValue;
            //this.ExpireSolution(true);
            this.ExpirePreview(true);
        }

        //private void ModeChange(object sender, EventArgs e)
        //{
        //    switch(sender.ToString())
        //    {
        //        case "Averange":
        //            this.m_AVERANGEmode = true;
        //            break;
        //        case "Max":
        //            this.m_AVERANGEmode = false;
        //            break;
        //    }
        //    this.RecordUndoEvent("ChangeMode");
        //    this.ExpireSolution(true);
        //}

        #endregion

    }

    public class Evaluation_CompAttributes : GH_ComponentAttributes
    {
        //FEM_Comp Owner;
        RectangleF info;
        RectangleF percente;

        public Evaluation_CompAttributes(Evaluation_Comp owner)
            : base(owner)
        {
            //Owner = owner;
            info = new RectangleF();
            percente = new RectangleF();
        }

        public override bool HasInputGrip { get { return true; } }
        public override bool HasOutputGrip { get { return true; } }

        private GH_Capsule m_capsule;

        #region LAYOUT
        protected override void Layout()
        {
            //this.Pivot = (PointF)GH_Convert.ToPoint(base.Pivot);
            //this.m_innerBounds = GH_ComponentAttributes.LayoutComponentBox(base.Owner);
            //this.m_innerBounds = new System.Drawing.RectangleF(Pivot, new SizeF(112, 48));
            
            info = new RectangleF(Owner.Attributes.Pivot.X - 0.5f * (float)1, Owner.Attributes.Pivot.Y+20, (float)70, (float)14);
            percente = new RectangleF(Owner.Attributes.Pivot.X - 0.5f * (float)1, Owner.Attributes.Pivot.Y+2, (float)70, (float)22);
            this.m_innerBounds = new RectangleF(Owner.Attributes.Pivot.X - 0.35f * (float)1, Owner.Attributes.Pivot.Y - 0.5f * (float)1, (float)70, (float)36);
            
            //this.Bounds = new System.Drawing.RectangleF(Pivot, new SizeF(112, 48));
            m_capsule = GH_Capsule.CreateCapsule(this.Bounds, GH_Palette.Hidden, 5, 30);
            
            GH_ComponentAttributes.LayoutInputParams(base.Owner, this.m_innerBounds);
            GH_ComponentAttributes.LayoutOutputParams(base.Owner, this.m_innerBounds);
            this.Bounds = GH_ComponentAttributes.LayoutBounds(base.Owner, this.m_innerBounds);
            
            //this.LayoutJaggedEdges();
        }
        #endregion

        #region RENDER
        protected override void Render(GH_Canvas canvas, Graphics graphics, GH_CanvasChannel channel)
        {
            switch (channel)
            {
                case GH_CanvasChannel.Wires:
                    base.Render(canvas, graphics, channel);
                    break;

                case GH_CanvasChannel.Objects:
                    base.RenderComponentCapsule(canvas, graphics, true, false, false, true, true, true);
                    //this.m_capsule.Render(graphics, this.Selected, this.Owner.Locked, true);
                    //m_capsule.Dispose();
                    if (((Evaluation_Comp)this.Owner).m_model == null)
                    {
                        graphics.DrawString("NaN%",
                                   Grasshopper.Kernel.GH_FontServer.Large,
                                   Brushes.Red,
                                   percente,
                                   Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);
                        graphics.DrawString("NaN%",
                                    Grasshopper.Kernel.GH_FontServer.Small,
                                    Brushes.Red,
                                    info,
                                    Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);
                        break;
                    }

                    double modelOverloading = ((Evaluation_Comp)this.Owner).m_model.Overloading;
                    if(modelOverloading == 0)
                    {
                        graphics.DrawString(""+((Evaluation_Comp)this.Owner).m_model.Efficiency*100+"%",
                                   Grasshopper.Kernel.GH_FontServer.Large,
                                   Brushes.Green,
                                   percente,
                                   Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);
                        graphics.DrawString("non overloading",
                                    Grasshopper.Kernel.GH_FontServer.Small,
                                    Brushes.Green,
                                    info,
                                    Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);
                    }
                    else
                    {
                        graphics.DrawString("" + ((Evaluation_Comp)this.Owner).m_model.Efficiency * 100 + "%",
                                       Grasshopper.Kernel.GH_FontServer.Large,
                                       Brushes.Red,
                                       percente,
                                       Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);
                        graphics.DrawString(modelOverloading * 100 + "%",
                                    Grasshopper.Kernel.GH_FontServer.Small,
                                    Brushes.Red,
                                    info,
                                    Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);
                        //graphics.DrawString(((Donkey_Comp)this.Owner).ModelAverageValue + "/" + modelMaxValue.ToString(),
                        //                     Grasshopper.Kernel.GH_FontServer.StandardBold,
                        //                     Brushes.Red,
                        //                     this.m_innerBounds,
                        //                     Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);                    
                    }

                    break;
            }
        }
        #endregion

        #region DOUBLE-CLICK

        public override GH_ObjectResponse RespondToMouseDoubleClick(GH_Canvas sender, GH_CanvasMouseEvent e)
        {
            //Tracer_Form tf = new Tracer_Form();
   
            Evaluation_Comp owner = this.Owner as Evaluation_Comp;
            if (owner == null)
                return base.RespondToMouseDoubleClick(sender, e);
            if (owner.m_form == null)
            {
                owner.m_design_tracking = true;
                owner.m_form = new DesignTracker_Form(owner);
                GH_WindowsFormUtil.CenterFormOnCursor((Form)owner.m_form, true);
                ((Control)owner.m_form).Show();
                //owner.m_form.TopMost = true;
                if (owner.FieldOfSolutions != null && owner.FieldOfSolutions.Count == 0)
                {
                    owner.m_form.AddSolution(1, owner.m_model, true);
                    owner.FieldOfSolutions.Add(owner.m_model);
                }
            }
            else
                owner.m_form.Select();
            return GH_ObjectResponse.Handled;
        }
        #endregion
    }
}