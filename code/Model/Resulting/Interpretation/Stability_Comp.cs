﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;

namespace Donkey.Model.Resulting.Interpretation
{
    public class Stability_Comp : GH_Component
    {
        private double previous_value;

        /// <summary>
        /// Initializes a new instance of the Stability_Comp class.
        /// </summary>
        public Stability_Comp()
            : base("Stability", "Stability",
                "Stability/buckling value is calculated for whole structure",// + Donkey.Info.Mark,
                "Donkey", "4.Results")
        {
            this.previous_value = 0;
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new Resulting.ResultModel_Param());
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddNumberParameter("stability", "S", "S", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            ResultModel m_model = new ResultModel();
            if (!DA.GetData<ResultModel>(0, ref m_model))
            {
                this.Message = "NaN";
                return;
            }

            //m_model = new ResultModel(m_model);
            if (m_model.Stability == 0.0)
            {
                this.Message = "NaN";
                DA.SetData(0, null);
            }
            else
            {
                this.Message = Math.Round(m_model.Stability, 2) + "/" + Math.Round(previous_value, 2); //m_model.Reactions.Max_axis+": " +
                DA.SetData(0, m_model.Stability);
            }

            this.previous_value = m_model.Stability;

        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.stability; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{c3552ca7-eac1-4ed1-af63-c64255ca1acb}"); }
        }
    }
}