﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using Rhino.Geometry;
using Rhino.Display;
using Grasshopper.GUI.Canvas;
using Grasshopper.Kernel.Attributes;
using System.Windows.Forms;
using Grasshopper.GUI;

namespace Donkey.Model.Resulting.Interpretation
{
    public class FEM_Comp : GH_Component
    {
        #region FIELD
        private ResultModel m_model = new ResultModel();
        private bool m_status = false;
        internal FEM_Form m_form;
        #endregion

        #region CONSTRUCTOR
        /// <summary>
        /// Initializes a new instance of the FEM_GC class.
        /// </summary>
        public FEM_Comp()
            : base("FEM", "FEM",
                "Basic FE analysis result interpetation" + Environment.NewLine + "(more detailed information about structural mechanical response) (beta)",// + Donkey.Info.Mark + " (beta)",
                "Donkey", "4.Results")
            {
                m_model = new ResultModel();
            }
        #endregion

        #region OVERRIDE
        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new Resulting.ResultModel_Param());
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //VTU_Reader reader = new VTU_Reader();
            //this.m_model = reader.Read("C:/MIDAS/TEMP/try3_old.rslts.vtu");

            if(!DA.GetData<ResultModel>(0, ref m_model))
                return;
            //if (m_model != null)
            //{
            //    this.m_model = new ResultModel(m_model);
            //}

        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.donkey; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.hidden; }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{732d05f7-66d8-449a-a1ce-872860fa1e7f}"); }
        }

        public override BoundingBox ClippingBox
        {
            get { return m_model.ClippingBox; }
        }

        public override bool IsBakeCapable
        {
            get{ return false;}
        }

        public override void DrawViewportWires(IGH_PreviewArgs args)
        {
            //base.DrawViewportWires(args); //Preview all param 
            if (m_model != null)
            {
                ResultModel model = new ResultModel(m_model); //HACK WHY NEW IS NEEDED!!
                Color colour;
                int thickness;
                if (this.Attributes.GetTopLevel.Selected)
                {
                    colour = args.WireColour_Selected;
                    thickness = 2;
                }
                else
                {
                    colour = args.WireColour;
                    thickness = 1;
                }
                //HACK why new? or use DrawGeometryDisplacement
                GH_PreviewWireArgs argsWire = new GH_PreviewWireArgs(args.Viewport, args.Display, colour, thickness);
                model.DrawWireGeometry(argsWire);
                model.DrawGeometryStress(args);
            }
        }

        public override bool IsPreviewCapable { get{return true;} }

        public override void CreateAttributes()
        {
            m_attributes = new FEM_Attributes(this);
        }

        public override void RemovedFromDocument(GH_Document document)
        {
            if (this.m_form != null)
            {
                this.m_form.Owner = (FEM_Comp)null;
                this.m_form.Close();
                this.m_form = (FEM_Form)null;
            }
            base.RemovedFromDocument(document);
        }

        #endregion

        #region DOUBLE-CLICK
        //public void ShowSettingsGui()
        //{
        //    System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
        //    ofd.Multiselect = true;
        //    ofd.Filter = "Data Sources (*.ini)|*.ini*|All Files|*.*";
        //    if (ofd.ShowDialog() == DialogResult.OK)
        //    {
        //        string[] filePath = ofd.FileNames;
        //        string[] safeFilePath = ofd.SafeFileNames;
        //    }

        //    //m_settings = Path.GetDirectoryName(ofd.FileName);
        //}
        #endregion

        #region MENU
        public override bool AppendMenuItems(ToolStripDropDown menu)
        {
            this.AppendAdditionalMenuItems(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            this.Menu_AppendPreviewItem(menu);
            this.Menu_AppendEnableItem(menu);
            this.Menu_AppendRuntimeMessages(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            this.Menu_AppendObjectHelp(menu);
            return true;
        }

        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Custom", new EventHandler(this.Menu_PopupClicked), true, this.m_status).ToolTipText = "Custom analysis interpretation.";
        }

        private void Menu_PopupClicked(object sender, EventArgs e)
        {
            if (this.m_status)
            {
                this.m_status = false;
                this.RecordUndoEvent("No message");
                this.Message = (string)null;
            }
            else
            {
                this.m_status = true;
                this.RecordUndoEvent("Message");
                this.Message = "Custom";
            }
            this.ExpireSolution(true);
        }
        #endregion
    }

    public class FEM_Attributes : GH_ComponentAttributes //GH_Attributes<FEM_Comp>
    {
        //FEM_Comp Owner;

        public FEM_Attributes(FEM_Comp owner)
            : base(owner)
        {
            //Owner = owner;
        }

        public override bool HasInputGrip { get { return true; } }
        public override bool HasOutputGrip { get { return false; } }

        #region LAYOUT
        protected override void Layout()
        {
            this.Pivot = (PointF) GH_Convert.ToPoint(base.Pivot);
            //this.m_innerBounds = GH_ComponentAttributes.LayoutComponentBox(base.Owner);
            //this.m_innerBounds = new System.Drawing.RectangleF(Pivot, new SizeF(112, 48));
            this.m_innerBounds = new RectangleF(Owner.Attributes.Pivot.X - 0.5f * (float) 1, Owner.Attributes.Pivot.Y - 0.5f * (float) 1, (float) 112, (float) 48);           
            //this.Bounds = new System.Drawing.RectangleF(Pivot, new SizeF(112, 48));
            GH_ComponentAttributes.LayoutInputParams(base.Owner, this.m_innerBounds);
            //GH_ComponentAttributes.LayoutOutputParams(base.Owner, this.m_innerBounds);
            this.Bounds = GH_ComponentAttributes.LayoutBounds(base.Owner, this.m_innerBounds);
            //this.LayoutJaggedEdges();
        }
        #endregion

        #region RENDER
        //protected override void Render(GH_Canvas canvas, Graphics graphics, GH_CanvasChannel channel)
        //{
        //    if (channel == GH_CanvasChannel.Wires)
        //    {
        //        base.Render(canvas, graphics, channel);
        //    }

        //    if (channel == GH_CanvasChannel.Objects)
        //    {
        //        GH_Capsule capsule = GH_Capsule.CreateCapsule(Bounds, GH_Palette.Normal);
        //        capsule.AddInputGrip(InputGrip);
        //        //RenderComponentParameters(canvas, graphics, base.Owner, new GH_PaletteStyle(Color.Black));
        //        capsule.Render(graphics, Selected, Owner.Locked, true);
        //        capsule.Dispose();
        //    }
        //}

        protected override void Render(GH_Canvas canvas, Graphics graphics, GH_CanvasChannel channel)
        {
            switch(channel)
            {
                case GH_CanvasChannel.Wires:
                    base.Render(canvas,graphics,channel);
                    break;

                case GH_CanvasChannel.Objects:
                    base.RenderComponentCapsule(canvas, graphics, true, false, false, true, true, true);

                    graphics.DrawString("FEM preview (beta)",
                                    Grasshopper.Kernel.GH_FontServer.Standard,
                                    Brushes.Black,
                                    this.m_innerBounds,
                                    Grasshopper.GUI.GH_TextRenderingConstants.CenterCenter);

                    break;
            }
        }
        #endregion

        #region DOUBLE-CLICK
         //public override GH_ObjectResponse RespondToMouseDoubleClick(GH_Canvas sender, GH_CanvasMouseEvent e)
         //      {
         //          ((FEM_Comp)this.Owner).ShowSettingsGui();
         //           return GH_ObjectResponse.Handled;
         //      } 
    
        public override GH_ObjectResponse RespondToMouseDoubleClick(GH_Canvas sender, GH_CanvasMouseEvent e)
            {
              FEM_Comp owner = this.Owner as FEM_Comp;
              if (owner == null)
                return base.RespondToMouseDoubleClick(sender, e);
              if (owner.m_form == null)
              {
                owner.m_form = new FEM_Form();
                owner.m_form.Owner = owner;
                GH_WindowsFormUtil.CenterFormOnCursor((Form) owner.m_form, true);
                ((Control) owner.m_form).Show();
                owner.m_form.TopMost = true;
              }
              else
                  owner.m_form.Select();
              return GH_ObjectResponse.Handled;
            }
        #endregion
    }
}