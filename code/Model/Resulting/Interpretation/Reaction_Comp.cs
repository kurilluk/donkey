﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Drawing;
using System.Windows.Forms;
using Donkey.Model.Resulting.Data;

namespace Donkey.Model.Resulting.Interpretation
{
    public class Reaction_Comp : GH_Component
    {
        #region FIELD
        private ResultModel m_model;
        private List<Line> m_resultants;
        private List<Double> m_values;
        private bool m_previewValue;
        //private GlobalValues m_global_values;
        private double m_prev_sum;
        #endregion

        /// <summary>
        /// Initializes a new instance of the Reaction class.
        /// </summary>
        public Reaction_Comp()
            : base("Reaction", "Reaction",
                "Reaction value",// + Donkey.Info.Mark,
                "Donkey", "4.Results")
        {
            this.m_model = null;
            this.m_resultants = null;
            this.m_values = null;
            this.m_previewValue = false;
            this.m_prev_sum = Double.NaN;
            //this.m_global_values = new GlobalValues();
            GlobalValues.ForceScaleExpired += PreviewForces;
        }

        void PreviewForces()
        {
            this.RecalculateLines_ForPreview();
            this.ExpirePreview(true);
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new Resulting.ResultModel_Param());
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            //pManager.AddTextParameter("reactions", "R", "Reaction on the supports [N]", GH_ParamAccess.item);
            pManager.AddVectorParameter("reaction", "V", "Reaction [N]", GH_ParamAccess.list);
            pManager.AddVectorParameter("sum", "\u03A3", "Sum of reaction [N]", GH_ParamAccess.item); //"\u2140"
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //ResultModel m_model = new ResultModel();
            this.m_model = new ResultModel();
            if (!DA.GetData<ResultModel>(0, ref m_model))
            {
                this.Message = "NaN";
                return;
            }

            if (this.m_prev_sum != Double.NaN)
                this.Message = Math.Round(m_prev_sum - (m_model.Reactions.Sum.Length * 0.001),3)+"kN";
            this.m_prev_sum = m_model.Reactions.Sum.Length * 0.001;


            RecalculateLines_ForPreview();
            StoreValues_ForPrevies();

            //m_model = new ResultModel(m_model);

            //this.Message = Math.Round(m_model.Reactions.Max_direction * 0.001, 2) + " kN"; //m_model.Reactions.Max_axis+": " +

            //DA.SetData(0, m_model.Reactions.ToString());
            DA.SetDataList(0, m_model.Reactions.Vectors);
            DA.SetData(1, m_model.Reactions.Sum);
        }

        private void StoreValues_ForPrevies()
        {
            this.m_values = new List<double>();
            Reactions re = m_model.Reactions;
            for (int i = 0; i < re.Supports_ID.Count; i++)
            {
                //recalculate value from N to kN and rouded
                m_values.Add(Math.Round(re.Vectors[i].Length*0.001,3));
            }
        }

        private void RecalculateLines_ForPreview()
        {
            if (m_model != null)
            {
                this.m_resultants = new List<Line>();
                Reactions re = m_model.Reactions;
                for (int i = 0; i < re.Supports_ID.Count; i++)
                {
                    Line line = new Line(re.Position(i), -re.Vectors[i] * GlobalValues.ForceScale);
                    line.Flip();
                    this.m_resultants.Add(line);
                }
            }
        }

        
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Donkey.Properties.Resources.reaction;
            }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{cabad36b-dfac-4518-bea0-84fb52d7421a}"); }
        }

        public override void ClearData()
        {
            if (this.m_model != null)
                this.m_model = null;
            if (this.m_resultants != null && this.m_resultants.Count > 0)
            {
                this.m_resultants.Clear();
                this.m_resultants = null;
            }
            if (this.m_values != null && this.m_values.Count > 0)
            {
                this.m_values.Clear();
                this.m_values = null;
            }
            this.Message = "NaN";
            base.ClearData();
        }

        public override bool IsPreviewCapable
        {
            get
            {
                return true;
            }
        }

        public override BoundingBox ClippingBox
        {
            get
            {
                BoundingBox bb = base.ClippingBox;
                if(m_resultants != null)
                    foreach (Line line in m_resultants)
                        bb.Union(line.BoundingBox);
                return bb;
            }
        }

        public override void DrawViewportWires(IGH_PreviewArgs args)
        {
            if (m_model != null && m_resultants != null)
            {
                for (int i = 0; i < m_resultants.Count; i++)
                {
                    //args.Display.DrawLineArrow(m_resultants[i], Color.Red, 5, 100);
                    args.Display.DrawArrowHead(m_resultants[i].To, m_resultants[i].Direction, Color.Black, 0.0, m_resultants[i].Length * 0.3);
                    args.Display.DrawLine(m_resultants[i], Color.Black, 4);

                    if (m_previewValue && m_values != null)
                    {
                        Model_Goo.DrawLabel(args,this.m_values[i] + "kN", m_resultants[i].PointAt(0.5), Color.Black, 150, 10, 80);
                        //Plane cam_plane;
                        //args.Viewport.GetCameraFrame(out cam_plane);
                        //Plane text_plane = new Plane(m_resultants[i].PointAt(0.5),m_resultants[i].Direction, cam_plane.YAxis);
                        //Rhino.Display.Text3d text3d = new Rhino.Display.Text3d(m_resultants[i].Length + "kN", text_plane, 80);
                        //args.Display.Draw3dText(text3d,Color.Red);
                    }
                }               
            }
        }

        //Reactions re = m_model.Reactions;
        // if (this.Attributes.GetTopLevel.Selected)
        //{

        //args.Display.Draw3dText("Support 0", Color.Black, Plane.WorldXY, 200, "Arial");
        //args.Display.Draw3dText("Support 420", Color.Black, Plane.WorldYZ, 200, "Arial");
        //args.Display.Draw3dText("Support 32", Color.Black, Plane.WorldZX, 200, "Arial");
        //}
        // else 
        // {
        ////args.Display.DepthMode = Rhino.Display.DepthMode.AlwaysInFront;
        //foreach (Line line in m_model.Reactions.Lines)
        //{
        //    if (line.Length > 0)
        //    {
        //        line.Flip();
        //        args.Display.DrawLineArrow(line, Color.GreenYellow, 3, 50); //args.WireColour, 2);
        //        //args.Display.DrawLine(line, Color.GreenYellow, 3);
        //        //args.Display.DrawMarker(line.From, line.Direction, Color.Red, 3, 50, 0);
        //        //args.Display.DrawArrow(line, Color.GreenYellow,10.0, 0.0);

        //        double distance = 100;
        //        if (line.Direction.Y != 0)
        //            distance = -distance;
        //        Model_Goo.DrawLabel(args, line.Length + "kN", line.PointAt(0.5), Color.Green,
        //            distance, 5,30);

        //        //Plane plane;
        //        //if (line.Direction.Equals(Plane.WorldXY.ZAxis))
        //        //    plane = new Plane(line.PointAt(0.5), line.Direction, Plane.WorldXY.ZAxis);
        //        //else
        //        //    plane = new Plane(line.PointAt(0.5), line.Direction, Plane.WorldXY.YAxis);


        //        //args.Viewport.GetCameraFrame(out plane);
        //        //plane.Origin = line.From;

        //        //Rhino.Display.Text3d text3d = new Rhino.Display.Text3d(line.Length + "kN", plane, 80);

        //        //BoundingBox bb = text3d.BoundingBox;
        //        //Vector3d aligment = origin - bb.Center;
        //        //aligment += direction;
        //        //Point3d text_point = origin + (direction + aligment);

        //        //args.Display.Draw3dText(text3d, Color.GreenYellow);
        //    }

        //}
        //    //Cylinder cyril = new Cylinder(new Circle(Plane.WorldZX, 50), 150);
        //    //args.Display.DrawCylinder(cyril, Color.Red);
        //    //Cone cone = new Cone(Plane.WorldXY, 150, 50);
        //    //args.Display.DrawCone(cone, Color.GreenYellow, 2);
        ////args.Display.DepthMode = Rhino.Display.DepthMode.Neutral;
        //}

        //if (m_previewValue)
        //{
        //int position = 0;
        //for (int i = 0; i < m_model.Reactions.Points.Count; i += 3)
        //{
        //    //args.Display.DrawDot(m_model.Reactions.Points[i], m_model.Reactions.GetReactionOn(position),Color.Gray,Color.Black);
        //    Point2d pt = new Point2d();
        //    pt = args.Viewport.WorldToClient(m_model.Reactions.Points[position]);
        //    args.Display.Draw2dText(this.m_model.Reactions.GetReactionOn(position), Color.Black, pt, true);
        //    position++;
        //}
        //}
        //}
        //}

        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetBoolean("preview_data", this.m_previewValue);
            return base.Write(writer);
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            this.m_previewValue = reader.GetBoolean("preview_data");
            return base.Read(reader);
        }


        #region MENU
        public override bool AppendMenuItems(ToolStripDropDown menu)
        {
            this.AppendAdditionalMenuItems(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            this.Menu_AppendPreviewItem(menu);
            this.Menu_AppendEnableItem(menu);
            this.Menu_AppendRuntimeMessages(menu);
            GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
            this.Menu_AppendObjectHelp(menu);
            return true;
        }

        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            //GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);

            ToolStripMenuItem toolStripMenuItem0 = GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Preview value", new EventHandler(this.PreviewChange), Donkey.Properties.Resources.donkey, true, this.m_previewValue);
            //toolStripMenuItem1.ToolTipText = "Rectangle cross section profile for Donkey." + Environment.NewLine + "If thikness is more that 0, then create tube profile";
        }

        private void PreviewChange(object sender, EventArgs e)
        {
            this.RecordUndoEvent("ChangePreviewMode");
            this.m_previewValue = !m_previewValue;
            //this.ExpireSolution(true);
            this.ExpirePreview(true);
        }
        #endregion
    }
}