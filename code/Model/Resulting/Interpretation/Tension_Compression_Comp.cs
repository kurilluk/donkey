﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Donkey.Model.Geometry;
using Donkey.Model.Resulting;
using Grasshopper.GUI.Gradient;
using Grasshopper.Kernel;
using Rhino.Geometry;

namespace Donkey.Model.Resulting.Interpretation
{
    public class Tension_Compression_Comp: GH_Component
    {
       #region FIELD
        ResultModel m_model;
        List<int> m_preview_thicknesses;
        List<Interval> m_domain_history;
        #endregion

        #region CONSTRUCTOR
        public Tension_Compression_Comp()
            : base("Tension/Compression", "Tension/Compression", "Tension and compression (pre-alpha)", "Donkey", "4.Results")
        {
            this.m_domain_history = new List<Interval>();
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new ResultModel_Param());
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new ResultModel_Param());
            //pManager.AddNumberParameter("Number", "N", "Scalar value", GH_ParamAccess.list);
            pManager.AddIntervalParameter("Domain", "D", "The maximal and minimal magnitude of the axial forces", GH_ParamAccess.list);
        }
        #endregion

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            this.m_model = new ResultModel();
            DA.GetData(0, ref m_model);

            List<double> axialForces = m_model.Forces.Beam_AxialForce_N;

            m_domain_history.Insert(0,GeneratePreviewThicknesses(axialForces, 15));

            DA.SetData(0, m_model);
            //DA.SetDataList(1, m_model.Forces.BeamStress_N);
            DA.SetDataList(1, this.m_domain_history);
        }

        public Interval GeneratePreviewThicknesses(List<double> axialForces, double max_thikness)
        {
            //CALCULATE MAX VALUE
            double min_force = Double.MaxValue;
            double max_force = Double.MinValue;

            foreach (double force  in axialForces)
            {
                if (force < min_force)
                    min_force = force;
                if (force > max_force)
                    max_force = force;
            }

            double max_value;
            double abs_minForce = Math.Abs(min_force);
            if (abs_minForce > max_force)
                max_value = abs_minForce;
            else
                max_value = max_force;


            //CALCULATE THICKNESS
            this.m_preview_thicknesses = new List<int>(axialForces.Count);
            foreach (double force in axialForces)
            {
                m_preview_thicknesses.Add(2 + (int)Math.Round(max_thikness * (Math.Abs(force) / max_value)));
            }

            return new Interval(min_force, max_force);
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{3D469C14-5CFB-4073-869B-262A92B4F573}"); }
        }

        #region COMPONENT SETUP
        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.tension_compression; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        public override bool IsPreviewCapable
        {
            get
            {
                return true;
            }
        }
        #endregion

        #region PREVIEW
        public override void DrawViewportWires(IGH_PreviewArgs args)
        {
            if (m_model != null && m_preview_thicknesses != null)
            {
                for (int i = 0; i < m_model.Geometries.Count; i++)
                {
                    Color color;
                    if (m_model.Geometries[i].Type == Types.VTK_TRIANGLE)
                        color = Color.Gray;
                   else if (m_model.Forces.Beam_AxialForce_N[i] == 0)
                        color = Color.Black;
                    else if (m_model.Forces.Beam_AxialForce_N[i] > 0)
                        color = Color.Blue; //Tension
                    else
                        color = Color.Red; //Compression

                    m_model.Geometries[i].DrawViewportWires(new GH_PreviewWireArgs(args.Viewport, args.Display, color, m_preview_thicknesses[i]));
                }
            }
        }

        //public override void DrawViewportMeshes(IGH_PreviewArgs args)
        //{
        //    //if (m_topology != null)
        //    //{
        //    //    foreach (Mule.Utilities.Preview.Arrow3D arrow in m_topology.Loads)
        //    //        arrow.Draw(args);
        //    //}
        //}

        public override BoundingBox ClippingBox
        {
            get
            {
                BoundingBox bb = base.ClippingBox;
                if (m_model != null)
                {
                    foreach (Geometry_Goo geometry in m_model.Geometries)
                        bb.Union(geometry.ClippingBox);
                }

                return bb;
            }
        }

        public override void ClearData()
        {
            this.m_model = (ResultModel)null;
            this.m_preview_thicknesses = null;
            //this.m_domain_history.Clear();
            base.ClearData();
        }
        #endregion

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {
            GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Reset history", this.ResetHistory)
                .ToolTipText = "Reset previously stored domain values";
            base.AppendAdditionalMenuItems(menu);
        }

        private void ResetHistory(object sender, EventArgs e)
        {
            this.m_domain_history.Clear();
            this.ExpireSolution(true);
        }
    }
}
