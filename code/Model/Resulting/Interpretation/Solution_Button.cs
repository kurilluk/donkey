﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donkey.Model.Resulting.Interpretation
{
    public partial class Solution_Button : RadioButton
    {
        #region FIELD
        private bool m_mouseOver = false;
        private Color m_color;
        public bool m_actual;
        public string Values { get; set; }
        #endregion

        #region CONSTRUCTOR
        public Solution_Button(Color color, bool actual)
        {
            this.m_color = color;
            InitializeComponent();
            this.IsActual = actual;
            this.Values = "";
        }
        #endregion

        #region PARAMETERS
        public bool IsActual
        {
            get { return this.m_actual; }
            set { this.m_actual = value; this.Invalidate(); }
        }
        #endregion

        #region METHODS
        protected override void OnPaint(PaintEventArgs pe)
        {
            //base.OnPaint(pevent);
            Rectangle rect_full = new Rectangle(0, 0, 26, 26); //20
            Rectangle rect_small = new Rectangle(2, 2, 22, 22); //16
            GraphicsPath grPath = new GraphicsPath();
            grPath.AddRectangle(rect_full);
            this.Region = new System.Drawing.Region(grPath);

            //pevent.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            Pen pen = Pens.Black;
            Pen a = new Pen(Brushes.Black, 1);

            //Brush color_brush = new Brush(
            SolidBrush color_brush = new SolidBrush(m_color);

            if ( this.IsActual && !this.Checked)
            {
                grPath = new GraphicsPath();
                grPath.AddRectangle(rect_full);
                this.Region = new System.Drawing.Region(grPath);

                pe.Graphics.FillRectangle(color_brush, rect_full);
                rect_full.Inflate(-1, -1);
                Pen selected_pen = new Pen(Brushes.Black, 2);
                pe.Graphics.DrawRectangle(selected_pen, rect_full);
                //this.Appearance = new Appearance();
                this.BringToFront();
                //this.TabIndex = 100;
                //this.SetTopLevel(true);
            }
            else if (this.Checked)
            {
                grPath = new GraphicsPath();
                grPath.AddRectangle(rect_full);
                this.Region = new System.Drawing.Region(grPath);

                pe.Graphics.FillRectangle(Brushes.White, rect_full);
                rect_full.Inflate(-1, -1);
                Pen selected_pen = new Pen(Brushes.Gray,2);
                //selected_pen.DashPattern = new float[] { 1.0F, 1.0F};
                pe.Graphics.DrawRectangle(selected_pen, rect_full);
                rect_full.Inflate(-2, -2);
                pe.Graphics.FillRectangle(color_brush, rect_full);
                //this.Appearance = new Appearance();
                this.BringToFront();
                //this.TabIndex = 100;
                //this.SetTopLevel(true);
            }
            else
            {
                grPath = new GraphicsPath();
                grPath.AddRectangle(rect_small);
                this.Region = new System.Drawing.Region(grPath);

                if (this.m_mouseOver)
                {
                    pe.Graphics.FillRectangle(Brushes.Gray, rect_small);
                }
                else
                {
                    pe.Graphics.FillRectangle(color_brush, rect_small);
                }
            }

            rect_small.Inflate(-1, -1);
            Pen basic_pen = new Pen(Brushes.White, 2);
            pe.Graphics.DrawRectangle(basic_pen, rect_small);
            //rect_small.Inflate(-4, -4);
            //pe.Graphics.DrawString(this.Name,this.Font, Brushes.Black, rect_small);


        }

        protected override void OnMouseEnter(EventArgs eventargs)
        {
            this.m_mouseOver = true;
            base.OnMouseEnter(eventargs);
        }

        protected override void OnMouseLeave(EventArgs eventargs)
        {
            this.m_mouseOver = false;
            base.OnMouseLeave(eventargs);
        }
        #endregion
    }
}
