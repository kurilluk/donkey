﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Donkey.Properties;

namespace Donkey.Model.Resulting.Interpretation
{
    public class FEM_Form: Form
    {
        private IContainer components;
        private Button Stop;
        private Button Play;
        private Button pause;
        private Button step;
        private Button skip;

        public FEM_Comp Owner { get; set; }

        public FEM_Form()
        {
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.skip = new System.Windows.Forms.Button();
            this.step = new System.Windows.Forms.Button();
            this.pause = new System.Windows.Forms.Button();
            this.Play = new System.Windows.Forms.Button();
            this.Stop = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // skip
            // 
            this.skip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skip.Image = global::Donkey.Properties.Resources.material;
            this.skip.Location = new System.Drawing.Point(140, 0);
            this.skip.Name = "skip";
            this.skip.Size = new System.Drawing.Size(35, 35);
            this.skip.TabIndex = 4;
            this.skip.UseVisualStyleBackColor = true;
            this.skip.Click += new System.EventHandler(this.Skip_Click);
            // 
            // step
            // 
            this.step.Dock = System.Windows.Forms.DockStyle.Left;
            this.step.Image = global::Donkey.Properties.Resources.donkey;
            this.step.Location = new System.Drawing.Point(105, 0);
            this.step.Name = "step";
            this.step.Size = new System.Drawing.Size(35, 35);
            this.step.TabIndex = 3;
            this.step.UseVisualStyleBackColor = true;
            this.step.Click += new System.EventHandler(this.Step_Click);
            // 
            // pause
            // 
            this.pause.Dock = System.Windows.Forms.DockStyle.Left;
            this.pause.Image = global::Donkey.Properties.Resources.donkey_icon_24x24;
            this.pause.Location = new System.Drawing.Point(70, 0);
            this.pause.Name = "pause";
            this.pause.Size = new System.Drawing.Size(35, 35);
            this.pause.TabIndex = 2;
            this.pause.UseVisualStyleBackColor = true;
            this.pause.Click += new System.EventHandler(this.Pause_Click);
            // 
            // Play
            // 
            this.Play.Dock = System.Windows.Forms.DockStyle.Left;
            this.Play.Image = global::Donkey.Properties.Resources.evaluate;
            this.Play.Location = new System.Drawing.Point(35, 0);
            this.Play.Name = "Play";
            this.Play.Size = new System.Drawing.Size(35, 35);
            this.Play.TabIndex = 1;
            this.Play.UseVisualStyleBackColor = true;
            this.Play.Click += new System.EventHandler(this.Play_Click);
            // 
            // Stop
            // 
            this.Stop.Dock = System.Windows.Forms.DockStyle.Left;
            this.Stop.Image = global::Donkey.Properties.Resources.efficiency;
            this.Stop.Location = new System.Drawing.Point(0, 0);
            this.Stop.Name = "Stop";
            this.Stop.Size = new System.Drawing.Size(35, 35);
            this.Stop.TabIndex = 0;
            this.Stop.UseVisualStyleBackColor = true;
            this.Stop.Click += new System.EventHandler(this.Stop_Click);
            // 
            // FEM_Form
            // 
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(175, 35);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.skip);
            this.Controls.Add(this.step);
            this.Controls.Add(this.pause);
            this.Controls.Add(this.Play);
            this.Controls.Add(this.Stop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;//FixedToolWindow;
            this.Name = "FEM_Form";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            //this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FEM_Preview";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GH_ActiveSimulationForm_FormClosing);
            this.Load += new System.EventHandler(this.GH_ActiveSimulationForm_Load);
            this.ResumeLayout(false);

        }

        private void UpdateUI()
        {
        }

        private void GH_ActiveSimulationForm_Load(object sender, EventArgs e)
        {
            this.UpdateUI();
            //this.Owner.reset = true;
        }

        private void Stop_Click(object sender, EventArgs e)
        {
            if (this.Owner != null)
            {
                //this.Owner.reset = true;
                //this.Owner.ActiveSimulation = false;
                this.Owner.ExpireSolution(true);
            }
            this.UpdateUI();
        }

        private void Play_Click(object sender, EventArgs e)
        {
            if (this.Owner == null)
                return;
            //this.Owner.reset = false;
            //this.Owner.ActiveSimulation = true;
            this.Owner.ExpireSolution(true);
        }

        private void Pause_Click(object sender, EventArgs e)
        {
            //if (this.Owner == null || this.Owner.reset)
                //return;
            //this.Owner.ActiveSimulation = !this.Owner.ActiveSimulation;
            this.Owner.ExpireSolution(true);
        }

        private void Step_Click(object sender, EventArgs e)
        {
            if (this.Owner == null)
                return;
            //this.Owner.reset = false;
            //this.Owner.ActiveSimulation = false;
            this.Owner.ExpireSolution(true);
        }

        private void Skip_Click(object sender, EventArgs e)
        {
            if (this.Owner == null)
                return;
            //this.Owner.ActiveSimulation = false;
            this.Owner.ExpireSolution(true);
        }

        private void GH_ActiveSimulationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.Owner == null)
                return;
            this.Owner.m_form = (FEM_Form)null;
        }
    }
}