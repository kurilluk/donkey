﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Donkey.Properties;

namespace Donkey.Model.Resulting.Interpretation
{
    public class DesignTracker_Form : Form
    {
        //private IContainer components;
        private Button Stop;
        private Button Play;
        private Button pause;
        private Button step;
        private Button skip;
        internal CheckBox tracking;
        private Panel panel1;
        private Label label1;
        private IContainer components;
        private CheckBox checkBox1;

        private List<Solution_Button> solutions;

        private Evaluation_Comp m_component;

        public DesignTracker_Form(Evaluation_Comp component)
        {
            this.m_component = component;
            this.InitializeComponent();
            this.tracking.Checked = component.m_design_tracking;
            //this.InitForm();
            this.InitContent();
        }

        public void DesingTrack_change()
        {
            this.tracking.Checked = m_component.m_design_tracking;
        }

        public void AddSolution(int sol_index, ResultModel model, bool actual)
        {
            int height = this.panel1.Size.Height;
            int width = this.panel1.Size.Width;

            int x_pos = (int)(width * model.Efficiency);

            int y_pos;
            if (model.Overloading > 0)
                y_pos = 25;
            else
              y_pos = 0;

            Color color;
            if (model.Overloading == 0)
                color = ResultModel.Gradient.ColourAt(model.Efficiency);
            else
                color = ResultModel.Gradient.ColourAt(1 + model.Overloading);

            if (actual)
            {
                if (this.solutions != null && this.solutions.Count > 0)
                    this.solutions[solutions.Count - 1].IsActual = false;
            }

            Solution_Button solution = new Solution_Button(color, actual);
            //solution.Checked = true;
            //Solution_radioButton.Appearance = System.Windows.Forms.Appearance.Button;
            solution.BackColor = System.Drawing.Color.White;
            //Solution_radioButton.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            solution.Cursor = System.Windows.Forms.Cursors.Hand;
            solution.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            solution.Font = new System.Drawing.Font("Arial Narrow", 7.00F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            solution.Location = new System.Drawing.Point(x_pos, y_pos);
            solution.Size = new System.Drawing.Size(30, 30);
            solution.TabIndex = 2;
            solution.TabStop = true;
            solution.Text = sol_index.ToString(); ;
            solution.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            solution.UseVisualStyleBackColor = false;
            solution.Name = sol_index.ToString();
            solution.Values = "Efficiency: " + model.Efficiency*100 + "%\r\nOverloading: " + model.Overloading*100+"%";

            if (actual)
                PrintSolution(solution);

            solution.CheckedChanged += new System.EventHandler(this.SelectedSolution);
            this.panel1.Controls.Add(solution);

            for (int i = 0; i < solutions.Count; i++)
            {
                if (solutions[i].Checked)
                    solutions[i].Checked = false;
            }

            this.solutions.Add(solution); 
        }

        private void PrintSolution(Solution_Button solution)
        {
            this.label1.Text = "Solution ID:" + solution.Name + "\r\n" + solution.Values;
        }


        private void SelectedSolution(object sender, EventArgs e)
        {
            Solution_Button s = ((Solution_Button) sender);

            if (s.Checked) //TODO stange calling from changing checked...
            {
                PrintSolution(s);
                this.m_component.m_model = this.m_component.FieldOfSolutions[(Int32.Parse(((RadioButton)sender).Name) - 1)];
                this.m_component.ExpirePreview(true);
            }
        }

        protected override void Dispose(bool disposing)
        {
            //if (disposing && this.components != null)
                //this.components.Dispose();
            if(this.solutions != null)
                this.solutions.Clear();
            if (m_component != null && m_component.FieldOfSolutions != null && m_component.FieldOfSolutions.Count > 0)
            {
                this.m_component.m_model = this.m_component.FieldOfSolutions[this.m_component.FieldOfSolutions.Count - 1];
                this.m_component.ExpirePreview(true);
            }
            base.Dispose(disposing);
        }

        //private void InitForm()
        //{
        //    this.SuspendLayout();
        //    // 
        //    // DesignTracker_Form
        //    // 
        //    this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        //    this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        //    this.AutoSize = true;
        //    this.ClientSize = new System.Drawing.Size(237, 101);
        //    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
        //    this.MaximizeBox = false;
        //    this.MinimizeBox = false;
        //    this.Name = "DesignTracker_Form";
        //    this.ShowIcon = false;
        //    this.ShowInTaskbar = false;
        //    this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
        //    this.Text = "Design Tracker";
        //    this.TopMost = true;
        //    this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GH_ActiveSimulationForm_FormClosing);
        //    this.Load += new System.EventHandler(this.GH_ActiveSimulationForm_Load);
        //    this.ResumeLayout(false);
        //}

        private void InitContent()
        {
            this.solutions = new List<Solution_Button>();
            int count = this.m_component.FieldOfSolutions.Count;
            if (m_component.FieldOfSolutions != null && count>0)
                for (int i = 0; i < count; i++ )
                {
                    ResultModel model = this.m_component.FieldOfSolutions[i];
                    if(i == count-1)
                        this.AddSolution(i + 1, model, true);
                    else
                        this.AddSolution(i + 1, model, false);
                }
        }

        public void InitButtons()
        {
            this.skip = new System.Windows.Forms.Button();
            this.step = new System.Windows.Forms.Button();
            this.pause = new System.Windows.Forms.Button();
            this.Play = new System.Windows.Forms.Button();
            this.Stop = new System.Windows.Forms.Button();

            // 
            // skip
            // 
            this.skip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skip.Image = global::Donkey.Properties.Resources.material;
            this.skip.Location = new System.Drawing.Point(140, 0);
            this.skip.Name = "skip";
            this.skip.Size = new System.Drawing.Size(97, 101);
            this.skip.TabIndex = 4;
            this.skip.UseVisualStyleBackColor = true;
            this.skip.Click += new System.EventHandler(this.Skip_Click);
            // 
            // step
            // 
            this.step.Dock = System.Windows.Forms.DockStyle.Left;
            this.step.Image = global::Donkey.Properties.Resources.donkey;
            this.step.Location = new System.Drawing.Point(105, 0);
            this.step.Name = "step";
            this.step.Size = new System.Drawing.Size(35, 101);
            this.step.TabIndex = 3;
            this.step.UseVisualStyleBackColor = true;
            this.step.Click += new System.EventHandler(this.Step_Click);
            // 
            // pause
            // 
            this.pause.Dock = System.Windows.Forms.DockStyle.Left;
            this.pause.Image = global::Donkey.Properties.Resources.donkey_icon_24x24;
            this.pause.Location = new System.Drawing.Point(70, 0);
            this.pause.Name = "pause";
            this.pause.Size = new System.Drawing.Size(35, 101);
            this.pause.TabIndex = 2;
            this.pause.UseVisualStyleBackColor = true;
            this.pause.Click += new System.EventHandler(this.Pause_Click);
            // 
            // Play
            // 
            this.Play.Dock = System.Windows.Forms.DockStyle.Left;
            this.Play.Image = global::Donkey.Properties.Resources.evaluate;
            this.Play.Location = new System.Drawing.Point(35, 0);
            this.Play.Name = "Play";
            this.Play.Size = new System.Drawing.Size(35, 101);
            this.Play.TabIndex = 1;
            this.Play.UseVisualStyleBackColor = true;
            this.Play.Click += new System.EventHandler(this.Play_Click);
            // 
            // Stop
            // 
            this.Stop.Dock = System.Windows.Forms.DockStyle.Left;
            this.Stop.Image = global::Donkey.Properties.Resources.efficiency;
            this.Stop.Location = new System.Drawing.Point(0, 0);
            this.Stop.Name = "Stop";
            this.Stop.Size = new System.Drawing.Size(35, 101);
            this.Stop.TabIndex = 0;
            this.Stop.UseVisualStyleBackColor = true;
            this.Stop.Click += new System.EventHandler(this.Stop_Click);
            //
            this.Controls.Add(this.skip);
            this.Controls.Add(this.step);
            this.Controls.Add(this.pause);
            this.Controls.Add(this.Play);
            this.Controls.Add(this.Stop);
 
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DesignTracker_Form));
            this.tracking = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // tracking
            // 
            this.tracking.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tracking.AutoSize = true;
            this.tracking.Location = new System.Drawing.Point(142, 78);
            this.tracking.Name = "tracking";
            this.tracking.Size = new System.Drawing.Size(98, 17);
            this.tracking.TabIndex = 1;
            this.tracking.Text = "tracking design";
            this.tracking.UseVisualStyleBackColor = true;
            this.tracking.CheckedChanged += new System.EventHandler(this.tracking_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.ForeColor = System.Drawing.Color.DimGray;
            this.panel1.Location = new System.Drawing.Point(142, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(550, 60);
            this.panel1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "label1";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(246, 78);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(95, 17);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "leave selected";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // DesignTracker_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(704, 96);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tracking);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "DesignTracker_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Design Tracker";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GH_ActiveSimulationForm_FormClosing);
            this.Load += new System.EventHandler(this.GH_ActiveSimulationForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void UpdateUI()
        {
        }

        private void GH_ActiveSimulationForm_Load(object sender, EventArgs e)
        {
            this.UpdateUI();
            //this.Owner.reset = true;
        }

        private void Stop_Click(object sender, EventArgs e)
        {
            if (this.m_component != null)
            {
                //this.Owner.reset = true;
                //this.Owner.ActiveSimulation = false;
                this.m_component.ExpireSolution(true);
            }
            this.UpdateUI();
        }

        private void Play_Click(object sender, EventArgs e)
        {
            if (this.m_component == null)
                return;
            //this.Owner.reset = false;
            //this.Owner.ActiveSimulation = true;
            this.m_component.ExpireSolution(true);
        }

        private void Pause_Click(object sender, EventArgs e)
        {
            //if (this.Owner == null || this.Owner.reset)
            //return;
            //this.Owner.ActiveSimulation = !this.Owner.ActiveSimulation;
            this.m_component.ExpireSolution(true);
        }

        private void Step_Click(object sender, EventArgs e)
        {
            if (this.m_component == null)
                return;
            //this.Owner.reset = false;
            //this.Owner.ActiveSimulation = false;
            this.m_component.ExpireSolution(true);
        }

        private void Skip_Click(object sender, EventArgs e)
        {
            if (this.m_component == null)
                return;
            //this.Owner.ActiveSimulation = false;
            this.m_component.ExpireSolution(true);
        }

        private void GH_ActiveSimulationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.m_component == null)
                return;
            this.m_component.m_form = (DesignTracker_Form)null;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void tracking_CheckedChanged(object sender, EventArgs e)
        {
            //CALL component status change
            this.m_component.m_design_tracking = this.tracking.Checked;
            this.m_component.ExpirePreview(true);
        }

        private void solution_Button1_CheckedChanged(object sender, EventArgs e)
        {

        }

        //private void checkBox1_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (((CheckBox)sender).Checked)
        //        this.TopMost = true;
        //    else
        //        this.TopMost = false;
        //}

    }
}