﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using Rhino.Geometry;
using Rhino.Display;
using Grasshopper.GUI.Canvas;
using Grasshopper.Kernel.Attributes;
using System.Windows.Forms;
using Grasshopper.GUI;
using Donkey.Model.Geometry;
using Rhino.DocObjects;
using Rhino;

namespace Donkey.Model.Resulting.Interpretation
{
    public class Volume_Comp : GH_Component
    {
        #region FIELD
        #endregion

        #region CONSTRUCTOR
        /// <summary>
        /// Initializes a new instance of the FEM_GC class.
        /// </summary>
        public Volume_Comp()
            : base("Volume", "V",
                "Get volume of entire structure", //+ Donkey.Info.Mark,
                "Donkey", "4.Results")
        {
        }
        #endregion

        #region OVERRIDE
        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new Resulting.ResultModel_Param());
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddNumberParameter("Volume", "V", "Volume of entire structure", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            ResultModel model = null;
            if (!DA.GetData<ResultModel>(0, ref model))
                return;

            double volume = model.Volume * 0.000000001;
            DA.SetData(0, volume);
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.volume; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("1041254F-54E9-4240-8A7C-F9DBB3C07BD7"); }
        }
        #endregion
    }
}