﻿using System;
using Donkey.Model.Resulting;
using Grasshopper.Kernel;


namespace Donkey.Model.Resulting.Interpretation
{
    public class Buclking_Comp : GH_Component
    {
        public Buclking_Comp()
            : base("Buckling", "Buckling", "Buckling (pre-alpha)", "Donkey", "4.Results")
        { }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new ResultModel_Param());
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new ResultModel_Param());
            pManager.AddNumberParameter("Number", "N", "Scalar value", GH_ParamAccess.item);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            ResultModel model = new ResultModel();
            DA.GetData(0, ref model);

            DA.SetData(0, model);
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{1D86AEBF-3D25-4CD4-B9BC-A7E754A8A210}"); }
        }

        #region COMPONENT SETUP
        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.buckling; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }
        #endregion
    }
}
