﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using Rhino.Geometry;
using Rhino.Display;
using Grasshopper.GUI.Canvas;
using Grasshopper.Kernel.Attributes;
using System.Windows.Forms;
using Grasshopper.GUI;
using Donkey.Model.Geometry;
using Rhino.DocObjects;
using Rhino;

namespace Donkey.Model.Resulting.Interpretation
{
    public class Moments_Comp : GH_Component
    {
        #region FIELD
        private List<Curve> m_crvs;
        private bool m_dynamic_bake;
        private int m_group_id;
        #endregion

        #region CONSTRUCTOR
        /// <summary>
        /// Initializes a new instance of the FEM_GC class.
        /// </summary>
        public Moments_Comp()
            : base("Moments", "M",
                "Visualize moments in the structure (beta)", //+ Donkey.Info.Mark + " (beta)",
                "Donkey", "4.Results")
        {
            m_crvs = new List<Curve>();
            m_dynamic_bake = false;
            m_group_id = 0;
        }
        #endregion

        #region OVERRIDE
        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new Resulting.ResultModel_Param());
            pManager.AddIntegerParameter("Axis", "A", "Choose for which axis you would like to visualize the moments" + Environment.NewLine + "[0 = x, 1 = y, 2 = z]", GH_ParamAccess.item, 1);
            pManager.AddNumberParameter("Scale", "S", "Scale the moment value", GH_ParamAccess.item, 1);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddNumberParameter("Number", "N", "Scalar value", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            if(m_crvs != null || m_crvs.Count > 0)
                m_crvs.Clear();

            ResultModel model = null;
            if (!DA.GetData<ResultModel>(0, ref model))
                return;

            double scale = 1;
            DA.GetData<double>(2, ref scale);
  
            int axis = -1;
            DA.GetData<int>(1, ref axis);
            if (axis < 0 || axis > 2)
                this.AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "The axis is only in domain from 0 to 2!");
            
            for (int i = 0; i < model.Elements.Count; i++)
            {
                Element element = model.Elements[i];
                List<int> geometries = element.Geometries_ID;
                
                //TODO if element is shell skip
                //IF IT IS SHELL - don't visualize moments
                if (model.Geometries[geometries[0]].Type == Types.VTK_TRIANGLE)
                    continue;

                int g = 0;
                List<Point3d> pts = new List<Point3d>();
                Vector3d directionA = Vector3d.Unset;
                Vector3d directionB = Vector3d.Unset;
                //bool first = true;
                foreach (int geo_ID in geometries)
                {
                    Geometry_Goo geometry = model.Geometries[geo_ID];
                    Geometry_Goo geometryB = null;
                    if (g != geometries.Count - 1)
                    {
                        geometryB = model.Geometries[geometries[g + 1]];
                        geometryB.GetLocalPlane(Plane.WorldXY.ZAxis);
                    }
                    Vector3d momentsA = model.Forces.Beam_BendingMoment_M_pt1[geo_ID];
                    Vector3d momentsB = model.Forces.Beam_BendingMoment_M_pt2[geo_ID];
                    double sizeA, sizeB;
                    geometry.GetLocalPlane(Plane.WorldXY.ZAxis);
                    switch (axis)
                    {
                        case 0:
                            //if (g == 0 || g == geometries.Count - 1)
                                directionA = new Vector3d(geometry.Local.XAxis);
                                directionB = new Vector3d(geometry.Local.XAxis);
                            //else
                            //    direction += geometry.Local.XAxis; 
                            sizeA = momentsA.X;
                            sizeB = momentsB.X;
                            break;
                        case 1:
                            //directionA = new Vector3d(geometry.Local.YAxis);
                            //directionB = new Vector3d(geometry.Local.YAxis);

                            if (g == 0)
                            {
                                directionA = new Vector3d(geometry.Local.YAxis);
                                directionB = directionA + geometryB.Local.YAxis;
                                directionB.Unitize();
                            }
                            else if (g == geometries.Count - 1)
                            {
                                directionB = new Vector3d(geometry.Local.YAxis);
                            }
                            else
                            {
                                directionB.Unitize();
                                directionA = new Vector3d(directionB);

                                geometry.Local.YAxis.Unitize();
                                directionB += geometryB.Local.YAxis;
                                directionB.Unitize();
                            }

                            sizeA = momentsA.Z;
                            sizeB = momentsB.Z;
                            break;
                        case 2:
                            directionA = new Vector3d(geometry.Local.ZAxis);
                            directionB = new Vector3d(geometry.Local.ZAxis);
                            directionB.Unitize();
                            sizeA = momentsA.Y;
                            sizeB = momentsB.Y;
                            break;
                        default:
                            this.AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "The axis is only in domain from 0 to 2!");
                            directionA = Vector3d.Unset;
                            sizeA = Double.NaN;
                            return;
                            //break;
                    }
                    if (directionA != Vector3d.Unset || sizeA != Double.NaN)
                    {
                        if (!directionA.IsUnitVector)
                            directionA.Unitize();
                        //overwriten!
                        momentsA = - directionA * (sizeA * 0.001 * scale); //ADDITIONAL SCALE by 0.001?
                        momentsB = - directionB * (sizeB * 0.001 * scale);
                        List<Point3d> pts_geo = geometry.GetPoints();

                        m_crvs.Add(new Line(pts_geo[0], pts_geo[0] + momentsA).ToNurbsCurve());
                        m_crvs.Add(new Line(pts_geo[0] + momentsA, pts_geo[1] + momentsB).ToNurbsCurve());
                        m_crvs.Add(new Line(pts_geo[1] + momentsB, pts_geo[1]).ToNurbsCurve());

                        ////IF IS LAST GEOMETRY FROM ELEMENT - ADD VALUE TO END POINT
                        //if (g == geometries.Count - 1)
                        //{
                        //    pts.Add(new Point3d(pts_geo[1] + momentsB));
                        //    m_crvs.Add(Curve.CreateInterpolatedCurve(pts, 3));
                        //    m_crvs.Add(new Line(pts_geo[1] + momentsB, pts_geo[1]).ToNurbsCurve());
                        //    //pts.Add(pts_geo[1]);
                        //}
                        ////IF IS FIRST GEOMETRY ELEMENT - ADD LINE
                        //else if (g == 0)
                        //{
                        //    //pts.Add(pts_geo[0]);
                        //    m_crvs.Add(new Line(pts_geo[0], pts_geo[0] + momentsA).ToNurbsCurve());
                        //    pts.Add(new Point3d(pts_geo[0] + momentsA));
                        //}
                        ////FOR EACH POINTS BETWEEN
                        //else
                        //{
                        //    pts.Add(new Point3d(pts_geo[1] + momentsB));
                        //}
                        ////if(first)
                        ////    pts.Add(new Point3d(pts_geo[0] + momentsA));
                        ////pts.Add(new Point3d(pts_geo[1] + momentsB));
                    }
                    //first = false;
                    g++;
                }
                //if(pts.Count > 0)
                    //m_crvs.Add(Curve.CreateInterpolatedCurve(pts, 3));
            }
            if (m_dynamic_bake)
            {
                //this.BakeGeometry(Rhino.RhinoDoc.ActiveDoc, new List<Guid>());
                Rhino.RhinoDoc doc = Rhino.RhinoDoc.ActiveDoc;
                ObjectAttributes att = doc.CreateDefaultAttributes();
                //att.AddToGroup(125);
                List<Guid> g_ids = new List<Guid>();
                doc.UndoRecordingEnabled = true;
                uint undo_id = doc.BeginUndoRecord("Moments_baking");
                foreach (Curve crv in m_crvs)
                {
                    g_ids.Add(doc.Objects.AddCurve(crv, att));
                }
                doc.Groups.AddToGroup(m_group_id, g_ids);
                doc.Modified = true;
                doc.Views.Redraw();
                doc.EndUndoRecord(undo_id);
            }
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.moments; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("E21A57B4-2CA7-499B-9C40-584DDAABC98C"); }
        }

        public override BoundingBox ClippingBox
        {
            get
            {
                if(m_crvs != null || m_crvs.Count > 0) 
                {
                    BoundingBox bp = new BoundingBox();
                    foreach(Curve crv in m_crvs)
                        bp.Union(crv.GetBoundingBox(false));
                    return bp;
                }
                else return base.ClippingBox;
            }
        }

        //BAKE FUNCTIONALITY
        public override bool IsBakeCapable
        {
            get { return true; }
        }

         public override void BakeGeometry(RhinoDoc doc, List<Guid> obj_ids)
        {
            ObjectAttributes att = Rhino.RhinoDoc.ActiveDoc.CreateDefaultAttributes();
            att.ColorSource = ObjectColorSource.ColorFromObject;
            this.BakeGeometry(doc, att, obj_ids);
        }

         public override void BakeGeometry(RhinoDoc doc, ObjectAttributes att, List<Guid> obj_ids)
         {
             att.ColorSource = ObjectColorSource.ColorFromObject;
             att.PlotColorSource = ObjectPlotColorSource.PlotColorFromObject;
             //int index = 0;
             foreach (Curve crv in m_crvs)
             {
                //List<Guid> baked_objects = new List<Guid>();
                obj_ids.Add(doc.Objects.AddCurve(crv, att));
                //doc.Groups.AddToGroup(index, obj_ids);
                //index++;
             }
             doc.Groups.Add(obj_ids);
         }

         public override void ClearData()
         {
             m_crvs.Clear();
             //m_crvs = null;
             base.ClearData();
         }

        public override bool IsPreviewCapable { get { return true; } }

        public override void DrawViewportWires(IGH_PreviewArgs args)
        {
            //base.DrawViewportWires(args); //Preview all param 
            if (m_crvs != null || m_crvs.Count > 0)
            {
                Color colour;
                int thickness;
                if (this.Attributes.GetTopLevel.Selected)
                {
                    colour = args.WireColour_Selected;
                    thickness = 2;
                }
                else
                {
                    colour = args.WireColour;
                    thickness = 1;
                }

                GH_PreviewWireArgs argsWire = new GH_PreviewWireArgs(args.Viewport, args.Display, colour, thickness);
                for (int c = 0; c < m_crvs.Count; c++)
                    args.Display.DrawCurve(m_crvs[c], colour, thickness);
            }
        }
        #endregion

        #region MENU

        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            ToolStripMenuItem toolStripMenuItem0 = GH_DocumentObject.Menu_AppendItem((ToolStrip)menu, "Dynamic Baking", new EventHandler(this.SetDynamicBaking), true, this.m_dynamic_bake);
            toolStripMenuItem0.ToolTipText = "Bake geometry direcly when the geometry is changed.";
        }

        private void SetDynamicBaking(object sender, EventArgs e)
        {
            this.RecordUndoEvent("DynamicBaking");
            this.m_dynamic_bake = !m_dynamic_bake;
            if (m_dynamic_bake)
                m_group_id = Rhino.RhinoDoc.ActiveDoc.Groups.Add();
            this.ExpireSolution(true);
            this.ExpirePreview(true);
        }
        #endregion
    }
}