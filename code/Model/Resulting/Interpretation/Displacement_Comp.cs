﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using Rhino;
using Rhino.DocObjects;
using Rhino.Geometry;
namespace Donkey.Model.Resulting.Interpretation
{
    public class Displacement_Comp : GH_Component, IGH_BakeAwareObject
    {
        #region FIELD
        private ResultModel m_model_displacement;
        private bool m_hidden;
        private double m_prev_displacement = double.NaN;
        private double m_threshold = double.NaN;
        #endregion

        /// <summary>
        /// Initializes a new instance of the Displacement_Comp class.
        /// </summary>
        public Displacement_Comp()
            : base("Displacement", "Displacement",
                "Visualize deformation/displacement of the structure",// + Donkey.Info.Mark,
                "Donkey", "4.Results") 
        {
            this.m_model_displacement = null;
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new Resulting.ResultModel_Param());
            pManager.AddNumberParameter("Threshold", "t", "Maximal allowed displacement. [mm]", GH_ParamAccess.item, 250);
            pManager.AddNumberParameter("Preview scale", "s", "Parameter to scale displacement on preview.", GH_ParamAccess.item, 1.0);
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            //pManager.AddParameter(new Resulting.ResultModel_Param());
            pManager.AddNumberParameter("displacement", "D", "Max displacement [mm]", GH_ParamAccess.item);
            //pManager.AddTextParameter("reactions", "R", "Reactions on the supports", GH_ParamAccess.item);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            ResultModel m_model = new ResultModel();
            if (!DA.GetData<ResultModel>(0, ref m_model))
                return;

            double scale = 1;
            DA.GetData<double>(2, ref scale);
            ResultModel model_displacement = new ResultModel(m_model);

            double threshold = Double.NaN;
            bool get_threshold = DA.GetData<double>(1, ref threshold);
            if (get_threshold && threshold > 0)
                this.m_threshold = threshold;
            else
                this.m_threshold = 1;

            //VTU_Reader reader = new VTU_Reader();
            //this.m_model = reader.Read("C:/MIDAS/TEMP/try3_old.rslts.vtu");
            //if (m_hidden)
            //{
            //    m_model.Displacement.SetScale(0);
            //}
            //else
            //{

            model_displacement.Displacement.SetScale(scale);
            this.m_model_displacement = model_displacement;
               //m_model.RegenerateGeometry();
            //}

            CompareWithPrevious();
            //this.Message = Math.Round(m_model_displacement.Displacement.Max,2)+" mm";

            //DA.SetData(0, m_model);
            DA.SetData(0, m_model_displacement.Displacement.Max);
            //DA.SetData(2, m_model.Reactions.ToString());
        }

        public override void ExpirePreview(bool redraw)
        {
            if (redraw)
                this.ExpireSolution(true);
            base.ExpirePreview(redraw);
        }

        private void CompareWithPrevious()
        {
            if (!double.IsNaN(this.m_prev_displacement))
            {
                double dif_displacement = Math.Round(m_model_displacement.Displacement.Max - m_prev_displacement, 2);
                if (dif_displacement > 0)
                    this.Message = "+" + dif_displacement+" mm";
                else
                    this.Message = dif_displacement+" mm";
            }
            this.m_prev_displacement = m_model_displacement.Displacement.Max;
        }
        
        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.displacement; }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }

        public override void ClearData()
        {
            base.ClearData();
            if (this.m_model_displacement != null)
            {
                this.m_model_displacement = null;
            }
            this.Message = " ";
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{9c054cbf-fdc5-4d58-b66f-cdc6f6fae687}"); }
        }

        #region PREVIEW

        public override bool IsPreviewCapable { get { return true; } }

        public override void DrawViewportWires(IGH_PreviewArgs args)
        {
            //if (m_model_displacement != null && m_model_displacement.Displacement.Active)
            //{
            //    if (m_threshold == Double.NaN)
            //    {
            //        if (this.Attributes.GetTopLevel.Selected)
            //        {
            //            m_model_displacement.DrawGeometryDisplacement(args, args.WireColour_Selected);
            //        }
            //        else { m_model_displacement.DrawGeometryDisplacement(args, Color.Gray); }
            //    }
            //    else
            //    {
            if(m_model_displacement != null)
                    m_model_displacement.DrawGeometryDisplacement(args, m_threshold);
            //    }
            //}
        }

        public new bool Hidden //HACK set 0 defomration in hidden component preview
        {
            get
            {
                return this.m_hidden;
            }
            set
            {
                this.m_hidden = value;
            }
        }
        #endregion

        #region BAKE
        public override bool IsBakeCapable
        {
            get
            {
                return true;
            }
        }

        public override void BakeGeometry(RhinoDoc doc, List<Guid> obj_ids)
        {
            ObjectAttributes att = Rhino.RhinoDoc.ActiveDoc.CreateDefaultAttributes();
            att.ColorSource = ObjectColorSource.ColorFromObject;
            this.BakeGeometry(doc, att, obj_ids);
        }

        public override void BakeGeometry(RhinoDoc doc, ObjectAttributes att, List<Guid> obj_ids)
        {
            att.ColorSource = ObjectColorSource.ColorFromObject;
            att.PlotColorSource = ObjectPlotColorSource.PlotColorFromObject;

            if (m_model_displacement != null && m_model_displacement.Geometries.Count > 0)
            {
                ResultModel model_displacement = new ResultModel(m_model_displacement);
                //model_displacement.Displacement.SetScale(1);
                for (int i = 0; i < model_displacement.Geometries.Count; i++)
                {
                    Color color = Color.Silver;
                    att.PlotColor = color;
                    att.ObjectColor = color;
                    model_displacement.Geometries[i].Bake_Geometry(doc, att);
                }
            }
        }
        #endregion
    }
}