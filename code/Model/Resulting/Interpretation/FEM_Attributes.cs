﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel.Attributes;
using Grasshopper.Kernel;

namespace Donkey.Model.Resulting.Interpretation
{
    class FEM_ComponentAttributes : GH_ComponentAttributes
    {

        FEM_ComponentAttributes(IGH_Component owner) : base(owner) { }

    }
}
