﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel.Types;
using Donkey.Extensions;
using Donkey.Model.Geometry;
using Rhino.Geometry;
using Grasshopper.Kernel;
using System.Drawing;
using Rhino.Display;
using Grasshopper.GUI.Gradient;

namespace Donkey.Model.Resulting
{
    public class ResultModel : Model_Goo, IEquatable<ResultModel>
    {
        #region FIELD
        public Data.Displacement Displacement { get; set; }
        public Data.Reactions Reactions { get; set; }
        public Data.Forces Forces { get; set; }
        public List<double> Geometries_Stress_Relative { get; set; }
        public List<double> Geometries_Stress_Real { get; set; }
        public List<double> Elements_Evaluation { get; set; }
        public double Stability { get; set; }
        public double Volume { get; set; }
        //public GH_Gradient Gradient { get; private set; }
        //Evaluation
        public double MaxStress { get; private set; }
        public double Efficiency { get; set; }
        public double Overloading { get; set; }

        static private GH_Gradient m_gradient = null;
        static public GH_Gradient Gradient
        {
            get 
            {
                if (m_gradient == null)
                {
                    m_gradient = new GH_Gradient();
                    m_gradient.AddGrip(0.0, Color.SkyBlue); //white
                    m_gradient.AddGrip(0.75, Color.Green);
                    //g.AddGrip(0.999999, Color.Green);
                    //g.AddGrip(0.7, Color.YellowGreen);
                    m_gradient.AddGrip(0.99999, Color.DarkGreen); //DarkSlateGray
                    m_gradient.AddGrip(1.000001, Color.Red);
                    m_gradient.AddGrip(2, Color.DarkRed);
                }
                return m_gradient;
            }
            set { m_gradient = value; }
        }

        #endregion

        #region CONSTRUCTOR
        //HACK goo not abstract!
        //BLANK constructor
        public ResultModel() 
            : base()
        { 
            this.Displacement = new Data.Displacement(this);
            this.Reactions = new Data.Reactions(this);
            this.Forces = new Data.Forces();
            this.Geometries_Stress_Relative = new List<double>();
            this.Geometries_Stress_Real = new List<double>();
            this.Elements_Evaluation = new List<double>();
            this.MaxStress = Double.NaN;
            this.Efficiency = Double.NaN;
            this.Overloading = Double.NaN;
        }
        //DATA constructor
        public ResultModel(DataList<Node> nodes, DataList<Geometry_Goo> geometries,DataList<Element> elements)
            : base(nodes, geometries,elements)
        {
            this.Displacement = new Data.Displacement(this);
            this.Reactions = new Data.Reactions(this);
            this.Forces = new Data.Forces();
            this.Geometries_Stress_Relative = new List<double>();
            this.Geometries_Stress_Real = new List<double>();
            this.Elements_Evaluation = new List<double>();
            this.MaxStress = Double.NaN;
            this.Efficiency = Double.NaN;
            this.Overloading = Double.NaN;
        }
        //COPY constructor
        public ResultModel(ResultModel instance)
            :base(instance)
        {
            if (instance != null)
            {
                this.Displacement = new Data.Displacement(instance.Displacement, this);
                this.Reactions = instance.Reactions;
                this.Forces = instance.Forces;
                this.Geometries_Stress_Relative = instance.Geometries_Stress_Relative.ToList();
                this.Geometries_Stress_Real = instance.Geometries_Stress_Real.ToList();
                this.Elements_Evaluation = instance.Elements_Evaluation.ToList();
                this.MaxStress = instance.MaxStress;
                this.Efficiency = instance.Efficiency;
                this.Overloading = instance.Overloading;
                //instance.Displacement.Active = false;
                //this.Nodes = new DataList<Node>(instance.Nodes);
            }
        }
        //DATA INITIALIZATON
        public void InitData()
        {
            this.MaxStress = Calculate_MaxStress();
            this.Evaluate_Elements();
            this.Evaulate_Model();
        }
        #endregion

        #region METHODS

        public void ReCalculate_RelativeStress(double yield_stress)
        {
            for (int i = 0; i < Geometries_Stress_Real.Count; i++)
            {
                Geometries_Stress_Relative[i] = Geometries_Stress_Real[i] / yield_stress;
            }
            this.Evaluate_Elements();
            this.Evaulate_Model();
        }

        private double Calculate_MaxStress()
        {
            double max = 0;
            foreach (double real_stress in Geometries_Stress_Real)
            {
                if (max < real_stress)
                    max = real_stress;
            }
            return Math.Round(max, 3);
        }
        //VALUE INIT METHOD
        private void Evaluate_Elements()
        {
            List<double> values = new List<double>(this.Elements.Count);
            foreach (Element element in Elements)
            {
                values.Add(Evaluate_Element(element));
            }
            this.Elements_Evaluation = values; //TODO: evaluation two values - efficiency and overloading for element
        }

        //PRIVATE VALUE INIT METHOD
        public double Evaluate_Element(Element element)
        {
            //AVERAGE VALUE- OBSOLETE
            //int count = element.Geometries_ID.Count;
            //double sum = 0;
            //foreach (int ID in element.Geometries_ID)
            //{
            //    sum += this.Relative_Stress[ID];
            //}
            //return Math.Round(sum / count,3);
            //MAX VALUE
            double max = 0;
            foreach (int ID in element.Geometries_ID)
            {
                if(max< this.Geometries_Stress_Relative[ID])
                 max= this.Geometries_Stress_Relative[ID];
            }
            return Math.Round(max,3);
        }

        private void Evaulate_Model()
        {
            this.Efficiency = CalculateEfficiency();
            this.Overloading = CalculateOverloading();
        }

        //PUBLIC MODEL VALUE
        private double CalculateAverage()
        {
            double totalValue = 0;
            int count = this.Elements.Count;
            if(count == 0)
                return 0;
            for (int i = 0; i < count; i++)
            {
                totalValue += this.Elements_Evaluation[i];
            }
            return Math.Round(totalValue/count, 3);
        }

        private double CalculateOverloading()
        {
            double maxValue = 0;
            double overloading;
            int count = this.Elements.Count;
            for (int i = 0; i < count; i++)
            {
                if (maxValue < this.Elements_Evaluation[i])
                    maxValue = this.Elements_Evaluation[i];
            }
            overloading = maxValue - 1; //remove 100% efficiency
            if (overloading < 0) //if max value in under 100%. no penalization
                overloading = 0;
            return Math.Round(overloading, 3);
        }

        private double CalculateEfficiency()
        {
            double value = 0;
            int count = this.Elements.Count;
            //values from 100%
            for (int i = 0; i < count; i++)
            {
                value += Math.Abs(1 - this.Elements_Evaluation[i]);
            }

            value /= count; //average
            value = 1 - value; //100% - average fom 100%
            if(value < 0) //if result is negative value then efficiency is 0%
            value = 0;
            return Math.Round(value, 3);
        }

        //private double CalculateEfficiency_old()
        //{
        //    double value = 0;
        //    int count = this.Elements.Count;
        //    for (int i = 0; i < count; i++)
        //    {
        //        double element_value = this.Required_Stress[i];
        //        if (element_value > 1 && element_value <= 2)
        //            value += (2 - element_value);// 2 - 1.8 = 0.2 efficieny
        //        else if (element_value > 2)
        //            value += 0; // 2.4 is in deformation - efficiency 0 or other penalty
        //        else
        //            value += element_value; //0.8 = 0.8 efficiency
        //    }
        //    return Math.Round(value/count, 3);
        //}

        //private double EfficiencyValue_old()
        //{
        //    double value = 0;
        //    int count = this.Elements.Count;
        //    for (int i = 0; i < count; i++)
        //    {
        //        double element_value = this.Required_Stress[i];
        //        if (element_value > 1)
        //            value += (element_value - 1);//*1.2; //PENALTY
        //        else
        //            value += 1 - element_value;
        //    }
        //    return Math.Round(value / count, 3);
        //}

        //public void RegenerateGeometry()
        //{
        //    foreach (Geometry_Goo e in this.Geometries)
        //    {
        //        if (e is VTK_Triangle)
        //        {
        //            VTK_Triangle x = (VTK_Triangle)e;
        //            if (x != null)
        //                x.GenerateMesh();
        //        }
        //    }
        //}

        //private void InitGradient()
        //{
        //    this.Gradient = new GH_Gradient();
        //    Gradient.AddGrip(0.0, Color.SkyBlue);
        //    Gradient.AddGrip(0.5, Color.Green);
        //    Gradient.AddGrip(0.99, Color.DarkSlateGray); //DarkSlateGray
        //    Gradient.AddGrip(1.01, Color.Red);
        //    Gradient.AddGrip(10, Color.Red);
        //}
        #endregion

        #region OVERRIDE
        public override IGH_Goo Duplicate()
        {
            return new ResultModel(this);
        }

        public override string ToString()
        {
            //return "Scale: " + this.Displacement.scale + "\n First node:" + this.Nodes[0].ToString() +"\n Geometry center:" + this.Geometries[0].GetCenter().ToString() +"\n Model rating, min and max rate/value of other data";
            //return "Model rating, min and max rate/value of other data";
            string str;
            str = "Element: " + this.Elements.Count + Environment.NewLine;
            str += "Efficiency: " + this.Efficiency*100 + "%" + Environment.NewLine;
            str += "Max use: " + this.Overloading*100 + "%" + Environment.NewLine;
            str += "Displacement: " + Math.Round(this.Displacement.Max, 1) + "mm" + Environment.NewLine;
            str += "Volume: " +Math.Round(this.Volume, 3) + "mm\u00B3";
            if (this.Stability != 0)
                str += Environment.NewLine + "Stability: " + Math.Round(this.Stability, 3);
            return str;
        }

        //public override string TypeDescription
        //{
        //    get { return "DONKEY model of analysis result"; }
        //}

        //public override string TypeName
        //{
        //    get { return "Result"; }
        //}

        //public override DataList<Node> Nodes
        //{
        //    get
        //    {
        //        if (this.Displacement.Active)
        //            return Displacement.Nodes;
        //        return base.Nodes; 
        //    }
        //}

        public void SetNodes(DataList<Node> nodes)
        {
            this.Nodes = nodes;
        }

        public void SetElements(DataList<Element> elements)
        {
            this.Elements = elements;
        }
        #endregion

        #region PREVIEW
        //GEOMETRY COLOUR
        public void DrawGeometryColour(IGH_PreviewArgs args) //efficiency
        {
            for (int i = 0; i < this.Geometries.Count; i++)
            {
                Color color;
                if (Geometries_Stress_Relative.Count == 0)
                    color = Color.White;
                else
                    color = Gradient.ColourAt(Geometries_Stress_Relative[i]); //TOTO: unhandle event OverflowException!
                //DisplayMaterial mat = new DisplayMaterial( color);  //HACK outhere?
                this.Geometries[i].DrawViewportWires(new GH_PreviewWireArgs(args.Viewport, args.Display, color, 5));
                //this.Geometries[i].DrawViewportMeshes(new GH_PreviewMeshArgs(args.Viewport,args.Display, mat, args.MeshingParameters));
            }
        }

        public void DrawGeometryDisplacement(IGH_PreviewArgs args, Color color)
        {
            GH_PreviewWireArgs preArgs = new GH_PreviewWireArgs(args.Viewport, args.Display, color, 3);
            for (int i = 0; i < this.Geometries.Count; i++)
            {
                this.Geometries[i].DrawVieportWires_Displacement(preArgs);              
            }
        }

        public void DrawGeometryDisplacement(IGH_PreviewArgs args, double threshold)
        {
            for (int i = 0; i < this.Geometries.Count; i++)
            {
                //Color color = Gradient.ColourAt(this.Displacement.Vectors[i].Length / threshold);
                //args.Display.DrawLine(this.Nodes[i], this.DisplacementNodes[i], color, 2);
                GH_PreviewWireArgs prvArgms = new GH_PreviewWireArgs(args.Viewport, args.Display, Color.Gray, 3);
                this.Geometries[i].DrawVieportWires_Displacement(prvArgms);
            }
            for (int i = 0; i < this.Nodes.Count; i++)
            {
                Color color = Gradient.ColourAt(this.Displacement.Vectors[i].Length / threshold);
                args.Display.DrawLine(this.Nodes[i], this.DisplacementNodes[i], color, 2);
            }
        }

        //GEOMETRY VALUE
        public void DrawGeometryStress(IGH_PreviewArgs args)
        {
            for (int i = 0; i < this.Geometries.Count; i++)
            {
                string value = " N: " + this.Forces.Beam_AxialForce_N[i];
                Point3d pt = this.Geometries[i].Center;
                //Plane plane = new Plane(pt, Plane.WorldXY.ZAxis);
                //Text3d text3d = new Text3d(value, plane, 15);
                //new TextDot(this.Forces.GetNValue(i).ToString(), pt);
                //args.Display.Draw3dText(text3d, Color.White);
                args.Display.Draw2dText(value, Color.White, pt, false, 10);
                //TODO FEM PREVIEW - moments arrow
                //args.Display.DrawLineArrow(new Line(pt, this.Forces.BeamStress_Mv[i], 15), Color.Red, 2, 2);              
            }
        }

        //GEOMETRY VALUE
        public void DrawGeometryValue(IGH_PreviewArgs args)
        {
            for (int i = 0; i < this.Geometries.Count; i++)
            {
                string value = "  " + Math.Round(this.Geometries_Stress_Relative[i],2);
                Point3d pt = this.Geometries[i].Center;
                args.Display.Draw2dText(value, Color.White, pt, false, 10);
            }
        }
        //ELEMENT VALUE
        public void DrawElementValue(IGH_PreviewArgs args)
        {
            for (int i = 0; i < this.Elements.Count; i++)
            {
                //string value = "[" + Required_Stress[i] + ", "+i+"]";
                string value = this.Elements_Evaluation[i].ToString();
                Point3d pt = Elements[i].Center;
                if (this.Elements_Evaluation[i] > 1)
                {
                    //args.Display.Draw2dText(value, Color.Red, pt, true, 20);
                    args.Display.DrawDot(pt, value, Color.Red, Color.White);
                }
                else
                {
                    //args.Display.Draw2dText(value, Color.White, pt, true, 20);
                    args.Display.DrawDot(pt, value, Color.White, Color.Black);
                }
            }
        }

        //GEOMETRY VALUE
        //public void DrawGeometryMoments(IGH_PreviewArgs args, int axis)
        //{
        //    for (int i = 0; i < this.Geometries.Count; i++) //FOR EACH ELEMENTS!!!
        //    {
        //        Vector3d moments = this.Forces.BeamStress_M_xyz[i];
        //        if (this.Geometries[i].Type == Types.VTK_TRIANGLE)
        //            continue;
        //        string value = " M: " + moments;
        //        List<Point3d> pts = this.Geometries[i].GetPoints();
        //        Point3d pt = pts[0];
        //        //Point3d pt = this.Geometries[i].Center;
        //        //Plane plane = new Plane(pt, Plane.WorldXY.ZAxis);
        //        //Text3d text3d = new Text3d(value, plane, 15);
        //        //new TextDot(this.Forces.GetNValue(i).ToString(), pt);
        //        //args.Display.Draw3dText(text3d, Color.White);
        //        args.Display.Draw2dText(value, Color.White, pt, false, 10);
        //        //TODO FEM PREVIEW - moments arrow
        //        //args.Display.DrawLineArrow(new Line(pt, this.Forces.BeamStress_Mv[i], 15), Color.Red, 2, 2);              

        //        if (i == this.Geometries.Count-1)
        //        {
        //            Point3d pt2 = pts[1];
        //            args.Display.Draw2dText("JOZKO", Color.White, pt2, false, 10);
        //        }
        //    }
        //}

        ////GEOMETRY
        //public void DrawGeometry(IGH_PreviewArgs args)
        //{
        //    GH_PreviewWireArgs argsWire = new GH_PreviewWireArgs(args.Viewport, args.Display, Color.Black, 1);
        //    for (int i = 0; i < this.Geometries.Count; i++)
        //    {
        //        this.Geometries[i].DrawViewportWires(argsWire);
        //    }
        //}

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            ResultModel objAsResultingModel = obj as ResultModel;
            if (objAsResultingModel == null) return false;
            else return Equals(objAsResultingModel);
        }

        public bool Equals(ResultModel model)
        {
            if (this.Efficiency == model.Efficiency && this.Overloading == model.Overloading)
                return true;
            return false;
        }
        #endregion
    }
}
