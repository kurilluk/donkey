﻿using System;
using System.Collections.Generic;
using Grasshopper.Kernel;
using Rhino.Geometry;
using Donkey.Model.Geometry;
using Donkey.Extensions;
using System.Text;

namespace Donkey.Model.Resulting.Data
{
    public class Reactions
    {
         #region FIELD
        public List<int> Supports_ID { get; private set; }
        //public List<Point3d> Points { get; private set; }
        public List<Vector3d> Vectors { get; private set; }
        //public List<Line> Lines { get; private set; }
        //private List<Vector3d> m_vectors;
        private ResultModel m_model;
        //public double Max { get; private set; }
        //public double Max_direction { get; private set; }
        //public string Max_axis { get; private set; }
        public Vector3d Sum { get; private set; }
        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        public Reactions(ResultModel model) 
        {
            this.m_model = model;
           //this.m_vectors = new List<Vector3d>();
           this.Supports_ID = new List<int>();
           //this.Max = 0;
           //this.Max_direction = 0;
           this.Vectors = new List<Vector3d>();
           //this.Points = new List<Point3d>();
           //this.Lines = new List<Line>();
           this.Sum = new Vector3d(0,0,0);
           //this.Max_axis = "";
        }

        //COPY CONSTRUCTOR
        //public Displacement(Displacement instance) :this(instance, new Result()){}
        public Reactions(Reactions instance, ResultModel model)
        {
            this.m_model = model;
            //this.m_vectors = new List<Vector3d>(instance.m_vectors); //list is new but vectors are still referenced to the same object!!!
            this.Supports_ID = instance.Supports_ID;
            //this.Max = instance.Max;
            //this.Max_direction = instance.Max_direction;
            this.Vectors = instance.Vectors; //DUPLICATE?
            //this.Points = instance.Points; //same..
            //this.Lines = instance.Lines;
            this.Sum = instance.Sum;
            //this.Max_axis = instance.Max_axis;
        }
        #endregion

        #region METHODS

        public Point3d Position(int index)
        {
            return m_model.Nodes[Supports_ID[index]];
        }

        //TODO read reactions moments
        public void SetReactions(List<Vector3d> vectors)
        {
            //double m_max = this.Max;
            for (int i = 0; i < vectors.Count; i++ )
            {
                //IF IS SUPPORT (vector is defined for all nodes)
                if (!vectors[i].IsZero)
                {
                    this.Vectors.Add(vectors[i]);
                    this.Supports_ID.Add(i);
                    this.Sum += vectors[i];
                    //if (Math.Abs(vectors[i].X) > Max_direction)
                    //    Max_direction = Math.Abs(vectors[i].X);
                    //    Max_axis = "X";
                    //if (Math.Abs(vectors[i].Y) > Max_direction)
                    //    Max_direction = Math.Abs(vectors[i].Y);
                    //    Max_axis = "Y";
                    //if (Math.Abs(vectors[i].Z) > Max_direction)
                    //    Max_direction = Math.Abs(vectors[i].Z);
                    //    Max_axis = "Z";

                    ////SUM OF ALL REACTION VECTORS (VALUE IS THE LENGTH OF THE REACION VECTOR, NO ONLY ONE DIRECTION)
                    //m_max += vectors[i].Length;
                }
            }
            //this.Max = m_max;

            //for (int i = 0; i < m_vectors.Count; i++)
            //{
            //    this.Points.Add(m_model.Nodes[Supports_ID[i]]);
            //    this.Vectors.Add(new Vector3d(-m_vectors[i].X,0,0));

            //    this.Points.Add(m_model.Nodes[Supports_ID[i]]);
            //    this.Vectors.Add(new Vector3d(0, -m_vectors[i].Y, 0));

            //    this.Points.Add(m_model.Nodes[Supports_ID[i]]);
            //    this.Vectors.Add(new Vector3d(0,0,-m_vectors[i].Z));
            //}

            //for (int i = 0; i < Points.Count; i++)
            //{
            //    this.Lines.Add(new Line(Points[i], Vectors[i], (Vectors[i].Length / Max_direction) * 1000));
            //}

        }

        public string GetReactionOn(int position)
        {
            if (position < Vectors.Count)
                return Math.Round(this.Vectors[position].X, 2) + " | " + Math.Round(this.Vectors[position].Y, 2) + " | " + Math.Round(this.Vectors[position].Z, 2);
            return "NaN/NaN/NaN";
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < Vectors.Count; i++)
            {
                sb.Append("N(" + Supports_ID[i] + "){" + Vectors[i].ToString() + "}\n\r");
            }
            return sb.ToString();
        }
        #endregion
    }
}
