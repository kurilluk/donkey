﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rhino.Geometry;

namespace Donkey.Model.Resulting.Data
{
    public class Forces
    {
        #region FIELD
        public List<double> Beam_AxialForce_N { get; set; }
        public List<double> Beam_ShearForce_V { get; set; }
        public List<double> Beam_BendingMoment_M { get; set; }
        public List<Vector3d> Beam_BendingMoment_M_pt1 { get; set; }
        public List<Vector3d> Beam_BendingMoment_M_pt2 { get; set; }
        public List<double> AverageNValue { get; private set; }
        #endregion

        #region CONSTRUCTOR
        public Forces() { }
        public Forces(Forces instance) { }
        #endregion

        //N min/max/range
        //N value maping

        public void CalculateAverageNValue()
        {
            this.AverageNValue = new List<double>(Beam_AxialForce_N.Count);
            double range = Beam_AxialForce_N.Max() - Beam_AxialForce_N.Min();
            foreach (double N in Beam_AxialForce_N)
            {
                AverageNValue.Add(N / range);
            }
        }

        public double GetNValue(int index)
        {
            //double range = BeamStress_N.Max() - BeamStress_N.Min();
            //return BeamStress_N[index] / range;
            return AverageNValue[index];
        }
    }
}
