﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using Donkey.Model.Geometry;
using Donkey.Extensions;

namespace Donkey.Model.Resulting.Data
{
    public class Displacement
    {
        #region FIELD
        public bool Active { get; set; }
        public DataList<Node> Nodes { get; private set; }
        private double m_scale = 0;
        public  List<Vector3d> Vectors;
        private ResultModel m_model;
        public double Max { get; private set; }
        #endregion

        #region CONSTRUCTOR
        //BLANK CONSTRUCTOR
        public Displacement(ResultModel model) 
        {
           this.m_model = model;
           this.Vectors = new List<Vector3d>();
           this.Active = false;
           this.Max = 0;
           this.Nodes = new DataList<Node>();
        }
        //COPY CONSTRUCTOR
        //public Displacement(Displacement instance) :this(instance, new Result()){}
        public Displacement(Displacement instance,ResultModel model)
        {
            this.m_model = model;
            this.Vectors = new List<Vector3d>(instance.Vectors);
            this.m_scale = instance.m_scale;
            this.Active = instance.Active;
            this.Max = instance.Max;
            if(instance.Nodes != null && instance.Nodes.Count > 0)
                this.Nodes = new DataList<Node>(instance.Nodes,model);
        }
        #endregion

        #region METHODS

        internal void SetScale(double scale)
        {
            this.m_scale = scale;
            //Calculate vectors and generate moved nodes
             this.Nodes = new DataList<Node>(); //HACK COUNT TO DATALIST CONSTRUCTOR/ IList
            for (int i = 0; i < m_model.Nodes.Count; i++)
            {
               Nodes.Add(m_model.Nodes[i]+(Vectors[i]*m_scale));
            }
            //set active true;
            this.Active = true;
            m_model.DisplacementNodes = this.Nodes;
        }

        //public List<Vector3d> GetVectors()
        //{
        //        List<Vector3d> scaledVectors = new List<Vector3d>(m_vectors.Count);
        //        foreach (Vector3d vector in m_vectors)
        //        {
        //            scaledVectors.Add(new Vector3d(vector*Scale)); //HACK new vector object is unless, foreach already clone object
        //        }
        //        return scaledVectors;
        //    }

        public void SetVectors(List<Vector3d> vectors)
        {
            this.Vectors = vectors;
            double m_max = 0;
            foreach (Vector3d vector in vectors)
            {
                if (vector.Length > m_max)
                    m_max = vector.Length;
            }
            this.Max = m_max;
        }
        #endregion

        ///// <summary>
        ///// Gets the unique ID for this component. Do not change this ID after release.
        ///// </summary>
        //public override Guid ComponentGuid
        //{
        //    get { return new Guid("{158db7a5-4db4-449b-bb9f-1341f18ec7c6}"); }
        //}
    }
}