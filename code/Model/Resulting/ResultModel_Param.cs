﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper.Kernel;
using Rhino.Geometry;

namespace Donkey.Model.Resulting
{
    class ResultModel_Param : GH_Param<ResultModel>//, IGH_PreviewObject
    {
        #region FIELDS
        //private bool m_hidden;
        #endregion

        #region CONSTRUCTOR
        //Default constructor
        public ResultModel_Param()
            : base(new Grasshopper.Kernel.GH_InstanceDescription("Result", "R", "Donkey resulting model", "Donkey", "Params")) { }
        //Overload constructor 1     
        public ResultModel_Param(GH_InstanceDescription nTag) : base(nTag){}
        //Overload constructor 2     
        public ResultModel_Param(GH_InstanceDescription nTag, GH_ParamAccess access) : base(nTag, access) { }
        //Component guid
        public override Guid ComponentGuid
        {
            get { return new Guid("{c1083d0d-b17e-4cff-b708-4256039446ed}"); }
        }
        #endregion

        #region OVERRIDE
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.result; }
        }

        protected override string Format(ResultModel Data)
        {
            if ((object)Data == null)
                return "Null";
            string str;
            str = Data.Elements.Count + " element(s)" + Environment.NewLine;
            str += Data.Efficiency*100 + "% efficiency" + Environment.NewLine;
            str += Data.Overloading*100 + "% overloading" + Environment.NewLine;
            str += Math.Round(Data.Displacement.Max, 1) + "mm displacement" + Environment.NewLine;
            str += Data.Displacement.Active.ToString() + Environment.NewLine;
            //str += Math.Round(Data.Volume,3) + "[mmmm\u00B3] volume";
            //if(Data.Stability != 0)
            //    str += Environment.NewLine + Math.Round(Data.Stability, 3) + "stability";
            return str;
        }
        #endregion

        //#region IGH_PreviewObject Members
        //public Rhino.Geometry.BoundingBox ClippingBox
        //{
        //    get { return BoundingBox.Empty; }
        //}

        //public void DrawViewportMeshes(IGH_PreviewArgs args)
        //{
        //}

        //public void DrawViewportWires(IGH_PreviewArgs args)
        //{
        //}

        //bool IGH_PreviewObject.Hidden
        //{
        //    get
        //    {
        //        return this.m_hidden;
        //    }
        //    set
        //    {
        //        this.m_hidden = value;
        //    }
        //}

        //bool IGH_PreviewObject.IsPreviewCapable
        //{
        //    get { return true; }
        //}
        //#endregion
    }
}
