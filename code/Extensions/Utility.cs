﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Donkey.Extensions
{
    static public class Utility
    {
        static public double RemapValue(double oldMin, double oldMax, double newMin, double newMax, double value)
        {
            double oldRange = oldMax - oldMin;
            double newRange = newMax - oldMin;
            double ratio = value / oldRange;
            double newValue = newRange * ratio;
            return newValue ;
        }
    }
}
