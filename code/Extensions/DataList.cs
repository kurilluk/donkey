﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Donkey.Interface;

namespace Donkey.Extensions
{
    public class DataList<T> : List<T> where T : IModelParts //where T : new()
    {
        public DataList() : base() {}

        public DataList(DataList<T> instance) : base(instance.Clone()) { }

        public DataList(DataList<T> instance, Model.Model_Goo model) : base(instance.Clone(ref model)) { }

        public DataList(int capacity) :base(capacity) {}


        public DataList<T> Clone()
        {
            DataList<T> clone = new DataList<T>(this.Count);
            foreach (T item in this)
            {
                clone.Add(item);
            }
            return clone;
        }

        public DataList<T> Clone( ref Model.Model_Goo model)
        {
            DataList<T> clone = new DataList<T>(this.Count);
            //for (int i = 0; i < base.Items.Count; i++)
            foreach (T item in this)
            {
                clone.Add(item);
                item.RegisterModel(model);
            }
            return clone;
        }

        public int AppendData(T item, bool removeDuplicates)
        {
            int count = base.Count;
            if (removeDuplicates)
            {
                for (int i = 0; i < count; i++)
                {
                    if (item.Equals(base[i]))
                    {
                        item = base[i];
                        //TODO removedDuplicate info
                        return i;
                    }
                }
            }
            base.Insert(count, item);
            return count;
        }
    }
}
