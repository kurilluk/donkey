﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donkey
{
    public delegate void Global_Handler();

    public class GlobalValues
    {

        public static event Global_Handler ForceScaleExpired;

        protected static void On_ForceScaleChange()
        {
            if (ForceScaleExpired != null)
                ForceScaleExpired();
        }

        #region Static Field

        private static double m_force_scale_factor = 1;
        public static double ForceScale
        {
            get { return m_force_scale_factor; }
            set
            {
                m_force_scale_factor = value;
                On_ForceScaleChange();
            }
        }

        #endregion
    }
}
