﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel.Data;
using Rhino.Geometry;
using Donkey.Model;
using Donkey.Model.Resulting;

namespace Donkey.Utilities.Components
{
    public class ListOfElements_Comp : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the RoadSplitting_Comp class.
        /// </summary>
        public ListOfElements_Comp()
            : base("Elements' values", "Elements",
                "List of elements' max values",// + Donkey.Info.Mark,
                "Donkey", "A.Utilities")
        {
        }
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new ResultModel_Param());
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddNumberParameter("elements", "E", "List of elements' max values", GH_ParamAccess.list);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            ResultModel m_model = new ResultModel();
            if (!DA.GetData<ResultModel>(0, ref m_model))
                return;

            List<double> max_of_element = new List<double>(); 
            foreach (Element element in m_model.Elements)
            {
                max_of_element.Add(m_model.Evaluate_Element(element));
            }

            DA.SetDataList(0, max_of_element);
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{36772404-C8E3-4468-967F-0D70FDF0656C}"); }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.donkey; }

        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }
    }
}
