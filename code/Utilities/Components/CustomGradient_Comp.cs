﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Donkey.Model.Resulting;
using Grasshopper.GUI.Gradient;

using Grasshopper.Kernel;

namespace Donkey.Utilities.Components
{
    public class CustomGradient_Comp : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the RoadSplitting_Comp class.
        /// </summary>
        public CustomGradient_Comp()
            : base("Change Gradient", "Gradient",
                "Change colors of default gradient",// + Donkey.Info.Mark,
                "Donkey", "A.Utilities")
        {
        }
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddNumberParameter("values", "V", "Values to colour", GH_ParamAccess.list);
            pManager.AddColourParameter("colour", "C", "Colour for values", GH_ParamAccess.list);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            List<double> values = new List<double>();
            List<Color> colours = new List<Color>();

            if (DA.GetDataList<double>(0, values) &&
            DA.GetDataList<Color>(1, colours))
            {
                if (values.Count != colours.Count)
                {
                    this.AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "A count of values and colours doesn't match!");
                    //return;
                }

                GH_Gradient gradient = new GH_Gradient(values, colours);
                ResultModel.Gradient = gradient;

            }
            else
            {
                return;
            }
        }

        public override void ClearData()
        {
            base.ClearData();

            GH_Gradient m_gradient;
            m_gradient = new GH_Gradient();
            m_gradient.AddGrip(0.0, Color.SkyBlue);
            m_gradient.AddGrip(0.75, Color.Green);
            //g.AddGrip(0.999999, Color.Green);
            //g.AddGrip(0.7, Color.YellowGreen);
            m_gradient.AddGrip(0.99999, Color.DarkGreen); //DarkSlateGray
            m_gradient.AddGrip(1.000001, Color.Red);
            m_gradient.AddGrip(2, Color.DarkRed);
            ResultModel.Gradient = m_gradient;
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{B4DE48F6-A2E0-429D-A999-987B776CA9AF}"); }
        }

        protected override Bitmap Icon
        {
            get { return Donkey.Properties.Resources.donkey; }

        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }
    }
}
