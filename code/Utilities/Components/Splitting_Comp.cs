﻿using System;
using System.Collections.Generic;
using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Windows.Forms;
using Donkey.Utilities.Geometry;

namespace Donkey.Utilities.Components
{
    public class Splitting_Comp : GH_Component
    {
        /// <summary>
        /// Initializes a new instance of the RoadSplitting_Comp class.
        /// </summary>
        public Splitting_Comp()
            : base("Splitting Curves", "Splitting",
                "Splits curves at intersection points" + Environment.NewLine + "and subdivides curves into segments",// + Donkey.Info.Mark,
                "Donkey", "A.Utilities")
        {
        }

        /// <summary>
        /// Registers all the input parameters for this component.
        /// </summary>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddCurveParameter("Curve", "C", "Geometry for splitting", GH_ParamAccess.list);
            pManager.AddPointParameter("Point", "P", "Points to split curves", GH_ParamAccess.list);
            pManager[1].Optional = true;
            pManager.AddNumberParameter("Length", "L", "Segment length [mm]", GH_ParamAccess.item); //+Space.Units, GH_ParamAccess.item);
            pManager[2].Optional = true;
        }

        /// <summary>
        /// Registers all the output parameters for this component.
        /// </summary>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddCurveParameter("Segments", "S", "Splitted segments", GH_ParamAccess.list);
        }

        /// <summary>
        /// This is the method that actually does the work.
        /// </summary>
        /// <param name="DA">The DA object is used to retrieve from inputs and store in outputs.</param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            //this.Params.Input[1].Description = "Maximum road segment length " + Space.Units;
            //this.m_attributes.ExpireLayout();
            //Grasshopper.Instances.InvalidateCanvas();

            //this.Attributes.TooltipEnabled = true;
            //this.Attributes.PerformLayout();
            //this.Attributes.ExpireLayout();
            List<Curve> inputCurves = new List<Curve>();
            if (!DA.GetDataList<Curve>(0, inputCurves))
                return;
            List<Curve> outputCurves;
            List<Point3d> splittingPoints = new List<Point3d>();
            if(DA.GetDataList<Point3d>(1, splittingPoints))
                outputCurves = CurveGeometry.SplitIntersectedCurves(inputCurves, splittingPoints);
            else
                outputCurves = CurveGeometry.SplitIntersectedCurves(inputCurves);

            double roadSegment_Size = 1000;
            if (DA.GetData(2, ref roadSegment_Size))
                outputCurves = CurveGeometry.SplitBySize(outputCurves, roadSegment_Size);

            DA.SetDataList(0, outputCurves);
        }

        /// <summary>
        /// Provides an Icon for the component.
        /// </summary>
        protected override System.Drawing.Bitmap Icon
        {
            get { return Donkey.Properties.Resources.donkey; }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("{57BD06A0-E33A-4C69-AF8E-A8AF00D2AD9D}"); }
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; } //.secondary; }
        }

        //#region MENU
        //public override bool AppendMenuItems(ToolStripDropDown menu)
        //{

        //    GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
        //    this.Menu_AppendPreviewItem(menu);
        //    this.Menu_AppendEnableItem(menu);
        //    this.Menu_AppendWarningsAndErrors(menu);
        //    GH_DocumentObject.Menu_AppendSeparator((ToolStrip)menu);
        //    //this.AppendAdditionalMenuItems(menu);
        //    this.Menu_AppendObjectHelp(menu);
        //    return true;
        //}
        //#endregion
    }
}