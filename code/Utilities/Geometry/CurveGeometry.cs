﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rhino.Geometry;
using System.Diagnostics;
using Rhino.Geometry.Intersect;

namespace Donkey.Utilities.Geometry
{
    class CurveGeometry
    {
        static double tolerance = Rhino.RhinoDoc.ActiveDoc.ModelAbsoluteTolerance;

        static public List<Curve> SplitBySize(List<Curve> curves, double size)
        {
            List<Curve> dividedCurves = new List<Curve>();
            foreach (Curve curve in curves)
            {
                double length = curve.GetLength();
                //TODO ADD TOLERANCE AND SPECIFIED ROAD SEGMENT SIZE
                if (length <= (1.5 * size)) // && length > 0.05) SOLVE PROBLEM WITH ARRAY? NO CURVE..
                {
                    dividedCurves.Add(curve);
                }
                else
                {
                    int count = Convert.ToInt16(Math.Round(length / size));
                    double[] divideParams = curve.DivideByCount(count, false);
                    Curve[] dividedCurve = curve.Split(divideParams);
                    dividedCurves.AddRange(dividedCurve);
                }
            }
            return dividedCurves;
        }

        static public List<Curve> SplitIntersectedCurves(List<Curve> C) { return SplitIntersectedCurves(C, new List<Point3d>()); }
        static public List<Curve> SplitIntersectedCurves(List<Curve> C, List<Point3d> pts)
        {
            if (C == null || C.Count == 0)
            {
                return null;
            }
            //create a list of lines, by dividing lines into two lines as soon as there are any intersections

            //first find all the intersection and create a list of points
            //there should be two find the current examples
            //List<Point3d> ints = new List<Point3d>();

            List<Curve> newCurves = new List<Curve>();

            //Rhino.Geometry.Intersect.Intersection.CurveSelf();

            List<List<double>> cutParams = new List<List<double>>(C.Count);
            for (int i = 0; i < C.Count; i++)
            {
                cutParams.Add(new List<double>());
                CurveIntersections intersections = Rhino.Geometry.Intersect.Intersection.CurveSelf(C[i], tolerance);
                if ((intersections != null) && (intersections.Count > 0))
                {
                    for (int k = 0; k < intersections.Count; k++)
                    {
                        cutParams[i].Add(intersections[k].ParameterA);
                        cutParams[i].Add(intersections[k].ParameterB);
                    }
                }

                if(pts != null || pts.Count > 0)
                    for (int p = 0; p < pts.Count; p++)
                    {
                        double param;
                        if (C[i].ClosestPoint(pts[p], out param, tolerance))
                            cutParams[i].Add(param);
                    }

            }

            for (int i = 0; i < C.Count; i++)
            {
                for (int j = i + 1; j < C.Count; j++)
                {

                    //
                    //CurveIntersections curveIntersections = Intersection.CurveCurve(list1[index1], list1[index2], GH_Component.DocumentTolerance(), 5.0 * GH_Component.DocumentTolerance());
                    //if (curveIntersections != null && curveIntersections.get_Count() != 0)
                    //{

                    
                    //Curve curve = C[i];
                    //Curve curve2 = C[j];
                    //timer.Start();
                    CurveIntersections intersections = Rhino.Geometry.Intersect.Intersection.CurveCurve(C[i], C[j], 0.1, 1);
                    //timer.Stop();
                    if ((intersections != null) && (intersections.Count > 0))
                    {
                        //List<Point3d> list = new List<Point3d>(intersections.Count);
                        //List<double> list2 = new List<double>(intersections.Count);
                        //List<double> list3 = new List<double>(intersections.Count);

                        for (int k = 0; k < intersections.Count; k++)
                        {
                            //list.Add((Point3d)(0.5 * (intersections[k].PointA + intersections[k].PointB)));
                            //list2.Add(intersections[k].ParameterA);
                            //list3.Add(intersections[k].ParameterB);
                            //Print("paramA " + intersections[k].ParameterA);
                            //Print("paramB " + intersections[k].ParameterB);
                            //Print("curve " + curve.Domain.Min + "," + curve.Domain.Max);
                            //Print("curve2 " + curve2.Domain.Min + "," + curve2.Domain.Max);

                            //if (isDividable(curve, intersections[k].ParameterA))
                            //{
                            cutParams[i].Add(intersections[k].ParameterA);
                            //}
                            //if (isDividable(curve2, intersections[k].ParameterB))
                            //{
                            cutParams[j].Add(intersections[k].ParameterB);
                            //}


                            //ints.AddRange(list);
                        }
                    }
                }
            }

            //split the curves at their cut params and add the new curves to newCurves

            for (int i = 0; i < C.Count; i++)
            {
                Curve crv = C[i];
                List<double> parms = cutParams[i];
                Curve[] newOnes = crv.Split(parms);
                newCurves.AddRange(newOnes);
            }

            //double time = timer.ElapsedMilliseconds;
            return newCurves;

        }

        private static bool isDividable(Curve c, double param)
        {
            return param > c.Domain.Min && param < c.Domain.Max;
        }

        private static List<Curve> divide(Curve c, double param)
        {
            List<Curve> newCrvs = new List<Curve>();
            if (!isDividable(c, param))
            {
                newCrvs.Add(c);
                return newCrvs;
            }
            Curve[] crvs = c.Split(param);
            newCrvs.AddRange(crvs);
            return newCrvs;
        }
    }
}
