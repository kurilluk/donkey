DONKEY: Structural Evaluation Assistant
==============================

***

**version 0.84**



*The tool is developed as an Grasshopper(R) add-on (.gha file).*



*For more information please visit  [sea.kurilluk.net](http://sea.kurilluk.net/)*

*For downloading MIDAS please visit  [MIDAS.homepage](http://mech.fsv.cvut.cz/~da/MIDAS/download/)*